## [0.0.21](https://gitlab.pasteur.fr/bis-aria/Ariaec/compare/0.1.0...0.0.21) (2019-07-08)


### Bug Fixes

* add debug log message in ssclash ([8e50b10](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/8e50b10))
* add debug log message in ssclash ([beb4935](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/beb4935))
* avoid key errors if all parameters don't exist with pdbqual command ([ea0e556](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/ea0e556))
* change api key for semantic delivery ([8eba464](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/8eba464))
* change dependencies in setup.py ([eb5c0be](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/eb5c0be))
* change depreacted CI var in gitlab ci conf file ([74463c0](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/74463c0))
* change depreacted CI var in gitlab ci conf file ([4464019](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/4464019))
* escape percent character within csh script for format function ([a785ce9](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/a785ce9))
* force python 2.7 dependency ([99041c7](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/99041c7))
* force python 2.7 dependency ([5f3380d](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/5f3380d))
* future dependency in dockerfile ([0d1d39e](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/0d1d39e))
* implement setting.get method used within pdbqual tool ([a8d66dc](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/a8d66dc))
* implement setting.get method used within pdbqual tool ([1832c26](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/1832c26))
* remove logger during serialization for distance restraint class ([0465e1b](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/0465e1b))
* remove sge_job_id var in csh script refine for sbatch options ([a036e7f](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/a036e7f))
* removed unicode strings during convert part ([39d1d47](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/39d1d47))
* revision of output slurm file for cns refine.csh ([8d14ebd](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/8d14ebd))
* solve conflict issue between pdbqual and setup config parameters ([48a05e2](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/48a05e2))
* solve import issue with pdbqual command ([737dd53](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/737dd53))
* solve issue [#24](https://gitlab.pasteur.fr/bis-aria/Ariaec/issues/24) ([aeda4d1](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/aeda4d1))
* solve issue with missing alignment for evcoupling contacts ([c6f3afc](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/c6f3afc))
* solve issue with warnings in the logger ([990419e](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/990419e))
* solve numpy dependency issue with hdbscan ([bf3c81c](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/bf3c81c))
* solve several format issues in the new logging system ([eff5776](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/eff5776))
* sphinx crash with 1.8 due to python 3 dependencies ([fd0efa1](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/fd0efa1))
* syntax error for clashlist flag in aria project templates ([9b270f0](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/9b270f0))
* Update ariabase.py getstate ([6c209af](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/6c209af))
* update doc confs with the new project tree ([46dff44](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/46dff44))
* update dockerfile ([541d72c](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/541d72c))
* update dockerfile ([40b59e9](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/40b59e9))
* update dockerfile ([00d6865](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/00d6865))
* wget dependency in dockerfile ([bdf572c](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/bdf572c))
* wrong installation order in dockerfile for husky npm module ([934a449](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/934a449))
* wrong installation order in dockerfile for husky npm module ([c83c314](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/c83c314))
* **aria.core.TypeChecking:** overwrite previous python3 patch ([dbca55a](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/dbca55a))
* **aria.core.TypeChecking:** solve byte string type checker issue when running the package with python3 ([07fb1c6](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/07fb1c6))


### Features

* add options to activate or deactivate pdq quality tools (pdbqual command) ([4a54d05](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/4a54d05))
* add structure_analysis_enabled option within conbox commands ([e88a654](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/e88a654))
* add v2.3.7 aria project template ([1b9fa48](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/1b9fa48))
* Automatic changelog ([8fda476](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/8fda476))
* Automatic changelog ([23e095b](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/23e095b))
* molecule settings hack in order to support multiple chains ([164f187](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/164f187))
* new logging system for aria core ([223c58d](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/223c58d))
* new logging system for aria core ([1fd579c](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/1fd579c))
* safe xml tag checker for python3 ([d8b091e](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/d8b091e))
* semantic delivery gitlab ([b11664f](https://gitlab.pasteur.fr/bis-aria/Ariaec/commit/b11664f))



