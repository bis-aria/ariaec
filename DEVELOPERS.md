# Developing conventions

* [Running Tests](#tests)
* [Coding Rules](#rules)
* [Commit Message Guidelines](#commits)
* [Writing Documentation](#documentation)

This document describes how to set up your development environment to build, test and
explains the basic mechanics and conventions adopted since version 0.1.

## <a name="tests"></a> Running Tests

### <a name="unit-tests"></a> Running the Unit Test Suite

We write mostly doctest and execute them with pytest runner. To run all of the
tests once run:

```shell
pytest -sx --doctest-modules --doctest-ignore-import-errors
```

## <a name="rules"></a> Coding Rules

To ensure consistency throughout the source code, keep these rules in mind as you are working:

* All features or bug fixes **must be tested** by one or more [specs][unit-testing].
* All public API methods **must be documented** with docstring. To see how we
 document our APIs, please check out the existing source code and see the section about [writing documentation](#documentation). 
 Usually, each docstring follows the rules from [Numpy docstring Guide][np-style-guide]


## <a name="commits"></a> Git Commit Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the change log**.

The commit message formatting can be added using a typical git workflow or through the use of a CLI
wizard. 

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read on GitHub as well as in various git tools.

### Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header
of the reverted commit.
In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit
being reverted.
A commit with this format is automatically created by the [`git revert`][git-revert] command.

### Type
Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing or correcting existing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation
  generation

### Scope
The scope could be anything specifying place of the commit change. For example `conbox`,
`legacy`, `scientific`, etc...

You can use `*` when the change affects more than a single scope.

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
[reference Gitlab issues that this commit closes][closing-issues].

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines.
The rest of the commit message is then used for this.

## <a name="documentation"></a> Writing Documentation

All the documentation files are stored in the ``docs`` folder where every 
file can be manually changed (except for ``docs/api`` folder which is 
automatically generated).

This means that since we generate the documentation from the source code, we can easily provide
version-specific documentation by simply checking out a version of the package and running the build.

Extracting the source code documentation, processing and building the docs is handled by the
documentation generation tool [Sphinx][sphinx] combined with [better-apidoc][better-apidoc].

### Building and viewing the docs locally
The docs can be built from scratch using the makefile in docs:

```shell
cd docs
make html
```

### General documentation with Markdown

Any text in tags can contain markdown syntax for formatting. Generally, you can use any markdown
feature.

#### Headings

Only use *h2* headings and lower, as the page title is set in *h1*. Also make sure you follow the
heading hierarchy. This ensures correct table of contents are created.

#### Code blocks
In line code can be specified by enclosing the code in back-ticks (\`).
A block of multi-line code can be enclosed in triple back-ticks (\`\`\`) but it is formatted better
if it is enclosed in &lt;pre&gt;...&lt;/pre&gt; tags and the code lines themselves are indented.


[closing-issues]: https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html
[git-revert]: https://git-scm.com/docs/git-revert
[git-setup]: https://help.github.com/articles/set-up-git
[Sphinx]: http://www.sphinx-doc.org/en/master/
[better-apidoc]: https://github.com/goerz/better-apidoc