============
Installation
============

.. note::

    For the moment, ``ariaec``  Command Line Interface (CLI) is only compatible
    for **Python 2.7** but the transition to Python 3 is planned in the near
    future. Installation procedure may change in the future especially after
    the transition to **Python 3** which should be the next major update.

.. warning::

    This package contains patches for **CNS-solve** which needs to be compiled
    with the sources after the installation. Please follow post installation
    instruction before using ``aria2`` pipeline.

Dependencies
============

Before starting the installation procedure, be sure to check if dependencies
below are actually correctly installed.

* **pip** (>= 18.0)
* **git** (>= 2.0)
* **cns-solve** (1.21)
* **ccpnmr analysis** (optional)
* **ccpn data model** (optional)

CNS-solve
---------

Academic users can obtain a free version of CNS from the `CNS web site
<http://cns.csb.yale.edu>`_. Commercial users need to purchase a CNX license
from Accelrys (MSI).

CCPNMR
------

In order to use the CCPN data model for import/export or data conversion, you
also need to install the CCPN data model, and the CCPNMR Analysis software in
case you want to export restraint lists, NOE assignments, and analyses of the
generated structures, e.g. the result of the violation analysis. The complete
CCPN distribution including the API, the FormatConverter, and CCPNMR Analysis
can be obtained `here <http://www.ccpn.ac.uk/ccpn/software/downloads-v2/>`_.


The CCPN distribution provides scripts which guide you through the
installation process.

Virtual environments
====================

The usual installation process is to install the package inside a virtual
environment for `Python <https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/>`_.
In order to create a virtual environment, we need an environment manager like
``virtualenv`` (or ``venv`` if we use Python 3) with the package manager
``pip``. Another possibility is to use the `conda CLI
<https://docs.conda.io/projects/conda/en/latest/commands.html#id2>`_ from the
`Anaconda Python distribution <https://www.anaconda.com/distribution/>`_ which
is a package and an environment manager.

.. note::

    This step is optional but strongly recommended in order to avoid overlaps
    between Python packages already installed in your global/local environment.

virtualenv
----------

To install the latest stable release, do the following steps on your terminal :

1. Install or update `pip` and `virtualenv` in your current python environment

.. code-block:: shell

    python -m pip install --user --upgrade pip virtualenv

2. Go to your project directory and create a virtual environment by giving the
name of a directory to place the new virtual environment (e.g. `venv`).

.. code-block:: shell

    python -m virtualenv venv

3. Activate the new environment.

.. code-block:: shell

    source venv/bin/activate

.. note::
    With this procedure, you will need to activate the environment before any
    call to ``ariaec`` CLI with ``source venv/bin/activate``

conda
-----

If you have Anaconda distribution instead of , you can use ``conda`` can perform the same operations as ``virtualenv``.

1. Create an environment

.. code-block:: shell

    conda create --name ariaec python=2.7

2. Activate the environment

.. code-block:: shell

    source activate ariaec


Python Package Index
====================

Since ``ariaec`` is hosted on gitlab, we will use ``pip`` with ``git`` in order
to install the package.

.. code-block:: shell

    pip install git+https://gitlab.pasteur.fr/bis-aria/ariaec

.. note::

    If you don't use a virtual environment for Python, it is recommemded to use
    the flag ``--user`` in order to install ``ariaec`` to the Python user
    install directory (typically ``~/.local`` folder)

From source
===========

Another possibility is to download the ZIP_ or TAR_ version from Gitlab_ and use
the following steps to install the package.

1. Download ZIP_ or TAR_ version from Gitlab_ website or through command line

.. code-block:: shell

    wget -O ariaec-master.tar.gz https://gitlab.pasteur.fr/bis-aria/ariaec/repository/master/archive.tar.gz
    mkdir ariaec-master && tar xzf ariaec-master.tar.gz -C ariaec-master --strip-components 1

2. You can then call ``pip`` locally to install the package in your environment.

.. code-block:: shell

    cd ariaec-master
    mkdir build_aria && cp -r src setup.py PKG-INFO MANIFEST.in build_aria/
    cd build_aria
    pip install --user .


.. note::

    Local pip install may lead to unexpected computation time if your directory
    contains more than the ARIA package. In most cases, this is due to
    virtualenv folders within the same directory. In those situations,
    the `pip` command will also look into virtualenvs folders. A simple
    workaround to solve this issue is to create a empty folder for the
    installation as described above.


Singularity
===========

For the sake of reproductibility, a Singularity_ recipe is also available
allowing cross platform and legacy OS support.


.. code-block:: shell

    sudo singularity build aria.simg aria.shub

The CLI described in [usage](usage.rst) section will
then be accessible by calling the generated singularity image.

.. code-block:: shell

    singularity run aria.simg COMMAND

Dockerfile
==========

Developers can also build their own Docker container using the Dockerfile_ [1]_.


Post installation instructions
==============================

ARIA CNS
--------

The only piece of Fortran code that needs to be compiled together with your
CNS sources can be found in `src/aria/cns/src`. Only the code for floating
chirality assignment is being used. The procedure has not changed since ARIA
version 1.2 :

1. Please copy all the ARIA fortran files located in the subdirectory
``src/aria/cns/src`` to the directory
``cns_solve_1.1/source`` of the unpacked cns_solve distribution
(this will overwrite some files which is okay).

2. Edit the file ``cns_solve_env`` in the directory ``cns_solve_1.1`` to define
the location of the CNSsolve directory.

3. Go to the directory ``cns_solve_1.1`` and type ``make install``. This should
start the compilation process.


.. Footnotes
.. [1] In order to build it, you have to be root on your computer.


.. Hyperlinks
.. _Dockerfile: https://gitlab.pasteur.fr/bis-aria/ariaec/raw/master/Dockerfile?inline=false
.. _ZIP: https://gitlab.pasteur.fr/bis-aria/ariaec/repository/master/archive.zip
.. _TAR: https://gitlab.pasteur.fr/bis-aria/ariaec/repository/master/archive.tar
.. _Gitlab: https://gitlab.pasteur.fr/bis-aria/ariaec
.. _Singularity: https://singularity.lbl.gov/
.. _CNSRequest: http://cns-online.org/cns_request/
.. _CNSDownload: http://cns-online.org/download/