.. ARIAEC documentation master file, created by
   sphinx-quickstart on Mon Oct 23 16:10:18 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url:

=====================================================
de Novo Ambiguous Restraints for Iterative Assignment
=====================================================

**A pipeline for automated de novo contact map assignment**

.. image:: https://gitlab.pasteur.fr/bis-aria/ariaec/badges/master/pipeline.svg
   :target: https://gitlab.pasteur.fr/bis-aria/ariaec/commits/master
   :alt: Pipeline status

.. image:: https://gitlab.pasteur.fr/bis-aria/ariaec/badges/master/coverage.svg
   :target: https://gitlab.pasteur.fr/bis-aria/ariaec/commits/master
   :alt: Coverage report

.. image:: https://img.shields.io/badge/DOI-10.1093%2Fbioinformatics%2Fbtl589-blue.svg
   :target: https://doi.org/10.1093/bioinformatics/btl589
   :alt: DOI status


**ariaec**  is a Python_ library that provides *de novo* structure prediction
based on ARIA_ pipeline and evolutionary restraints inferred from co-evolution.


The package add a new command line interface aside the usual ARIA commands in
order to convert data, setup an ARIA project with evolutionary restraints,
analyze contact maps or protein structures and generate statistics from a culled
list of PDB files.


Quick Start
-----------

Be sure to check if the following packages are correctly installed with
your python installation or virtual environment.

* **pip** (>= 18.0)
* **git** (>= 2.0)
* **cns-solve** (1.21)
* **ccpnmr analysis** (optional)
* **ccpn data model** (optional)

Then the easiest solution is to call the **pip** command below :

.. code-block:: shell

   pip install git+https://gitlab.pasteur.fr/bis-aria/ariaec

.. warning::

   This package contains patches for **CNS-solve** which needs to be compiled
   after the installation. Please follow post installation instruction
   before using ``aria2`` pipeline.

For more information about installation and usage, please refer to the
`ARIAEC documentation <http://bis-aria.pages.pasteur.fr/ariaec>`_.

Contributing
------------

Please read `guidelines for contributing <https://gitlab.pasteur.fr/bis-aria/ariaec/blob/master/CONTRIBUTING.md>`_ for any
suggestions or bug report.


License
-------
.. mdinclude:: ../COPYRIGHT.md

.. include:: contents.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. Hyperlinks
.. _ZIP: https://gitlab.pasteur.fr/bis-aria/ariaec/repository/master/archive.zip
.. _TAR: https://gitlab.pasteur.fr/bis-aria/ariaec/repository/master/archive.tar
.. _Gitlab: https://gitlab.pasteur.fr/bis-aria/ariaec
.. _ARIA: http://aria.pasteur.fr
.. _Python: https://www.python.org
