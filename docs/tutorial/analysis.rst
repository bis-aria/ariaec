Analysis
========

Contact map
-----------

Aside ConKit_  :abbr:`Command Line Interface CLI)` tools, the :ref:`maplot <../usage>`_
command line has been implemented to show statistics between residu contact maps
and the reference which can be a structure in PDB_ format.

.. Analyse des cartes de contacts via la commande maplot

ARIA run
--------

Here is a brief list of files you should look at when all the iteration are finished:

* ARIA ensemble for each iteration. Depending of the activation of the water refinment step, the last ensemble is located within ``runX/structures/itY`` or ``runX/structures/refine`` folder (X is the number related to the last iteration).
* ``report`` (and ``report.clustering`` if clustering is activated) related to ensemble of generated structure
* ``itY/analysis`` folder with all structure quality reports generated with Procheck, molprobity, whatif & prosa (iff the executable path are correct in the initial configuration file)

You can find a more detailed explanation of ARIA output files on the ARIA_
website.

.. Analyse de structures au format PDB via la commande pdbqual (en ayant précisé les chemins des programmes dans le fichier de configuration)





.. _ConKit: http://www.conkit.org/en/0.9/
.. _PDB: ftp://ftp.wwpdb.org/pub/pdb/doc/format_descriptions/Format_v33_A4.pdf
.. _ARIA: http://aria.pasteur.fr/