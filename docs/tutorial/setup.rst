Setup
=====

Modifier les paramètres indiquant le chemin de CNS et du ou des programmes d'analyse qu'on souhaite utiliser
Lancer la commande setup en prenant en compte l'ensemble des fichiers qu'on souhaite convertir
Lister les différents fichiers générés
Verifier dans les log si les étapes se sont correctement déroulé

The ``ariaec`` :abbr:`Command Line Interface CLI)` is the main tool for
converting and analyze contact map information as physical restraints