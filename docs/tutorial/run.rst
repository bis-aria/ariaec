Run
===

Once the setup step is done. We can run the generated ARIA_ project using the
command lines below. This step start by initialising the project using
``aria2 -s``. All the generated files will be saved by default in the ``run1``
folder.

The project can then be executed by calling ``aria2`` with the project file. Be
sure to check if the host command given in your initial configuration file
have the correct job scheduler.

.. warning::

  Depending on your environment, you probably have to give ``--no-test`` option
  for the second command in order to disable the dry run of the commands
  specified for the structure calculation.



.. _ARIA: http://aria.pasteur.fr
