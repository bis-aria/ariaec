.. toctree::
   :maxdepth: 2
   :caption: Documentation

   changelog
   installation
   configuration
   usage
   tutorial
   contributing

.. toctree::
   :maxdepth: 4
   :caption: API

   Core package <api/aria.core>
   Conbox package <api/aria.conbox>

