
:autogenerated:

aria.core.Chain module
======================

.. currentmodule:: aria.core.Chain

.. automodule:: aria.core.Chain
   :members: ALWAYS, ARIA_ENV, ARRAY, AtomNomenclatureEntity, BOOL, CCPN_EXTENSION, Chain, ChainSettings, ChainXMLPickler, DICT, FLOAT, FLOAT64, GZIP, INT, LIST, MODULE_NAME, NO, NONE, ORDEREDICT, PATH_MODULES, PRINT_LOCK, PROJECT_TEMPLATE, STRING, TUPLE, TYPES, TYPE_DNA, TYPE_NONPOLYMER, TYPE_PROTEIN, TYPE_RNA, UNICODE, VL_LOW, VL_SETTINGS, VL_STANDARD, YES
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   AtomNomenclatureEntity
   Chain
   ChainSettings
   ChainXMLPickler

Data:

.. autosummary::
   :nosignatures:

   ALWAYS
   ARIA_ENV
   ARRAY
   BOOL
   CCPN_EXTENSION
   DICT
   FLOAT
   FLOAT64
   GZIP
   INT
   LIST
   MODULE_NAME
   NO
   NONE
   ORDEREDICT
   PATH_MODULES
   PRINT_LOCK
   PROJECT_TEMPLATE
   STRING
   TUPLE
   TYPES
   TYPE_DNA
   TYPE_NONPOLYMER
   TYPE_PROTEIN
   TYPE_RNA
   UNICODE
   VL_LOW
   VL_SETTINGS
   VL_STANDARD
   YES





Reference
---------