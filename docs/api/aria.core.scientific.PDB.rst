
:autogenerated:

aria.core.scientific.PDB module
===============================

.. currentmodule:: aria.core.scientific.PDB

.. automodule:: aria.core.scientific.PDB
   :members: AminoAcidResidue, Atom, Chain, DummyChain, Group, HetAtom, Molecule, NucleotideChain, NucleotideResidue, PDBFile, PeptideChain, Residue, ResidueNumber, Structure, amino_acids, defineAminoAcidResidue, defineNucleicAcidResidue, nucleic_acids
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   AminoAcidResidue
   Atom
   Chain
   DummyChain
   Group
   HetAtom
   Molecule
   NucleotideChain
   NucleotideResidue
   PDBFile
   PeptideChain
   Residue
   ResidueNumber
   Structure

Functions:

.. autosummary::
   :nosignatures:

   defineAminoAcidResidue
   defineNucleicAcidResidue

Data:

.. autosummary::
   :nosignatures:

   amino_acids
   nucleic_acids





Reference
---------