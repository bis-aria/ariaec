
:autogenerated:

aria.core.gui.panels module
===========================

.. currentmodule:: aria.core.gui.panels

.. automodule:: aria.core.gui.panels
   :members: ALWAYS, ARIA_ENV, ARRAY, AnalysisPanel, AnnealingAmbigPanel, AnnealingDihedralPanel, AnnealingFBHWPanel, AnnealingHBondPanel, AnnealingKarplusPanel, AnnealingRDCPanel, AnnealingSymmetryPanel, AnnealingUnambigPanel, BOOL, BUTTON_WIDTH, CCPNPanel, CCPN_EXTENSION, CNSPanel, ChainCodeSelector, CisProPatchPanel, CmapOptionsPanel, ContactMapPanel, ContribPanel, ControlPanel, ControlPanelSettings, DATA_PDB, DATA_TEXT, DICT, DataAmbigPanel, DataDihedralPanel, DataHBondPanel, DataIsoPeptidePatchPanel, DataKarplusPanel, DataOtherPanel, DataPanel, DataRDCPanel, DataSSBondPanel, DataSequencePanel, DataSpectrumPanel, DataTemplatePanel, DataUnambigPanel, DataZnPatchPanel, DynamicsPanel, FLOAT, FLOAT64, GZIP, HBDBPanel, HisPatchPanel, INT, IterationPanel, JobManagerPanel, LIST, LogHarmonicPanel, MODULE_NAME, NO, NONE, ORDEREDICT, PATH_MODULES, PRINT_LOCK, PROJECT_TEMPLATE, PY3, Panel, PanelEx, PlotPanel, PreferencesPanel, ProjectPanel, ProtocolPanel, RamaPanel, ReportPanel, SSBridgePanel, SSBridgePanel2, STRING, SymmetryPanel, TUPLE, TYPES, UNICODE, VL_LOW, VL_SETTINGS, VL_STANDARD, WaterRefinementPanel, YES, YES_NO_DICT, YES_NO_GZIP_DICT, file_and_format
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   AnalysisPanel
   AnnealingAmbigPanel
   AnnealingDihedralPanel
   AnnealingFBHWPanel
   AnnealingHBondPanel
   AnnealingKarplusPanel
   AnnealingRDCPanel
   AnnealingSymmetryPanel
   AnnealingUnambigPanel
   CCPNPanel
   CNSPanel
   ChainCodeSelector
   CisProPatchPanel
   CmapOptionsPanel
   ContactMapPanel
   ContribPanel
   ControlPanel
   ControlPanelSettings
   DataAmbigPanel
   DataDihedralPanel
   DataHBondPanel
   DataIsoPeptidePatchPanel
   DataKarplusPanel
   DataOtherPanel
   DataPanel
   DataRDCPanel
   DataSSBondPanel
   DataSequencePanel
   DataSpectrumPanel
   DataTemplatePanel
   DataUnambigPanel
   DataZnPatchPanel
   DynamicsPanel
   HBDBPanel
   HisPatchPanel
   IterationPanel
   JobManagerPanel
   LogHarmonicPanel
   Panel
   PanelEx
   PlotPanel
   PreferencesPanel
   ProjectPanel
   ProtocolPanel
   RamaPanel
   ReportPanel
   SSBridgePanel
   SSBridgePanel2
   SymmetryPanel
   WaterRefinementPanel

Functions:

.. autosummary::
   :nosignatures:

   file_and_format

Data:

.. autosummary::
   :nosignatures:

   ALWAYS
   ARIA_ENV
   ARRAY
   BOOL
   BUTTON_WIDTH
   CCPN_EXTENSION
   DATA_PDB
   DATA_TEXT
   DICT
   FLOAT
   FLOAT64
   GZIP
   INT
   LIST
   MODULE_NAME
   NO
   NONE
   ORDEREDICT
   PATH_MODULES
   PRINT_LOCK
   PROJECT_TEMPLATE
   PY3
   STRING
   TUPLE
   TYPES
   UNICODE
   VL_LOW
   VL_SETTINGS
   VL_STANDARD
   YES
   YES_NO_DICT
   YES_NO_GZIP_DICT





Reference
---------