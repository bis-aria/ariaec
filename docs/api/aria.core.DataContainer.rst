
:autogenerated:

aria.core.DataContainer module
==============================

.. currentmodule:: aria.core.DataContainer

.. automodule:: aria.core.DataContainer
   :members: ALWAYS, ARIA_ENV, ARRAY, AmbiguousDistanceData, AmbiguousDistanceDataXMLPickler, AmbiguousParameters, AmbiguousParametersXMLPickler, AnnealingParameters, AnnealingParametersXMLPickler, BASE, BOOL, BoundCorrection, BoundCorrectionXMLPickler, CCPNData, CCPNDataXMLPickler, CCPN_EXTENSION, CisProPatch, CisProPatchXMLPickler, CysPatch, CysPatchXMLPickler, DATA_AMBIGUOUS, DATA_ANNEALING, DATA_ANNEALING_AMBIG, DATA_ANNEALING_DIHEDRAL, DATA_ANNEALING_FBHW, DATA_ANNEALING_HBDB, DATA_ANNEALING_HBOND, DATA_ANNEALING_KARPLUS, DATA_ANNEALING_LOGHARMONIC, DATA_ANNEALING_RAMA, DATA_ANNEALING_RDC, DATA_ANNEALING_SCORING, DATA_ANNEALING_SYM, DATA_ANNEALING_UNAMBIG, DATA_CCPN, DATA_CISPROPATCH, DATA_CYSPATCH, DATA_DEFAULT, DATA_DIHEDRALS, DATA_DYNAMICS, DATA_EXPERIMENT, DATA_HBONDS, DATA_HISPATCH, DATA_INITIAL_STRUCTURE, DATA_ISOPATCH, DATA_ITERATION, DATA_KARPLUS, DATA_OTHER, DATA_PEAKS, DATA_RDCS, DATA_SEQUENCE, DATA_SHIFTS, DATA_SPECTRUM, DATA_SSBONDS, DATA_SSBRIDGE, DATA_SYMMETRY, DATA_TEMPLATE_STRUCTURE, DATA_TYPES, DATA_UNAMBIGUOUS, DATA_ZNPATCH, DICT, DRParametersXMLPickler, DataContainer, DataContainerXMLPickler, DihedralData, DihedralDataXMLPickler, DihedralParameters, DihedralParametersXMLPickler, DistanceDataXMLPickler, ExperimentData, ExperimentDataXMLPickler, FBHWParameters, FBHWParametersXMLPickler, FFP, FLOAT, FLOAT64, FileFormatXMLPickler, GZIP, HBDBParameters, HBDBParametersXMLPickler, HBondData, HBondDataXMLPickler, HBondParameters, HBondParametersXMLPickler, HisPatch, HisPatchXMLPickler, INT, InitialStructureData, InitialStructureDataXMLPickler, IsoPeptidePatch, IsoPeptidePatchXMLPickler, KarplusData, KarplusDataXMLPickler, KarplusParameters, KarplusParametersXMLPickler, LIST, LogHarmonicParameters, LogHarmonicParametersXMLPickler, LowerBoundCorrection, MDParameters, MDParametersXMLPickler, MODULE_NAME, NO, NONE, ORDEREDICT, OtherData, OtherDataXMLPickler, PATH_MODULES, PRINT_LOCK, PROJECT_TEMPLATE, PeakData, PeakDataXMLPickler, RDCData, RDCDataXMLPickler, RDCParameters, RDCParametersXMLPickler, RamaParameters, RamaParametersXMLPickler, SSBondData, SSBondDataXMLPickler, SSBridge, SSBridgeXMLPickler, STRING, ScoringParameters, ScoringParametersXMLPickler, SequenceData, SequenceDataXMLPickler, ShiftData, ShiftDataXMLPickler, SimpleDCXMLPickler, SimpleDataContainer, SpectrumData, SpectrumDataXMLPickler, Symmetry, SymmetryParameters, SymmetryParametersXMLPickler, SymmetryXMLPickler, TUPLE, TYPES, TemplateData, TemplateDataXMLPickler, UNICODE, UnambiguousDistanceData, UnambiguousDistanceDataXMLPickler, UnambiguousParameters, UnambiguousParametersXMLPickler, UpperBoundCorrection, VL_LOW, VL_SETTINGS, VL_STANDARD, WaterRefinementParameters, WaterRefinementXMLPickler, YES, ZnPatch, ZnPatchXMLPickler
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   AmbiguousDistanceData
   AmbiguousDistanceDataXMLPickler
   AmbiguousParameters
   AmbiguousParametersXMLPickler
   AnnealingParameters
   AnnealingParametersXMLPickler
   BASE
   BoundCorrection
   BoundCorrectionXMLPickler
   CCPNData
   CCPNDataXMLPickler
   CisProPatch
   CisProPatchXMLPickler
   CysPatch
   CysPatchXMLPickler
   DRParametersXMLPickler
   DataContainer
   DataContainerXMLPickler
   DihedralData
   DihedralDataXMLPickler
   DihedralParameters
   DihedralParametersXMLPickler
   DistanceDataXMLPickler
   ExperimentData
   ExperimentDataXMLPickler
   FBHWParameters
   FBHWParametersXMLPickler
   FFP
   FileFormatXMLPickler
   HBDBParameters
   HBDBParametersXMLPickler
   HBondData
   HBondDataXMLPickler
   HBondParameters
   HBondParametersXMLPickler
   HisPatch
   HisPatchXMLPickler
   InitialStructureData
   InitialStructureDataXMLPickler
   IsoPeptidePatch
   IsoPeptidePatchXMLPickler
   KarplusData
   KarplusDataXMLPickler
   KarplusParameters
   KarplusParametersXMLPickler
   LogHarmonicParameters
   LogHarmonicParametersXMLPickler
   LowerBoundCorrection
   MDParameters
   MDParametersXMLPickler
   OtherData
   OtherDataXMLPickler
   PeakData
   PeakDataXMLPickler
   RDCData
   RDCDataXMLPickler
   RDCParameters
   RDCParametersXMLPickler
   RamaParameters
   RamaParametersXMLPickler
   SSBondData
   SSBondDataXMLPickler
   SSBridge
   SSBridgeXMLPickler
   ScoringParameters
   ScoringParametersXMLPickler
   SequenceData
   SequenceDataXMLPickler
   ShiftData
   ShiftDataXMLPickler
   SimpleDCXMLPickler
   SimpleDataContainer
   SpectrumData
   SpectrumDataXMLPickler
   Symmetry
   SymmetryParameters
   SymmetryParametersXMLPickler
   SymmetryXMLPickler
   TemplateData
   TemplateDataXMLPickler
   UnambiguousDistanceData
   UnambiguousDistanceDataXMLPickler
   UnambiguousParameters
   UnambiguousParametersXMLPickler
   UpperBoundCorrection
   WaterRefinementParameters
   WaterRefinementXMLPickler
   ZnPatch
   ZnPatchXMLPickler

Data:

.. autosummary::
   :nosignatures:

   ALWAYS
   ARIA_ENV
   ARRAY
   BOOL
   CCPN_EXTENSION
   DATA_AMBIGUOUS
   DATA_ANNEALING
   DATA_ANNEALING_AMBIG
   DATA_ANNEALING_DIHEDRAL
   DATA_ANNEALING_FBHW
   DATA_ANNEALING_HBDB
   DATA_ANNEALING_HBOND
   DATA_ANNEALING_KARPLUS
   DATA_ANNEALING_LOGHARMONIC
   DATA_ANNEALING_RAMA
   DATA_ANNEALING_RDC
   DATA_ANNEALING_SCORING
   DATA_ANNEALING_SYM
   DATA_ANNEALING_UNAMBIG
   DATA_CCPN
   DATA_CISPROPATCH
   DATA_CYSPATCH
   DATA_DEFAULT
   DATA_DIHEDRALS
   DATA_DYNAMICS
   DATA_EXPERIMENT
   DATA_HBONDS
   DATA_HISPATCH
   DATA_INITIAL_STRUCTURE
   DATA_ISOPATCH
   DATA_ITERATION
   DATA_KARPLUS
   DATA_OTHER
   DATA_PEAKS
   DATA_RDCS
   DATA_SEQUENCE
   DATA_SHIFTS
   DATA_SPECTRUM
   DATA_SSBONDS
   DATA_SSBRIDGE
   DATA_SYMMETRY
   DATA_TEMPLATE_STRUCTURE
   DATA_TYPES
   DATA_UNAMBIGUOUS
   DATA_ZNPATCH
   DICT
   FLOAT
   FLOAT64
   GZIP
   INT
   LIST
   MODULE_NAME
   NO
   NONE
   ORDEREDICT
   PATH_MODULES
   PRINT_LOCK
   PROJECT_TEMPLATE
   STRING
   TUPLE
   TYPES
   UNICODE
   VL_LOW
   VL_SETTINGS
   VL_STANDARD
   YES





Reference
---------