
:autogenerated:

aria.core.NOESYSpectrum module
==============================

.. currentmodule:: aria.core.NOESYSpectrum

.. automodule:: aria.core.NOESYSpectrum
   :members: ALWAYS, ARIA_ENV, ARRAY, BOOL, CCPN_EXTENSION, ConstraintList, DICT, FLOAT, FLOAT64, GZIP, INT, LIST, MODULE_NAME, NO, NOESYSpectrum, NOESYSpectrumXMLPickler, NONE, ORDEREDICT, PATH_MODULES, PRINT_LOCK, PROJECT_TEMPLATE, STRING, TUPLE, TYPES, UNICODE, VL_LOW, VL_SETTINGS, VL_STANDARD, YES
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   ConstraintList
   NOESYSpectrum
   NOESYSpectrumXMLPickler

Data:

.. autosummary::
   :nosignatures:

   ALWAYS
   ARIA_ENV
   ARRAY
   BOOL
   CCPN_EXTENSION
   DICT
   FLOAT
   FLOAT64
   GZIP
   INT
   LIST
   MODULE_NAME
   NO
   NONE
   ORDEREDICT
   PATH_MODULES
   PRINT_LOCK
   PROJECT_TEMPLATE
   STRING
   TUPLE
   TYPES
   UNICODE
   VL_LOW
   VL_SETTINGS
   VL_STANDARD
   YES





Reference
---------