
:autogenerated:

aria.core.gui.gui module
========================

.. currentmodule:: aria.core.gui.gui

.. automodule:: aria.core.gui.gui
   :members: ACTIVE, ALL, ANCHOR, ARC, ARIA_ENV, AboutBox, BASELINE, BEVEL, BOTH, BOTTOM, BROWSE, BUTT, CANCEL, CASCADE, CENTER, CHAR, CHECKBUTTON, CHORD, COMMAND, CURRENT, DISABLED, DOTBOX, DiscardDialog, E, END, EW, EXTENDED, EntriesMissingDialog, FALSE, FIRST, FLAT, GROOVE, GUI, GUISettings, GUI_MODULES, GUI_SETTINGS_FILENAME, GUI_VERSION, HIDDEN, HORIZONTAL, INSERT, INSIDE, LAST, LEFT, LIST, MITER, MOVETO, MULTIPLE, N, NE, NO, NONE, NORMAL, NS, NSEW, NUMERIC, NW, NotSavedDialog, OFF, ON, OUTSIDE, PAGES, PATH_GUI_MODULES, PATH_MODULES, PIESLICE, PROJECTING, PROJECT_TEMPLATE, PY3, RADIOBUTTON, RAISED, RIDGE, RIGHT, ROUND, S, SCROLL, SE, SEL, SEL_FIRST, SEL_LAST, SEPARATOR, SINGLE, SOLID, SUNKEN, SW, TCL_ALL_EVENTS, TCL_DONT_WAIT, TCL_FILE_EVENTS, TCL_IDLE_EVENTS, TCL_TIMER_EVENTS, TCL_WINDOW_EVENTS, TK_OPTIONS_TEMPLATE, TOP, TRUE, UNDERLINE, UNITS, VERTICAL, W, WORD, X, Y, YES, go, popup
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Summary
-------

Classes:

.. autosummary::
   :nosignatures:

   GUI
   GUISettings

Functions:

.. autosummary::
   :nosignatures:

   AboutBox
   DiscardDialog
   EntriesMissingDialog
   NotSavedDialog
   go
   popup

Data:

.. autosummary::
   :nosignatures:

   ACTIVE
   ALL
   ANCHOR
   ARC
   ARIA_ENV
   BASELINE
   BEVEL
   BOTH
   BOTTOM
   BROWSE
   BUTT
   CANCEL
   CASCADE
   CENTER
   CHAR
   CHECKBUTTON
   CHORD
   COMMAND
   CURRENT
   DISABLED
   DOTBOX
   E
   END
   EW
   EXTENDED
   FALSE
   FIRST
   FLAT
   GROOVE
   GUI_MODULES
   GUI_SETTINGS_FILENAME
   GUI_VERSION
   HIDDEN
   HORIZONTAL
   INSERT
   INSIDE
   LAST
   LEFT
   LIST
   MITER
   MOVETO
   MULTIPLE
   N
   NE
   NO
   NONE
   NORMAL
   NS
   NSEW
   NUMERIC
   NW
   OFF
   ON
   OUTSIDE
   PAGES
   PATH_GUI_MODULES
   PATH_MODULES
   PIESLICE
   PROJECTING
   PROJECT_TEMPLATE
   PY3
   RADIOBUTTON
   RAISED
   RIDGE
   RIGHT
   ROUND
   S
   SCROLL
   SE
   SEL
   SEL_FIRST
   SEL_LAST
   SEPARATOR
   SINGLE
   SOLID
   SUNKEN
   SW
   TCL_ALL_EVENTS
   TCL_DONT_WAIT
   TCL_FILE_EVENTS
   TCL_IDLE_EVENTS
   TCL_TIMER_EVENTS
   TCL_WINDOW_EVENTS
   TK_OPTIONS_TEMPLATE
   TOP
   TRUE
   UNDERLINE
   UNITS
   VERTICAL
   W
   WORD
   X
   Y
   YES





Reference
---------