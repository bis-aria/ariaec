
:autogenerated:

aria.core package
=================

.. automodule:: aria.core
   



Submodules:

.. toctree::
   :maxdepth: 1

   aria.core.Analyser
   aria.core.AriaPeak
   aria.core.AriaXML
   aria.core.Assignment
   aria.core.AssignmentFilter
   aria.core.Atom
   aria.core.Calibrator
   aria.core.Chain
   aria.core.ChemicalShiftFilter
   aria.core.ChemicalShiftList
   aria.core.Cluster
   aria.core.Contribution
   aria.core.ContributionAssigner
   aria.core.ConversionTable
   aria.core.CovalentDistances
   aria.core.CrossPeak
   aria.core.CrossPeakFilter
   aria.core.DataContainer
   aria.core.Datum
   aria.core.Experiment
   aria.core.Factory
   aria.core.FloatFile
   aria.core.Infrastructure
   aria.core.Iteration
   aria.core.JobManager
   aria.core.Merger
   aria.core.MolMol
   aria.core.Molecule
   aria.core.Molprobity
   aria.core.NOEModel
   aria.core.NOESYSpectrum
   aria.core.NOESYSpectrumFilter
   aria.core.Network
   aria.core.OrderedDict
   aria.core.PDBReader
   aria.core.PeakAssigner
   aria.core.Project
   aria.core.Protocol
   aria.core.Relaxation
   aria.core.Report
   aria.core.Residue
   aria.core.RmsReport
   aria.core.Settings
   aria.core.ShiftAssignment
   aria.core.ShiftAssignmentFilter
   aria.core.Singleton
   aria.core.SpinPair
   aria.core.StructureEnsemble
   aria.core.SuperImposer
   aria.core.Topology
   aria.core.TypeChecking
   aria.core.ViolationAnalyser
   aria.core.WhatifProfile
   aria.core.ariabase
   aria.core.ccpn2top
   aria.core.ccpn_conversion
   aria.core.cns
   aria.core.conversion
   aria.core.exportToCcpn
   aria.core.importFromCcpn
   aria.core.mathutils
   aria.core.tools
   aria.core.xmlparser
   aria.core.xmlutils

Subpackages:

.. toctree::
   :maxdepth: 1

   aria.core.gui
   aria.core.legacy
   aria.core.scientific






