## de Novo Ambiguous Restraints for Iterative Assignment

**A pipeline for automated de novo contact map assignment**

[![chat](https://img.shields.io/badge/chat-mattermost-informational.svg)](https://chatlab.pasteur.fr/bis-aria/channels/aria)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](http://bis-aria.pages.pasteur.fr/ariaec)
[![pipeline status](https://gitlab.pasteur.fr/bis-aria/ariaec/badges/master/pipeline.svg)](https://gitlab.pasteur.fr/bis-aria/ariaec/commits/master)
[![coverage report](https://gitlab.pasteur.fr/bis-aria/ariaec/badges/master/coverage.svg)](https://gitlab.pasteur.fr/bis-aria/ariaec/commits/master)


**ariaec**  is a [Python](https://www.python.org/) library that provides *de novo* structure prediction
based on [ARIA](http://aria.pasteur.fr/) pipeline and evolutionary restraints inferred from co-evolution.

The package add a new command line interface aside the usual ARIA commands in
order to convert data, setup an ARIA project with evolutionary restraints,
analyze contact maps or protein structures or generate statistics from a culled
list of PDB files.


## Quick Start

Be sure to check if the following packages are correctly installed with
your python installation or virtual environment.

- **pip** (>= 18.0)
- **git** (>= 2.0)
- **cns-solve** (1.21)
- **ccpnmr analysis** (optional)
- **ccpn data model** (optional)

Then the easiest solution is to call the **pip** command below :

   `pip install git+http://gitlab.pasteur.fr/bis-aria/ariaec.git`

For more information about installation and usage, please refer to the
[ARIAEC documentation](http://bis-aria.pages.pasteur.fr/ariaec)

**ARIA contains patches for CNS-solve which needs to be compiled after the
installation. Please follow [post installation instructions](http://bis-aria.pages.pasteur.fr/ariaec/installation.html#post-installation-instructions).**

    
## Contributing

Please read [guidelines for contributing](https://gitlab.pasteur.fr/bis-aria/ariaec/blob/master/CONTRIBUTING.md) for any
suggestions or bug report.
