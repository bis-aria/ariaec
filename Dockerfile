FROM centos:centos7
MAINTAINER Fabrice Allain <fabrice.allain@pasteur.fr>

RUN yum install -y epel-release
RUN yum install -y python2-pip python-devel gcc gcc-c++ make git wget which
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -; yum -y install nodejs

RUN npm install -g --save-dev @commitlint/{cli,config-conventional}; npm install -g conventional-changelog-cli
RUN echo "module.exports = {extends: ['@commitlint/config-conventional']};" > commitlint.config.js

RUN pip install --upgrade pip setuptools; pip install coverage; pip install pytest; pip install pytest-cov

RUN wget http://www2.ccpn.ac.uk/download/ccpnmr/analysis2.4.2.tar.gz; tar xvzf analysis2.4.2.tar.gz; export CCPNMR_TOP_DIR=$(pwd)/ccpnmr; export PYTHONPATH=${PYTHONPATH}:$CCPNMR_TOP_DIR/ccpnmr2.4/python

RUN mkdir build-aria && mkdir build-aria/src
ADD src/aria /build-aria/src/aria
ADD *.py PKG-INFO /build-aria/
RUN ls build-aria/src/aria
RUN which pip; which python
RUN cd build-aria; pip install .; npm install --save-dev husky
RUN npx yarn add husky --dev
