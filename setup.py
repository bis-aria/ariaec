# coding=utf-8
"""

          ARIA -- Ambiguous Restraints for Iterative Assignment

                 A software for automated NOE assignment

                               Version 2.3


Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,
              Wolfgang Rieping, and Michael Nilges

All rights reserved.


NO WARRANTY. This software package is provided 'as is' without warranty of
any kind, expressed or implied, including, but not limited to the implied
warranties of merchantability and fitness for a particular purpose or
a warranty of non-infringement.

Distribution of substantively modified versions of this module is
prohibited without the explicit permission of the copyright holders.

$Author: bardiaux $
$Revision: 1.1.1.1 $
$Date: 2010/03/23 15:27:16 $
"""
from __future__ import absolute_import, division, print_function

import re
import os
import sys
import textwrap
import subprocess
import pkg_resources
from os.path import dirname, isdir, join
from setuptools import Command, find_packages, setup
# from pkg_resources import resource_filename

# Setup file mainly copied from scipy setup file
DOCLINES = re.sub("\n", " ", re.sub(" {2,}", "", __doc__))
# DOCLINES = __doc__.split("\n")

CLASSIFIERS = """\
Development Status :: 5 - Production/Stable
Intended Audience :: Science/Research
Intended Audience :: Developers
License :: OSI Approved :: BSD License
Programming Language :: Python
Programming Language :: Python :: 2
Programming Language :: Python :: 2.6
Programming Language :: Python :: 2.7
Topic :: Software Development :: Libraries :: Python Modules
Topic :: Scientific/Engineering
Topic :: Utilities
Operating System :: POSIX
Operating System :: Unix
Operating System :: MacOS
"""

MAJOR = 2
MINOR = 3
MICRO = 2
ISRELEASED = False
# TODO: get version tag in ariabase.py ?
VERSION = '%d.%d.%d' % (MAJOR, MINOR, MICRO)


VERSION_RE = r'^(Version: )(.+)$'
VERSION_REC = re.compile(VERSION_RE, re.M)


def get_version(full=True):
    """

    Returns
    -------

    """
    d = dirname(__file__)
    # TODO: actually no warning when git command is not available !!
    if isdir(join(d, '.git')):
        # Get the version using "git describe".
        cmd = 'git describe --tags'.split()
        try:
            version = subprocess.check_output(cmd).decode().strip()
        except subprocess.CalledProcessError:
            print('Unable to get version number from git tags')
            sys.exit(1)

        with open(join(d, 'PKG-INFO'), 'r+') as foo:
            lines = foo.read()
            pkg_version = VERSION_REC.search(lines).group(2)
            if pkg_version.strip("v") != version.strip("v"):
                foo.seek(0)
                print("Update PKG_INFO file with actual git tag")
                foo.write(re.sub(VERSION_RE, r"\1", lines) +
                          str(version.split('-')[0].strip("v")))

        # PEP 386 compatibility
        if '-' in version and full:
            version = '.post'.join(version.split('-')[:2])
        else:
            version = version.split('-')[0]

        # Don't declare a version "dirty" merely because a time stamp has
        # changed. If it is dirty, append a ".dev1" suffix to indicate a
        # development revision after the release.
        with open(os.devnull, 'w') as fd_devnull:
            subprocess.call(['git', 'status'],
                            stdout=fd_devnull, stderr=fd_devnull)

        cmd = 'git diff-index --name-only HEAD'.split()
        try:
            dirty = subprocess.check_output(cmd).decode().strip()
        except subprocess.CalledProcessError:
            print('Unable to get git index status')
            sys.exit(1)

        if dirty != '' and full:
            version += '.dev1'

    else:
        # Extract the version from the PKG-INFO file.
        with open(join(d, 'PKG-INFO')) as foo:
            version = VERSION_REC.search(foo.read()).group(2)

    return version


# Compatibility checks
# TODO: Change this line after python 3 compatibility done !
# TODO: if python 2 still supported, use sys.version to update dependencies
# if sys.version_info[:2] < (2, 6) or sys.version_info[:2] > (3, 0):
#     raise RuntimeError("Python version 2.6, 2.7 required.")

# TODO: Uncomment lines below after python 3 compatibility
# TODO: Update package with f strings after full python 3 support
# if sys.version_info[0] < 3:
#     import __builtin__ as builtins
# else:
#     import builtins

# BEFORE importing setuptools, remove MANIFEST. Otherwise it may not be
# properly updated when the contents of directories change (true for distutils,
# not sure about setuptools).
if os.path.exists('MANIFEST'):
    os.remove('MANIFEST')

if os.path.exists('README'):
    with open('README') as f:
        LONG_DOCLINES = f.read()
else:
    LONG_DOCLINES = DOCLINES

if os.path.exists('LICENSE'):
    with open('LICENSE') as f:
        LICENSE = f.read()
else:
    LICENSE = ''


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    @staticmethod
    def run():
        """Clean building directories"""
        os.system('rm -vrf ./.eggs ./build ./dist ./*.pyc ./*.tgz '
                  './*.egg-info ./*.egg')


def is_installed(requirement):
    """Check if requirement package is installed"""
    try:
        pkg_resources.require(requirement)
    except pkg_resources.ResolutionError:
        return False
    finally:
        return True


def setup_package():
    """Main function"""

    metadata = dict(
        name='ariaec',
        version=get_version(),
        maintainer="ARIA Developers",
        # maintainer_email="bardiaux@pasteur.fr",
        author="Fabrice Allain, Benjamin Bardiaux, Michael Habeck,"
               " Therese Malliavin, Wolfgang Rieping, and Michael Nilges",
        author_email="bardiaux@pasteur.fr",
        description=DOCLINES[0],
        long_description=LONG_DOCLINES,
        url="http://aria.pasteur.fr",
        # download_url="",
        license=LICENSE,
        cmdclass={
            'clean': CleanCommand
        },
        classifiers=[_f for _f in CLASSIFIERS.split('\n') if _f],
        platforms=["Linux", "Solaris", "Mac OS-X", "Unix"],
        python_requires='>=2.7,<3.0.*',
        setup_requires=[
            # Setuptools 18.0 properly handles Cython extensions.
            'setuptools>=18.0',
            'cython==0.28.2',
            'numpy==1.16.0',
            'biopython==1.73',
            'scipy<1.3.0',
            'matplotlib<3.0',
            'pytest-runner',
            'networkx==2.2',
        ],
        tests_requires=[
            'pytest<=4.6',
            'pytest-cov<=2.7'
        ],
        # Project uses reStructuredText, so ensure that the docutils get
        # installed or upgraded on the target machine
        install_requires=[
            'future==0.17.1',
            'setuptools>=18.0',
            'cython==0.28.2',
            'numpy==1.16.0',
            'matplotlib<3.0',
            'docutils>=0.3',
            'six>=1.10',
            'Sphinx<1.8',
            'sphinx_rtd_theme',
            'm2r<=0.2.1',
            # 'sphinx_bootstrap_theme',
            # 'sphinxcontrib-napoleon',
            'configparser',
            'mako',
            'pandas<=0.24',
            'seaborn',
            'scikit-learn<=0.20',
            'colorlog',
            'mako',
            # TODO: Solve the issue with pbxplore
            #'pbxplore',
            'pathos',
            'tqdm',
            'hdbscan==0.8.18',
            'conkit==0.11.2',
            # 'nbsphinx',
            'recommonmark',
            'numpydoc',
            'better_apidoc',
            'requests[security]',
            # 'scikit-bio'          Doesn't works with python 2.7. Only 3.4 +
            'MDAnalysis<0.19'
        ],
        entry_points={
            'console_scripts': [
                'aria2 = aria.aria2:main',
                'ariaec = aria.conbox.commands:main',
                'ariacns = aria.ariacns:main',
            ],
        },
        # bin folder is normally for non python code that could be executed
        # directly on terminal (bash scripts, ...)
        scripts=['src/aria/aria2.py',
                 'src/aria/check.py'],
        # Force setuptools to unzip in order to access data files with pkgutil
        # since pkg_resources don't like relative paths in zipped egg dir :/
        # TODO: solution to enable zip_safe
        zip_safe=False,

        # hack for old versions to include data files & respect aria tree
        # structure during
        # installation. We should change aria tree structure to include
        # data files used by the package inside the main directory if we want
        # to respect pep recommandations. Non data files can be includes outside
        # the package with data_files key (non data files are not accessible by
        # the package after the installation)
        package_dir={
            '': 'src'
        },
        #     '': 'src/py',
        #     'aria': 'src/py/aria',
        #     'aria.cns': 'cns',
        #     'aria.cns.protocols': 'cns/protocols',
        #     'aria.cns.protocols.analysis': 'cns/protocols/analysis',
        #     'aria.cns.src': 'cns/src',
        #     'aria.cns.src.helplib': 'cns/src/helplib',
        #     'aria.cns.toppar': 'cns/toppar',
        #     'aria.src.py.data': 'src/py/data',
        #     'aria.src.xml': 'src/xml',
        #     'aria.src.csh': 'src/csh'},

        packages=find_packages("src", exclude=("docs*",)),
        # Line below works for Python 3 only
        # packages=find_namespace_packages(where='src'),

        # Install any data files found in the package
        include_package_data=True,

        exclude_package_data={'': ['.gitignore'],
                              'examples': ['*'],
                              'examples-dev': ['*']},

        # Using MANIFEST file, it seems we do not need package_data which
        # doesn't work for all kind of building process ...
        # package_data={
        #     # If any package contains these files, include them
        #     '': [
        #         '*.txt', '*.rst', '*.md', '*.dtd', '*.xml', '*.cns', '*.gif',
        #         '*.xpm', '*.csh', '*.inp', '*.pdb', '*.dat', '*.tbl',
        #         '*.datbis', '*.f', '*.inc', 'cns*', '*.top', '*.param',
        # '*.pro',
        #         '*.link', '*.psf', '*.sol', '*.pro', '*.pep'],
        #     # And include files found in the 'conbox' package, too:
        #     'aria.conbox': ['conf/*',
        #                     'templates/*',
        #                     'data/pdbdists/*'
        #                     'data/cullpdb/160427/*',
        #                     'data/cullpdb/170315/*'
        #                     'data/*',
        #                     ],
        # },

        # If we want to include non data files (documentation, examples, ..)
        # in the sys.prefix directory. The package can't access these files !
        # data_files= []

    )

    setup(**metadata)


for package in ('pip>=9.0', 'setuptools>=18.0', 'numpy>=1.11.0', 'matplotlib<3.0'):
    if not is_installed(package):
        print(textwrap.dedent("""
Installation needs {package}. You can install it via:

$ pip install --upgrade {package}
""".format(package=package)), file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':

    setup_package()
