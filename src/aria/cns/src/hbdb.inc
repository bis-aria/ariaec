c     Backbone H-bonding database term (HBDB) data block
c     Alexander Grishaev, NIH, 2003-2004
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer           ndirpotmax,nlinpotmax,naamax,nhbmax
      integer           ndxmax,ndymax,ndzmax,ndrmax,ndtmax,natmax

      parameter        (ndirpotmax = 11)
      parameter        (nlinpotmax =  4)
	  parameter        (natmax  = 12000)
      parameter        (naamax  =  1000)
      parameter        (nhbmax  =   700)
      parameter        (ndxmax  =  62)
      parameter        (ndymax  =  62)
      parameter        (ndzmax  =  33)
      parameter        (ndrmax  =  30)
      parameter        (ndtmax  =  30)
      
      integer           ndirpot,nlinpot
      integer           nresid(natmax)
      integer           ndr,ndt,ndx,ndy,ndz
      integer           hbfixijstor(nhbmax,4),hbfreeijstor(nhbmax,4)
      integer           nhbfix,nhbfree
      integer           hbfixres(nhbmax,2),hbfreeres(nhbmax,2)
      integer           hbnat(naamax,2)
      integer           hbmap(naamax,naamax)
      integer           nminhb(10),nmaxhb(10),nseghb
      integer           hbupdfrq,hbprnfrq
      integer           ihb,ihbfree,ihbfix
c     BARDIAUX
      integer	        skipinter
ccccccccccccccccccccccccccccccccc
      integer           minfindd,minfindl
      integer           lenfiledir(ndirpotmax),lenfilelin(nlinpotmax)
      integer           update
      integer           idfixpot(nhbmax,2),idfreepot(nhbmax,2)
      integer           frameid(natmax,3),donid(natmax,2)
      integer           nhbact
      integer           idres(naamax),totres
      
      double precision  edir(ndirpotmax,ndxmax,ndymax,ndzmax)
      double precision  fdir(ndirpotmax,ndxmax,ndymax,ndzmax,3)
      double precision  elin(nlinpotmax,ndrmax,ndtmax)
      double precision  flin(nlinpotmax,ndrmax,ndtmax)
      double precision  kdir,klin
      double precision  kdirmin,klinmin
      double precision  renf,kenf
      double precision  dxyz,dr,dt
      double precision  offr,offt,offx,offy,offz
      double precision  xmin,ymin,zmin,xmax,ymax,zmax
      double precision  rcut,tcut3,tcut45,tttcut
      double precision  hbfixstor(nhbmax,6),hbfixprop(naamax,naamax,4)
      double precision  hbfreestor(nhbmax,6),hbfreeprop(naamax,naamax,4)
      double precision  dirmin(ndirpotmax,3),linmin(nlinpotmax,ndrmax)
      double precision  ntotdir,ntotlin,etotdir,etotlin
      double precision  ct,st,cp,sp,cp1,sp1,ct1,st1
      double precision  etothb
      
      character         rname(naamax)*3
      character         dirpotype(ndirpotmax)*12
      character         linpotype(nlinpotmax)*5
      character         filedir(ndirpotmax)*512,filelin(nlinpotmax)*512
      character         hbfixat(nhbmax,2)*4,hbfreeat(nhbmax,2)*4
      character         hbfixseg(nhbmax,2)*4,hbfreeseg(nhbmax,2)*4
      character         segrangehb(10)*4
      character         segres(naamax)*4
     
      common /intvar/   
     &                  ndirpot,nlinpot,
     &                  nresid,
     &                  ndr,ndt,ndx,ndy,ndz,
     &                  hbfixijstor,hbfreeijstor,
     &                  nhbfix,nhbfree,
     &                  hbfixres,hbfreeres,
     &                  hbmap,
     &                  nminhb,nmaxhb,nseghb,
     &                  hbupdfrq,hbprnfrq,
     &                  ihb,ihbfree,ihbfix,
     &                  minfindd,minfindl,
     &                  update,
     &                  frameid,donid,
     &                  nhbact,
     &                  hbnat,
     &                  idfixpot,idfreepot,
     &                  idres,totres,
c    BARDIAUX
     &                  skipinter
      
      common /fpvar / 
     &                  edir,fdir,
     &                  elin,flin,
     &                  kdir,klin,
     &                  kdirmin,klinmin,
     &                  renf,kenf,
     &                  dxyz,dr,dt,
     &                  offr,offt,
     &                  offx,offy,offz,
     &                  xmin,ymin,zmin,
     &                  xmax,ymax,zmax,
     &                  rcut,tcut3,tcut45,tttcut,
     &                  hbfixstor,hbfixprop,
     &                  hbfreestor,hbfreeprop,
     &                  dirmin,linmin,
     &                  ntotdir,ntotlin,etotdir,etotlin,
     &                  ct,st,cp,sp,cp1,sp1,ct1,st1,etothb
      
      common /charvar / rname,
     &                  dirpotype,linpotype,
     &                  filedir,filelin,
     &                  hbfixat,hbfreeat,hbfixseg,segrangehb,segres

