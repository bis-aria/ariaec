cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone H-bonding database term
c     Alexander Grishaev, NIH, 2003-2004
c     initialization subroutine, called by xplorfunc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine hbdbinit

      implicit none
      include 'cns.inc'
      include 'comand.inc'
      include 'hbdb.inc'
      include 'mtf.inc'
      include 'consta.inc'
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
c     step sizes in r,theta
      dr = 0.05d0
      dt = 3.0d0
c     offset values in r,theta
      offr =    1.5d0
      offt =   90.0d0
c     array dimensions in r,theta
      ndr = 30
      ndt = 30
c     step size in x,y,z
      dxyz = 0.1d0
c     offset values in x,y,z
      offx = -3.1d0
      offy = -3.1d0
      offz = -0.5d0
c     array dimensions in x,y,z
      ndx = 62
      ndy = 62
      ndz = 33
c     number of the potential classes
      ndirpot = 11
      nlinpot = 4
c     initial settings & defaults 
      ihb     = 0
      nhbfix  = 0
      nhbfree = 0
      update  = 0
      renf    =  2.3d0 ! ~98% of all non-bifurc are <2.3A, ~99.5% are <2.5 A
      kenf    = 50.0d0
      rcut    =  2.6d0 ! detection cutoff (HN-O), free mode
      tcut3   = 90.0d0 ! detection cutoff (C-O-HN,310), free mode
      tcut45  = 90.0d0 ! detection cutoff (C-O-HN,other), free mode
      tttcut  = 90.0d0 ! detection cutoff (O-HN-N), free mode
      minfindd = 0 ! no directional force in the flat areas of the PMF
      minfindl = 0 ! no linearity   force in the flat areas of the PMF
      kdirmin = 0.0d0
      klinmin = 0.0d0
      kdir = 0.20d0 ! default force constant for the directional term
      klin = 0.08d0 ! default force constant for the linearity   term
      hbupdfrq = 1000
      hbprnfrq = 1000
      nseghb = 0
c     BARDIAUX
      skipinter = 0
ccccccccccccccccccccc
      nminhb(1) = 1
      nmaxhb(1) = 1000
      segrangehb(1) = '    '
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone H-bonding database term
c     Alexander Grishaev, NIH, 2003-2004
c     parses the HBDB statement in the command script
c     called by xplorfunc if 'HBDB' keyword is present in command script
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine readhbdb
      
      implicit none 
      include 'cns.inc'
      include 'comand.inc'
      include 'timer.inc'
      include 'hbdb.inc'

      integer ipot,i,isegm,length
      character filetype*4,fileloc*512,wd3*3,wd1*1
      character wd41*4,wd42*4,wd43*4,flag3*3
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
      update = update+1
      isegm = 0
      
c     read in the input parameters for the HB potential...
      call pusend('HBDB>')
      done =.false.
      do while(.not.done)
       call nextwd('HBDB>')
       call miscom('HBDB>', used)
       if (.not.used) then
        if (wd(1:4).eq.'HELP')then
         WRITE(DUNIT,'(20X,A)')
     &   'HBDB {<hb_database-statement>} END ',
     &   '<HBDB-statement>:== ',
     &   'kdir, force constant for directional term ',
     &   'klin, force constant for linear term',
     &   'nseg, number of segments over which hbdb operates (max=10)',
     &   'segm, segid over which hbdb operates ',
     &   'nmin, min resid for which hbdb operates within segm',
     &   'nmax, max resid for which hbdb operates within segm',
     &   'ohcut, distance cutoff for detection of hbonds ',
     &   'coh1, cutoff for C-O-H angle in 3-10 helix ',
     &   'coh2, cutoff for C-O-H angle for everything else',
     &   'ohnc, cutoff for O-H-N angle',
     &   'updfrq, update frequency',
     &   'prnfrq, print frequency',
     &   'freemode=1, fixedmode=0: uses free search ',
     &   'freemode=0, fixedmode=1: uses hblist file',
     &   '@hblist_file entry format:',
     &   'assign (don and resid 2 and segid A and name O  )',
     &   '       (acc and resid 8 and segid A and name HN )',
     &   ' or ',
     &   'assign (don and resid 2 and name O  )',
     &   '       (acc and resid 8 and name HN )',
     &   'ATTENTION:',
     &   'HBDB will only work properly in the absence of RESID skips', 
     &   'within a given SEGID (chain)',
     &   'and in the absence of alphanumeric RESIDs (e.g. 39A)'
         goto 1000
        
c       set directional and linearity force constants
        else if(wd(1:4).eq.'KDIR')then
         call nextf('directional HB force constant =', kdir)
        else if(wd(1:4).eq.'KLIN')then
         call nextf('linearity   HB force constant =', klin)
        
c       set min and max # of residues within the H-bonded regions
        else if(wd(1:4).eq.'NSEG') then
         call nexti('number of HB residue ranges =', nseghb)

        else if(wd(1:4).eq.'NMIN') then
         isegm = isegm+1
         call nexti('free search HB residue range: min =',nminhb(isegm))
        else if(wd(1:4).eq.'NMAX') then
         call nexti('free search HB residue range: max =',nmaxhb(isegm))
        else if(wd(1:4).eq.'SEGM') then
         call nextan('select  flag  ',segrangehb(isegm) ,0,4) 
        
c       set detection cutoffs for the free search
        else if(wd(1:4).eq.'OHCU')then
         call nextf(' HB detection cutoff in OH    =',   rcut)
        else if(wd(1:4).eq.'COH1')then
         call nextf(' COH detection cutoff (i/i+3) =',  tcut3)
        else if(wd(1:4).eq.'COH2')then
         call nextf(' COH detection cutoff (other) =', tcut45)
        else if(wd(1:4).eq.'OHNC')then
         call nextf(' HB detection cutoff in OHN   =', tttcut)
        
c       update and print frequencies
        else if(wd(1:4).eq.'UPDF') then
         call nexti('HB list update frequency =', hbupdfrq)
        else if(wd(1:4).eq.'PRNF') then
         call nexti('HB printing frequency =', hbprnfrq)
        
c       HB search mode flags
        else if(wd(1:4).eq.'FREE') then
         call nexti('HB free search flag =', ihbfree)
        else if(wd(1:4).eq.'FIXE') then
         call nexti('HB fixed  list flag =',  ihbfix)

c       BARDIAUX skip inter-chain for multicopy
        else if(wd(1:4).eq.'INTR') then
         call nexti('Skip inter-segid HB =',  skipinter)
         
c       directional minimum search flag
        else if(wd(1:4).eq.'MFDI') then
         call nexti('directional minimum search flag =', minfindd)
        
c       linearity minimum search flag
        else if(wd(1:4).eq.'MFLI') then
         call nexti('linearity minimum search flag =', minfindl)
      
c       directional and linearity minimum enforcement force constants
        else if(wd(1:4).eq.'KMFD')then
         call nextf('directional min HB force constant =', kdirmin)
        else if(wd(1:4).eq.'KMFL')then
         call nextf('linearity   min HB force constant =', klinmin)
      
c       directional and linearity minimum enforcement force constants
        else if(wd(1:4).eq.'RENF')then
         call nextf('directional min HB force constant =', renf)
        else if(wd(1:4).eq.'KENF')then
         call nextf('linearity   min HB force constant =', kenf)
        
c       read in the assignment...
        else if(wd(1:4).eq.'ASSI')then
         nhbfix = nhbfix+1
         hbfixres(nhbfix,1) = 0
         hbfixres(nhbfix,2) = 0
         hbfixseg(nhbfix,1) = '   '
         hbfixseg(nhbfix,2) = '   '
         hbfixat (nhbfix,1) = '   '
         hbfixat (nhbfix,2) = '   '

         call nextan('open bracket  ',wd1  ,0,1) ! (
         call nextan('don/acc flag  ',flag3,0,3) ! DON ACC
         call nextan('and     flag  ',wd3  ,0,3) ! AND
         call nextan('select  flag  ',wd41 ,0,4) ! RESI SEGI NAME
         if(flag3.eq.'ACC')then
          if(wd41.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
          if(wd41.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
          if(wd41.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
          call nextan('and     flag  ',wd3 ,0,3) ! AND )
          if(wd3.eq.'AND')then
           call nextan('select  flag  ',wd42,0,4) ! RESI SEGI NAME
           if(wd42.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
           if(wd42.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
           if(wd42.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
           call nextan('and     flag  ',wd3 ,0,3) ! AND )
           if(wd3.eq.'AND')then
            call nextan('select  flag  ',wd43,0,4) ! RESI SEGI NAME
            if(wd43.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
            if(wd43.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
            if(wd43.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
           else
            if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
             goto 100
            else
             write(6,*)'unknown HBDB ASSI statement entry ',wd3
             pause
            endif
           endif
          else
           if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
            goto 100
           else
            write(6,*)'unknown HBDB ASSI statement entry ',wd3
            pause
           endif
          endif
         endif

         if(flag3.eq.'DON')then
          if(wd41.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
          if(wd41.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
          if(wd41.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
          call nextan('and     flag  ',wd3 ,0,3) ! AND )
          if(wd3.eq.'AND')then
           call nextan('select  flag  ',wd42,0,4) ! RESI SEGI NAME
           if(wd42.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
           if(wd42.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
           if(wd42.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
           call nextan('and     flag  ',wd3 ,0,3) ! AND )
           if(wd3.eq.'AND')then
            call nextan('select  flag  ',wd43,0,4) ! RESI SEGI NAME
            if(wd43.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
            if(wd43.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
            if(wd43.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
           else
            if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
             goto 100
            else
             write(6,*)'unknown HBDB ASSI statement entry ',wd3
             pause
            endif
           endif
          else
           if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
            goto 100
           else
            write(6,*)'unknown HBDB ASSI statement entry ',wd3
            pause
           endif
          endif
         endif

         call nextan('close bracket ',wd3  ,0,3) ! )

100      continue

         if((wd3.ne.') (').and.(wd3.ne.')( '))then
          call nextan('     ',wd1  ,0,1) ! (
         endif
         call nextan('don/acc flag  ',flag3,0,3) ! DON ACC
         call nextan('and     flag  ',wd3  ,0,3) ! AND
         call nextan('select  flag  ',wd41 ,0,4) ! RESI SEGI NAME
         if(flag3.eq.'ACC')then
          if(wd41.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
          if(wd41.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
          if(wd41.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
          call nextan('and     flag  ',wd3 ,0,3) ! AND )
          if(wd3.eq.'AND')then
           call nextan('select  flag  ',wd42,0,4) ! RESI SEGI NAME
           if(wd42.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
           if(wd42.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
           if(wd42.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
           call nextan('and     flag  ',wd3 ,0,3) ! AND )
           if(wd3.eq.'AND')then
            call nextan('select  flag  ',wd43,0,4) ! RESI SEGI NAME
            if(wd43.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,1))
            if(wd43.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,1))
            if(wd43.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,1))
           else
            if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
             goto 200
            else
             write(6,*)'unknown HBDB ASSI statement entry ',wd3
             pause
            endif
           endif
          else
           if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
            goto 200
           else
            write(6,*)'unknown HBDB ASSI statement entry ',wd3
            pause
           endif
          endif
         endif

         if(flag3.eq.'DON')then
          if(wd41.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
          if(wd41.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
          if(wd41.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
          call nextan('and     flag  ',wd3 ,0,3) ! AND )
          if(wd3.eq.'AND')then
           call nextan('select  flag  ',wd42,0,4) ! RESI SEGI NAME
           if(wd42.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
           if(wd42.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
           if(wd42.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
           call nextan('and     flag  ',wd3 ,0,3) ! AND )
           if(wd3.eq.'AND')then
            call nextan('select  flag  ',wd43,0,4) ! RESI SEGI NAME
            if(wd43.eq.'RESI')call nexti ('resi ',hbfixres(nhbfix,2))
            if(wd43.eq.'SEGI')call nexta4('segi ',hbfixseg(nhbfix,2))
            if(wd43.eq.'NAME')call nexta4('name ',hbfixat (nhbfix,2))
           else
            if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
             goto 200
            else
             write(6,*)'unknown HBDB ASSI statement entry ',wd3
             pause
            endif
           endif
          else
           if((wd3.eq.')  ').or.(wd3.eq.') (').or.(wd3.eq.')( '))then
            goto 200
           else
            write(6,*)'unknown HBDB ASSI statement entry ',wd3
            pause
           endif
          endif
         endif

         call nextan('close bracket ',wd3  ,0,3) ! )
  
200      continue
      
         if(nhbfix.gt.nhbmax)then
          CALL WRNDIE(-5, 'READHBDB',
     &         'maximum allocation size for HB entries exceeded.')
         endif
         
c       print mode...
        else if(wd(1:4).eq.'PRIN') then 
         call printhbdb(punit)
        
c       locations of the PMF files...
        else if(wd(1:4).eq.'FILE') then
         call nextan('potential type =',filetype,0,3) ! modified nexta
         call nexti ('potential ID =', ipot)
         do i=1,512
          fileloc(i:i)=' '
         enddo
         call nextan('file location =',fileloc,1,length)
         if(filetype.eq.'DIR')then
          lenfiledir(ipot) = length
          do i=1,length
           filedir(ipot)(i:i) = fileloc(i:i)
          enddo
         endif
         if(filetype.eq.'LIN')then
          lenfilelin(ipot) = length
          do i=1,length
           filelin(ipot)(i:i) = fileloc(i:i)
          enddo
         endif
         
c       check for the END statement...
        else
         call chkend('HBDB>', done)
        endif
       endif
      enddo  
      done = .false.

      if(nseghb.eq.0) nseghb = 1
      
c     read in the potential files and calculate the forces...
      if(update.eq.1) call readhbdb2

1000  continue


c     write(6,*)nseghb
c     do isegm=1,nseghb
c      write(6,*)isegm
c      write(6,*)nminhb(isegm)
c      write(6,*)nmaxhb(isegm)
c      write(6,*)segrangehb(isegm)
c      pause
c     enddo

      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone/backbone H-bonding database term
c     Alexander Grishaev, NIH, 5/03 - 3/04
c     CO/HN tables initialization
c     called during the first pass of ehbdb
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine hbdbinit2
      
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'mtf.inc'
      include 'consta.inc'

      integer i,j,k,l,count,resi,nstop,nstart,isign,istore,ifound,jfound
      integer resni
      character resida*4,typei*4,typej*4,astore*4
      character resname(natom)*4
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
c     convert character RESIds into integer residue numbers...
c     Left alignment is assumed. 
c     Can handle negative resids, but not alphanumerics (39A)
c     Any skip in RESID numbering is assumed to be a real skip in the sequence

      do i=1,natom
       resida = resid(i) ! character*4 RESID
       astore = res  (i) ! character*4 residue name
       nstart = 1
       nstop  = 4
       isign  = 1
       if(resida(1:1).eq.'-')then
        nstart = 2
        isign = -1
       endif
       do j=1,4
        if(resida(j:j).eq.' ')then
         nstop = j-1
         goto 10
        endif
       enddo
10     continue
       istore = 0
       do j=nstop,nstart,-1
        if((ichar(resida(j:j))-48.lt.0).or.
     &     (ichar(resida(j:j))-48.gt.9))then
         write(6,*)'HBDBINIT2 WARNING: alphanumeric RESID! ',resida
         pause
        endif
        istore = istore+(ichar(resida(j:j))-48)*(10**(nstop-j))
       enddo
       istore = isign*istore
       nresid (i) = istore
       resname(i) = astore
      enddo
      
      totres = 0
      do i=1,natom-1
       if((resid(i).ne.resid(i+1)).or.
     &    (segid(i).ne.segid(i+1)).or.
     &    (i.eq.natom-1))then 
        totres = totres+1
        idres(totres) = nresid(i)
        segres(totres) = segid(i)
        rname(totres)(1:3) = resname(i)(1:3)
        hbnat(totres,1) = 0
        hbnat(totres,2) = 0
       endif
      enddo

      do i=1,natom
       typei = type(i)
       resni = 0
       do j=1,totres
        if(idres(j).eq.nresid(i).and.(segres(j).eq.segid(i)))then
         resni = j
        endif
       enddo
       if(resni.eq.0)then
        write(6,*)'atom not found',i,nresid(i),' ',typei
        pause
       endif
       if((typei.eq.'O   ').or.(typei.eq.'OT1 '))then
        hbnat(resni,1) = i
        frameid(i,1) = i
        do j=1,natom
         typej = type(j)
         if((segid(j).eq.segid(i)).and.(nresid(j).eq.nresid(i)))then
          if(typej.eq.'C   ') frameid(i,2) = j
          if(typej.eq.'CA  ') frameid(i,3) = j
         endif
        enddo
         if((frameid(i,2).eq.0).or.(frameid(i,3).eq.0))then
         frameid(i,1) = 0
         frameid(i,2) = 0
         frameid(i,3) = 0
         hbnat(resni,1) = 0
        endif
       endif

       if((typei.eq.'HN  ').or.(typei.eq.'HT1 ').or.
     &    (typei.eq.'H   '))then
        hbnat(resni,2) = i
        donid(i,1) = i
        do j=1,natom
         typej = type(j)
         if((segid(j).eq.segid(i)).and.(nresid(j).eq.nresid(i)))then
          if(typej.eq.'N   ') donid(i,2) = j
         endif
        enddo
        if(donid(i,2).eq.0)then
         donid(i,1) = 0
         donid(i,2) = 0
         hbnat(resni,2) = 0
        endif
       endif

      enddo

c     re-write the fixed resid pointer array in terms of (1,...,totres) numbering
      if(ihbfix.eq.1)then
       do i=1,nhbfix
        ifound = 0
        jfound = 0
        do j=1,totres
         if((hbfixres(i,1).eq.idres(j)).and.
     &      (hbfixseg(i,1).eq.segres(j))) ifound = j
         if((hbfixres(i,2).eq.idres(j)).and.
     &      (hbfixseg(i,2).eq.segres(j))) jfound = j
        enddo
        if(ifound.eq.0)then
         write(6,*)'HBDB entry missing from the structure: ',
     &   hbfixseg(i,1),' ',hbfixres(i,1)
         pause
        endif
        if(jfound.eq.0)then
         write(6,*)'HBDB entry missing from the structure: ',
     &   hbfixseg(i,2),' ',hbfixres(i,2)
         pause
        endif
        hbfixres(i,1) = ifound
        hbfixres(i,2) = jfound
       enddo
      endif
            
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone/backbone H-bonding database term
c     Alexander Grishaev, NIH, 2003 - 2004
c     energy/forces calculation
c     called by the ENERGY subroutine if the 'HBDB' flag is on
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine ehbdb(totener,key)
      implicit none
      include 'cns.inc'
      include 'mtf.inc'
      include 'coord.inc'
      include 'deriv.inc'
      include 'hbdb.inc'
      include 'consta.inc'
      include 'cnst.inc'
      include 'ener.inc' 
      include 'comand.inc' 

      integer i,j,k,l,iflag,jflag
      integer resi,resj,nij
      integer nati,natj
      integer idx,idy,idz,idr,idt
      integer ipotra,ipot
      integer ifix,ifree
      integer ifr1,ifr2,ifr3
      integer jdon1,jdon2

      double precision prx,pry,prz
      double precision xoh,yoh,zoh
      double precision r,theta,phi,ohn,dno
      double precision edirij,elinij
      double precision xyzfr(3,4),normfr(4)
      double precision totener

      character key*7
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      ihb  = ihb+1
      if(ihb.eq.1) call hbdbinit2

      nhbact  = 0
      totener = 0.0d0
      etotdir = 0.0d0
      etotlin = 0.0d0
      etothb  = 0.0d0

      if(kdir.eq.0.0d0) kenf = 0.0d0
      
c     establish and process the active HB lists...
cccccccccccccccccccccccccccccccccccccccccccccccccc
      
c     periodic updates based on the fixed and free HB lists...
      if(mod(ihb-1,hbupdfrq).eq.0.0)then

       do i=1,totres
        do j=1,totres
         hbmap(i,j) = 0
         do k=1,4
          hbfreeprop(i,j,k) = 0.0d0
          hbfixprop (i,j,k) = 0.0d0
         enddo
        enddo
       enddo

c      fixed list "updates"...
       if(ihbfix.eq.1)then
        do ifix=1,nhbfix
         resi = hbfixres(ifix,1)
         resj = hbfixres(ifix,2)
         nati = hbnat(resi,1) ! pointer to the O  of res i (1...totres)
         natj = hbnat(resj,2) ! pointer to the HN of res j (1...totres)
         if(resi*resj*nati*natj.ne.0)then
          ifr1 = frameid(nati,1) ! O_i
          ifr2 = frameid(nati,2) ! C_i
          ifr3 = frameid(nati,3) ! Ca_i
          call frame(ifr1,ifr2,ifr3,xyzfr,normfr)
          jdon1 = donid(natj,1)  ! HN_j
          jdon2 = donid(natj,2)  !  N_j
c         get r,theta,phi, xyz projections and the ohn angle...
          call rtp(ifr1,jdon1,jdon2,xyzfr,r,theta,phi,ohn, 
     &             prx,pry,prz,xoh,yoh,zoh)
c         store fixed HB list data...
          hbfixprop(resi,resj,1) = r
          hbfixprop(resi,resj,2) = theta
          hbfixprop(resi,resj,3) = phi
          hbfixprop(resi,resj,4) = ohn
          hbmap(resi,resj) = 1
         endif
        enddo
       endif
       
c      free list updates...
       if(ihbfree.eq.1)then
        nhbfree = 0
        do resi=1,totres
         iflag = 0
         do k=1,nseghb
          if((segres(resi).eq.segrangehb(k)).and.
     &       (idres (resi).ge.nminhb(k)).and.
     &       (idres (resi).le.nmaxhb(k)))then

           iflag = 1
          endif
         enddo
         if(iflag.eq.0) goto 100 ! the acceptor has to be within the allowed range
         nati = hbnat(resi,1)
         if(nati.eq.0) goto 100  ! acceptor's O has to exist
c        build the acceptor's molecular frame...
         ifr1 = frameid(nati,1) ! O_i
         ifr2 = frameid(nati,2) ! C_i
         ifr3 = frameid(nati,3) ! Ca_i
         call frame(ifr1,ifr2,ifr3,xyzfr,normfr)

         do resj=1,totres
          jflag = 0
          do k=1,nseghb
           if((segres(resj).eq.segrangehb(k)).and.
     &        (idres (resj).ge.nminhb(k)).and.
     &        (idres (resj).le.nmaxhb(k)))then
            jflag = 1
           endif
          enddo
          if(jflag.eq.0) goto 50 ! the donor has to be within the allowed range
          natj = hbnat(resj,2)
          if(natj.eq.0) goto 50 ! donor's HN has to exist
          if(segres(resj).eq.segres(resi))then ! sequence separtion depends on the segids
           nij = idres(resj)-idres(resi)
          else
            nij = 100
c           BARDIAUX
            if (skipinter.eq.1) goto 50
          endif
          if(hbmap(resi,resj).eq.1) goto 50 !do not duplicate fixed list
	    if(abs(nij).le.1) goto 50
c         no i/i+2 in the free search mode
c         they tend to be misidentified (especially at high T) within the extended strands
c         if you want them, be absolutely sure and use fixed mode
	    if(nij.eq.2) goto 50
c         acceptor atoms...
          jdon1 = donid(natj,1) ! HN_j
          jdon2 = donid(natj,2) !  N_j
c         get r,theta,phi, xyz projections and the ohn angle...
          call rtp(ifr1,jdon1,jdon2,xyzfr,r,theta,phi,ohn, 
     &             prx,pry,prz,xoh,yoh,zoh)
c         filter by the O-H distance (or O-N for the N-term donor)...
          if((resj.ne.1).and.(type(jdon1).ne.'HT1 '))then
           if(r.gt.rcut) goto 50
          else
           dno = dsqrt((x(jdon2)-x(ifr1))**2+
     &                 (y(jdon2)-y(ifr1))**2+
     &                 (z(jdon2)-z(ifr1))**2)
           if(dno.gt.3.5d0) goto 50 ! r(N-O) detection is more stable for the N-term
          endif
c         filter by the C-O-H angle...
	    if((nij.ne.3).and.(theta.lt.tcut45)) goto 50 ! i/i+3 C-O-H...
	    if((nij.eq.3).and.(theta.lt.tcut3 )) goto 50 ! remaining C-O-H...
c         filter by the O-H-N angle...
          if((resj.ne.1).and.(type(jdon1).ne.'HT1 ').and.
     &    (ohn.lt.tttcut)) goto 50 ! no OHN-based detection for the N-term
c         store free HB list data...
          nhbfree = nhbfree+1
	    hbfreeres(nhbfree,1) = resi
	    hbfreeres(nhbfree,2) = resj
          hbfreeprop(resi,resj,1) = r
          hbfreeprop(resi,resj,2) = theta
          hbfreeprop(resi,resj,3) = phi
          hbfreeprop(resi,resj,4) = ohn
50        continue
         enddo
100      continue
        enddo
        call hbdbpatt ! filtering by pattern recognition if free search is active
       endif
       call hbdbsetpottype ! set the potential type based on second struct
      endif
      
c     refinement: energies and forces...
cccccccccccccccccccccccccccccccccccccccc

c     enforce the fixed list...
      if(ihbfix.eq.1)then
       do ifix=1,nhbfix
        resi = hbfixres(ifix,1)
        resj = hbfixres(ifix,2)
        nati = hbnat(resi,1)
        natj = hbnat(resj,2)
        edirij = 0.0d0
        elinij = 0.0d0
c       build the acceptor's molecular frame...
        ifr1 = frameid(nati,1) ! O_i
        ifr2 = frameid(nati,2) ! C_i
        ifr3 = frameid(nati,3) ! Ca_i
        call frame(ifr1, ifr2, ifr3, xyzfr, normfr)
c       acceptor atoms...
        jdon1 = donid(natj,1) ! HN_j
        jdon2 = donid(natj,2) !  N_j
c       get r,theta,phi, xyz projections and the ohn angle...
         call rtp(ifr1, jdon1, jdon2, xyzfr, r, theta, phi, ohn, 
     &               prx, pry, prz, xoh, yoh, zoh)
c       calculate directional energy...
        ipot = idfixpot(ifix,1)
        idx = 1+int((prx-xmin)/dxyz)
        idy = 1+int((pry-ymin)/dxyz)
        idz = 1+int((prz-zmin)/dxyz)
        if((prz.gt.zmin).and.(prz.le.zmax))then
         if((prx.gt.xmin).and.(prx.le.xmax))then
          if((pry.gt.ymin).and.(pry.le.ymax))then
           edirij = edir(ipot,idx,idy,idz)
          endif
         endif
        endif
        nhbact = nhbact+1
        etotdir = etotdir+edirij
c       calculate directional forces...
        if(key.ne.'ANALYZE')then
         call dirforce(ifr1,ifr2,ifr3,jdon1,prx,pry,prz,xyzfr,normfr,r, 
     &                 ipot,edirij,xoh,yoh,zoh)
        endif
c       calculate linearity energy...
        ipotra = idfixpot(ifix,2)        
        if((r.le.1.5d0).or.(r.ge.2.5d0)) goto 200
        if(ohn.le.90.d0) goto 200
        if(ipotra.eq.4)then
         if((r.le.1.65d0).or.(r.ge.2.55d0)) goto 200
        endif
        idr  = 1+int((r  -offr)/dr)
        idt  = 1+int((ohn-offt)/dt)
        elinij = elin(ipotra,idr,idt)
c       calculate directional forces...
        etotlin = etotlin+elinij
        if(key.ne.'ANALYZE')then
         call linforce(ifr1,jdon1,jdon2,r,ohn,xyzfr,normfr,xoh,yoh,zoh, 
     &                 ipotra,elinij)
        endif
200     continue
c       store the stats...
        hbfixstor  (ifix,1) = r
        hbfixstor  (ifix,2) = theta
        hbfixstor  (ifix,3) = phi
        hbfixstor  (ifix,4) = ohn
        hbfixstor  (ifix,5) = edirij
        hbfixstor  (ifix,6) = elinij
        hbfixijstor(ifix,1) = resi
        hbfixijstor(ifix,2) = resj
        hbfixijstor(ifix,3) = ipot
        hbfixijstor(ifix,4) = ipotra
       enddo
      endif 
      
c     enforce the free list...
      if(ihbfree.eq.1)then
       do ifree=1,nhbfree
        resi = hbfreeres(ifree,1)
        resj = hbfreeres(ifree,2)
        nati = hbnat(resi,1)
        natj = hbnat(resj,2)
        edirij = 0.0d0
        elinij = 0.0d0
c       build the acceptor's molecular frame...
        ifr1 = frameid(nati,1) ! O_i
        ifr2 = frameid(nati,2) ! C_i
        ifr3 = frameid(nati,3) ! Ca_i
        call frame(ifr1,ifr2,ifr3,xyzfr,normfr)
c       acceptor atoms...
        jdon1 = donid(natj,1) ! HN_j
        jdon2 = donid(natj,2) !  N_j
c       get r,theta,phi, xyz projections and the ohn angle...
        call rtp(ifr1,jdon1,jdon2,xyzfr,r,theta,phi,ohn,prx,pry,prz, 
     &           xoh,yoh,zoh)
c       calculate directional energy...
        ipot = idfreepot(ifree,1)
        idx = 1+int((prx-xmin)/dxyz)
        idy = 1+int((pry-ymin)/dxyz)
        idz = 1+int((prz-zmin)/dxyz)
        if((prz.gt.zmin).and.(prz.le.zmax))then
         if((prx.gt.xmin).and.(prx.le.xmax))then
          if((pry.gt.ymin).and.(pry.le.ymax))then
           edirij = edir(ipot,idx,idy,idz)
          endif
         endif
        endif
        nhbact = nhbact+1
        etotdir = etotdir+edirij
c       calculate directional forces...
        if(key.ne.'ANALYZE')then
         call dirforce(ifr1,ifr2,ifr3,jdon1,prx,pry,prz,xyzfr,normfr,r, 
     &                 ipot,edirij,xoh,yoh,zoh)
        endif
c       calculate linearity energy...
        ipotra = idfreepot(ifree,2)        
        if((r.le.1.5d0).or.(r.ge.2.5d0)) goto 300
        if(ohn.le.90.d0) goto 300
        if(ipotra.eq.4)then
         if((r.le.1.65d0).or.(r.ge.2.55d0)) goto 300
        endif
        idr  = 1+int((r  -offr)/dr)
        idt  = 1+int((ohn-offt)/dt)
        elinij = elin(ipotra,idr,idt)
        etotlin = etotlin+elinij
c       calculate directional forces...
        if(key.ne.'ANALYZE')then
         call linforce(ifr1,jdon1,jdon2,r,ohn,xyzfr,normfr,xoh,yoh,zoh, 
     &                 ipotra,elinij)
        endif
300     continue
c       store the stats...
        hbfreestor  (ifree,1) = r
        hbfreestor  (ifree,2) = theta
        hbfreestor  (ifree,3) = phi
        hbfreestor  (ifree,4) = ohn
        hbfreestor  (ifree,5) = edirij
        hbfreestor  (ifree,6) = elinij
        hbfreeijstor(ifree,1) = resi
        hbfreeijstor(ifree,2) = resj
        hbfreeijstor(ifree,3) = ipot
        hbfreeijstor(ifree,4) = ipotra
       enddo
      endif 
      
c     output energies...
      etothb = etotdir+etotlin
      if(nhbact.ne.0) etotdir = etotdir/dble(nhbact)
      if(nhbact.ne.0) etotlin = etotlin/dble(nhbact)
      totener = etothb
      
      if(mod(ihb-1,hbprnfrq).eq.0.0)then
c      write to the console...
       call printhbdb(DUNIT)
      endif
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone/backbone H-bonding database term
c     Alexander Grishaev, NIH, 8/03 - 3/04
c     H-bonding pattern filtering (free list)
c     called from EHBDB in the free search mode
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine hbdbpatt
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'comand.inc'

      integer i,j,k
      integer resi,resj,n1,n2,n3,istore
      integer hbfiltmap(naamax,naamax)
      double precision hbprop(naamax,naamax)
      character r1*3,r2*3,r3*3,r4*3
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     initalize and fill hbfiltmap array...
      do i=1,totres
       do j=1,totres
        hbfiltmap(i,j) = 0
       enddo
      enddo
      
      do i=1,nhbfix
       resi = hbfixres(i,1)
       resj = hbfixres(i,2)
       hbfiltmap(resi,resj) = 1 ! fixed list identifier
       hbprop(resi,resj) = hbfixprop(resi,resj,1)
      enddo
      
      do i=1,nhbfree
       resi = hbfreeres(i,1)
       resj = hbfreeres(i,2)
       if(hbfiltmap(resi,resj).ne.0)then
        write(DUNIT,*)'fixed/free list entry duplication'
        write(DUNIT,*)idres(resi),idres(resj),hbfiltmap(resi,resj)
        call WRNDIE(-5,'HBDBPATT','fixed/free list entry duplication')
       endif
       hbfiltmap(resi,resj) = 2 ! free list identifier
       hbprop(resi,resj) = hbfreeprop(resi,resj,1)
      enddo
            
c     clean up Schellman C-cap (should be relatively solid)
cccccccccccccccccccccccccccccccccccc
301   continue
      do i=1,totres
       if(hbfiltmap(i,i+4).ne.0)then
        if(segres(i).eq.segres(i+6))then ! this tentative solution only works 
         if(hbfiltmap(i+1,i+6).ne.0)then ! if the are no real resid skips between i and i+6 
          if(hbfiltmap(i+2,i+5).ne.0)then
           if(hbfiltmap(i  ,i+3).eq.2) hbfiltmap(i  ,i+3) = 0
           if(hbfiltmap(i+1,i+3).eq.2) hbfiltmap(i+1,i+3) = 0
           if(hbfiltmap(i+1,i+4).eq.2) hbfiltmap(i+1,i+4) = 0
           if(hbfiltmap(i+1,i+5).eq.2) hbfiltmap(i+1,i+5) = 0
           if(hbfiltmap(i+2,i+4).eq.2) hbfiltmap(i+2,i+4) = 0
           if(hbfiltmap(i+2,i+6).eq.2) hbfiltmap(i+2,i+6) = 0
           if(hbfiltmap(i+3,i+5).eq.2) hbfiltmap(i+3,i+5) = 0
           if(hbfiltmap(i+3,i+6).eq.2) hbfiltmap(i+3,i+6) = 0
          else
           if(hbprop(i+2,i+5).lt.3.5d0)then 
            hbfiltmap(i+2,i+5) = 1 
            if(hbfiltmap(i  ,i+3).eq.2) hbfiltmap(i  ,i+3) = 0
            if(hbfiltmap(i+1,i+3).eq.2) hbfiltmap(i+1,i+3) = 0
            if(hbfiltmap(i+1,i+4).eq.2) hbfiltmap(i+1,i+4) = 0
            if(hbfiltmap(i+1,i+5).eq.2) hbfiltmap(i+1,i+5) = 0
            if(hbfiltmap(i+2,i+4).eq.2) hbfiltmap(i+2,i+4) = 0
            if(hbfiltmap(i+2,i+6).eq.2) hbfiltmap(i+2,i+6) = 0
            if(hbfiltmap(i+3,i+5).eq.2) hbfiltmap(i+3,i+5) = 0
            if(hbfiltmap(i+3,i+6).eq.2) hbfiltmap(i+3,i+6) = 0
           endif
          endif
         endif
        endif
       endif
      enddo   

c     remove i/i+3 if i/i+4 is present
c     not the best thing to do, but keep it until I learn how to deal
c     better with kinks in a-helices.
cccccccccccccccccccccccccccccccccccccc        
      do i=1,totres
       if((hbfiltmap(i,i+3).ne.0).and.(hbfiltmap(i,i+4).ne.0))then
        if(segres(i).eq.segres(i+4))then ! this tentative solution only works if the are no resid skips between i and i+4 
         if(hbprop(i,i+3).gt.hbprop(i,i+4))then
          if(hbprop(i,i+4).lt.2.4d0)then
           if(hbfiltmap(i,i+3).eq.2)then
            hbfiltmap(i,i+3) = 0
           endif
          endif
         else
          if(hbfiltmap(i+1,i+4).ne.0)then
           if(hbprop(i+1,i+4).lt.2.4d0)then
            if(hbfiltmap(i,i+4).eq.2)then
             hbfiltmap(i,i+4) = 0
            endif
           endif
          endif
         endif
        endif
       endif
      enddo
      
c     alphaL C-cap coming soon!!!   
      
c     clean up i/i+5,i+5/i antiparallel hairpin
ccccccccccccccccccccccccccccccccccccccccccccccc
      do i=1,totres-5
       if((hbfiltmap(i,i+5).ne.0).and.(hbfiltmap(i+5,i).ne.0))then
        if(segres(i).eq.segres(i+4))then ! this tentative solution only works if the are no resid skips between i and i+5 
400      continue
         n1 = hbfiltmap(i,i+4)
         n2 = hbfiltmap(i,i+3)
         n3 = hbfiltmap(i+1,i+4)
         r1 = rname(i+1)
         r2 = rname(i+2)
         r3 = rname(i+3)
         r4 = rname(i+4)
c        no i/i+3 in any such pattern
         if(n2.eq.1)then
          if(hbfiltmap(i,i+3).eq.2) hbfiltmap(i,i+3) = 0
          goto 400
         endif
        
c        no PRO in i+3,i+4 in any pattern except 000 
         if(n1+n2+n3.ne.0)then
          if((r3.eq.'PRO').or.(r4.eq.'PRO'))then
           if(hbfiltmap(i  ,i+3).eq.2) hbfiltmap(i  ,i+3) = 0
           if(hbfiltmap(i  ,i+4).eq.2) hbfiltmap(i  ,i+4) = 0
           if(hbfiltmap(i+1,i+4).eq.2) hbfiltmap(i+1,i+4) = 0
           goto 400
          endif
         endif
        
c        no PRO in (i+1 through i+4) in 011, 111, 001, 101 patterns 
         if(n3.ne.0)then
          if((r1.eq.'PRO').or.(r2.eq.'PRO').or.
     &       (r3.eq.'PRO').or.(r4.eq.'PRO'))then
           if(hbfiltmap(i+1,i+4).eq.2) hbfiltmap(i+1,i+4) = 0
           goto 400
          endif
         endif       
        
c        101 -> 100
         if((n1.eq.1).and.(n2.eq.0).and.(n3.eq.1))then 
          if(hbfiltmap(i+1,i+4).eq.2)  hbfiltmap(i+1,i+4) = 0
          goto 400
         endif
        
c        the only remaining patterns should be 000, 100 and 001
        
        endif
       endif
      enddo
      
c     clean up the i/i-4, i-6/i+2 antiparallel hairpin
c     000 23.7%
c     100 11.2% 78 of 010 is xxxxGxx
c     010 53.8% any patttern with the i-4/i-1 present becomes 010
c     001  9.7%
c     011  0.9% -> 010
cccccccccccccccccccccccccccccccccccccccccccccccccccccc
      do i=4,totres-5
       if((hbfiltmap(i,i-4).ne.0).and.(hbfiltmap(i+5,i).ne.0))then
        if(segres(i-4).eq.segres(i+5))then ! this tentative solution only works if the are no resid skips between i-4 and i+5
410      continue
         n1 = hbfiltmap(i-4,i)
         n2 = hbfiltmap(i-4,i-1)
         n3 = hbfiltmap(i-3,i)
         r1 = rname(i-1)        
         r2 = rname(i-2)
         r3 = rname(i-3)
c        no PRO in i-3,i-2 anywhere except 000 
c        sequence pattern constitutes 15% of all such structural patterns
c        (60% of 000 have them), false neg rate is ~1% (OK for government work)
         if(n1+n2+n3.ne.0)then
          if((r3.eq.'PRO').or.(r4.eq.'PRO'))then
           if(hbfiltmap(i-4,i  ).eq.2) hbfiltmap(i-4,i  ) = 0
           if(hbfiltmap(i-4,i-1).eq.2) hbfiltmap(i-4,i-1) = 0
           if(hbfiltmap(i-3,i  ).eq.2) hbfiltmap(i-3,i  ) = 0
           goto 410
          endif
         endif
        
         if(n1+n2+n3.ne.0)then ! impose 010 if seq=xxxxGxx
          if((r1.eq.'GLY').and.(n1+n3.ne.0).and.(n2.ne.0))then
           if(hbfiltmap(i-4,i).eq.2) hbfiltmap(i-4,i) = 0
           if(hbfiltmap(i-3,i).eq.2) hbfiltmap(i-3,i) = 0
           goto 410
          endif
         endif
        
        endif
       endif
      enddo
      
c     i/i+3, i+3/i beta hairpin coming soon...
      
c     modify hblist0 in accordance with hbfiltmap...
cccccccccccccccccccccccccccccccccccccccccccccccccccc
c     remove H-bonds...
302   continue                 
      do i=1,nhbfree
       resi = hbfreeres(i,1)
       resj = hbfreeres(i,2)
       if(hbfiltmap(resi,resj).eq.0)then
        do j=i+1,nhbfree
         hbfreeres(j-1,1) = hbfreeres(j,1)
         hbfreeres(j-1,2) = hbfreeres(j,2)
        enddo
        nhbfree = nhbfree-1
        goto 302
       endif
      enddo
c     add H-bonds...
      do i=1,totres
       do j=1,totres
        if(hbfiltmap(i,j).ne.0)then
         istore = 0
         do k=1,nhbfree
          if(hbfreeres(k,1).eq.i)then
           if(hbfreeres(k,2).eq.j)then
            istore = 1
           endif
          endif
         enddo
         if(istore.eq.0)then
          do k=1,nhbfix
           if(hbfixres(k,1).eq.i)then
            if(hbfixres(k,2).eq.j)then
             istore = 1
            endif
           endif
          enddo
          if(istore.eq.0)then
           nhbfree = nhbfree+1
           hbfreeres(nhbfree,1) = i
           hbfreeres(nhbfree,2) = j
          endif
         endif
        endif
       enddo
      enddo
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     Backbone/backbone H-bonding database term
c     Alexander Grishaev, NIH, 8/03 - 3/04
c     Reads in the database HB potentials and calculates forces
c     called by READHBDB
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine readhbdb2
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'consta.inc'
      include 'comand.inc'

      integer iunit,ierror
      integer i,j,k,l,m,n,j1,j2,ipot,ist,jst,kst
      double precision store,dxst,dyst,dzst,dist,dx,dy,dz,grad,du
      double precision dtr,estore,xst,yst,zst,tst,y
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
c     angular step sizes in radians...
      dtr = dt*pi/180.0d0
c     directional potentials' internal names
      dirpotype( 1) = '310    right'
      dirpotype( 2) = '310     left'
      dirpotype( 3) = 'a N-t & cent'
      dirpotype( 4) = 'a C-t & isol'
      dirpotype( 5) = 'b anti  cent'
      dirpotype( 6) = 'b anti  long'
      dirpotype( 7) = 'b para  cent'
      dirpotype( 8) = 'b para  edge'
      dirpotype( 9) = 'lr  isolated'
      dirpotype(10) = 'g turn right'
      dirpotype(11) = 'g turn  left'
c     linearity potentials' internal names
      linpotype( 1) = 'i/i+3'
      linpotype( 2) = 'i/i+4'
      linpotype( 3) = '  lr '
      linpotype( 4) = 'i/i+2'
c     
c     E(x,y,z) - HB directional potentials and forces...
cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     1: i/i+3,      right-handed 310 helix
c     2: i/i+3,      left -handed 310 turn
c     3: i/i+4,      center & N-terminal alpha helix
c     4: i/i+4,      C-terminal alpha helix & isolated alpha turn
c     5: long range, anti-parallel beta, center & short cycle
c     6: long range, anti-parallel beta, long cycle
c     7: long range, parallel beta, center
c     8: long range, parallel beta, edge
c     9: long range, non-beta
c    10: i/i+2,      right-handed gamma turn
c    11: i/i+2,      left -handed gamma turn
      
c     read in the files...
      do ipot=1,ndirpot
       write(DUNIT,5)'reading directional HB PMF - ',dirpotype(ipot)
5      format(a,1x,a)
       call assfil(filedir(ipot),iunit,'READ','FORMATTED',ierror)
       rewind iunit
       do k=1,ndz
        read(iunit,*)
        read(iunit,*)
        do j=ndy,1,-1
         read(iunit,10)(edir(ipot,i,j,k),i=1,ndx)
10       format(8x,200(f6.3,1x))
        enddo
       enddo
       call vclose(iunit,'keep',ierror)
      enddo
      
c     (x,y,z) forces for the directional potentials...
      write(DUNIT,*)'calculating directional HB PMF forces'
      xmin = offx
      ymin = offy
      zmin = offz
      xmax = xmin+ndx*dxyz
      ymax = ymin+ndy*dxyz
      zmax = zmin+ndz*dxyz
      do ipot=1,ndirpot
       estore = 0.0d0
       ist = 0.0d0
       jst = 0.0d0
       kst = 0.0d0
       do i=1,ndx
        do j=1,ndy
         do k=1,ndz
          if(edir(ipot,i,j,k).ne.0.0d0)then
           y = ymin+(dble(j)-0.5d0)*dxyz
           dist = dsqrt((xmin+(dble(i)-0.5d0)*dxyz)**2+
     &                  (ymin+(dble(j)-0.5d0)*dxyz)**2+
     &                  (zmin+(dble(k)-0.5d0)*dxyz)**2)
          endif
          if((ipot.eq.2).and.(y.gt.0.0d0)) goto 11
          estore = min(estore,edir(ipot,i,j,k))
          if(estore.eq.edir(ipot,i,j,k))then
           ist = i
           jst = j
           kst = k
          endif
11        continue
          store = 100.0d0
          dxst = 0.0d0
          dyst = 0.0d0
          dzst = 0.0d0
          fdir(ipot,i,j,k,1) = 0.0d0
          fdir(ipot,i,j,k,2) = 0.0d0
          fdir(ipot,i,j,k,3) = 0.0d0
          do l=max(1,i-1),min(ndx,i+1)
           dx = dxyz*dble(l-i)
           do m=max(1,j-1),min(ndy,j+1)
            dy = dxyz*dble(m-j)
            do n=max(1,k-1),min(ndz,k+1)
             dz = dxyz*dble(n-k)
             dist = dsqrt(dx**2+dy**2+dz**2)
             if(dist.ne.0.0d0)then
              du = edir(ipot,l,m,n)-edir(ipot,i,j,k)
              grad = du/dist
             else
              grad = 0.0d0
             endif
             if(grad.lt.store)then
              store = grad
              dxst = dx
              dyst = dy
              dzst = dz
             endif
            enddo
           enddo
          enddo
          if(store.ge.0.0d0)then
           dxst = 0.0d0
           dyst = 0.0d0
           dzst = 0.0d0
          else
           dist = dsqrt(dxst**2+dyst**2+dzst**2)
           fdir(ipot,i,j,k,1) = -dxst*store*kdir/dist
           fdir(ipot,i,j,k,2) = -dyst*store*kdir/dist
           fdir(ipot,i,j,k,3) = -dzst*store*kdir/dist
           endif
         enddo
        enddo
       enddo
       if(minfindd.eq.1)then
        if(ist*jst*kst.eq.0)then
         write(DUNIT,*)dirpotype(ipot), 'minimum not found'
         write(DUNIT,*)'ist, jst, kst = ',ist,jst,kst
         call WRNDIE(1,'READHBDB2','minimum not found')
        endif
        xst = offx+(dble(ist)-0.5d0)*dxyz
        yst = offy+(dble(jst)-0.5d0)*dxyz
        zst = offz+(dble(kst)-0.5d0)*dxyz
        dirmin(ipot,1) = xst
        dirmin(ipot,2) = yst
        dirmin(ipot,3) = zst
        write(DUNIT,15)'minimum of ',dirpotype(ipot),' : x,y,z = ',
     &             xst,yst,zst
15      format(a,2x,a,2x,a,2x,3(f5.2,2x))
       endif
      enddo
      write(DUNIT,16)dirpotype(ipot),': directional forces done'
16    format(a,2x,a)
c     
c     E(O-H-N|O-H) - HB linearity potentials and forces...
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c     1: i/i+3, all
c     2: i/i+4, all  
c     3: |i-j|>4, i/i-3, i/i-4
c     4: i/i+2, all
      
c     read in the files...
      do ipot=1,nlinpot
       write(DUNIT,5)'reading linearity HB PMF - ',linpotype(ipot)
        call assfil(filelin(ipot),iunit,'READ','FORMATTED', ierror)
       rewind iunit
       do i=1,ndr
        do j=1,ndt
         read(iunit,20)elin(ipot,i,j)
 20      format(30x,e10.3)
        enddo
       enddo
       call vclose(iunit,'KEEP',ierror)
      enddo
      
c     r/ohn PMF forces in the ohn dimension...
      write(DUNIT,*)'calculating linearity HB PMF forces'
      do ipot=1,nlinpot
       do i=1,ndr
        estore = 0.0d0
        jst = 0.0d0
        do j=1,ndt
         j1 = j-1
         j2 = j+1
         if(j.eq.  1) j1 = j
         if(j.eq.ndt) j2 = j
         flin(ipot,i,j) = -(elin(ipot,i,j2)-elin(ipot,i,j1))*
     &   klin/dtr/dble(j2-j1)
         estore = min(estore,elin(ipot,i,j))
         if(estore.eq.elin(ipot,i,j))then
          jst = j
         endif
        enddo
        if(minfindd.eq.1)then
         tst = offt+(dble(jst)-0.5d0)*dt
         if(elin(ipot,i,jst).ne.4.0d0)then
          linmin(ipot,i) = jst
         endif
        endif
       enddo
      enddo
      write(DUNIT,*)'linearity HB PMF forces done'
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine printhbdb(nunit)
      implicit none
      include 'hbdb.inc'

      integer nunit,i,j
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
      if(nunit.ne.6)then
      write(nunit,'(a)')'REMARK HBDB '
      write(nunit,800)'REMARK HBDB Nstep = ',ihb
800   format(a,2x,i7)
      
      if(ihbfix.eq.1)then
       write(nunit,'(a)')'REMARK HBDB '
       write(nunit,'(a)') 'REMARK HBDB Fixed HB list:'
       write(nunit,'(a)')'REMARK HBDB '
       write(nunit,900)'REMARK HBDB   #   CO      HN   '//
     & '  dir_type   lin_type'//
     & ' O-H  C-O-H Ca-C-O-H O-H-N   Edir  Elin'
900    format(a)
       write(nunit,'(a)')'REMARK HBDB '
       do i=1,nhbfix
        write(nunit,1000)'REMARK HBDB ',i,
     &  idres(hbfixres(i,1)),segres(hbfixres(i,1)),
     &  idres(hbfixres(i,2)),segres(hbfixres(i,2)),
     &  dirpotype(hbfixijstor(i,3)),linpotype(hbfixijstor(i,4)),
     &  (hbfixstor(i,j),j=1,6)
1000    format(a,i3,2x,i3,x,a,i3,x,a,a,x,a,x,
     &  f4.2,3(x,f6.1),3x,f5.2,x,f5.2)
       enddo
       write(nunit,'(a)')'REMARK HBDB '
      endif
      
      if(ihbfree.eq.1)then
       write(nunit,'(a)')'REMARK HBDB '
       write(nunit,'(a)') 'REMARK HBDB Free  HB list:'
       write(nunit,'(a)')'REMARK HBDB '
       write(nunit,900)'REMARK HBDB   #   CO      HN   '//
     & '  dir_type   lin_type'//
     & ' O-H  C-O-H Ca-C-O-H O-H-N   Edir  Elin'
       write(nunit,'(a)')'REMARK HBDB '
       do i=1,nhbfree
        write(nunit,1000)'REMARK HBDB ',i,
     &  idres(hbfreeres(i,1)),segres(hbfreeres(i,1)),
     &  idres(hbfreeres(i,2)),segres(hbfreeres(i,2)),
     &  dirpotype(hbfreeijstor(i,3)),linpotype(hbfreeijstor(i,4)),
     &  (hbfreestor(i,j),j=1,6)
       enddo
       write(nunit,'(a)')'REMARK HBDB '
      endif
      
      write(nunit,'(a,1x,i4)')
     &'REMARK HBDB # of active HBDB terms   :',nhbact
      write(nunit,'(a,1x,f7.3)')
     &'REMARK HBDB Kdir - directional force :',kdir
      write(nunit,'(a,1x,f7.3)')
     &'REMARK HBDB Klin - linearity   force :',klin 
      write(nunit,'(a)')'REMARK HBDB '
      write(nunit,'(a,1x,f7.3)')
     &'REMARK HBDB Edir - average energy/HB :',etotdir
      write(nunit,'(a,1x,f7.3)')
     &'REMARK HBDB Elin - average energy/HB :',etotlin
      write(nunit,'(a)')'REMARK HBDB '
      endif
      
c     ***********************************************************

      if(nunit.eq.6)then
      write(nunit,*)
      write(nunit,'(a,2x,i7)')'Nstep = ',ihb
      
      if(ihbfix.eq.1)then
       write(nunit,*)
       write(nunit,'(a)') 'Fixed HB list:'
       write(nunit,*)
       write(nunit,900)'  #   CO      HN   '//
     & '  dir_type   lin_type'//
     & ' O-H  C-O-H Ca-C-O-H O-H-N   Edir  Elin'
       write(nunit,*)
       do i=1,nhbfix
        write(nunit,1001)i,
     &  idres(hbfixres(i,1)),segres(hbfixres(i,1)),
     &  idres(hbfixres(i,2)),segres(hbfixres(i,2)),
     &  dirpotype(hbfixijstor(i,3)),linpotype(hbfixijstor(i,4)),
     &  (hbfixstor(i,j),j=1,6)
1001    format(i3,2x,i3,x,a,i3,x,a,a,x,a,x,
     &  f4.2,3(x,f6.1),3x,f5.2,x,f5.2)
       enddo
       write(nunit,*)
      endif
      
      if(ihbfree.eq.1)then
       write(nunit,*)
       write(nunit,'(a)') 'Free  HB list:'
       write(nunit,*)
       write(nunit,900)'  #   CO      HN   '//
     & '  dir_type   lin_type'//
     & ' O-H  C-O-H Ca-C-O-H O-H-N   Edir  Elin'
       write(nunit,*)
       do i=1,nhbfree
        write(nunit,1001)i,
     &  idres(hbfreeres(i,1)),segres(hbfreeres(i,1)),
     &  idres(hbfreeres(i,2)),segres(hbfreeres(i,2)),
     &  dirpotype(hbfreeijstor(i,3)),linpotype(hbfreeijstor(i,4)),
     &  (hbfreestor(i,j),j=1,6)
       enddo
       write(nunit,*)
      endif
      
      write(nunit,'(a,1x,i4)')'# of active HBDB terms   :',nhbact
      write(nunit,'(a,1x,f7.3)')'Kdir - directional force :',kdir
      write(nunit,'(a,1x,f7.3)')'Klin - linearity   force :',klin 
      write(nunit,*)
      write(nunit,'(a,1x,f7.3)')'Edir - average energy/HB :',etotdir
      write(nunit,'(a,1x,f7.3)')'Elin - average energy/HB :',etotlin
      write(nunit,*)
      endif


      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine nextan(prompt,value,lenflag,length)
c
c     if lenflag=0 gets next word and returns the first length characters
c 
c     if lenflag=1 gets next word, gets the # of non-blank characters
c     and returns the non-blank part of the word and its length
c
c     modification of the nexta4 routine, Alexander Grishaev, NIH, 04/04
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit none
      include 'cns.inc'
      include 'comand.inc'
      
      integer lenflag,length,i,j
      double precision dpval
      double complex dcval
      character prompt*(*)
      character value*(*)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      call nextwd(prompt)
      if (wd(1:4).eq.'=   ') call nextwd(prompt)
      
      if (wd(1:4).eq.'?   ') then
       write(6,'(1x,2a)') prompt,value
       call declar('result', 'st' ,value, dcval, dpval )
      else
       if(lenflag.eq.0)then
        value=wd(1:length)
       else
c       BARDIAUX changed to have env variable subs
        CALL XXNXTFI(wd,WDLEN,WDMAX)
        do i=1,512
         if(wd(i:i).eq.' ')then  
          length = i-1
          do j=1,length
           value(j:j) = wd(j:j)
          enddo
          goto 100
         endif
        enddo
       endif
      endif
      
100   continue
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     establishes acceptor frame orientation...
      subroutine frame(i1,i2,i3,xyzfr,normfr)
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'coord.inc'

      integer i1,i2,i3,i,j
      double precision xyzfr(3,4),normfr(4)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
c     i1=O, i2=C, i3=Ca
      
c     component 3 - z is the C-O...
      xyzfr(1,3) = x(i1)-x(i2)
      xyzfr(2,3) = y(i1)-y(i2)
      xyzfr(3,3) = z(i1)-z(i2)
c     component 4 is the Ca-C
      xyzfr(1,4) = x(i2)-x(i3)
      xyzfr(2,4) = y(i2)-y(i3)
      xyzfr(3,4) = z(i2)-z(i3)
c     component 2 - y is the vector product of Ca-C and C-O...
      xyzfr(1,2) = xyzfr(2,3)*xyzfr(3,4) - xyzfr(3,3)*xyzfr(2,4)
      xyzfr(2,2) = xyzfr(3,3)*xyzfr(1,4) - xyzfr(1,3)*xyzfr(3,4)
      xyzfr(3,2) = xyzfr(1,3)*xyzfr(2,4) - xyzfr(2,3)*xyzfr(1,4)
c     component 1- x is the vector product of z and y...
      xyzfr(1,1) = xyzfr(2,2)*xyzfr(3,3) - xyzfr(3,2)*xyzfr(2,3)
      xyzfr(2,1) = xyzfr(3,2)*xyzfr(1,3) - xyzfr(1,2)*xyzfr(3,3)
      xyzfr(3,1) = xyzfr(1,2)*xyzfr(2,3) - xyzfr(2,2)*xyzfr(1,3)
c     normalize...
      do i=1,4
       normfr(i) = xyzfr(1,i)**2+xyzfr(2,i)**2+xyzfr(3,i)**2
       normfr(i) = 1.0d0/dsqrt(normfr(i))
       do j=1,3
        xyzfr(j,i) = xyzfr(j,i)*normfr(i)
       enddo
      enddo
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     establishes r,theta,phi,ohn within a coordinate frame
      subroutine rtp(noi,nhj,nnj,xyzfr,r,theta,phi,ohn,prx,pry,prz, 
     &               xoh,yoh,zoh)
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'coord.inc'

      integer noi,nhj,nnj
      double precision r,theta,phi,ohn
      double precision prx,pry,prz
      double precision xoh,yoh,zoh
      double precision norm,mconst
      double precision bx,by,bz,rbr
      double precision rijx,rijy,rijz,rij
      double precision rjkx,rjky,rjkz,rjk
      double precision xyzfr(3,4),pi
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      mconst = 1.0d-4
      pi = 3.141592654d0
      
c     O-H distance...
      xoh = x(nhj)-x(noi)             ! these are in the molecular frame
      yoh = y(nhj)-y(noi)             ! these are in the molecular frame
      zoh = z(nhj)-z(noi)             ! these are in the molecular frame
      norm = xoh**2+yoh**2+zoh**2
      r = dsqrt(max(mconst,norm))
c     theta (COH angle)...
      xoh = xoh/r
      yoh = yoh/r
      zoh = zoh/r
      ct = -xoh*xyzfr(1,3)-yoh*xyzfr(2,3)-zoh*xyzfr(3,3)
      st = dsqrt(max(0.0d0,1.0d0-ct**2))
      theta = 180.0d0/pi*dacos(min(1.0d0,max(-1.0d0,ct)))
c     phi (-180 to 180 degrees)...
      bx =  xyzfr(2,3)*zoh - xyzfr(3,3)*yoh
      by =  xyzfr(3,3)*xoh - xyzfr(1,3)*zoh
      bz =  xyzfr(1,3)*yoh - xyzfr(2,3)*xoh
      rbr = 1.0d0/dsqrt(bx**2+by**2+bz**2)
      bx =  bx*rbr
      by =  by*rbr
      bz =  bz*rbr
      cp = -bx*xyzfr(1,2)-by*xyzfr(2,2)-bz*xyzfr(3,2)
      sp = -bx*xyzfr(1,1)-by*xyzfr(2,1)-bz*xyzfr(3,1)
      phi= -dsign(180.0d0/pi*dacos(min(1.0d0,max(-1.0d0,cp))),sp)
      cp1 = dcos(phi*pi/180.0d0)
      sp1 = dsin(phi*pi/180.0d0)
c     O-H-N angle...
      rijx = -xoh
      rijy = -yoh
      rijz = -zoh
      rij  = r
      rjkx = x(nnj)-x(nhj)
      rjky = y(nnj)-y(nhj)
      rjkz = z(nnj)-z(nhj)
      rjk  = dsqrt(rjkx**2+rjky**2+rjkz**2)
      rjkx = rjkx/rjk
      rjky = rjky/rjk
      rjkz = rjkz/rjk
      ct1  = rijx*rjkx+rijy*rjky+rijz*rjkz
      st1  = dsqrt(1.0d0-ct1**2)
      ohn  = dacos(min(1.0d0,max(-1.0d0,ct1)))*180.0d0/pi
      
      prx = -r*st*cp1                  ! these are in the acceptor frame
      pry = -r*st*sp1                  ! these are in the acceptor frame
      prz = -r*ct                      ! these are in the acceptor frame
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     calculates firectional PMF forces
      subroutine dirforce(noi,nci,nai,nhj,prx,pry,prz,xyzfr,normfr,r, 
     &                    ipot,edirij,xoh,yoh,zoh)
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'coord.inc'
      include 'deriv.inc'

      integer noi,nci,nai,nhj,idx,idy,idz,ipot,switch
      double precision prx,pry,prz
      double precision xoh,yoh,zoh
      double precision xyzfr(3,4),normfr(4)
      double precision mconst,eps
      double precision dxtarg,dytarg,dztarg
      double precision dedx,dedy,dedz
      double precision dedr,dedt,dedp
      double precision rij,rijx,rijy,rijz
      double precision rjk,rjkx,rjky,rjkz
      double precision rkj,rkjx,rkjy,rkjz
      double precision rkl,rklx,rkly,rklz
      double precision r
      double precision edirij
      double precision dxdr,dydr,dzdr
      double precision dxdt,dydt,dzdt
      double precision dxdp,dydp,dzdp
      double precision recst,dedrdx,dedrdy,dedrdz
      double precision dedtdrijx,dedtdrijy,dedtdrijz
      double precision dedtdrkjx,dedtdrkjy,dedtdrkjz
      double precision ax,ay,az
      double precision bx,by,bz
      double precision cx,cy,cz
      double precision rar,rbr,rcr
      double precision recsp,reccp
      double precision dcpax,dcpay,dcpaz
      double precision dcpbx,dcpby,dcpbz
      double precision dspbx,dspby,dspbz
      double precision dspcx,dspcy,dspcz
      double precision dprijx,dprijy,dprijz
      double precision dprjkx,dprjky,dprjkz
      double precision dprklx,dprkly,dprklz
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      mconst = 1.0d-4
      eps    = 1.0d-1
      
      dedr = 0.0d0
      dedt = 0.0d0
      dedp = 0.0d0
      
      dedx = 0.0d0
      dedy = 0.0d0
      dedz = 0.0d0
      
      idx = 1+int((prx-xmin)/dxyz)
      idy = 1+int((pry-ymin)/dxyz)
      idz = 1+int((prz-zmin)/dxyz)
      
      if(r.gt.renf) dedr = kenf*(1.95d0-r)
      if((prz.le.zmin).or.(prz.ge.zmax)) goto 10
      if((prx.le.xmin).or.(prx.ge.xmax)) goto 10
      if((pry.le.ymin).or.(pry.ge.ymax)) goto 10
      
      dedx = fdir(ipot,idx,idy,idz,1)
      dedy = fdir(ipot,idx,idy,idz,2)
      dedz = fdir(ipot,idx,idy,idz,3)
      
10    continue
c     optional directional basin enforcement...
      if(minfindd.eq.1)then
       if(edirij.eq.0.0d0)then
        if((dedx.eq.0.0d0).and.
     &     (dedy.eq.0.0d0).and.
     &     (dedz.eq.0.0d0))then
         dxtarg = dirmin(ipot,1)-prx
         dytarg = dirmin(ipot,2)-pry
         dztarg = dirmin(ipot,3)-prz
         dedx   = kdirmin*dxtarg
         dedy   = kdirmin*dytarg
         dedz   = kdirmin*dztarg
        endif
       endif
      endif
      
c     partial derivatives in (r,t,p) from (x,y,z) by chain rule...
      dxdr = -st*cp1
      dydr = -st*sp1
      dzdr = -ct
      dxdt = -r*ct*cp1
      dydt = -r*ct*sp1
      dzdt =  r*st
      dxdp =  r*st*sp1
      dydp = -r*st*cp1
      dzdp =  0.0d0
      dedr = dedr+dedx*dxdr+dedy*dydr+dedz*dzdr
      dedt =      dedx*dxdt+dedy*dydt+dedz*dzdt
      dedp =      dedx*dxdp+dedy*dydp+dedz*dzdp
      
c     r force: dE/drij = dE/dr * dr/drij...
      if(dedr.eq.0.0d0) goto 100
      dedrdx = dedr*xoh
      dedrdy = dedr*yoh
      dedrdz = dedr*zoh
      
      dx(nhj) = dx(nhj)-dedrdx
      dy(nhj) = dy(nhj)-dedrdy
      dz(nhj) = dz(nhj)-dedrdz
      dx(noi) = dx(noi)+dedrdx
      dy(noi) = dy(noi)+dedrdy
      dz(noi) = dz(noi)+dedrdz   
      
c     calculation of angular & dihedral forces closely follows 
c     Axel Brunger's code in eangle & etor.
c     Signs of his forces APPEAR opposite to mine in the expressions 
c     below because his dE/dr, dE/dt, dE/dp are opposite to mine.
      
c     theta force (C-O-H)...  
ccccccccccccccccccccccccccccc  
100   continue 
      if(dedt.eq.0.0d0) goto 120
      rij   =  normfr(3)
      rkj   =  1.0d0/r
      rijx  = -xyzfr(1,3)
      rijy  = -xyzfr(2,3)
      rijz  = -xyzfr(3,3)
      rkjx  =  xoh
      rkjy  =  yoh
      rkjz  =  zoh
      recst =  dedt/max(mconst,st)
      dedtdrijx = recst*rij*(ct*rijx-rkjx)
      dedtdrijy = recst*rij*(ct*rijy-rkjy)
      dedtdrijz = recst*rij*(ct*rijz-rkjz)
      dedtdrkjx = recst*rkj*(ct*rkjx-rijx)
      dedtdrkjy = recst*rkj*(ct*rkjy-rijy)
      dedtdrkjz = recst*rkj*(ct*rkjz-rijz)  
c     update forces...
      dx(nci) = dx(nci)-dedtdrijx
      dy(nci) = dy(nci)-dedtdrijy
      dz(nci) = dz(nci)-dedtdrijz
      dx(noi) = dx(noi)+dedtdrijx+dedtdrkjx
      dy(noi) = dy(noi)+dedtdrijy+dedtdrkjy
      dz(noi) = dz(noi)+dedtdrijz+dedtdrkjz
      dx(nhj) = dx(nhj)          -dedtdrkjx
      dy(nhj) = dy(nhj)          -dedtdrkjy
      dz(nhj) = dz(nhj)          -dedtdrkjz
c     
c     phi force (Ca-C-O-H)...
ccccccccccccccccccccccccccccc 
120   continue 
      if(dedp.eq.0.0d0) goto 140
c     rij is my -cac, rjk is my -z,rkl is -oh...
      rij  =  normfr(4)
      rijx = -xyzfr(1,4)
      rijy = -xyzfr(2,4)
      rijz = -xyzfr(3,4) 
      rjk  =  normfr(3)
      rjkx = -xyzfr(1,3)
      rjky = -xyzfr(2,3)
      rjkz = -xyzfr(3,3)
      rkl  =  1.0d0/r
      rklx = -xoh
      rkly = -yoh
      rklz = -zoh
c     A is -y, C is -x...
      ax  = -xyzfr(1,2)
      ay  = -xyzfr(2,2)
      az  = -xyzfr(3,2)
      bx =  xyzfr(2,3)*zoh - xyzfr(3,3)*yoh
      by =  xyzfr(3,3)*xoh - xyzfr(1,3)*zoh
      bz =  xyzfr(1,3)*yoh - xyzfr(2,3)*xoh
      rbr = 1.0d0/dsqrt(bx**2+by**2+bz**2)
      bx =  bx*rbr
      by =  by*rbr
      bz =  bz*rbr
      cx  = -xyzfr(1,1)
      cy  = -xyzfr(2,1)
      cz  = -xyzfr(3,1)
      rar =  normfr(2)
      rcr =  normfr(1)
c     Heavyside function switches between two alternatives 
c     to compute d phi/ d r
      switch = -min(1,int(dabs(sp)-eps+1.0d0))
      recsp = dedp*dble(switch  )*sign(1.0d0,sp)/max(dabs(sp),mconst)
      reccp = dedp*dble(switch+1)*sign(1.0d0,cp)/max(dabs(cp),mconst)
c     dcos(phi)/dA, dcos(phi)/dB...
      dcpax = -rar*(bx-cp*ax)
      dcpay = -rar*(by-cp*ay)
      dcpaz = -rar*(bz-cp*az)
      dcpbx = -rbr*(ax-cp*bx)
      dcpby = -rbr*(ay-cp*by)
      dcpbz = -rbr*(az-cp*bz)
c     dsin(phi)/dC, dsin(phi)/dB...
      dspcx = -rcr*(bx-sp*cx)
      dspcy = -rcr*(by-sp*cy)
      dspcz = -rcr*(bz-sp*cz)
      dspbx = -rbr*(cx-sp*bx)
      dspby = -rbr*(cy-sp*by)
      dspbz = -rbr*(cz-sp*bz)
c     dE/dp * dp/drij (get rid of singularity by using two alternatives)
      dprijx = recsp*(rjky*dcpaz-dcpay*rjkz)+reccp*
     &((rjky**2+rjkz**2)*dspcx-rjkx*rjky*dspcy-rjkx*rjkz*dspcz)
      dprijy = recsp*(rjkz*dcpax-dcpaz*rjkx)+reccp*
     &((rjkz**2+rjkx**2)*dspcy-rjky*rjkz*dspcz-rjky*rjkx*dspcx)
      dprijz = recsp*(rjkx*dcpay-dcpax*rjky)+reccp*
     &((rjkx**2+rjky**2)*dspcz-rjkz*rjkx*dspcx-rjkz*rjky*dspcy)
      dprklx = recsp*(dcpby*rjkz-rjky*dcpbz)+
     &         reccp*(dspby*rjkz-rjky*dspbz)
      dprkly = recsp*(dcpbz*rjkx-rjkz*dcpbx)+
     &         reccp*(dspbz*rjkx-rjkz*dspbx)
      dprklz = recsp*(dcpbx*rjky-rjkx*dcpby)+
     &         reccp*(dspbx*rjky-rjkx*dspby)
      dprjkx = recsp*(dcpay*rijz-dcpaz*rijy+dcpbz*rkly-dcpby*rklz)+
     &         reccp*(-(rjky*rijy+rjkz*rijz)*dspcx
     &         +(2.0d0*rjkx*rijy-rijx*rjky)*dspcy
     &         +(2.0d0*rjkx*rijz-rijx*rjkz)*dspcz
     &         +dspbz*rkly-dspby*rklz)
      dprjky = recsp*(dcpaz*rijx-dcpax*rijz+dcpbx*rklz-dcpbz*rklx)+
     &         reccp*(-(rjkz*rijz+rjkx*rijx)*dspcy
     &         +(2.0d0*rjky*rijz-rijy*rjkz)*dspcz
     &         +(2.0d0*rjky*rijx-rijy*rjkx)*dspcx
     &         +dspbx*rklz-dspbz*rklx)
      dprjkz = recsp*(dcpax*rijy-dcpay*rijx+dcpby*rklx-dcpbx*rkly)+
     &         reccp*(-(rjkx*rijx+rjky*rijy)*dspcz
     &         +(2.0d0*rjkz*rijx-rijz*rjkx)*dspcx
     &         +(2.0d0*rjkz*rijy-rijz*rjky)*dspcy
     &         +dspby*rklx-dspbx*rkly)
c     update forces...
      dx(nai) = dx(nai)-dprijx
      dy(nai) = dy(nai)-dprijy
      dz(nai) = dz(nai)-dprijz
      dx(nci) = dx(nci)+dprijx-dprjkx
      dy(nci) = dy(nci)+dprijy-dprjky
      dz(nci) = dz(nci)+dprijz-dprjkz
      dx(noi) = dx(noi)       +dprjkx-dprklx
      dy(noi) = dy(noi)       +dprjky-dprkly
      dz(noi) = dz(noi)       +dprjkz-dprklz
      dx(nhj) = dx(nhj)              +dprklx
      dy(nhj) = dy(nhj)              +dprkly
      dz(nhj) = dz(nhj)              +dprklz
      
140   continue
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     linearity (O-H/O-H-N) force in the OHN dimension...
      subroutine linforce(noi,nhj,nnj,r,ohn,xyzfr,normfr,xoh,yoh,zoh, 
     &                    ipotra,elinij)
      implicit none
      include 'cns.inc'     
      include 'hbdb.inc'
      include 'coord.inc'
      include 'deriv.inc'

      integer          noi,nhj,nnj,ipotra,idr,idt
      double precision r,ohn,elinij,dedt,dttarg
      double precision xyzfr(3,4),normfr(4)
      double precision rijx,rijy,rijz,rij
      double precision rjkx,rjky,rjkz,rjk
      double precision xoh,yoh,zoh
      double precision recst,mconst
      double precision dedtdrijx,dedtdrijy,dedtdrijz
      double precision dedtdrjkx,dedtdrjky,dedtdrjkz
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      mconst = 1.0d-4
      
      if(ipotra.eq.0) goto 100
      
      rijx = -xoh
      rijy = -yoh
      rijz = -zoh
      rij  = r
      rjkx = x(nnj)-x(nhj)
      rjky = y(nnj)-y(nhj)
      rjkz = z(nnj)-z(nhj)
      rjk  = dsqrt(rjkx**2+rjky**2+rjkz**2)
      rjkx = rjkx/rjk
      rjky = rjky/rjk
      rjkz = rjkz/rjk
      
      if((r.le.1.5d0).or.(r.ge.2.5d0)) goto 100
      if(ohn.le.90.d0) goto 100
      if(ipotra.eq.4)then
       if((r.le.1.65d0).or.(r.ge.2.55d0)) goto 100
      endif
      
      idr  = 1+int((r  -offr)/dr)
      idt  = 1+int((ohn-offt)/dt)
      dedt = flin(ipotra,idr,idt)

c     optional directional basin enforcement...
      if(minfindl.eq.1)then
       if(elinij.eq.0.0d0)then
        if(dedt.eq.0.0d0)then
         dttarg = linmin(ipotra,idr)-ohn
         dedt   = -klinmin*dttarg
        endif
       endif
      endif
      
      if(dedt.eq.0.0d0) goto 100
      
      recst=dedt/max(mconst,st1)
      dedtdrijx = recst*rij*(ct1*rijx-rjkx)
      dedtdrijy = recst*rij*(ct1*rijy-rjky)
      dedtdrijz = recst*rij*(ct1*rijz-rjkz)
      dedtdrjkx = recst*rjk*(ct1*rjkx-rijx)
      dedtdrjky = recst*rjk*(ct1*rjky-rijy)
      dedtdrjkz = recst*rjk*(ct1*rjkz-rijz)
      
      dx(noi) = dx(noi)-dedtdrijx
      dy(noi) = dy(noi)-dedtdrijy
      dz(noi) = dz(noi)-dedtdrijz
      dx(nhj) = dx(nhj)+dedtdrijx+dedtdrjkx
      dy(nhj) = dy(nhj)+dedtdrijy+dedtdrjky
      dz(nhj) = dz(nhj)+dedtdrijz+dedtdrjkz
      dx(nnj) = dx(nnj)          -dedtdrjkx
      dy(nnj) = dy(nnj)          -dedtdrjky
      dz(nnj) = dz(nnj)          -dedtdrjkz
      
100   continue
      
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     sets the HB potential types
      subroutine hbdbsetpottype
      implicit none
      include 'cns.inc'
      include 'hbdb.inc'
      include 'comand.inc'

      integer i,j,nij
      integer resi,resj
      integer ifix,ifree
      integer ipot,ipotra
      double precision phicurr 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
      do i=1,totres
       do j=1,totres
        hbmap(i,j) = 0
       enddo
      enddo
      
      if(ihbfix.eq.1)then
       do ifix=1,nhbfix
        resi = hbfixres(ifix,1)
        resj = hbfixres(ifix,2)
        hbmap(resi,resj) =  1
       enddo
      endif
      
      if(ihbfree.eq.1)then
       do ifree=1,nhbfree
        resi = hbfreeres(ifree,1)
        resj = hbfreeres(ifree,2)
        hbmap(resi,resj) =  1
       enddo
      endif
      
      do i=1,totres
       do j=1,totres
        
        if(hbmap(i,j).eq.0) goto 100

        if(segres(i).eq.segres(j))then
         nij = idres(j)-idres(i) ! this works regardless of the skips in the resids
        else
         nij = 100
        endif

        if(hbfixprop(i,j,1).ne.0.0d0)then
         phicurr = hbfixprop (i,j,3)
        else
         phicurr = hbfreeprop(i,j,3)
        endif 
      
c       selection of the potential class...
c       default - long range, isolated...
        ipot = 9
        if(nij.eq.3)then
         if(phicurr.lt.0.0d0)then
c         3_10 helix, right-handed...
          ipot = 1
          goto 30
         else
c         3_10 turn, left-handed...
	    ipot = 2
          goto 30
         endif
        endif
        if(nij.eq.4)then
c        alpha helix, center or N-terminal...
         if(hbmap(i+1,j+1).eq.1)then
          ipot = 3
          goto 30
         endif
c        alpha helix, C-terminal or isolated turn...
         if(hbmap(i+1,j+1).eq.0)then
	    ipot = 4
          goto 30
         endif
        endif
        if(abs(nij).gt.4)then
c        beta sheet, anti-parallel, center or short cycle ...
         if(hbmap(j,i).eq.1)then
          ipot = 5
          goto 30
         endif
c        beta sheet, anti-parallel, long cycle ...
         if((hbmap(j,i).eq.0).and.
     &      (hbmap(j-2,i+2).eq.1))then
          ipot = 6
          goto 30
         endif
c        beta sheet, parallel, center ...
         if((hbmap(j  ,i+2).eq.1).and.
     &      (hbmap(j-2,i  ).eq.1))then
          ipot = 7
          goto 30
         endif
c        beta sheet, parallel, edge ...
         if((hbmap(j  ,i+2).eq.0).and.
     &      (hbmap(j-2,i  ).eq.1))then
          ipot = 8
          goto 30
         endif
         if((hbmap(j  ,i+2).eq.1).and.
     &      (hbmap(j-2,i  ).eq.0))then
          ipot = 8
          goto 30
         endif
c        long-range, isolated...
         if(hbmap(j,i).eq.0)then
          if(hbmap(j-2,i+2).eq.0)then
           if(hbmap(j,i+2).eq.0)then
            if(hbmap(j-2,i).eq.0)then
             ipot = 9
            endif
           endif
          endif
         endif
        endif
        if(nij.eq.2)then
         if(phicurr.lt.0.0d0)then
c         2_7 helix, right-handed...
          ipot = 10
          goto 30
         else
c         gamma turn, left-handed...
		ipot = 11
          goto 30
	   endif
        endif

30      continue

        if(nij.eq. 3)     ipotra = 1
        if(nij.eq. 4)     ipotra = 2
        if(abs(nij).gt.4) ipotra = 3
        if(nij.eq.-3)     ipotra = 3
        if(nij.eq.-4)     ipotra = 3
        if(nij.eq. 2)     ipotra = 4

        if(ihbfix.eq.1)then
         do ifix=1,nhbfix
          resi = hbfixres(ifix,1)
          resj = hbfixres(ifix,2)
          if((resi.eq.i).and.(resj.eq.j))then
           idfixpot(ifix,1) = ipot
           idfixpot(ifix,2) = ipotra
           goto 100
          endif
         enddo
        endif

        if(ihbfree.eq.1)then
         do ifree=1,nhbfree
          resi = hbfreeres(ifree,1)
          resj = hbfreeres(ifree,2)
          if((resi.eq.i).and.(resj.eq.j))then
           idfreepot(ifree,1) = ipot
           idfreepot(ifree,2) = ipotra
           goto 100
          endif
         enddo
        endif
        
        write(DUNIT,*)'problem with setting the potential types'
        write(DUNIT,*)'between residues       ',segres(i),segres(j)
        write(DUNIT,*)'residues are not found in the fixed & free lists'
        write(DUNIT,*)'dir and lin types are  ',ipot,ipotra
        write(DUNIT,*)'fix and free flags are ',ihbfix,ihbfree
        call WRNDIE(1,'HBDBSETPOTTYPE',
     $       'problem with setting the potential types')
        
100     continue

       enddo
      enddo

      return
      end
