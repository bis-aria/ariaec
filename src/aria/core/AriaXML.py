# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""

from __future__ import absolute_import
# from .ariabase import AriaBaseClass
from .TypeChecking import is_type, check_elements, LIST, TUPLE
from .xmlutils import *
from .tools import last_traceback

from .ariabase import *
from .xmlutils import *
# TODO[FALLAIN]: import Exception is not necessary all exceptions are provided
# in the built in namespace. Furthermore, this package do not exist in python 3
# from exceptions import Exception
from .tools import last_traceback

# DTD base names

DTD_CHEMICAL_SHIFT_LIST = 'chemical_shift_list'
DTD_MOLECULE = 'molecule'
DTD_NOESY_SPECTRUM = 'noesy_spectrum'
DTD_PROJECT = 'project'
DTD_NOE_RESTRAINT = 'noe_restraint'
DTD_DIST_RESTRAINT = 'distance_restraint'

# DOCTYPEs

DOCTYPE_NOE_RESTRAINTS = 'noe_restraint_list'
DOCTYPE_DIST_RESTRAINTS = 'distance_restraint_list'


class DOCManager(AriaBaseClass):
    """
    Generic class refering all ARIA XML Document Type Definition file used for
    validation.
    """
    VERSION = 1.0

    def __init__(self):

        from .Project import Project
        from .AriaPeak import AriaPeak, DistanceRestraint
        from .Topology import Topology
        from .Molecule import Molecule
        from .NOESYSpectrum import NOESYSpectrum
        from .ChemicalShiftList import ChemicalShiftList
        from .Project import ProjectSingleton
        from .conversion import ConverterSettings

        AriaBaseClass.__init__(self)

        # DTD base names

        d = {Project: DTD_PROJECT,
             AriaPeak: DTD_NOE_RESTRAINT,
             DistanceRestraint: DTD_DIST_RESTRAINT,
             Topology: 'topology',
             Molecule: DTD_MOLECULE,
             NOESYSpectrum: DTD_NOESY_SPECTRUM,
             ChemicalShiftList: DTD_CHEMICAL_SHIFT_LIST,
             ProjectSingleton: DTD_PROJECT,
             ConverterSettings: 'conversion'}

        for key, value in d.items():
            # Compile dtd file names with DOCManager version
            d[key] = self._compile_name(value)

        self.__dtd_base_names = d

        # DOCTYPEs

        d = {Project: 'project',
             AriaPeak: DOCTYPE_NOE_RESTRAINTS,
             DistanceRestraint: DOCTYPE_DIST_RESTRAINTS,
             Topology: 'topology',
             Molecule: 'molecule',
             NOESYSpectrum: 'spectrum',
             ChemicalShiftList: 'chemical_shift_list',
             ProjectSingleton: 'project',
             ConverterSettings: 'conversion'}

        self.__doc_types = d

    def _compile_name(self, base):
        return '%s%s.dtd' % (base, self.VERSION)

    def getDOCTypes(self):
        return self.__doc_types.values()

    def getDTDs(self):
        return self.__dtd_base_names.values()

    def is_validDOCType(self, name):
        return name in self.getDOCTypes()

    def is_validDTD(self, name):
        return name in self.getDTDs()

    def getDTD(self, c, is_class=0):
        """
        returns DTD for class 'c'

        Parameters
        ----------
        c :
            
        is_class :
             (Default value = 0)

        Returns
        -------

        """

        if not is_class:

            if not type(c) == type(self):
                s = 'Instance expected, "%s" given.' % str(type(c))
                self.error(msg=s)

            c = c.__class__

        if c not in self.__dtd_base_names:
            s = 'DTD for class "%s" unknown'
            self.error(ValueError, s % c.__name__)

        dtd_name = self.__dtd_base_names[c]

        return dtd_name

    def getDOCType(self, c, is_class=0):

        if not is_class:

            if not type(c) == type(self):
                s = 'Instance expected, "%s" given.' % str(type(c))
                self.error(msg=s)

            c = c.__class__

        if c not in self.__doc_types:
            s = 'DOCTYPE for class "%s" unknown'
            self.error(ValueError, s % c.__class__.__name__)

        return self.__doc_types[c]


# TODO: Should normally implements abstract methods
class AriaXMLContentHandler(XMLContentHandler):
    """Generic class containing all ARIA xml content handler (picklers)"""

    def __init__(self):

        from .Project import ProjectXMLPickler
        from .AriaPeak import AriaPeakXMLPickler, DistanceRestraintXMLPickler
        from .Topology import TopologyXMLPickler
        from .Molecule import MoleculeXMLPickler
        from .NOESYSpectrum import NOESYSpectrumXMLPickler
        from .ChemicalShiftList import ChemicalShiftListXMLPickler
        from .conversion import ConverterSettingsXMLPickler

        self.sub_pickler = None

        XMLContentHandler.__init__(self)

        pickler = {'project': ProjectXMLPickler(),
                   DOCTYPE_NOE_RESTRAINTS: AriaPeakXMLPickler(),
                   DOCTYPE_DIST_RESTRAINTS: DistanceRestraintXMLPickler(),
                   'topology': TopologyXMLPickler(),
                   'molecule': MoleculeXMLPickler(),
                   'spectrum': NOESYSpectrumXMLPickler(),
                   'chemical_shift_list': ChemicalShiftListXMLPickler(),
                   'conversion': ConverterSettingsXMLPickler()}

        # register pickler for different DOCTYPEs
        self.pickler = pickler

    def select(self, doc_type):
        """
        Define the correct sub pickler according to the document type.

        Parameters
        ----------
        doc_type : :py:class:`str`
            Document type label supported (project, molecule, conversion, ...)
        """
        self.sub_pickler = self.pickler[doc_type] if doc_type in self.pickler \
            else None

    def load_from_element(self, name, e):
        """
        Switch to sub pickler if exists and return python data structure using
        the selected pickler. Otherwise return the raw xml element

        Parameters
        ----------
        name : :py:class:`str`
            xml tag name
        e : :py:class:`.xmlutils.XMLElement`
            xml parent element

        Returns
        -------
        e : {:py:class:`dict`, :py:class:`.xmlutils.XMLElement`}
            python structure or raw xml element related to ``name``
        """
        # TODO: try to avoid this kind of except catch. For the moment, the
        # pickler will not work if we catch only several errors like
        # AttributeError or XMLTageError
        try:
            pickler = getattr(self.sub_pickler, name)
        except:
            return e

        return pickler.load_from_element(e)


class AriaXMLPickler(XMLPickler, AriaBaseClass):
    def __init__(self):

        AriaBaseClass.__init__(self)

        handler = AriaXMLContentHandler()
        XMLPickler.__init__(self, handler)

        self.doc_manager = DOCManager()

    def createParser(self):
        """Instantiate the XML parser"""
        parser = XMLPickler.createParser(self)

        if hasattr(parser, 'is_self_made'):
            msg = 'Expat XML parser unavailable, using Python version.'
            self.message(msg)

        return parser

    def check_document(self, doctype, dtd):

        if not self.doc_manager.is_validDOCType(doctype):
            s = 'Unknown DOCTYPE: "%s". Check your XML file.'
            self.error(TypeError, s % doctype)

        if not self.doc_manager.is_validDTD(dtd):
            s = 'Unknown DTD: "%s". Check your XML file.'
            self.error(TypeError, s % dtd)

    def parse_doc_type(self, f):
        """
        Fetch document type and initialize the related content handler.

        Parameters
        ----------
        f : file
            XML document file

        Returns
        -------
        doc_type : str
            Document type label in DOCTYPE tag
        dtd : str
            DTD file name related to this DOCTYPE
        """
        doc_type, dtd = XMLPickler.parse_doc_type(self, f)

        self.check_document(doc_type, dtd)

        handler = self.getContentHandler()
        # Select the correct handler according to the document type
        # (project, molecule, ...)
        handler.select(doc_type)

        return doc_type, dtd

    def __load(self, filename, gzip):
        try:
            doc = XMLPickler.load(self, filename, gzip)

        except XMLTagError as msg:
            print(last_traceback())
            msg = 'XML file %s: %s' % (filename, msg)
            msg += '\nMake sure that all tags/attributes are ' + \
                   'spelled correctly and that no mandatory tags' + \
                   '/attributes are missing.'
            self.error(XMLReaderError, msg)

        except Exception as msg:
            print(last_traceback())
            self.error(Exception, '%s: %s' % (filename, msg))

        # TODO: hacked

        return getattr(doc, doc.get_doc_type().split(':')[-1])

    def load(self, filename, gzip=0):
        """
        Load ARIA XML file and return the related ARIA object

        Parameters
        ----------
        filename : str
            xml file path
        gzip : {0, 1}, optional

        Returns
        -------
        type: {:py:class:`.Project.Project`, :py:class:`.Molecule.Molecule`}
            ARIA object

        """
        XMLBasePickler.relaxed = 0
        return self.__load(filename, gzip)

    def load_relaxed(self, filename, gzip=0):
        XMLBasePickler.relaxed = 1
        x = self.__load(filename, gzip)
        XMLBasePickler.relaxed = 0

        return x

    def create_document(self, object):

        dtd = self.doc_manager.getDTD(object)
        doc_type = self.doc_manager.getDOCType(object)

        doc = XMLDocument(doc_type, dtd)
        setattr(doc, doc_type, object)

        return dtd, doc

    def dumps(self, object):

        if is_type(object, LIST) or is_type(object, TUPLE):

            try:
                check_elements(object, 'AriaPeak')
                is_peak = 1
            except:
                is_peak = 0

            if is_peak:

                if object:
                    dtd = self.doc_manager.getDTD(object[0])
                    doc_type = self.doc_manager.getDOCType(object[0])

                else:

                    from .AriaPeak import AriaPeak

                    dtd = self.doc_manager.getDTD(AriaPeak, is_class=1)

                    doc_type = self.doc_manager.getDOCType(AriaPeak,
                                                           is_class=1)

                doc = XMLDocument(doc_type, dtd)

                new_object = XMLElement()
                new_object.peak = object
                object = new_object

                setattr(doc, doc_type, object)

            # TODO: what happens if is_peak == 0 ?

        else:
            dtd, doc = self.create_document(object)

        handler = self.getContentHandler()

        handler.set_dtd_name(doc.get_dtd())

        xml = XMLPickler.dumps(self, doc)

        handler.release()

        return xml


if __name__ == '__main__':
    pass
