"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux                                            ..
..               Structural Bioinformatics, Institut Pasteur, Paris           ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""
from __future__ import absolute_import, division, print_function

# Settings
from ..Settings import Settings


# BARDIAUX
class CmapSettings(Settings):
    def create(self):
        from ..TypeChecking import DICT, LIST

        from ..Settings import TypeEntity, NonNegativeInt, \
            AbsolutePath, String

        kk = {}
        entity = TypeEntity(DICT)
        entity.set({})
        kk['xy'] = entity
        entity = TypeEntity(LIST)
        entity.set([])
        kk['posit'] = entity
        entity = TypeEntity(DICT)
        entity.set({})
        kk['plist'] = entity
        entity = TypeEntity(DICT)
        entity.set({})
        kk['clist'] = entity

        kk['file'] = AbsolutePath()
        kk['it'] = NonNegativeInt()

        kk['selection'] = String()

        return kk

    def create_default_values(self):
        d = {'xy': {}, 'posit': [], 'plist': {}, 'clist': {}, 'file': "",
             'selection': ""}

        return d


class ContribSettings(Settings):
    def create(self):
        from ..Settings import NonNegativeInt, String
        d = {'ariapeak': NonNegativeInt(), 'id': NonNegativeInt(),
             'dist': String(), 'weight': String(), 'res1': NonNegativeInt(),
             'at1': String(), 'seg1': String(), 'res2': NonNegativeInt(),
             'at2': String(), 'seg2': String()}

        return d

    def create_default_values(self):
        d = {'ariapeak': 0, 'id': 0, 'dist': '%5.3f' % 0.,
             'weight': '%5.3f' % 0., 'res1': 0, 'at1': "", 'seg1': "",
             'res2': 0, 'at2': "", 'seg2': ""}

        return d


class PeakSettings(Settings):
    def create(self):
        from ..Settings import NonNegativeInt, String, ChoiceEntity
        from ..ariabase import YES, NO
        d = {'id': NonNegativeInt(), 'ref_peak': NonNegativeInt(),
             'spec': String()}

        val = ('active', 'inactive')
        d['state'] = ChoiceEntity(val)

        d['dist'] = String()
        d['up'] = String()
        d['low'] = String()

        d['weight'] = String()

        d['deg_viol'] = String()
        d['dist_avg'] = String()

        val = (YES, NO)
        d['viol'] = ChoiceEntity(val)

        val = ('unambiguous', 'ambiguous')
        d['type'] = ChoiceEntity(val)

        return d

    def create_default_values(self):
        d = {'id': 0, 'ref_peak': 0, 'spec': "", 'state': 'inactive',
             'dist': '%5.3f' % 0., 'up': '%5.3f' % 0., 'low': '%5.3f' % 0.,
             'weight': '%5.3f' % 0., 'deg_viol': '%5.2f %%' % 0.,
             'dist_avg': '%5.3f' % 0., 'viol': 'no'}

        return d


class PeakListSettings(Settings):
    def create(self):
        from ..TypeChecking import LIST
        from ..Settings import NonNegativeInt, TypeEntity
        d = {'id': NonNegativeInt()}

        entity = TypeEntity(LIST)
        entity.set([])
        d['pk_list'] = entity

        return d

    def create_default_values(self):
        d = {'id': 0, 'pk_list': []}

        return d


class ContribListSettings(Settings):
    def create(self):
        from ..TypeChecking import LIST
        from ..Settings import NonNegativeInt, TypeEntity
        d = {'id': NonNegativeInt()}

        entity = TypeEntity(LIST)
        entity.set([])
        d['c_list'] = entity

        return d

    def create_default_values(self):
        d = {'id': 0, 'c_list': []}

        return d


class DisplaySettings(Settings):
    def create(self):
        from ..Settings import String
        d = {'display': String()}

        return d

    def create_default_values(self):
        d = {'display': 'all'}

        return d


# PEAK LIST CALUCLATION AND CMAP DETREMINATION
def get_cmap_list(ap_list, sel):
    if sel == 'all':
        peaks = [p for p in ap_list if p.isActive()]
    elif sel == 'ambig':
        peaks = [p for p in ap_list if p.isActive() and p.isAmbiguous()]
    elif sel == 'unambig':
        peaks = [p for p in ap_list if p.isActive() and not p.isAmbiguous()]

    n = 0
    cmap = []
    pk_list = {}
    con_list = {}

    for p in peaks:

        contr = [c for c in p.getContributions() if c.getWeight() > 0.]

        for c in contr:

            cple = []

            for spin_sys in c.getSpinSystems():
                at = spin_sys.getAtoms()[0]
                re = at.getResidue().getNumber()
                cple.append(re)

            cples = [cple]
            if cple[0] != cple[1]:
                cples.append([cple[1], cple[0]])

            for cple in cples:

                if cple not in cmap:
                    cmap.append(cple)
                    con_list.setdefault(n, [setContrib([c, p.getId()])])
                    pk_list.setdefault(n, [setPeak(p)])
                    n += 1
                else:
                    i = cmap.index(cple)
                    if [c, p.getId()] not in con_list[i]:
                        con_list[i].append(setContrib([c, p.getId()]))
                    if setPeak(p) not in pk_list[i]:
                        pk_list[i].append(setPeak(p))

                        # uniq=reduce(lambda l, x: x not in l and l.append(x)
                        # or l, cmap , [])

    return cmap, pk_list, con_list


def setPeak(p):
    #     from gui_beta import PeakSettings

    from ..ariabase import YES, NO

    YN = [NO, YES]
    AC = ['inactive', 'active']
    AMB = ['unambiguous', 'ambiguous']

    pp = {'id': p.getId(), 'ref_peak': p.getReferencePeak().getNumber(),
          'spec': p.getReferencePeak().getSpectrum().getName(),
          'state': AC[p.isActive()], 'dist': '%5.3f' % p.getDistance(),
          'up': '%5.3f' % p.getUpperBound(), 'low': '%5.3f' % p.getLowerBound(),
          'weight': '%5.3f' % p.getWeight(),
          'deg_viol': '%5.2f %%' % (p.analysis.getDegreeOfViolation() * 100),
          'dist_avg': '%5.3f' % p.analysis.getAverageDistance().getValue(),
          'viol': YN[p.analysis.isViolated()], 'type': AMB[p.isAmbiguous()]}

    return pp


def setContrib(c):
    #     from gui_beta import PeakSettings

    pp = {}

    cc = c[0]

    INT = ['INTER', 'INTRA']

    pp['id'] = cc.getId()
    pp['ariapeak'] = c[1]
    if cc.getAverageDistance().getValue() is not None:
        pp['dist'] = '%5.3f' % cc.getAverageDistance().getValue()
    else:
        pp['dist'] = 'None'
    pp['weight'] = '%5.3f' % cc.getWeight()

    at1 = cc.getSpinSystems()[0].getAtoms()
    atoms1 = pseudoString([a.getName() for a in at1])

    at2 = cc.getSpinSystems()[1].getAtoms()
    atoms2 = pseudoString([a.getName() for a in at2])

    pp['res1'] = at1[0].getResidue().getNumber()
    pp['at1'] = ' / '.join(atoms1)
    pp['seg1'] = at1[0].getSegid()
    pp['res2'] = at2[0].getResidue().getNumber()
    pp['at2'] = ' / '.join(atoms2)
    pp['seg2'] = at2[0].getSegid()

    return pp


def pseudoString(at):
    import re
    seen = []
    s = '(\D+)\d+'
    m = re.compile(s)
    for a in at:
        x = m.search(a)
        if x:
            _a = x.group(1) + '*'
            if _a not in seen:
                seen.append(_a)
        else:
            if a not in seen:
                seen.append(a)

    return seen


class Exporter:
    def __init__(self):
        pass

    def write(self, filename):
        pass
