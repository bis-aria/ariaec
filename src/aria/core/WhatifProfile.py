"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux                                            ..
..               Structural Bioinformatics, Institut Pasteur, Paris           ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""
from __future__ import print_function

import os
import time

CHECK_LIST = ['QUACHK', 'NQACHK', 'RAMCHK', 'C12CHK',
              'BBCCHK', 'BMPCHK']

OTHER = ['BNDCHK', 'ANGCHK', 'PLNCHK', 'INOCHK']

LEGEND_LIST_I = {'1st generation packing quality Z-score': 'QUACHK',
                 '2nd generation packing quality Z-score': 'NQACHK',
                 'Ramachandran plot appearance Z-score': 'RAMCHK',
                 'Chi-1 chi-2 rotamer normality Z-score': 'C12CHK',
                 'Backbone conformation Z-score': 'BBCCHK',
                 'Bond lengths RMS Z-score': 'BNDCHK',
                 'Bond angles RMS Z-score': 'ANGCHK',
                 'Side chain planarity RMS Z-score': 'PLNCHK',
                 'Improper dihedral distribution RMS Z-score': 'HNDCHK',
                 'Inside/outside distribution RMS Z-score': 'INOCHK',
                 'Inter-atomic bumps': 'BMPCHK'}

LEGEND_LIST = dict(zip(LEGEND_LIST_I.values(), LEGEND_LIST_I.keys()))

SCORES_THRE = {'QUACHK': (-5., -3.),
               'NQACHK': (-3., -2.5),
               'C12CHK': (-4., -3.),
               'BBCCHK': (5., 10.),
               'BMPCHK': (0., 0.1),
               'RAMCHK': (-4., -3.)}

BAD = 'r'
GOOD = 'g'
POOR = 'orange'

FILENAME_WHATIF_PROFILE = 'whatif_profiles'
FILENAME_WHATIF_PROFILE_EPS = FILENAME_WHATIF_PROFILE + '.eps'
FILENAME_WHATIF_PROFILE_PS = FILENAME_WHATIF_PROFILE + '.ps'
FILENAME_WHATIF_PROFILE_PDF = FILENAME_WHATIF_PROFILE + '.pdf'

_use_matplotlib = 0

try:
    import matplotlib

    _use_matplotlib = 1
    matplotlib.use('pdf', warn=False)
except:
    pass


# FILENAME_WHATIF_PROFILE_TEX = FILENAME_WHATIF_PROFILE + '.tex'

class WhatifProfile:

    def __init__(self, wdir, whatIfExe='whatif'):

        self.checks = None
        self.data = None
        self.wdir = wdir
        self.is_whatcheck = 0

        if os.path.split(whatIfExe)[1] in ['whatcheck', 'DO_WHATCHECK.COM']:
            self.is_whatcheck = 1

    def parseWhatifChecks(self):

        from legacy.QualityChecks.QualityChecks import FILENAME_REPORT_WHATIF
        import re

        input = open(os.path.join(self.wdir, FILENAME_REPORT_WHATIF))

        fileID = re.compile("^ID\s+\:\s+(\S+)")
        checkID = re.compile("^CheckID\s+\:\s+(\S+)")
        resid = re.compile("\s+Name\s+\:\s(\S*)\-?\s*(\d+)\-(\w+)(\-\s*(CA))?")
        if self.is_whatcheck:
            resid = re.compile(
                "\s+Name\s+\:\s+(\d+)\s+;\s+\S*\s+;\s+(\d+)\s+;\s+(\w+)\s+;"
                "\s+\S*\s*(;\s+CA)?")
        value = re.compile("\s+Value\s+\:\s+(\-?\d+\.\d+)")

        checks = {}

        self.check_list = []
        # parsing
        while 1:

            line = input.readline()

            fID = fileID.search(line)
            cID = checkID.search(line)
            res = resid.search(line)
            val = value.search(line)

            if fID:
                file = fID.group(1)
                checks.setdefault(file, {})

            if cID:

                ch = cID.group(1)
                checks[file].setdefault(ch, {})
                if ch not in self.check_list and ch in CHECK_LIST:
                    self.check_list.append(ch)

            if res:
                resn = res.group(2)

            if line.startswith("   Name") and not res:
                resn = line.split(";")[2].strip()

            if val:
                v = float(val.group(1))
                try:
                    # TODO[FALLAIN]: x not used
                    x = checks[file][ch][resn]
                    continue
                except:
                    checks[file][ch][resn] = v

            if not line:
                break

        self.checks = checks

    def getProfilesData(self):

        import numpy

        self.plength = len(self.checks[self.checks.keys()[0]]['NQACHK'])
        self.pids = map(lambda a: int(a),
                        self.checks[self.checks.keys()[0]]['NQACHK'].keys())
        self.pids.sort()

        # print self.plength

        scores = {}
        extrem = {}
        sd = {}

        for elt in self.check_list:

            score = numpy.array(
                [cv for k, v in self.checks.items() for ck, cv in v.items() if
                 ck == elt])

            new_score = []

            # print elt
            for s in score:
                struc_score = numpy.zeros(self.plength, numpy.float)

                for g in range(self.plength):
                    try:
                        # struc_score[g] = s[str(g+1)]
                        struc_score[g] = s[str(self.pids[g])]

                    except:
                        pass
                new_score.append(struc_score)

            new_score = numpy.array(new_score)
            # if new_score
            # print new_score
            scores[elt] = numpy.mean(new_score, 0)
            extrem[elt] = [numpy.min(new_score, 0), numpy.max(new_score, 0)]
            if len(new_score) > 1:
                sd[elt] = numpy.std(new_score, 0)
            else:
                sd[elt] = numpy.zeros(len(new_score[0]), numpy.float)

        self.data = [scores, extrem, sd]

    def plotProfiles(self, output_ps):

        # import biggles
        from matplotlib.backends.backend_pdf import PdfPages
        import matplotlib.pylab as pylab

        from matplotlib import rcParams

        # from numpy import ma
        # rcParams['numerix'] = 'numpy'

        from math import ceil

        # Test a one-col table
        n_l = len(self.check_list)

        pdf = PdfPages(output_ps)

        f = pylab.figure(figsize=(8.27, 11.69))

        titles = "Whatif scores residue profiles"
        # pylab.figtext(0.1,0.95, titles)
        f.suptitle(titles, fontsize=14, fontweight='bold')

        pylab.figtext(0.35, 0.95, "Good", color=GOOD, fontweight='bold')
        pylab.figtext(0.45, 0.95, "Poor", color=POOR, fontweight='bold')
        pylab.figtext(0.55, 0.95, "Bad", color=BAD, fontweight='bold')

        titles = "%s - %s" % (time.ctime(), output_ps)
        pylab.figtext(0.05, 0.02, titles, fontsize=8)

        for e in range(len(self.check_list)):

            name = self.check_list[e]
            if e >= n_l:
                x = e - n_l
                y = 1
            else:
                x = e
                y = 0

            # test suite
            y = 0
            x = e

            # TODO[FALLAIN]: x & y actually not used
            scores = self.data[0]
            splotax = pylab.subplot(len(self.check_list), 1, e + 1)
            # TODO[FALLAIN] unitaryPlot return nothing actually
            pp = self.unitaryPlot(name, scores[name], splotax)

        pylab.subplots_adjust(hspace=0.5)
        # t1.write_eps(output_ps)
        # pylab.savefig(output_ps, papertype='a4')
        pylab.savefig(pdf, papertype='a4', format='pdf')

        pdf.close()

    def unitaryPlot(self, t, data, ax):

        # import biggles
        import matplotlib.pylab as pylab
        from matplotlib.ticker import MultipleLocator
        from numpy import array, putmask, ma
        from numpy import less, greater_equal, greater, less_equal

        cutoffs = SCORES_THRE[t]

        majorLocator = MultipleLocator(10)
        minorLocator = MultipleLocator(1)

        # TODO[FALLAIN]: linet not used
        linet = ['dotted', 'solid']
        profile = []

        if type(data) == type([]):
            profile = data
        else:
            profile.append(data)

        x = self.pids

        n = 1

        for d in profile:

            colors = array([POOR] * len(x))
            if t == 'BMPCHK':
                putmask(colors, greater(d, cutoffs[1]), BAD)
                putmask(colors, less_equal(d, cutoffs[0]), GOOD)
            else:
                putmask(colors, less(d, cutoffs[0]), BAD)
                putmask(colors, greater_equal(d, cutoffs[1]), GOOD)

            pylab.vlines(x, [0], d, colors=colors, lw=2)
            # if t == 'BMPCHK':
            #    pylab.plot(x, ma.masked_where(colors <> 'g', d),
            # marker=".", c= 'g', ms=5)

            pylab.grid(True)
            if min(d) < 0:
                pylab.axhline(y=0, c='black', zorder=1)
            ax.set_axisbelow(True)
            pylab.ylabel(t)
            pylab.title(LEGEND_LIST[t])
            pylab.xlim(x[0] - 1, x[-1] + 1)
            ax.xaxis.set_major_locator(majorLocator)
            ax.xaxis.set_minor_locator(minorLocator)
            ax.tick_params(axis='y', which='major', labelsize=8)
            ax.tick_params(axis='x', which='major', labelsize=10)

            n += 1

    def writeTextProfiles(self, dst):
        """
        write profiles in text file

        .. resid         check1             check2
        ..          avg max min sd     avg max min sd

        Parameters
        ----------
        dst :
            

        Returns
        -------

        """

        line = """# Whatif scores residues profiles

# Check  : name of the whatif analysis
# Res    : residue number
# Mean   : mean score values among all conformers
# Sd     : unbiased standard deviation
# Max    : maximum score
# Min    : minimum score

"""

        line += '#%6s\t%3s\t%6s\t%6s\t%6s\t%6s\n\n' % (
        'Check', 'Res', 'Mean', 'Sd', 'Min', 'Max')

        for e in self.check_list:
            for r in range(self.plength):
                idn = self.pids[r]

                line += '%6s\t%3d\t%6.3f\t%6.3f\t%6.3f\t%6.3f\n' % (
                e, idn, self.data[0][e][r], self.data[2][e][r],
                self.data[1][e][0][r], self.data[1][e][1][r])
            line += '\n'

        f = open(dst, 'w')
        f.write(line)
        f.close()

    def makeProfiles(self):

        # Parse whatif output residue file
        self.parseWhatifChecks()

        # average scores on all        
        self.getProfilesData()

        # try:
        # plot all checks profiles
        if _use_matplotlib:
            output_ps = os.path.join(self.wdir, FILENAME_WHATIF_PROFILE_PDF)
            self.plotProfiles(output_ps)

            # if we are not on water refinement, change location of ps and eps
            # to /graphics
            from .tools import copy_file
            src = output_ps
            dst = os.path.join(os.path.join(self.wdir, 'graphics'),
                               FILENAME_WHATIF_PROFILE_PDF)
            if os.path.exists(os.path.join(self.wdir, 'graphics')):
                try:
                    copy_file(src, dst)
                    os.remove(src)
                except IOError:
                    s = 'File "%s" does not exist or cannot be accessed.'
                    print(IOError, s % src)

        # except:
        #    print "Plotting abording. Check if module biggles/ps installed."

        # write ASCII whatif residue profiles 
        output = os.path.join(self.wdir, FILENAME_WHATIF_PROFILE)
        self.writeTextProfiles(output)


if __name__ == '__main__':
    import sys
    import os

    path = os.environ['ARIA2']

    sys.path.insert(0, path + '/src/py')

    p = WhatifProfile(sys.argv[1])
    p.makeProfiles()
