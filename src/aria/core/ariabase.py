# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. .......................................................................... ..
"""
from __future__ import absolute_import, division, print_function
from future.builtins import input
from threading import Lock
import inspect
import pkg_resources as pkgr
from .TypeChecking import *
from ..conbox.common import CustomLogging

# set correct python-path
ARIA_ENV = 'ARIA2'
MODULE_NAME = 'aria'
# TODO: sounds like PATH_MODULES global variable is not used anymore
# PATH_MODULES = 'src/py/aria2'
PATH_MODULES = 'aria/core'

# program-wide constants

YES = 'yes'
NO = 'no'
GZIP = 'gzip'
ALWAYS = 'always'
PROJECT_TEMPLATE = 'project_template.xml'
CCPN_EXTENSION = '.ccpn'

# verbose level constants

VL_STANDARD = 0
VL_SETTINGS = 2
VL_LOW = 1

# Print lock
PRINT_LOCK = Lock()

# Logging Config
# TODO: generate doc string at the package level with up to date version number
LOG_CONF = CustomLogging(desc=__doc__, decorator=False)
LOG = logging.getLogger(__name__)


def get_path():
    """Ask user to give aria installation path in ARIA2 env variable"""
    print('Aria environment variable (%s) missing. ' % ARIA_ENV +
          'Please specify root path:')

    return input()


# TODO [FALLAIN]: Using pkg_resources module should be better in the future. It
# requires to change aria package organisation in order to have all the data
# accessible within the package. See python package guide for further
# explanations (https://packaging.python.org/)
def get_aria_root():
    """

    Returns
    -------

    """
    import aria
    import os
    new_flag = True

    # New installation whee all data files are accessible with pkgr
    ext_paths = ('data', 'templates/xml', 'templates/csh', 'cns/toppar',
                 'cns/protocols', 'cns/protocols/analysis')

    module_path = os.path.abspath(os.path.dirname(aria.__file__))

    if all([pkgr.resource_exists(MODULE_NAME, ext_path) for ext_path in
            ext_paths]):
        return module_path, new_flag

    new_flag = False

    # First we check if aria is installed as a python package. In this case,
    # we need to check if data and cns directories are in the aria module (could
    # probably change in the future if we modify aria tree structure and setup
    ext_paths = ('src/py/data', 'cns/toppar', 'cns/protocols',
                 'cns/protocols/analysis')

    # Depending on the installation process, aria module can or not be at the
    # root

    # ARIA installed with a pointer or by extending PYTHONPATH
    ariaroot_path = os.path.abspath(
        os.path.join(module_path, os.path.pardir, os.path.pardir,
                     os.path.pardir))

    manual_paths = [
        os.path.join(ariaroot_path, data_path) for data_path in ext_paths]
    site_paths = [
        os.path.join(module_path, data_path) for data_path in ext_paths]

    # If data files exists in the actual aria installation return module path
    if all([os.path.exists(mypath) for mypath in site_paths]):
        del os
        return module_path, new_flag
    elif all([os.path.exists(mypath) for mypath in manual_paths]):
        del os
        return ariaroot_path, new_flag
    else:
        # Old installation, same code as previous version of get_aria_root
        # function to get the correct installation path with ARIA_ENV bash var
        aria_path = ''

        if ARIA_ENV not in os.environ:
            missing = 1
        else:
            aria_path = os.environ[ARIA_ENV]
            missing = not os.path.exists(aria_path)

        # while missing:
        #     aria_path = get_path()
        #     missing = not os.path.exists(aria_path)
        #     if not missing:
        #         os.environ[ARIA_ENV] = aria_path
        # del os
        return aria_path, new_flag


def safe_execute(default, exception, fnct, *args):
    """
    Execute safely a function by catching a specific exception

    Parameters
    ----------
    default :
        default action if exception
    exception :
        exception class
    fnct :
        callable function
    args :
        list of args

    Returns
    -------

    """
    try:
        return fnct(*args)
    except exception:
        return default


# TODO[FALLAIN]: If we want to convert AriaBaseClass into new style class, we
# have to change Singleton class before (can't raise new style classes)
class AriaBaseClass:
    """Main aria base class which defines settings and log messages"""

    use_restraint_weights = 0

    VERSION = 2.3
    VERSION_RELEASE = 2
    VERSION_FORMAT = '%.1f.%d'
    VERSION_STRING = VERSION_FORMAT % (VERSION, VERSION_RELEASE)

    display_warnings = 1
    display_messages = 1
    display_deprecated = 1
    display_debug = 0

    warnings_as_errors = 0

    wrap_lines = 1
    line_length = 80
    description_length = 100
    cache = 1
    save_memory = 1
    log_file = None
    # BB extendNmr
    log_gui = None
    log_stdout = 1
    logger = LOG
    # BB
    check_type.active = 0
    verbose_level = 0

    # if Aria root path has not been set yet, do it now
    has_root = 1 if 'install_path' in locals() and 'data_path' in locals() \
        else 0

    if not has_root:

        import sys
        import os

        install_path, new_flag = get_aria_root()

        if new_flag:
            data_path = pkgr.resource_filename(MODULE_NAME, 'data')
            toppar_path = pkgr.resource_filename(MODULE_NAME, 'cns/toppar')
            protocols_path = pkgr.resource_filename(MODULE_NAME,
                                                    'cns/protocols')
            analysis_path = pkgr.resource_filename(MODULE_NAME,
                                                   'cns/protocols/analysis')

            cns_directories = {'toppar': toppar_path,
                               'protocols': protocols_path,
                               'analysis': analysis_path}
        else:
            data_path = os.path.join(install_path, 'src/py/data')
            toppar_path = os.path.join(install_path, 'cns/toppar')
            protocols_path = os.path.join(install_path, 'cns/protocols')
            analysis_path = os.path.join(protocols_path, 'analysis')

            cns_directories = {'toppar': toppar_path,
                               'protocols': protocols_path,
                               'analysis': analysis_path}
        del os
        del sys

    def __init__(self, settings=None, name=None):
        """
        'name': every class which is inherited from AriaBaseClass can
        be assigned with its own name. it will be used when displaying
        warnings, errors, messages etc.
        """
        self.logger = logging.getLogger(self.__module__)

        if settings is not None:
            self.setSettings(settings)
        else:
            self.__settings = None

        if name is not None:
            self._set_name(name)
        
        if self.__class__.display_debug:
            logging.getLogger().setLevel(logging.DEBUG)

    def __setstate__(self, d):
        d['logger'] = logging.getLogger(self.__module__)
        self.__dict__.update(d)

        # if self.__class__.display_debug:
        #     logging.getLogger().setLevel(logging.DEBUG)

    def __getstate__(self):
        """
        Remove logger attribute in order to pickle every ariabase object instance
        """
        d = dict(self.__dict__)
        if 'logger' in d:
            del d['logger']
        return d

    @staticmethod
    def get_version_string():
        return AriaBaseClass.VERSION_FORMAT % (
            AriaBaseClass.VERSION, AriaBaseClass.VERSION_RELEASE)

    def _set_name(self, name):
        check_string(name)
        self._name = name

    def deprecated(self, msg):
        """
        can be used to display information about code-changes etc.

        Parameters
        ----------
        msg :
            

        Returns
        -------

        """

        if self.display_deprecated:
            prefix = self.__compile_name('DEPRECATED')
            print(self.__format(prefix, msg))

    def debug(self, msg):
        """
        checks the class variable display_debug. if non-zero
        'msg' is displayed.

        Parameters
        ----------
        msg :
            

        Returns
        -------

        """

        if self.__class__.display_debug:
            self.message(msg, prefix='DEBUG')

    def error(self, exception=None, error='', msg=""):
        """
        Error log

        Parameters
        ----------
        exception :
             (Default value = None)
        error :
             (Default value = '')
        msg :
             (Default value = None)

        Returns
        -------

        
        """

        import inspect

        msg = str(msg) if msg else ''

        if exception is None:
            exception = Exception

        # shut-down job manager

        self.shutdown()
        frame = None
        try:
            frame = inspect.currentframe().f_back.f_back
            have_frame = 1
        except Exception as excmsg:
            self.debug(excmsg)
            have_frame = 0

        if have_frame:
            code = frame.f_code
            func_name = code.co_name
            filename = code.co_filename
            lineno = frame.f_lineno

            descr = 'File "%s", line %d in %s\n%s'
            msg += descr % (filename, lineno, func_name, str(error))

        # msg = 'USER ERROR <%s> \n' % str(self.__class__) + msg
        self.message(msg, 'ERROR')
        # self.__log(msg)

        raise exception(msg)

    def __format(self, tag, msg):

        check_string(tag)
        check_string(msg)

        from . import tools as tools

        if self.wrap_lines:
            lines = tools.make_block(msg, self.line_length - len(tag) - 8)
        else:
            lines = [msg]

        lines = tools.indent(lines, tag, extra=9)

        return lines

    def __compile_name(self, prefix):
        if hasattr(self, '_name'):
            name = self._name
        else:
            name = self.__class__.__name__

        return ' '.join(filter(None, (prefix, "[" + name + "]"))) + ': '

    def __print(self, prefix, msg, verbose_level, func=None):

        if verbose_level <= self.verbose_level or self.display_debug:
            lvl = logging.getLevelName(prefix)
            lines = self.__format(self.__compile_name(""), msg)
            if self.log_gui:
                self.log_gui.write(lines + '\n')
            if self.log_stdout:
                with PRINT_LOCK:
                    kw = {"extra": {
                        "abc_name": func.co_filename,
                        "abc_funcName": func.co_name,
                        "abc_lineno": func.co_firstlineno}}
                    self.logger.log(lvl, lines, **kw)
            # self.__log(lines)

    def __log(self, s):
        if self.log_file is not None:
            self.log_file.write(s + '\n')
            self.log_file.flush()

    def message(self, msg, prefix="INFO", verbose_level=VL_STANDARD):
        """
        Message log

        Parameters
        ----------
        msg :
            
        prefix :
             (Default value = 'INFO')
        verbose_level :
             (Default value = VL_STANDARD)

        Returns
        -------

        
        """
        func = inspect.currentframe().f_back.f_code
        if self.display_messages:
            self.__print(prefix, str(msg), verbose_level, func=func)

    def warning(self, msg, verbose_level=VL_STANDARD):
        """
        Warning log

        Parameters
        ----------
        msg :
            
        verbose_level :
             (Default value = VL_STANDARD)

        Returns
        -------

        
        """
        func = inspect.currentframe().f_back.f_code
        if self.display_warnings:

            msg = str(msg)

            if self.warnings_as_errors:
                self.error(msg=msg)
            else:
                # TODO: Find a way to use prefix correctly
                # prefix = self.__compile_name('WARNING')
                self.__print('WARNING', str(msg), verbose_level, func=func)

    def halt(self):
        """aborts ARIA"""

        import time
        import sys

        msg = '\nARIA halted at %s.\n' % str(time.ctime())

        # close log-file

        if self.log_file is not None:
            self.log_file.write(msg)
            self.log_file.close()

        print(msg)

        sys.exit(0)

    # TODO: disabled. Should we delete it ?
    @staticmethod
    def shutdown():
        """Shutdown job manager"""
        # from .Singleton import ProjectSingleton
        # try:
        #     project = ProjectSingleton()
        #     job_manager = project.getStructureEngine().getJobScheduler()
        #     job_manager.shutdown()
        #
        # except:
        #     from . import tools as tools
        #     print(tools.last_traceback())
        #     print('Could not shutdown job-manager.')
        #     s = 'If this message occurs more than once, the method' + \
        #         ' AriaBaseClass.error has not been used properly!' + \
        #         ' E.g. within a try-except statement.'
        #     print(s)
        # return

    def setSettings(self, s):
        """
        Define settings attribute

        Parameters
        ----------
        s :
            

        Returns
        -------

        
        """

        check_type(s, 'Settings')
        self.__settings = s

    # TODO: maybe use property decorator instead of this method
    def getSettings(self):
        """Get settings attribute"""

        if self.__settings is None:
            s = '%s: Settings are None.' % self.__class__.__name__
            self.error(msg=s)

        return self.__settings
