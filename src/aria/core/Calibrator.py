"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""

from .ariabase import AriaBaseClass
from .Settings import Settings, PositiveFloat, ChoiceEntity, PeakType, \
    YesNoChoice
from .xmlutils import XMLElement, XMLBasePickler

from . import TypeChecking as TCheck


class DistanceCutoffEntity(PositiveFloat):
    def __init__(self):
        descr = ''
        err_msg = 'Distance-cutoff must be positive float or None'

        PositiveFloat.__init__(self, description=descr,
                               error_message=err_msg)

    def is_valid(self, value):
        v = PositiveFloat.is_valid(self, value)
        return v or (value is None)


class CalibratorSettings(Settings):
    order = ('distance_cutoff', 'volume_or_intensity', 'estimator',
             'relaxation_matrix', 'error_estimator', 'use_bounds')

    def create(self):
        CE = ChoiceEntity

        keywords = {'distance_cutoff': DistanceCutoffEntity(),
                    'volume_or_intensity': PeakType(),
                    'estimator': CE(('ratio_of_averages',)),
                    'relaxation_matrix': YesNoChoice(),
                    'error_estimator': CE(('distance', 'intensity',)),
                    # Malliavin/Bardiaux rMat
                    'use_bounds': YesNoChoice()}

        return keywords

    def create_default_values(self):
        defaults = {'distance_cutoff': 6.0,
                    'volume_or_intensity': 'volume',
                    'estimator': 'ratio_of_averages',
                    'relaxation_matrix': 'no',
                    'error_estimator': 'distance',
                    'use_bounds': 'no'}

        return defaults


class Calibrator(AriaBaseClass):
    def __init__(self, settings, model):

        TCheck.check_type(settings, 'CalibratorSettings')
        TCheck.check_type(model, 'NOEModel')

        AriaBaseClass.__init__(self)

        self.setSettings(settings)
        self.setModel(model)

    def setModel(self, model):

        TCheck.check_type(model, 'NOEModel')

        self.__model = model

    def getModel(self):
        return self.__model

    def calculateEstimator(self, peaks, ensemble, store_analysis=0,
                           use_cutoff=1):
        """
        This method should be called if you want the Calibrator to
        calculate the calibration factor for the data-set 'spectrum'.
        'peaks': list of AriaPeaks. Every restaint in that list
        is only used to query the experimental peak-volume/intensity
        and (if 'store_analysis' is non-zero) to store the calculated
        peaksize in its analysis section.
        
        'store_analysis': if non-zero, calculated volumes are stored
        in the 'analysis' section of every AriaPeak.
        
        if 'use_cutoff' is zero, all distances are used to calculate
        the estimator.

        Parameters
        ----------
        peaks :
            
        ensemble :
            
        store_analysis :
             (Default value = 0)
        use_cutoff :
             (Default value = 1)

        Returns
        -------

        """

        TCheck.check_type(peaks, TCheck.LIST, TCheck.TUPLE)

        import numpy

        if not len(peaks):
            self.error(ValueError, 'No peaks specified.')

        # TCheck.check_elements(peaks, 'AriaPeak')
        TCheck.check_elements(peaks, 'AbstractPeak')
        TCheck.check_type(ensemble, 'StructureEnsemble')

        from .Datum import Datum

        settings = self.getSettings()

        if settings['volume_or_intensity'] == 'volume':
            exp_peak_sizes = [p.getReferencePeak().getVolume()[0]
                              for p in peaks]
        else:
            exp_peak_sizes = [p.getReferencePeak().getIntensity()[0]
                              for p in peaks]

        f = self.getModel().calculatePeaksize

        model_peak_sizes = numpy.array([f(p, ensemble) for p in peaks])

        # consider only crosspeaks with volume/intensity
        # larger than NOE_cutoff.

        if use_cutoff:
            NOE_cutoff = settings['distance_cutoff'] ** (-6.)
        else:
            NOE_cutoff = 0.

        strong_NOEs = numpy.greater_equal(model_peak_sizes, NOE_cutoff)

        sum_noe_model = numpy.sum(numpy.compress(strong_NOEs,
                                                 model_peak_sizes))
        sum_noe_exp = numpy.sum(numpy.compress(strong_NOEs,
                                               exp_peak_sizes),
                                dtype=numpy.float64)

        # if there are no NOEs larger than NOE_cutoff,
        # return None.

        if sum_noe_model <= 1.e-30:
            return None

        # calculate estimator
        if settings['estimator'] == 'ratio_of_averages':
            factor = sum_noe_exp / sum_noe_model

        # store calculated peak-size

        if store_analysis:
            calculated_peak_sizes = model_peak_sizes * factor

            for i in range(len(peaks)):
                d = Datum(calculated_peak_sizes[i], None)
                peaks[i].analysis.setCalculatedPeaksize(d)

        return factor


class CalibratorXMLPickler(XMLBasePickler):
    def _xml_state(self, calib_settings):
        """
        Define calibration xml tag from CalibratorSettings object

        Parameters
        ----------
        calib_settings : CalibratorSettings object
            

        Returns
        -------

        
        """
        xml_elt = XMLElement(tag_order=CalibratorSettings.order)

        for setting in CalibratorSettings.order:
            setattr(xml_elt, setting, calib_settings.get(setting))

        return xml_elt

    def load_from_element(self, xml_elt):
        """
        Fill CalibratorSettings settings by casting related entities with
         values from xml_elt

        Parameters
        ----------
        xml_elt : XMLElement object
            calibration xml tag object

        Returns
        -------

        
        """
        settings = CalibratorSettings()
        # Initialize all fields to default values
        settings.reset()

        # TODO: maybe put code below in XMLBasePickler and call super method
        for setting in settings.keys():
            if hasattr(xml_elt, setting):
                settings[setting] = settings.getEntity(setting).cast(
                    getattr(xml_elt, setting))

        return settings


CalibratorSettings._xml_state = CalibratorXMLPickler()._xml_state
