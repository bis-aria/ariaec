# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""

from .ariabase import AriaBaseClass, NO
from .TypeChecking import check_type
from .Settings import YesNoChoice, ChoiceEntity, AutoInteger, Settings
from .xmlutils import XMLBasePickler, XMLElement
from .SuperImposer import SuperImposer
from .StructureEnsemble import StructureEnsemble
from hdbscan import HDBSCAN
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.metrics import silhouette_score
from .Singleton import ProjectSingleton

import numpy as np
# from numpy import *


class ClusteringSettings(Settings):
    """Aria clustering settings"""

    order = ('enabled', 'method', 'n_clusters', 'mask', 'sort_criterion')

    def create(self):
        """
        Initialize settings entities

        Returns
        -------
        settings :  :py:class:`dict` of :py:class:`.Settings.Entity`
        """

        return {
            'enabled': YesNoChoice(description='''\
Clustering step group generated structures into clusters. Cluster ensemble with\
the lowest energy will then be selected at the end of each iteration.'''),
            'method': ChoiceEntity(
                ('kmeans', 'hclust', 'hdbscan'),
                description="Clustering method used."),
            'n_clusters': AutoInteger(description="Number of clusters"),
            'mask': ChoiceEntity(("CA", "CB"),
                                 description="Atom coordinate mask"),
            'sort_criterion':  ChoiceEntity(("total_energy", "korp"),
                                 description="Criterion for choosing best cluster"),
      
        }

    def create_default_values(self):
        """
        Create default values

        Returns
        -------
        default_values : :py:class:`dict`
        """
        return {
            'enabled': NO,
            'method': 'kmeans',
            'n_clusters': 2,
            'mask': "CA",
            'sort_criterion': "total_energy"
        }


class ClusteringXMLPickler(XMLBasePickler):
    """ARIA XML project pickler for clustering tag"""

    def _xml_state(self, settings):
        """
        Define clustering xml tag from ClusteringSettings object

        Parameters
        ----------
        settings : :py:class:`ClusteringSettings`
            
        Returns
        -------
        xml_elt : :py:class:`.xmlutils.XMLElement`
            clustering xml tag element
        """

        xml_elt = XMLElement(tag_order=ClusteringSettings.order)

        for setting in ClusteringSettings.order:
            setattr(xml_elt, setting, settings.get(setting))

        return xml_elt

    def load_from_element(self, xml_elt):
        """
        Instantiate clustering settings from XMLElement

        Parameters
        ----------
        xml_elt : :py:class:`.xmlutils.XMLElement`
            clustering xml tag element

        Returns
        -------
        settings : :py:class:`ClusteringSettings`
            updated clustering settings
        """
        # Can't use settings as a class attribute. Otherwise, the reset
        # method will reset ALL clustering settings in the userdict
        settings = ClusteringSettings()
        # Reset to default values
        settings.reset()

        for setting in settings.keys():
            if hasattr(xml_elt, setting):
                settings[setting] = settings.getEntity(setting).cast(
                    getattr(xml_elt, setting))

        return settings


class Clustering(AriaBaseClass, object):
    """Clustering class for structure ensemble"""

    REPORT = """\
## Cluster {cluster_id}
## Total number of structures           : {cluster_size}
## Mean {criterion} of best structures : {avg_energy}
"""

    def __init__(self, settings):

        check_type(settings, 'ClusteringSettings')

        AriaBaseClass.__init__(self)

        self.setSettings(settings)
        self.cluster_ensembles = []
        self._n_clusters = None
        self._cluster_labels = None

    @property
    def n_clusters(self):
        """Attribute related to number of cluster parameter"""
        if self.getSettings()['n_clusters'] == 'auto' and \
                self.getSettings()['method'] == 'kmeans' and self._n_clusters:
            return self._n_clusters
        elif self._n_clusters:
            return self._n_clusters
        else:
            try:
                return int(self.getSettings()['n_clusters'])
            except ValueError:
                self.error(ValueError, msg='Wrong cluster number')

    @staticmethod
    def _silhouette_scores(matrix, range_n_clusters=(2, 3, 4, 5, 6)):
        """
        Silhouette analysis to help determine the number of cluster using
        the KMeans approach

        Parameters
        ----------
        matrix : :py:class:`numpy.array`
            matrix used for the clustering approach
        range_n_clusters : :py:class:`list`, optional
            number of cluster parameter tested (Default value = (2, 3, 4, 5, 6))

        Returns
        -------
        silhouette_avgs : :py:class:`dict`
            Associate silhouette score foreach number of cluster

        Notes
        -----
        The KMeans algorithm is initialized for each cluster size tested with
        a random generator seed of 10 for reproducibility purpose.
        The silhouette_score gives the average value for all the samples.
        This gives a perspective into the density and separation of the formed
        clusters.
        """
        silhouette_avgs = {}
        for idx, n_clusters in enumerate(range_n_clusters):
            # Initialize the clusterer with n_clusters value and a random
            # generator seed of 10 for reproducibility.
            clusterer = KMeans(n_clusters=n_clusters, random_state=10)
            cluster_labels = clusterer.fit_predict(matrix)

            # The silhouette_score gives the average value for all the samples.
            # This gives a perspective into the density and separation of the
            # formed clusters
            silhouette_avgs[n_clusters] = silhouette_score(matrix,
                                                           cluster_labels)

        return silhouette_avgs

    def _auto_n_clusters(self, matrix):
        """
        Use silhouette analysis, elbow score, Bayesian information criteria, ...
        to define number of clusters used with KMeans if n_cluster setting
        set to 'auto'

        Parameters
        ----------
        matrix : :py:class:`numpy.array`
            matrix used for the clustering approach

        Returns
        -------
        n_cluster : :py:class:`int`
            Number of cluster computed by the selection criteria
        
        """

        # TODO: define setting related to criteria used
        criteria = 'silhouette'

        try:
            scores = getattr(self, '_%s_scores' % criteria)(matrix)
        except Exception as msg:
            self.error(Exception, msg='%s\nCriteria %s not known' % (msg,
                                                                     criteria))
            return None

        return int(max(scores, key=lambda key: scores[key]))

    def cluster(self, molecule, ensemble):
        """
        Cluster all structures based on molecule coordinates defined by a mask

        Parameters
        ----------
        molecule : :py:class:`.Molecule.Molecule`
        ensemble : :py:class:`.StructureEnsemble.StructureEnsemble`
        """
        project = ProjectSingleton()
        clumeth = self.getSettings()['method']
        coor = ensemble.getCoordinates()
        ensinfo = ensemble.getInfo()
        nbest = ensemble.getSettings()['number_of_best_structures']
        self.message('Computing {0} clustering on generated structures'.format(
            clumeth
        ))
        seed = project.getStructureEngine().getMDParameters()['random_seed']

        # First, get Korp energies
        if self.getSettings()['sort_criterion'] == "korp":
            korp_valid = ensemble.get_korp_scores()
            if not korp_valid:
                self.getSettings()['sort_criterion'] = 'total_energy'
                self.warning("""Using 'total_energy' instead of KORP""")

        # Matrix containing all CA atomids for all residues for all chains
        mask = [a.getId() for c in molecule.get_chains() for r in
                c.getResidues()
                for a in r.getAtoms()
                if a.getName() == self.getSettings()['mask']]

        # Align ensemble with molecule based on the selected mask
        si = SuperImposer(ensemble, molecule)
        si.getSettings()['number_of_best_structures'] = 'all'
        si._fit(mask)

        # get fitted matrix of coordinates
        # (n matrix for n structures in the ensemble)
        ncoor = si.getFittedCoordinates()
        ncoor = np.take(ncoor, mask, axis=1)

        # ns: nstructures; na: natoms; xyz: ncoordinates
        ns, na, xyz = ncoor.shape

        ncoor.shape = (ns, na * xyz)

        if clumeth == 'kmeans':
            # Fix random_state for reproductibility
            clu = KMeans(n_clusters=self.n_clusters, random_state=int(seed))
        elif clumeth == 'hclust':
            clu = AgglomerativeClustering(
                n_clusters=self.n_clusters)
        elif clumeth == 'hdbscan':
            clu = HDBSCAN(min_cluster_size=nbest)
            # TODO: Seems the last cluster with HDBSCAN corresponds to the
            # outlier cluster. Have to check if this is effectively the case in
            # order to delete this cluster of the cluster list
        else:
            # Since KMeans is the faster algorithm, use it as default clustering
            clu = KMeans(n_clusters=self.n_clusters, random_state=10)

        clu.fit(ncoor)

        self._cluster_labels = clu.labels_

        ensemble.getSettings()['number_of_best_structures'] = nbest

        self.cluster_ensembles = []
        for a in set(self._cluster_labels):
            # Select each cluster and save on cluster_ensembles list
            coorc = np.compress(self._cluster_labels == a, coor, axis=0)
            infoc = np.compress(self._cluster_labels == a, ensinfo, axis=0)

            se_settings = ensemble.getSettings()
            se_settings['sort_criterion'] = self.getSettings()['sort_criterion']
            se = StructureEnsemble(se_settings)

            se.setCoordinates(coorc)
            se.setInfo(infoc)
            se.sort()

            self.cluster_ensembles.append(se)

    def getLowestEnergyClusterEnsemble(self):
        """
        Select ensemble with the lowest average total energy. If the density of
        the cluster is lowest or equal than the number of best structures
        option it another cluster has been selected

        Returns
        -------
        best_cluster : :py:class:`.StructureEnsemble.StructureEnsemble`
            Structure ensemble with the lowest energy
        """

        energies = []

        for a in range(0, len(self.cluster_ensembles)):
            ens = self.cluster_ensembles[a]

            nstructs = len(ens)

            nbest = ens.getSettings()['number_of_best_structures'] if \
                ens.getSettings()['number_of_best_structures'] != 'all' else \
                nstructs

            criterion = self.getSettings()['sort_criterion']

            # Get average tot_energy of structures in cluster ensemble IIF
            # number of structures in the ensemble is higher than the number
            # of best structures. Otherwise return None
            # With this filter we avoid clusters with less structures than
            # the number of structures asked by the user
            avg_tot_ener = np.mean(
                [d[criterion] for d in ens.getInfo()[:, 1]][:nbest]) if \
                nstructs >= nbest else None

            energies.append(avg_tot_ener)

        best = np.argsort(np.array(energies, dtype=np.float))[0]

        return self.cluster_ensembles[best]

    def write_clusters(self, molecule, root):
        """
        Write cluster ensemble pdb and list files

        Parameters
        ----------
        molecule : :py:class:`.Molecule.Molecule`
        root : :py:class:`str`
            Iteration folder path used to write files

        Returns
        -------
        report : :py:class:`str`
            Updated report
        """
        self.message("Write cluster ensemble and pdb files at {0}".format(root))

        report = ""

        criterion = self.getSettings()['sort_criterion']
   
        for a in range(0, len(self.cluster_ensembles)):

            ens = self.cluster_ensembles[a]
            clusize = ens.getCoordinates().shape[0]
            sic = SuperImposer(ens, molecule)

            nbest = ens.getSettings()['number_of_best_structures'] if \
                ens.getSettings()['number_of_best_structures'] != 'all' else \
                clusize

            filename = "%s%d.pdb" % (root, a)

            avg_tot_ener = "%.1f" % np.mean(
                [d[criterion] for d in ens.getInfo()[:, 1]][:nbest]) if \
                clusize >= nbest else 'None'

            report += self.REPORT.format(cluster_id=a, cluster_size=clusize,
                                         avg_energy=avg_tot_ener, criterion=criterion)

            # Avoid writing report for small or clusters with only 1 structure
            if clusize >= nbest:
                report = report[:-1] + sic.doReport()

            sic.write_ensemble(filename)

            # write lists

            filename = "%s%d.list" % (root, a)

            file_list = ens.getFiles()

            try:
                f = open(filename, 'w')
            except IOError as err:
                self.warning("I/O error({number}): {msg}".format(
                    number=err.errno, msg=err.strerror))
            except Exception as err:
                self.warning('Error {0}.\nCould not create {1}.'.format(
                    err, filename))
                return None

            s = '\n'.join(file_list)

            try:
                f.write(s)
                f.close()
            except IOError as err:
                self.warning("I/O error({0}): {1}\n"
                             "Could not write cluster "
                             "PDB-file list ({2}).".format(err.errno,
                                                           err.strerror,
                                                           filename))
            except Exception as err:
                self.warning(
                    'Error {0}\nCould not write cluster '
                    'PDB-file list ({1}).'.format(err, filename))
                return None

        return report


ClusteringSettings._xml_state = ClusteringXMLPickler()._xml_state
