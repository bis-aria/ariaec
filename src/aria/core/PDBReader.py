# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""
from __future__ import absolute_import, division, print_function
from .ariabase import AriaBaseClass
from .Chain import TYPE_PROTEIN, TYPE_RNA, TYPE_DNA, TYPE_NONPOLYMER
from .Topology import TYPE_AMINO_ACID, TYPE_DNA_BASE, TYPE_RNA_BASE, \
    TYPE_NONBASE
from .ConversionTable import CNS_CONVENTION, IUPAC_CONVENTION  # ,

# TODO: since grepping of energies is hard-coded. changes in
# pdb-format could be implemented in a sub-class.

# DYANA_CONVENTION


BASE_TYPES = {TYPE_PROTEIN: TYPE_AMINO_ACID,
              TYPE_RNA: TYPE_RNA_BASE,
              TYPE_DNA: TYPE_DNA_BASE,
              TYPE_NONPOLYMER: TYPE_NONBASE}


class PDBReader(AriaBaseClass):
    """Aria pdb reader class"""

    def __init__(self, table=None):

        from .TypeChecking import check_type, NONE

        check_type(table, 'ConversionTable', NONE)

        AriaBaseClass.__init__(self)

        # from .ConversionTable import ConversionTable
        # from Scientific.IO.PDB import PDBFile
        from .scientific.PDB import PDBFile

        if table is None:
            from .ConversionTable import ConversionTable

            table = ConversionTable()

        self.table = table
        self.PDB = PDBFile

        self.base_types = BASE_TYPES

    def get_energy(self, filename, nrjtype='total_energy'):
        """
        Grep cns energy term in REMARK section

        Parameters
        ----------
        filename : str
            path of pdb file
        nrjtype : str
            energy field name (Default value = 'total_energy')

        Returns
        -------

        
        """

        import os

        pdbfile = self.PDB(os.path.expanduser(filename))
        eof = 0
        ttype, data = None, None

        # BARDIAUX 2.2 = add 'restraint_energy', 'noe_violations',
        # 'restraint_violations'
        criterion = {'total_energy': 0.,
                     'restraint_energy': 0.,
                     'noe_violations': 0.,
                     'restraint_violations': 0.,
                     'korp' : 0.}

        ln = 1
        while not eof:
            try:
                ttype, data = pdbfile.readLine()
                ln += 1

            except Exception as msg:
                from . import tools as tools
                self.warning(tools.last_traceback())
                self.error(
                    Exception,
                    error='Could not read PDB-file %s, line %d' % (
                        filename, ln), msg=msg)

            if ttype == 'END':
                eof = 1

            elif ttype == 'REMARK':

                if data.find('energies') >= 0:
                    # assumed here: special cns energy-header
                    criterion['total_energy'] = data[11:].split(',')[0]
                    rest_e = data[11:].split(',')[-5:]
                    criterion['restraint_energy'] = sum(
                        [float(e) for e in rest_e])
                    # return float(energy[type])

                if data.find('violations') >= 0:
                    criterion['noe_violations'] = data[13:].split(',')[0]
                    criterion['restraint_violations'] = sum(
                        [float(e) for e in data[13:].split(',')])

                if criterion.get(nrjtype) is None:
                    nrjtype = 'total_energy'
                return float(criterion[nrjtype])

        return None

    def read(self, filename, chain_types=None, outformat=CNS_CONVENTION):
        """
        Reads all (het)atoms in a PDB-file and returns a dict
        of dicts of the following hierachy:

        * segment level (keys are SEGIDs)
        * residue level (keys are residue numbers)
        * atom level (keys are atom names)

        Parameters
        ----------
        filename : str
            pdb file path
        chain_types : dict
            dictionary containing chain types. Supported types are in
            PDBReader.BASE_TYPES dict (Default value = None)
        outformat : str {IUPAC_CONVENTION, DYANA_CONVENTION, CNS_CONVENTION}
            atom naming convention for the output dict (Default value = CNS_CONVENTION)

        Returns
        -------

        
        """

        from .TypeChecking import check_string
        from .tools import string_to_segid
        from .OrderedDict import OrderedDict

        import os

        check_string(filename)

        if not self.table.has_format(outformat):
            self.error(ValueError, 'Atom naming "%s" not supported.'
                       % str(outformat))

        if chain_types is not None:
            for segid, chain_type in chain_types.items():
                # if not self.base_types.has_key(chain_type):
                if chain_type not in self.base_types:
                    m = 'Type "%s" for chain "%s" not supported.'
                    self.error(TypeError, m % (str(chain_type), str(segid)))

        pdbfile = self.PDB(os.path.expanduser(filename))

        eof = 0

        dict = OrderedDict()
        dict['total_energy'] = None
        # BARDIAUX 2.2
        dict['restraint_energy'] = None
        dict['noe_violations'] = None
        dict['restraint_violations'] = None

        ln = 1
        while not eof:
            try:
                linetype, data = pdbfile.readLine()
                ln += 1

            except Exception as msg:
                from . import tools as tools
                self.warning(tools.last_traceback())
                self.error(Exception,
                           error='Could not read PDB-file %s, line %d' % (
                               filename, ln), msg=msg)

            # PDB file terminated

            if linetype == 'END':
                eof = 1

            # try to grep energies

            elif linetype == 'REMARK':

                if data.find('energies') >= 0:
                    # assumed here: special cns energy-header

                    energy = data[11:].split(',')[0]
                    dict['total_energy'] = float(energy)
                    # BARDIAUX 2.2
                    rest_e = data[11:].split(',')[-5:]
                    dict['restraint_energy'] = float(
                        sum([float(e) for e in rest_e]))

                if data.find('violations') >= 0:
                    dict['noe_violations'] = float(data[13:].split(',')[0])
                    dict['restraint_violations'] = sum(
                        [float(e) for e in data[13:].split(',')])

            # read atom / hetatom information

            elif linetype == 'ATOM' or linetype == 'HETATM':

                residue_type = data['residue_name'].strip()

                atom_name = data['name'].strip()
                atom_coor = data['position']  # .array

                residue_number = data['residue_number']

                segid = string_to_segid(data['segment_id'])

                # if not dict.has_key(segid):
                if segid not in dict:
                    dict[segid] = {}

                # if not dict[segid].has_key(residue_number):
                if residue_number not in dict[segid]:
                    dict[segid][residue_number] = {}
                    dict[segid][residue_number]['residue_type'] = residue_type

                # if dict[segid][residue_number].has_key(atom_name):
                if atom_name in dict[segid][residue_number]:
                    m = 'Atom "%s" ocurred twice in residue "%s%d".' \
                        % (atom_name, residue_type, residue_number)

                    self.error(ValueError, m)

                dict[segid][residue_number][atom_name] = atom_coor

        # convert residue and atom names to IUPAC format

        if chain_types is None:
            chain_types = {}

        for segid in dict.keys():

            if not len(segid) == 4:
                continue

            # if not chain_types.has_key(segid):
            if segid not in chain_types:
                chain_types[segid] = None
            else:
                base_type = self.base_types[chain_types[segid]]

            for number, residue in dict[segid].items():

                residue_type = residue['residue_type']

                if chain_types[segid] is None:
                    for chain_type in [TYPE_PROTEIN, TYPE_DNA, TYPE_RNA]:
                        base_type = self.base_types[chain_type]
                        args = (residue_type, outformat, IUPAC_CONVENTION,
                                base_type, 0)
                        if self.table.convert_residue(*args):
                            chain_types[segid] = chain_type
                            break
                    else:
                        chain_types[segid] = TYPE_NONPOLYMER
                        base_type = TYPE_NONBASE

                if base_type == TYPE_NONBASE:
                    break

                atom_names = residue.keys()
                atom_names.remove('residue_type')
                atom_coors = [residue[x] for x in atom_names]
                for x in atom_names:
                    del residue[x]

                args = (residue_type, atom_names, outformat, IUPAC_CONVENTION,
                        base_type, 0)
                new_names = self.table.convert_atoms(*args)
                if new_names is not None:
                    atom_names = new_names

                for x, y in zip(atom_names, atom_coors):
                    residue[x] = y

                args = (residue_type, outformat, IUPAC_CONVENTION, base_type, 0)

                new_type = self.table.convert_residue(*args)
                if new_type is not None:
                    residue_type = new_type

                residue['residue_type'] = residue_type

            dict[segid]['chain_type'] = chain_types[segid]

        return dict

    def write(self, dict, filename, input_format=IUPAC_CONVENTION,
              output_format=IUPAC_CONVENTION):
        """
        Writes a dict of dicts of the hierarchy {<seg_id>: {<residue_number>:
        {'residue_type': <three-letter-code>, <atom-name>: <coordinates>}}}
        to a file in PDB format.

        Parameters
        ----------
        dict :
            
        filename :
            
        input_format :
             (Default value = IUPAC_CONVENTION)
        output_format :
             (Default value = IUPAC_CONVENTION)

        Returns
        -------

        """

        table = self.table

        if not table.has_format(input_format):
            self.error(ValueError, 'Atom naming "%s" not supported.'
                       % str(input_format))

        if not table.has_format(output_format):
            self.error(ValueError, 'Output format "%s" not supported.'
                       % str(output_format))

        from .tools import string_to_segid

        import os

        pdbfile = self.PDB(os.path.expanduser(filename), 'w')

        serial_number = 1

        for segid in dict.keys():

            if len(segid) > 4:
                continue

            chain_type = dict[segid]['chain_type']
            # if not self.base_types.has_key(chain_type):
            if chain_type not in self.base_types:
                m = 'Chain "%s" has type "%s", this type is not supported.'
                self.error(TypeError, m % (str(segid), str(chain_type)))

            base_type = self.base_types[dict[segid]['chain_type']]

            data = {'segment_id': string_to_segid(segid)}

            residues = dict[segid]
            residue_numbers = residues.keys()
            residue_numbers.remove('chain_type')
            residue_numbers.sort()

            for residue_number in residue_numbers:

                residue = residues[residue_number]

                residue_type = residue['residue_type']

                if base_type is not TYPE_NONBASE:
                    args = (residue_type, input_format, output_format,
                            base_type, 0)
                    new_type = table.convert_residue(*args)
                    if new_type is not None:
                        residue_type = new_type

                data['residue_name'] = residue_type
                data['residue_number'] = residue_number

                atom_names = residue.keys()
                atom_names.remove('residue_type')

                atom_coors = [residue[x] for x in atom_names]

                if base_type is not TYPE_NONBASE:
                    args = (residue['residue_type'], atom_names, input_format,
                            output_format, base_type, 0)
                    new_names = table.convert_atoms(*args)

                    if new_names is not None:
                        atom_names = new_names

                for atom_name, atom_coor in zip(atom_names, atom_coors):
                    # enforce correct alignment of atom names

                    atom_name = ' ' * (len(atom_name) < 4) + atom_name

                    data['name'] = atom_name
                    data['position'] = atom_coor
                    data['serial_number'] = serial_number

                    serial_number += 1

                    pdbfile.writeLine('ATOM', data)

        pdbfile.close()

    def write_ensemble(self, header, dict, filename,
                       input_format=IUPAC_CONVENTION,
                       output_format=IUPAC_CONVENTION):
        """
        Writes a dict of dicts of the hierarchy {<seg_id>: {<residue_number>:
        {'residue_type': <three-letter-code>, <atom-name>: <coordinates>}}}
        to a pdbfile in PDB format.

        Parameters
        ----------
        header :
            
        dict :
            
        filename :
            
        input_format :
             (Default value = IUPAC_CONVENTION)
        output_format :
             (Default value = IUPAC_CONVENTION)

        Returns
        -------

        """

        table = self.table

        if not table.has_format(input_format):
            self.error(ValueError, 'Atom naming "%s" not supported.'
                       % str(input_format))

        if not table.has_format(output_format):
            self.error(ValueError, 'Output format "%s" not supported.'
                       % str(output_format))

        from .tools import string_to_segid

        import os

        pdbfile = self.PDB(os.path.expanduser(filename), 'w')

        pdbfile.writeComment(header)

        for model in dict.keys():

            data = {'serial_number': model}
            pdbfile.writeLine('MODEL', data)

            serial_number = 1

            for segid in dict[model].keys():

                if len(segid) > 4:
                    continue

                chain_type = dict[model][segid]['chain_type']
                # if not self.base_types.has_key(chain_type):
                if chain_type not in self.base_types:
                    m = 'Chain "%s" has type "%s", this type is not supported.'
                    self.error(TypeError, m % (str(segid), str(chain_type)))

                base_type = self.base_types[dict[model][segid]['chain_type']]

                data = {'segment_id': string_to_segid(segid)}

                residues = dict[model][segid]
                residue_numbers = residues.keys()
                residue_numbers.remove('chain_type')
                residue_numbers.sort()

                for residue_number in residue_numbers:

                    residue = residues[residue_number]

                    residue_type = residue['residue_type']

                    if base_type is not TYPE_NONBASE:
                        args = (residue_type, input_format, output_format,
                                base_type, 0)
                        new_type = table.convert_residue(*args)
                        if new_type is not None:
                            residue_type = new_type

                    data['residue_name'] = residue_type
                    data['residue_number'] = residue_number

                    atom_names = residue.keys()
                    atom_names.remove('residue_type')

                    atom_coors = [residue[x][0] for x in atom_names]
                    atoms_rmsfs = [residue[x][1] for x in atom_names]

                    if base_type is not TYPE_NONBASE:
                        args = (
                            residue['residue_type'], atom_names, input_format,
                            output_format, base_type, 0)
                        new_names = table.convert_atoms(*args)

                        if new_names is not None:
                            atom_names = new_names

                    for atom_name, atom_coor, atom_rmsf in zip(atom_names,
                                                               atom_coors,
                                                               atoms_rmsfs):
                        # enforce correct alignment of atom names

                        atom_name = ' ' * (len(atom_name) < 4) + atom_name

                        data['name'] = atom_name
                        data['position'] = atom_coor
                        data['serial_number'] = serial_number
                        data['temperature_factor'] = atom_rmsf

                        serial_number += 1

                        pdbfile.writeLine('ATOM', data)

            pdbfile.writeLine('ENDMDL', '')

        pdbfile.close()
