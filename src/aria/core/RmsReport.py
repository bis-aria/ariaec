# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux                                            ..
..               Structural Bioinformatics, Institut Pasteur, Paris           ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""
from .ariabase import AriaBaseClass
import os
import numpy as np

RMS_TEXT_REPORT = 'noe_restraints.rms'
RMS_PS_REPORT = 'rms_analysis.ps'
RMS_PDF_REPORT = 'rms_analysis.pdf'

RMS_PS_MAP_LEGEND = 'rms_legend.eps'
RMS_PS_MAP = 'rms_2D_residue_map.eps'
RMS_PS_PROFILE = 'rms_residue_profile.eps'

try:
    import matplotlib
    _USE_MATPLOTLIB = 1
    matplotlib.use('pdf', warn=False)
except ImportError:
    matplotlib = None
    _USE_MATPLOTLIB = 0


class RmsReport(AriaBaseClass):
    """RMS Report"""
    def __init__(self, peaks, iteration_n, text_path, graphics_path):

        AriaBaseClass.__init__(self)
        self.peaks = peaks
        self.graphics_path = graphics_path
        self.text_report = os.path.join(text_path, RMS_TEXT_REPORT)
        self.ps_report = os.path.join(graphics_path, RMS_PS_REPORT)
        self.pdf_report = os.path.join(self.graphics_path, RMS_PDF_REPORT)
        self.iteration_n = iteration_n

        self.segids = []
        self.dimer = None
        self.rmat = None
        self.mol = None
        self.rms = None
        self.rms_u = None
        self.rms_a = None
        self.profile = None
        self.ids = None
        self.rms_matrix = None

    @staticmethod
    def getOverallRms(peaks):
        """
        

        Parameters
        ----------
        peaks :
            

        Returns
        -------

        
        """

        if not len(peaks):
            return 0.

        viol = np.power(np.array(
            [p.analysis.getUpperBoundViolation()[0] for p in peaks]), 2)
        rms = sum(viol) / len(viol)
        rms = np.sqrt(rms)

        return rms

    @staticmethod
    def getRmsPerResidue(peaks):
        """
        

        Parameters
        ----------
        peaks :
            

        Returns
        -------

        
        """

        violations = {}
        weights = {}

        for p in peaks:

            target_dist = p.getUpperBound()
            for c in p.getActiveContributions():
                r = c.getSpinSystems()[0].getAtoms()[0].getResidue()
                r1 = r.getNumber()
                #                 s1 = r.getChain().getSegid()

                r = c.getSpinSystems()[1].getAtoms()[0].getResidue()
                r2 = r.getNumber()

                #                 s2 = r.getChain().getSegid()

                d = c.getAverageDistance()[0]
                w = c.getWeight()
                viol = d - target_dist

                #             if w >0:
                #                 print p.getId(),
                # p.analysis.getUpperBoundViolation()[0], d, target_dist,
                # (viol), w

                violations.setdefault(r1, [])
                weights.setdefault(r1, [])
                violations[r1].append(viol)
                weights[r1].append(w)

                if r1 != r2:
                    violations.setdefault(r2, [])
                    weights.setdefault(r2, [])
                    violations[r2].append(viol)
                    weights[r2].append(w)

        rms_per_resid = {}

        for r, v in violations.items():
            viol = np.array(v)
            viol = np.power(
                viol * np.greater(viol, 0), 2) * np.array(weights[r])
            rms = sum(viol) / sum(weights[r])
            rms = np.sqrt(rms)
            rms_per_resid[r] = rms

        return rms_per_resid

    def getRmsPerResiduePair(self, peaks):
        """
        

        Parameters
        ----------
        peaks :
            

        Returns
        -------

        
        """

        residues = {}
        weights = {}

        for p in peaks:

            target_dist = p.getUpperBound()
            for c in p.getActiveContributions():
                r1 = c.getSpinSystems()[0].getAtoms()[0].getResidue()
                # r1 = r.getNumber()
                # s1 = r.getChain().getSegid()

                r2 = c.getSpinSystems()[1].getAtoms()[0].getResidue()
                # r2 = r.getNumber()
                # s2 = r.getChain().getSegid()

                # check if we have a dimer
                if not self.dimer:
                    if r1.getChain().getSegid() != r2.getChain().getSegid():
                        self.dimer = 1
                    else:
                        self.dimer = 0

                d = c.getAverageDistance()[0]
                w = c.getWeight()
                viol = d - target_dist

                residues.setdefault(r1, {})
                weights.setdefault(r1, {})
                residues[r1].setdefault(r2, [])
                weights[r1].setdefault(r2, [])
                residues[r1][r2].append(viol)
                weights[r1][r2].append(w)

                residues.setdefault(r2, {})
                weights.setdefault(r2, {})
                residues[r2].setdefault(r1, [])
                weights[r2].setdefault(r1, [])
                residues[r2][r1].append(viol)
                weights[r2][r1].append(w)

        # TODO [FALLAIN] rms_per_resid use ?
        # rms_per_resid = {}

        resid = residues.keys()
        resid.sort()

        matrix = []
        self.segids = []

        k = residues.keys()[0]
        m = k.getChain().getResidues()[-1].getNumber()
        self.rmat = np.zeros((m + 1, m + 1), float)

        for res1, res2s in residues.items():
            for res2, v in res2s.items():

                viol = np.array(v)
                viol = np.power(viol * np.greater(viol, 0), 2) * np.array(
                    weights[res1][res2])

                if sum(weights[res1][res2]) > 0:
                    rms = sum(viol) / sum(weights[res1][res2])
                    rms = np.sqrt(rms)

                    # simple rules to distinct intra from inter
                    # if intra [a, b] with a > b
                    # if inter [a, b] with b > a
                    i1, i2 = self._get_order_from_type(res1, res2)

                    s1, r1 = i1.getChain().getSegid(), i1.getNumber()
                    s2, r2 = i2.getChain().getSegid(), i2.getNumber()

                    matrix.append([r1, r2, rms])
                    self.segids.append([s1, s2])

                    self.rmat[r1][r2] = rms
                    self.rmat[r2][r1] = rms

        return resid, matrix

    # simple utiliy
    @staticmethod
    def _get_order_from_type(a, b):
        """
        

        Parameters
        ----------
        a :
            
        b :
            

        Returns
        -------

        
        """

        ra = a.getNumber()
        rb = b.getNumber()
        sa = a.getChain().getSegid()
        sb = b.getChain().getSegid()

        if sa != sb:

            if ra > rb:
                return b, a
            else:
                return a, b

        else:
            if ra > rb:
                return a, b
            else:
                return b, a

    def dumpRmsAnalysis(self, rms_text_report_path=None):
        """
        

        Parameters
        ----------
        rms_text_report_path :
             (Default value = None)

        Returns
        -------

        
        """
        if rms_text_report_path is None:
            rms_text_report_path = self.text_report

        # Overall
        h = open(rms_text_report_path, 'w')
        s = '''# Active restraints RMS violation
all restraints           %5.3f
ambiguous restraints     %5.3f
unambiguous restraints   %5.3f
''' % (self.rms, self.rms_a, self.rms_u)

        h.write(s)

        # profile
        s = '''
# RMS violation per residue
'''
        res = self.profile.keys()
        res.sort()
        for l in res:
            s += '%d    %5.3f\n' % (l, self.profile[l])

        h.write(s)

        # residue pairs
        s = '''
# RMS violation 2D map
'''

        for i in zip(self.rms_matrix, self.segids):
            s += '%3d %s - %3d %s    %5.3f\n' % (
                i[0][0], i[1][0], i[0][1], i[1][1], i[0][2])

        h.write(s)
        h.close()

    # def plotRmsProfile(self, file=None):
    def plotRmsProfile(self):
        """ """
        # TODO [FALLAIN] file var use ?
        import matplotlib.pylab as pylab

        pylab.axes([0.125, 0.7, 0.8, 0.2])
        x = [r.getNumber() for r in self.mol.get_chains()[0].getResidues()]
        # x = self.profile.keys()
        # x.sort()
        # dat = [self.profile[i] for i in x]
        dat = np.zeros(len(x), float)
        for a in range(0, len(x)):
            dat[a] = self.profile.get(x[a]) or 0.

        pylab.plot(x, dat, ls='-', c='b', marker="")
        pylab.grid(True)
        pylab.title('RMS violation residue profile')
        pylab.ylabel('RMS (A)')

    # def plot2dRmsMap(self, file=None):
    def plot2dRmsMap(self):
        """ """
        import matplotlib.pylab as pylab
        from numpy import ma
        # rcParams['numerix'] = 'numpy'

        first = self.mol.get_chains()[0].getResidues()[0].getNumber()
        last = self.mol.get_chains()[0].getResidues()[-1].getNumber()

        self.rmat = self.rmat[first:, first:]
        rmat_mask = ma.array(self.rmat, mask=np.equal(self.rmat, 0))
        pylab.axes([0.125, 0.1, 0.8, 0.5])
        # pylab.subplot(212)
        pylab.imshow(rmat_mask, origin='lower', cmap=pylab.cm.Reds,
                     interpolation='nearest', extent=(first, last, first, last))
        pylab.colorbar()
        pylab.title("RMS violation map")
        pylab.xlabel("Sequence")
        pylab.ylabel("Sequence")

    def plotPsReport(self, pdf_report_path=None):
        """
        

        Parameters
        ----------
        pdf_report_path :
             (Default value = None)

        Returns
        -------

        
        """

        import matplotlib.pylab as pylab
        from matplotlib.backends.backend_pdf import PdfPages
        import time

        if pdf_report_path is None:
            pdf_report_path = self.pdf_report

        it_n = self.iteration_n

        pdf = PdfPages(pdf_report_path)

        f = pylab.figure(figsize=(8.27, 11.69))
        f.suptitle('RMS violation analysis for iteration %s' % str(it_n),
                   fontsize=14, fontweight='bold')

        pylab.subplots_adjust(hspace=.9)

        self.plotRmsProfile()
        self.plot2dRmsMap()

        titles = "%s - %s" % (time.ctime(), pdf_report_path)
        pylab.figtext(0.05, 0.02, titles, fontsize=8)

        # pylab.subplots_adjust(hspace=0.5)
        pylab.savefig(pdf, papertype='a4', format='pdf')

        pdf.close()

    def doRmsAnalysis(self):
        """ """

        active = [p for p in self.peaks if p.isActive()]
        # inactive = [p for p in self.peaks if not p.isActive()]

        ambig = [p for p in active if p.isAmbiguous()]
        unambig = [p for p in active if not p.isAmbiguous()]

        self.mol = active[0].getContributions()[0]. \
            getSpinSystems()[0].getAtoms()[0]. \
            getResidue().getChain().getMolecule()

        # rms overall / ambig / unambig

        self.rms = self.getOverallRms(active)
        self.rms_u = self.getOverallRms(unambig)
        self.rms_a = self.getOverallRms(ambig)

        # rms profile

        self.profile = self.getRmsPerResidue(active)

        # rms 2D plot

        self.ids, self.rms_matrix = self.getRmsPerResiduePair(active)

    def go(self):
        """ """

        try:
            # if something wrong there, no need to continue
            self.doRmsAnalysis()
        except Exception as msg:
            self.debug(msg)
            return

        # noe_restraints.rms
        self.dumpRmsAnalysis()

        # RmsPlot.ps
        if _USE_MATPLOTLIB:
            self.plotPsReport()


# if __name__ == '__main__':
#     import sys
#     # import os
#
#     path = os.environ['ARIA2']
#     sys.path.insert(0, path + "/src/py/")
#
#     from .tools import Load
#
#     n = 8
#     out_path = '/home/Bis/bardiaux/devel/aria2.2_release/test/run2/structur
# es/it' + str(
#         n)
#
#     file = out_path + '/noe_restraints.pickle'
#     peaks = Load(file)
#
#     rp = RmsReport(peaks, n, out_path, out_path)
#     rp.go()
