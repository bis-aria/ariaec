"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""
from .TypeChecking import *
from threading import Thread
from .ariabase import *
from six import iteritems
from shutil import move
from tempfile import mkstemp
from .Settings import *
from .xmlutils import XMLElement, XMLBasePickler
# From Simeon Cartsens #
import os
import re
import string
import subprocess
import pkg_resources as pkgr


class PartlyUsedNodesFinder:
    def __init__(self, queue, exclude_list=None):
        if exclude_list is None:
            exclude_list = []
        self.queue = queue
        self.exclude_list = exclude_list

    def find(self):
        # Retrieve qstat -f result and split in lines
        qstat_array = subprocess.check_output(['qstat', '-f']).split('\n')[:-1]
        # Filter results for the appropiate queue
        qstat_array = filter(lambda x: self.queue in x, qstat_array)
        # split qstat -f row into columns
        qstat_array = [x.split() for x in qstat_array]
        # filter out machines in -NA- state
        qstat_array = filter(lambda x: '-NA-' not in x, qstat_array)
        # create a list of dictionaries keeping hostnames and number of free
        # cores and total cores
        hosts = [{'hostname':
                      x[0][x[0].find('@') + 1:x[0].find('.',
                                                        x[0].find('@') + 1)],
                  'free_cores':
                      int(x[2].split('/')[2]) - int(x[2].split('/')[1]),
                  'number_of_cores':
                      int(x[2].split('/')[2])} for x in qstat_array]
        # exclude hosts in exclude_list
        hosts = filter(lambda x: not x['hostname'] in self.exclude_list and not x['free_cores'] == 0, hosts)
        # sort list by number of free cores in increasing order
        hosts.sort(key=lambda x: x['free_cores'])

        return hosts


# From Simeon Cartsens #

class HostSettings(Settings):

    def create(self):
        d = {'n_cpu': PositiveInteger(), 'command': String(),
             'executable': Path(exists=0), 'enabled': YesNoChoice()}

        descr = """If set to '%s' (default), we use the absolute path of the csh-script to launch the calculation. Otherwise, only its basename is used."""
        
        d['use_absolute_path'] = YesNoChoice(description = descr % str(YES))

        return d

    def create_default_values(self):
        d = {'n_cpu': 1, 'command': 'csh', 'enabled': YES,
             'use_absolute_path': YES}

        return d


class JobSchedulerSettings(Settings):

    def create(self):

        s = 'Default command to run a remote job, e.g. "csh", "ssh [hostname] csh" or "rsh [hostname] csh", etc. A job (csh script) is then started as follows: COMMAND job.'

        keywords = {'host_list': TypeEntity(LIST),
                    'default_command': String(description = s)}
        
        return keywords


class JobManager(AriaBaseClass):
    """base-class common to all job-managers"""

    def shutdown(self):
        pass


class JobSettings(Settings):
    def create(self):
        from .Settings import Path, String
        
        d = {'command': String(), 'script': Path(), 'check_script': Path(),
             'working_directory': Path(), 'use_absolute_path': YesNoChoice()}

        return d


class Job(Thread, AriaBaseClass):
    """Job class used to launch a job with cmd_template on a host"""
    
    cmd_template = 'cd %(working_directory)s; %(command)s %(script)s'
    checkcmd_template = 'cd %(working_directory)s; %(command)s %(check_script)s'
    outscheduler_regex = {
        "lsf": re.compile(
            r"^Job\s+<(?P<job_id>\d+)>\s+is\s+submitted.*$"),
        "pbs": re.compile(
            r"^Your\s+job\s+(?P<job_id>\d+)\s+\([\"\w]+\)\s+has\s+been\s+"
            r"submitted.*$"),
        "slurm": re.compile(
            r"^Submitted\s+batch\s+job\s+(?P<job_id>\d+).*$")
    }

    def __init__(self, settings, *args, **kw):

        check_type(settings, 'JobSettings')

        Thread.__init__(self, *args, **kw)
        AriaBaseClass.__init__(self, settings)

        self.__stop = 0
        
        # callbacks
        self.setCallback(None)

    @staticmethod
    def _up_script(script, data, upscript):
        """
        Create a copy of input script with data using string Formatter

        Parameters
        ----------
        script: str
            file path of origin script
        data: dict
            dictionnary used to format script file
        upscript: str
            updated script path
        """
        with open(script) as old_script:
            content = old_script.read()
            content = string.Formatter().vformat(content, (), data)
        with open(upscript, 'w') as up_script:
            up_script.write(content)

    def get_jobid(self, output):
        """
        Find job id by parsing output of the scheduler

        Parameters
        ----------
        output

        Returns
        -------
        jobid or None
        """
        for scheduler, regex in iteritems(self.outscheduler_regex):
            match = regex.match(output)
            if match:
                return match.groupdict()['job_id']

        return None

    def stop(self):
        self.__stop = 1

    def isStopped(self):
        return self.__stop

    def setCallback(self, f):
        self.__callback = f

    def getCallback(self):
        return self.__callback

    def patch(self, host): #, qsubhosts):     ##### BARDIAUX
        """
        attempts to patch host-specific settings'
        """

        settings = self.getSettings()
        settings.update(host)
        
        d = {'executable': host['executable']}
        #     'sge_job_hostname' : qsubhosts}     ##### BARDIAUX

        f = open(settings['script'])
        s = f.read()
        s = string.Formatter().vformat(s, (), d)
        f.close()

        # write new csh script

        f = open(settings['script'], 'w')
        f.write(s)
        f.close()

    def run(self):
        """
        Method normally called in a separate thread using Thread.start() method

        Returns
        -------
        
        """

        import os
        import time

        # compile filename that will be polled during runtime
        
        filename = os.path.join(self.getSettings()['working_directory'],
                                'done')
        checkfile = os.path.join(self.getSettings()['working_directory'],
                                 'checkpoint')

        # Remove the file path (if exists)
        try:
            [os.unlink(foo) for foo in (filename, checkfile)]
        except Exception as e:
            # TODO: same
            # self.logger.exception(e)
            pass

        # start job

        d = self.getSettings().as_dict()

        # if use shall use the local filename of the script,
        # modify name. this gimmick is necessary for certain
        # queuing systems.
        job_desc = os.path.basename(os.path.dirname(d['script']))

        if d['use_absolute_path'] != YES:
            d['script'] = os.path.basename(d['script'])
            d['check_script'] = os.path.basename(d['check_script'])

        command = self.cmd_template % d
        # msg = 'Starting job %s'
        # self.message(msg % job_desc)
        self.debug(command)

        try:
            process = subprocess.Popen(command, shell=True,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            (out, err) = process.communicate()
        except Exception as e:
            # TODO : make it functionnal
            # self.logger.exception(e)
            pass

        self.message(out.replace("\n", "") + " (%s)" % job_desc)

        # Parse output to get jobid and launch another job which depend on it to
        # create checkpoint file
        jobid = self.get_jobid(out)
        checkscript = '_%s' % str(jobid) if jobid else ''

        if jobid:
            # Update script output with job id
            script = checkscript.join(os.path.splitext(d['script']))
            self._up_script(d['script'], {'sge_job_id': jobid},
                            script)
            # Create specific checkpoint file
            checkscript = checkscript.join(os.path.splitext(d['check_script']))
            self._up_script(d['check_script'], {'sge_job_id': jobid},
                            checkscript)
            d['script'] = script
            d['check_script'] = checkscript

        checkcmd = self.checkcmd_template % d
        checkprocess = subprocess.Popen(
            checkcmd, shell=True, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE) if jobid else None

        retcode = 0
        checkcode = 0
        terminated = 0
        checkpoint = 0

        # If callback for submit state, use it here
        # TODO[FALLAIN] : better log msgs when jobs end due to time limit !
        # while not terminated and not self.isStopped() and not retcode:
        while not terminated and not checkpoint and not self.isStopped() \
                and not retcode and not checkcode:
            # Exit if done or checkpoint file exists
            terminated = os.path.exists(filename)
            checkpoint = os.path.exists(checkfile)
            retcode = process.poll()
            if checkprocess:
                checkcode = checkprocess.poll()
            time.sleep(5.)

        if retcode or checkcode:
            therror = subprocess.CalledProcessError(
                    returncode=retcode,
                    cmd=command,
                    output=out) if retcode else \
                subprocess.CalledProcessError(
                    returncode=checkcode,
                    cmd=checkcmd,
                    output=out)
            self.error(msg=str(therror))

        # Remove if exists updated checkpoint script
        try:
            os.unlink(checkscript)
        except Exception as e:
            LOG.exception(e)

        if not self.isStopped() and terminated and checkpoint:
            self.message('Job %s completed.' % job_desc)
        else:
            self.debug('Job %s has been canceled.\n' % job_desc)
            self.debug('%s' % command)

        # notify that we are done.

        # TODO[FALLAIN]: what happen when two (or more) jobs ends at the same
        #  time and want to access JobScheduler.running_jobs ? (race condition,
        # deadlock, ... ?) Should be ok due to the GIL. There is no order
        # priority to delete job parameter in running jobs and dispatch another
        #  job since we have one key per thread.
        # CF http://jessenoller.com/blog/2009/02/01/python-threads-and-the-glo
        # bal-interpreter-lock
        if not self.isStopped():
            f = self.getCallback()

            if f is not None:
                f(self)


class JobScheduler(JobManager):
    
    def __init__(self, settings):

        check_type(settings, 'JobSchedulerSettings')
        AriaBaseClass.__init__(self, settings, name = 'Job manager')
        self.set_callback(None)

    def __check_content(self, filename, match):
        """
        reads the file 'filename'. if not equal
        to 'match' a string giving the reason is returned, None otherwise.
        """

        reason = None

        try:
            f = open(filename)
        except:
            reason = 'connection failed or unable to open temporary file.'

        if reason is None:

            s = 'dummy'

            try:
                s = f.read()[:-1]
            except:
                reason = """could not read temporary file or temporary file is empty.'"""

            if s != match:
                reason = ''

        try:
            f.close()
        except Exception as e:
            LOG.exception(e)

        return reason

    def probeHosts(self, project):
        """
        Probes the commands specfied for every item in the host-list.
        'project' is actually an InfraStructure instance. Infrastructure,
        however, will be absorbed in Project anyway...
        NB: Only used in Project.check actually

        Parameters
        ----------
        project :
            

        Returns
        -------

        """

        cmd_template = 'cd %(working_directory)s; %(command)s %(script)s' + \
                       ' %(message)s %(output_filename)s'

        import os
        import random

        hosts = self.getSettings()['host_list']

        temp = project.get_temp_path()
        working_dir = pkgr.resource_filename(MODULE_NAME, 'templates/csh') if \
            get_aria_root()[1] else os.path.join(project.get_aria_path(),
                                                 'src/csh')
        src = os.path.join(working_dir, 'check_host.csh')
        
        #d = {'working_directory': working_dir}

        passed = 1

        # BARDIAUX 2.2
        # move src/csh/check_host.csh to tempdir/check_host.csh before running
        from .tools import copy_file
        script = os.path.join(temp, 'check_host.csh')
        
        try:
            copy_file(src, script)
        except Exception:
            msg = 'Could not copy %s to %s' % (src, script)            
            self.error(Exception, msg)

        d = {'working_directory': temp}
        ##
        
        for host in hosts:

            if host['enabled'] != YES:
                continue

            d['command'] = host['command']

            tag = str(random.random())
            d['message'] = tag
            
            output_filename = os.path.join(temp,
                                           d['command'].replace(' ', '_'))

            d['output_filename'] = output_filename

            if host['use_absolute_path'] != YES:
                d['script'] = os.path.basename(script)
            else:
                d['script'] = script

            os.system(cmd_template % d)

            reason = self.__check_content(output_filename, tag)

            if reason is not None:
                # try to read file again, after a delay of 1s

                import time
                time.sleep(2.)

                reason = self.__check_content(output_filename, tag)

            msg = 'Command "%s" ... ' % d['command']
            
            if reason is None:
                self.message(msg + 'ok.')
                
            else:

                if reason == '':
                    msg += 'failed.'
                else:
                    msg += 'failed (%s)' % reason

                self.warning(msg)

            passed = passed and reason is None

            if not passed:
                break

        return passed

    def job_done(self, job):
        """
        Method called when the job is finished. Get back the host (HostSettings)
        related to this job and use it to launch a new job

        Parameters
        ----------
        job : Job object
            

        Returns
        -------

        
        """
        host = self.running_jobs[job]
        del self.running_jobs[job]

        self.dispatch_job(host)

    # BARDIAUX
    def get_minimal_qsubhosts(self, njobs):

        # return list of host for which sum of available cores
        # is enough to run the remianing jobs all together
        
        queue = 'all.q'
        exclude_list = ['scrappy', 'yippy', 'oopy', 'morgan','wyatt', 'navis', 'houyo', 'mestizo', 'margy']
        
        p = PartlyUsedNodesFinder(queue, exclude_list).find()
        
        nfree = sum([x['free_cores'] for x in p])

        # if not enough free cpus, just submit to all hosts minus exclude_list
        if nfree < njobs:
            return '-l hostname="!%s"' % ("&!".join(exclude_list))
            #return ''
        
        if not p:
            return '-l hostname="!%s"' % ("&!".join(exclude_list))

        tot_cpu = 0
        hosts = []
        while tot_cpu < njobs:
            tot_cpu += p[0]['free_cores']
            hosts.append(p[0]['hostname'])
            p.pop(0)

        return '-l hostname="%s"' % ("|".join(hosts))

    # BARDIAUX
        
    def dispatch_job(self, host):
        """
        Patch host settings in a new job (from self.jobs) and start it on the
        specified host with Thread.start. It arranges for the Job object's run()
        method to be invoked in a separate thread of control.

        Parameters
        ----------
        host : HostSettings object
            

        Returns
        -------

        
        """
        # If no job in job_list and no running job, call done method
        if not len(self.jobs):
            if not len(self.running_jobs):
                self.done()

        # start new job
            
        else:
            # BARDIAUX
            # find list of hosts for qsub
            # njobs = min(len(self.jobs), self.nhosts)
            # qsubhosts = self.get_minimal_qsubhosts(njobs)
            # BARDIAUX
            
            # Get script and remove it from job-list
            job = self.jobs.pop()

            # patch host-specific settings
            # and set callback
            job.patch(host)     # , qsubhosts)
            job.setCallback(self.job_done)

            self.running_jobs[job] = host
            # Start the job in a new thread scheduled by python interpreter
            # (green thread)
            job.start()

    def go(self, job_list):
        """
        Main method used to run job_list. Use all cpus defined per host to
        dispatch the jobs

        Parameters
        ----------
        job_list : list of aria.JobManager.Job object
            List of all jobs to run with the jobscheduler

        Returns
        -------

        
        """

        check_list(job_list)

        # TODO[FALLAIN]: define default values in __init__
        self.jobs = job_list
        self.__done = 0

        # compile host-list

        host_list = []

        for host in self.getSettings()['host_list']:
            if host['enabled'] == YES:
                # Append as many HostSettings as the number of cpus per host
                host_list += [host] * host['n_cpu']
        # Host list is now a 1D list of HostSettings objects => Each cpu has
        # his own HostSettings
                
        # BARDIAUX
        # TODO[FALLAIN]: define default values in __init__
        # Number of HostSettings/cpus defined for job_list
        self.nhosts = len(host_list)
        # BARDIAUX

        # if host-list is empty, we are done.

        if not host_list:
            s = 'Empty host-list. Ensure that at least 1 host is enabled.'
            self.message(s)
            self.done()

        # Start as many jobs as cpus are in host-list using self.jobs instance
        # which should be the list of all the jobs to run
        # TODO[FALLAIN]: define default values in __init__
        # Define dict of running_jobs linking Job object to their HostSettings
        self.running_jobs = {}
        [self.dispatch_job(host) for host in host_list]

    def done(self):
        
        if self.__callback is not None:
            self.__callback()
            
        self.__done = 1

    def is_done(self):
        return self.__done

    def set_callback(self, f):
        """
        The callback, 'f', is called when all jobs have been
        processed.

        Parameters
        ----------
        f : function
            callback function

        Returns
        -------

        
        """

        self.__callback = f

    def get_callback(self):
        return self.__callback

    def shutdown(self):
        """
        called whenever an error occurs (only if AriaBaseClass.error
        was called)

        Parameters
        ----------

        Returns
        -------

        """

        # empty list of unfinished job
        self.jobs = []

        # stop running jobs
        [job.stop() for job in self.running_jobs]

        self.message('shutdown.')


class HostSettingsXMLPickler(XMLBasePickler):

    order = ('enabled', 'command', 'executable', 'n_cpu', 'use_absolute_path')
    
    def _xml_state(self, x):

        e = XMLElement(tag_order = self.order)

        e.enabled = x['enabled']
        e.n_cpu = x['n_cpu']
        e.command = x['command']
        e.executable = x['executable']
        e.use_absolute_path = x['use_absolute_path']

        return e

    def load_from_element(self, e):
        s = HostSettings()
        
        s['enabled'] = str(e.enabled)
        s['n_cpu'] = int(e.n_cpu)
        s['command'] = str(e.command)
        s['executable'] = str(e.executable)

        # TODO: remove for release version

        if hasattr(e, 'use_absolute_path'):
            value = str(e.use_absolute_path)
        else:
            value = YES
            
        s['use_absolute_path'] = value

        return s


class JobManagerXMLPickler(XMLBasePickler):
    """ARIA XML project pickler for job_manager tag"""

    def _xml_state(self, x):
        """
        Define job_manager xml tag from JobSchedulerSettings object

        Parameters
        ----------
        x : JobSchedulerSettings object
            

        Returns
        -------

        
        """
        e = XMLElement()

        s = x.getSettings()

        e.default_command = s['default_command']

        # If host-list is empty, do not pickle
        # any host.

        entity = s.getEntity('host_list')

        if entity.is_initialized():
            e.host = entity.get()

        return e

    def setup_host_list(self, s):
        """
        Replace all host commands in host list by default if command is equal to
        "default"

        Parameters
        ----------
        s : JobSchedulerSettings
            

        Returns
        -------

        
        """

        # Foreach HostSettings object in host_list
        for host in s['host_list']:
            if host['command'] == 'default':
                host['command'] = s['default_command']

    def load_from_element(self, e):
        """
        Load job scheduler settings from XMLElement

        Parameters
        ----------
        e : XMLElement object
            job_manager xml tag object

        Returns
        -------

        
        """
        from .tools import as_tuple

        s = JobSchedulerSettings()

        s['default_command'] = str(e.default_command)

        # If the host-list is empty, we leave
        # the attached entity uninitialized.

        # The  element host contain the parameters related to the generation
        # of conformers on several computers
        if hasattr(e, 'host'):
            # Get list of HostSettings object
            s['host_list'] = list(as_tuple(e.host))
            self.setup_host_list(s)
        else:
            s['host_list'] = []

        jm = JobScheduler(s)

        return jm

HostSettings._xml_state = HostSettingsXMLPickler()._xml_state
JobScheduler._xml_state = JobManagerXMLPickler()._xml_state
