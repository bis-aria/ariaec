"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux, Michael Habeck, Therese Malliavin,        ..
..              Wolfgang Rieping, and Michael Nilges                          ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""

from .ariabase import AriaBaseClass
from .TypeChecking import *
from numpy import *
from numpy.linalg import svd, det


class SuperImposer(AriaBaseClass):
    REPORT = \
        """
## Ensemble RMSD (%(nstruc)d structures)
## All residues
## Backbone (N,CA,C,O) : %(bb)s A
## Heavy atoms         : %(heavy)s A
##
## Ordered residues : %(ordered)s
## Backbone (N,CA,C,O) : %(bb_ordered)s A
## Heavy atoms         : %(heavy_ordered)s A
## -----------------------------------------------------------------
"""

    HEADER = \
        """%d best structures superimposed on backbone atoms (N,CA,C,O)
Mean RMSD vs. average :  %.2f +/- %.2f A
Temperature column is RMSF over the ensemble
"""

    HEADER_AVG = \
        """Average structure of %d best structures
superimposed on backbone atoms (N,CA,C,O)
Mean RMSD vs. average :  %.2f +/- %.2f A
Temperature column is RMSF over the ensemble
"""

    def __init__(self, ensemble, molecule):

        AriaBaseClass.__init__(self)

        self.setSettings(ensemble.getSettings())

        self.ensemble = ensemble
        self.molecule = molecule
        self.ordered_atoms = None
        self.ordered_residues = None

        self.fitted_masked = None
        self.fitted = None

        self.nstruc = 0

    def _superpose(self, fixed, mobile, weight=None):
        """
        super imprpose n*3 matrices
        n*3 matrices of mobile coordinates
        n*3 matrices of fixed coordinates

        Parameters
        ----------
        fixed :
            
        mobile :
            
        weight :
             (Default value = None)

        Returns
        -------

        """

        n = len(mobile)

        if weight is None:
            weight = ones(n)

        W = sum(weight)

        # centers
        mobile_c = dot(weight, mobile) / W
        fixed_c = dot(weight, fixed) / W

        # relative coords
        mobile_r = mobile - mobile_c
        fixed_r = fixed - fixed_c

        # correlation matrix
        c = dot(weight * transpose(mobile_r), fixed_r)
        # svd
        u, d, vt = svd(c)
        rot = transpose(dot(transpose(vt), transpose(u)))
        if det(rot) < 0:
            vt[2] = -vt[2]
            rot = transpose(dot(transpose(vt), transpose(u)))
        tran = fixed_c - dot(mobile_c, rot)

        return rot, tran

    def _rmsf(self, x):

        """
        RMSF per atom

        Parameters
        ----------
        x :
            

        Returns
        -------

        """

        return sqrt(sum(sum(power(x - self.average, 2), 0), 1) / len(x))

    def _rmsd(self, a, b):

        """
        overall RMSD between a and b

        Parameters
        ----------
        a :
            
        b :
            

        Returns
        -------

        """

        return sqrt(sum(power(a - b, 2) / len(a)))

    def _average_coordinates(self, x):
        self.average = sum(x, 0) / len(x)

    def getFittedCoordinates(self):

        self.ensemble.sort()
        self.__X = self.ensemble.getCoordinates()

        n = self.getSettings()['number_of_best_structures']
        if n == 'all':
            n = len(self.__X)
        else:
            n = min(n, len(self.__X))

        self.nstruc = n

        self.__X = self.__X[:n]

        na = len(self.__X[0])

        x = zeros((n, na, 3), 'd')

        for i in range(0, n):
            coord = self.__X[i]
            r = self.rotations[i]
            t = self.translations[i]

            x[i] = dot(coord, r) + t

        return x

    def getRMSD(self, mask=None, ordered=False):

        """
        

        Parameters
        ----------
        mask :
             (Default value = None)
        ordered :
             (Default value = False)

        Returns
        -------
        type
            and for ordered residues if ordered = True

        """

        if ordered:
            self.findOrderedResidues()
            if not self.ordered_atoms:
                return None
            if mask is not None:
                mask = [a for a in mask if a in self.ordered_atoms]
            else:
                mask = self.ordered_atoms

        self.fitted_masked = self._fit(mask)
        n = len(self.fitted_masked)

        rmsd = zeros(n)

        for i in range(0, n):
            rmsd[i] = self._rmsd(self.average, self.fitted_masked[i])

        return average(rmsd), std(rmsd)

    def getBackboneRMSD(self, ordered=False):

        """
        N,CA,C,O RMSD

        Parameters
        ----------
        ordered :
             (Default value = False)

        Returns
        -------

        """

        mask = [a.getId() for c in self.molecule.get_chains() for r in
                c.getResidues()
                for a in r.getAtoms() if a.getName() in ['N', 'CA', 'C', 'O']]

        return self.getRMSD(mask, ordered)

    def getHeavyAtomsRMSD(self, ordered=False):

        """
        Heavy atoms RMSD

        Parameters
        ----------
        ordered :
             (Default value = False)

        Returns
        -------

        """

        mask = [a.getId() for c in self.molecule.get_chains() for r in
                c.getResidues()
                for a in r.getAtoms() if not a.isProton()]

        return self.getRMSD(mask, ordered)

    def _fit(self, mask=None):

        """
        magic fit (weighted iterative fit)
        weight parts courtesy A. Bernard (IP)

        Parameters
        ----------
        mask :
             (Default value = None)

        Returns
        -------

        """

        self.ensemble.sort()
        self.__X = self.ensemble.getCoordinates()

        if mask is not None:
            self.__X = take(self.__X, mask, axis=1)

        nit = 20
        threshold = 0.001
        max_weight = 4.

        n = self.getSettings()['number_of_best_structures']
        if n == 'all':
            n = len(self.__X)
        else:
            n = min(n, len(self.__X))

        self.nstruc = n

        na = len(self.__X[0])

        x = zeros((n, na, 3), 'd')

        fixed = self.__X[0]

        it = 1
        delta = 1.

        weight = ones(na)

        while it <= nit and delta >= threshold:

            self.rotations = []
            self.translations = []

            for i in range(0, n):
                coord = self.__X[i]
                r, t = self._superpose(fixed, coord, weight)
                ncoord = dot(coord, r) + t
                x[i] = ncoord
                self.rotations.append(r)
                self.translations.append(t)

            self._average_coordinates(x)

            fixed = self.average

            rmsf = self._rmsf(x)

            _weight = 1. / rmsf

            delta = sqrt(sum(power(weight - _weight, 2), 0) / len(weight))

            weight = where(_weight <= max_weight, _weight, max_weight)

            it += 1

        return x

    def findOrderedResidues(self):

        """
        find ordered residues according to
        circular order parameters for phi/psi
        ordered if S(Phi) >= 0.9 && S(Psi) >= 0.9

        Parameters
        ----------

        Returns
        -------

        """

        # self.ordered_atoms = []
        # self.ordered_residues = []

        # if self.ordered_atoms is not None:
        #    return 

        self.ordered_atoms = []
        self.ordered_residues = []

        for c in self.molecule.get_chains():
            # TODO [FALLAIN] segid use ?
            segid = c.getSegid()
            residues = c.getResidues()
            if c.getType() != 'PROTEIN':
                continue

            for rid in range(0, len(residues)):
                r = residues[rid]

                # TODO [FALLAIN] nb use ?
                nb = r.getNumber()

                # TODO [FALLAIN] phi/psi use ?
                phi = psi = None

                if rid < len(residues) - 1:
                    next = residues[rid + 1]
                    psi = self.getTorsions(r.getAtom('N').getId(),
                                           r.getAtom('CA').getId(),
                                           r.getAtom('C').getId(),
                                           next.getAtom('N').getId())
                else:
                    psi = None

                if rid > 0:
                    prev = residues[rid - 1]
                    phi = self.getTorsions(prev.getAtom('C').getId(),
                                           r.getAtom('N').getId(),
                                           r.getAtom('CA').getId(),
                                           r.getAtom('C').getId())
                else:
                    phi = None

                cop_phi = cop_psi = 0.
                if phi is not None:
                    # TODO [FALLAIN] n use ?
                    n = len(phi)
                    cop_phi = self._circular_order_parameter(phi)
                if psi is not None:
                    # TODO [FALLAIN] n use ?
                    n = len(psi)
                    cop_psi = self._circular_order_parameter(psi)

                if cop_phi >= 0.9 and cop_psi >= 0.9:
                    self.ordered_residues.append(r)
                    for a in r.getAtoms():
                        self.ordered_atoms.append(a.getId())

    def _circular_order_parameter(self, angles):

        """
        COP for list of angles across structure ansemble

        Parameters
        ----------
        angles :
            

        Returns
        -------

        """

        n = len(angles)
        return sqrt(power(sum(sin(angles), axis=0) / n, 2) + power(
            sum(cos(angles), axis=0) / n, 2))

    def getTorsions(self, a, b, c, d):

        """
        list of torsion angles from atom Id over
        all structures

        Parameters
        ----------
        a :
            
        b :
            
        c :
            
        d :
            

        Returns
        -------

        """

        self.ensemble.sort()
        self.__X = self.ensemble.getCoordinates()

        n = self.getSettings()['number_of_best_structures']
        if n == 'all':
            n = len(self.__X)
        else:
            n = min(n, len(self.__X))

            # self.nstruc = n

        self.__X = self.__X[:n]

        atoms = take(self.__X, [a, b, c, d], axis=1)

        t = [self._torsion(*i) for i in atoms]

        return t

    def _torsion(self, a, b, c, d):

        """
        

        Parameters
        ----------
        a :
            
        b :
            
        c :
            
        d :
            

        Returns
        -------
        type
            

        """

        v12x, v12y, v12z = a - b
        v32x, v32y, v32z = c - b
        v43x, v43y, v43z = d - c

        vn13x = v12y * v32z - v12z * v32y
        vn13y = v12z * v32x - v12x * v32z
        vn13z = v12x * v32y - v12y * v32x

        vn24x = v32z * v43y - v32y * v43z
        vn24y = v32x * v43z - v32z * v43x
        vn24z = v32y * v43x - v32x * v43y

        v12 = vn13x * vn24x + vn13y * vn24y + vn13z * vn24z
        v11 = vn13x ** 2 + vn13y ** 2 + vn13z ** 2
        v22 = vn24x ** 2 + vn24y ** 2 + vn24z ** 2

        ang = v12 / sqrt(v11 * v22)
        ang = arccos(ang)

        vtmp = vn13x * (vn24y * v32z - vn24z * v32y) + \
               vn13y * (vn24z * v32x - vn24x * v32z) + \
               vn13z * (vn24x * v32y - vn24y * v32x) < 0.0
        if vtmp:
            return -ang
        else:
            return ang

    def _ordered_residues_as_string(self):

        """ """

        if not self.ordered_residues:
            return "None"

        s = {}
        x = []

        # TODO [FALLAIN] text use ?
        text = ""
        for a in range(0, len(self.ordered_residues) - 1):
            residue = self.ordered_residues[a]
            next = self.ordered_residues[a + 1]
            segid = residue.getChain().getSegid()
            s.setdefault(segid, [])
            n = residue.getNumber()

            x.append(n)

            if next.getNumber() != n + 1:
                s[segid].append(x)
                x = []

        x.append(self.ordered_residues[-1].getNumber())
        segid = self.ordered_residues[-1].getChain().getSegid()
        s.setdefault(segid, [])
        s[segid].append(x)

        text = []
        for segid, regions in s.items():
            for region in regions:
                if len(region) > 1:
                    text.append(
                        "%s%s:%s" % (segid.strip(), region[0], region[-1]))
                else:
                    text.append("%s%s" % (segid.strip(), region[0]))

        return ",".join(text)

    def doReport(self):

        """ """

        floats = "%.2f +/- %.2f"
        na = "-"

        self.rmsd_bb = self.getBackboneRMSD()
        self.rmsd_heavy = self.getHeavyAtomsRMSD()
        self.rmsd_bb_ordered = self.getBackboneRMSD(ordered=True)
        self.rmsd_heavy_ordered = self.getHeavyAtomsRMSD(ordered=True)

        if self.rmsd_bb_ordered is None:
            rmsd_bb_ordered = na
        else:
            rmsd_bb_ordered = floats % self.rmsd_bb_ordered

        if self.rmsd_heavy_ordered is None:
            rmsd_heavy_ordered = na
        else:
            rmsd_heavy_ordered = floats % self.rmsd_heavy_ordered

        d = {'bb': floats % self.rmsd_bb,
             'heavy': floats % self.rmsd_heavy,
             'ordered': self._ordered_residues_as_string(),
             'bb_ordered': rmsd_bb_ordered,
             'heavy_ordered': rmsd_heavy_ordered,
             'nstruc': self.nstruc}

        return self.REPORT % d

    def write_ensemble(self, filename, with_average=None):

        """
        write multi-model pdb for n best fitted structure
        fitted on backbone by default

        Parameters
        ----------
        filename :
            
        with_average :
             (Default value = None)

        Returns
        -------

        """

        check_string(filename)

        import os
        from .OrderedDict import OrderedDict
        from .PDBReader import PDBReader

        reader = PDBReader()

        rmsd = self.getBackboneRMSD()

        model_dict = OrderedDict()

        serial = 1

        fitted = self.getFittedCoordinates()
        self._average_coordinates(fitted)
        rmsf = self._rmsf(fitted)

        header = self.HEADER % (self.nstruc,
                                rmsd[0], rmsd[1])

        for model in fitted:

            model_dict[serial] = OrderedDict()

            for chain in self.molecule.get_chains():

                model_dict[serial][chain.getSegid()] = {}

                chain_dict = model_dict[serial][chain.getSegid()]
                chain_dict['chain_type'] = chain.getSettings()['type']

                for residue in chain.getResidues():

                    residue_dict = {'residue_type': residue.getType()}

                    for atom in residue.getAtoms():
                        atom_id = atom.getId()
                        residue_dict[atom.getName()] = [model[atom_id],
                                                        rmsf[atom_id]]

                    chain_dict[residue.getNumber()] = residue_dict

            serial += 1

        filename = os.path.expanduser(filename)

        if os.path.exists(filename):
            s = 'File "%s" does already exist and will be overwritten.'
            self.warning(s % filename)

        reader.write_ensemble(header, model_dict, filename, 'iupac', 'cns')

        # now average structure

        if with_average is None:
            return

        filename = os.path.expanduser(with_average)

        if os.path.exists(filename):
            s = 'File "%s" does already exist and will be overwritten.'
            self.warning(s % filename)

        header = self.HEADER_AVG % (self.nstruc,
                                    rmsd[0], rmsd[1])

        serial = 1
        # dump average

        model_dict = OrderedDict()

        model_dict[serial] = OrderedDict()

        for chain in self.molecule.get_chains():

            model_dict[serial][chain.getSegid()] = {}

            chain_dict = model_dict[serial][chain.getSegid()]
            chain_dict['chain_type'] = chain.getSettings()['type']

            for residue in chain.getResidues():

                residue_dict = {'residue_type': residue.getType()}

                for atom in residue.getAtoms():
                    atom_id = atom.getId()
                    residue_dict[atom.getName()] = [self.average[atom_id],
                                                    rmsf[atom_id]]

                chain_dict[residue.getNumber()] = residue_dict

        reader.write_ensemble(header, model_dict, filename, 'iupac', 'cns')
