# coding=utf-8
"""
.. .......................................................................... ..
..          ARIA -- Ambiguous Restraints for Iterative Assignment             ..
..                                                                            ..
..                A software for automated NOE assignment                     ..
..                                                                            ..
..                               Version 2.3                                  ..
..                                                                            ..
..                                                                            ..
.. Copyright (C) Benjamin Bardiaux                                            ..
..               Structural Bioinformatics, Institut Pasteur, Paris           ..
..                                                                            ..
.. All rights reserved.                                                       ..
..                                                                            ..
.. NO WARRANTY. This software package is provided 'as is' without warranty of ..
.. any kind, expressed or implied, including, but not limited to the implied  ..
.. warranties of merchantability and fitness for a particular purpose or      ..
.. a warranty of non-infringement.                                            ..
..                                                                            ..
.. Distribution of substantively modified versions of this module is          ..
.. prohibited without the explicit permission of the copyright holders.       ..
..                                                                            ..
.. $Author: bardiaux $                                                        ..
.. $Revision: 1.1.1.1 $                                                       ..
.. $Date: 2010/03/23 15:27:24 $                                               ..
.. .......................................................................... ..
"""

from __future__ import print_function, absolute_import
# from .TypeChecking import *
from .ariabase import AriaBaseClass, safe_execute
import logging


LOG = logging.getLogger(__name__)


# Behaviour change btw py2 & 3 of hasattr builtins ! In py3 hasattr catch the
# result of getattr contrary to py2 so we shouldn't use it for checking if a
# XML tag exist !
def safe_elt(elt, tag, default=None):
    """
    Get xml element or return default value

    Parameters
    ----------
    elt :
    tag :
    default :

    Returns
    -------

    """
    return safe_execute(default, XMLTagError, getattr, elt, tag)


class XMLReaderError(Exception):
    pass


class XMLTagError(XMLReaderError):
    def __str__(self):
        return 'XML tag "%s" not known or missing.' % \
               Exception.__str__(self)


class XMLDocument:
    def __init__(self, doc_type, dtd):
        self.__doc_type = doc_type
        self.__dtd = dtd

    def get_doc_type(self):
        return self.__doc_type

    def get_dtd(self):
        return self.__dtd


class XMLElement:
    def __init__(self, name=None, attr=None, tag_order=None):

        self.__cdata = None

        if name is not None:
            self.__name = name

        if attr is not None:
            for name, value in attr.items():
                setattr(self, name, value)

        self.set_tag_order(tag_order)

    def set_cdata(self, data):
        if self.__cdata is None:
            self.__cdata = data
        else:
            self.__cdata += data

    def get_cdata(self):
        return self.__cdata

    # def get_name(self):
    #     return self.__name if self.__name else self.__class__.__name__

    #     def get_name(self):
    # raise
    #         attr = '_%s__name' % self.__class__.__name__

    #         if attr in self.__dict__:
    #             return self.__dict__[attr]
    #         else:
    #             return None

    def set_tag_order(self, o):
        self.__tag_order = o

    def get_tag_order(self):
        return self.__tag_order

    def __getattr__(self, name):

        attr = '_%s__name' % self.__class__.__name__

        if attr in self.__dict__:
            my_name = self.__dict__[attr]
        else:
            my_name = None

        name = str(my_name) + '.' + name

        raise XMLTagError(name)

    def __str__(self):
        return 'XMLElement(name=%s, tag_order=%s)' % \
               (self.get_name(), str(self.get_tag_order()))


class ContentConverter:
    """
    Class which interprets and converts XMLElements instances
    as Python data stuctures.
    """

    dtd_name = []

    def set_dtd_name(self, name):
        """Set definition file for the content handler"""
        if name is None:
            # raise
            # TODO[FALLAIN]: python 3 doesn't not allow syntax above
            raise XMLReaderError

        ContentConverter.dtd_name.append(name)

    def release(self):
        """Remove definition file related to the content handler"""
        if ContentConverter.dtd_name:
            ContentConverter.dtd_name = ContentConverter.dtd_name[:-1]
        else:
            # raise 'Inconsistency: ContentConverter cannot be released.'
            # TODO[FALLAIN]: same as previous todo
            raise XMLReaderError('Inconsistency: ContentConverter cannot be '
                                 'released.')

    def get_dtd_name(self):
        """
        Get the actual definition

        Returns
        -------
        dtd_name
            last element in dtd_name array
        Return the actual definition of element names

        Notes
        -----
        Get the value of a class variable 'dtd_name'.
        Since definition of element names may differ
        in different DTDs, the main purpose of the method
        is to determine the DTD within the '_xml_state'
        method in order to know, how to map an object
        to an XML element structure.
        """

        # return ContentConverter.dtd_name  # self.__class__.dtd_name
        return ContentConverter.dtd_name[-1]

    def _xml_state(self, x):
        """
        Converts the python data structure, x, into
        a instance of XMLElement.

        Parameters
        ----------
        x : {:py:class:`dict`, :py:class:`.ariabase.AriaBaseClass`, :py:class:`.Settings.Settings`}
            python data structure

        Returns
        -------
        e : :py:class:`XMLElement`
        """
        raise NotImplementedError

    def load_from_element(self, e):
        """
        converts an element, 'e', into a Python data structure.

        Parameters
        ----------
        e : :py:class:`XMLElement`

        Returns
        -------
        e: {:py:class:`dict`, :py:class:`.ariabase.AriaBaseClass`, :py:class:`.Settings.Settings`}
            python structure
        """
        return e


class ContentHandler(ContentConverter):
    """
    Abstract base class for content handler. Event-driven parsers
    must be subclassed from ContentHandler.
    """

    def startElementHandler(self, name, attr):
        raise NotImplementedError

    def endElementHandler(self, name):
        raise NotImplementedError

    def charDataHandler(self, data):
        raise NotImplementedError


class XMLContentHandler(ContentHandler):
    def __init__(self):
        # TODO[FALLAIN]: should call ContentHandler.__init__ ?
        self.elements = []
        self.current = None

    def setDocument(self, d):
        """
        sets the main document of the xml file, that is the
        root of the hierarchy of XMLElement instances.

        Parameters
        ----------
        d : :py:class:`XMLDocument`
            XML Document object
        """
        self.elements = [d]
        self.current = d

    def startElementHandler(self, name, attr):

        try:
            name = str(name).split(':')[1]
        except:
            pass

        new_element = XMLElement(name, attr)
        self.elements.append(self.current)
        self.current = new_element

    def endElementHandler(self, name):
        """
        Called at the end of each XML tag during the parsing. Convert the xml
        tag and save as a python data structure

        Parameters
        ----------
        name : str
            xml tag name
        """
        try:
            name = str(name).split(':')[1]
        except:
            pass

        if not len(self.elements):
            return
        # Load python object using the pickler related to xml element.
        e = self.load_from_element(name, self.current)
        # Get parent or previous XML element from element list
        self.current = self.elements.pop()

        try:
            # Fetch value of xml element in parent tag
            value = getattr(self.current, name)
            try:
                # xml element is a list of xml elements in parent tag
                value.append(e)
            except:
                # If xml element is not a list and already exists in parent tag,
                #  update it as a list
                setattr(self.current, name, [value, e])
        except (XMLTagError, XMLReaderError, AttributeError):
            # LOG.debug(e)
            # If no value, set the loaded element as unique in the parent
            # element
            setattr(self.current, name, e)

    def charDataHandler(self, data):

        data = data.strip()

        if not len(data):
            return

        self.current.set_cdata(str(data))

    # TODO[FALLAIN]: Not the same signature as in ContentConverter class
    def load_from_element(self, name, e):
        return e


class BaseReader:
    """Abstract reader class"""

    def __init__(self, handler):
        self.__handler = handler

    def createParser(self):
        raise NotImplementedError

    def getContentHandler(self):
        return self.__handler


class XMLReader(BaseReader):
    """
    Abstract class able to read xml files and converts it into
    a hierarchy of XMLElements according to the content handler.
    """

    def __init__(self, content_handler):
        BaseReader.__init__(self, content_handler)

    def createParser(self):
        """
        Creates the parser using expat module and defined content handler

        Returns
        -------
        parser : :py:obj:`aria.core.xmlparser.SelfmadeXMLParser`
            XML initialized parser
        """

        content_handler = self.getContentHandler()

        if content_handler is None:
            # raise 'No content handler set.'
            # TODO[FALLAIN]: raise syntax in python 3
            raise XMLReaderError('No content handler set.')

        try:
            import xml.parsers.expat as expat
            parser = expat.ParserCreate()

        except ImportError:
            from .xmlparser import SelfmadeXMLParser as Parser

            print('Could not import the Python module xml.parsers.expat. '
                  'Please check your Python setup. '
                  'Using Python implementation of XML parser. '
                  'This could lead to a substancial performance loss.')

            parser = Parser()

        parser.StartElementHandler = content_handler.startElementHandler
        parser.EndElementHandler = content_handler.endElementHandler
        parser.CharacterDataHandler = content_handler.charDataHandler

        return parser

    def parse_doc_type(self, infile):
        """
        Parse DOCTYPE value at the beginning of the XML file

        Parameters
        ----------
        infile : :py:class:`file` or :py:class:`typing.IO`
            XML input file

        Returns
        -------
        tag : str
            Document type label in DOCTYPE tag
        dtd : str
            DTD file name related to this DOCTYPE
        """

        line = None
        doctype_found = 0
        seek = infile.tell()
        seek_previous = seek

        while not doctype_found:

            line = infile.readline()
            doctype_found = line.find('<!DOCTYPE') > -1

            # If we haven't move since the last readline call, break the loop
            if seek_previous == infile.tell():
                break

            seek_previous = infile.tell()

        # Return to the starting point in the file
        infile.seek(seek)

        if not doctype_found:
            tag, dtd = None, None

        else:
            tag = line.split()[1]
            dtd = line.split()[3][1:-2]

        try:
            tag = str(tag).split(':')[1]
        except:
            pass

        return tag, dtd

    def load(self, filename, gzip=0):

        import os
        from io import open
        filename = os.path.expanduser(filename)

        if gzip:
            from .tools import gzip_open as open_func
        else:
            open_func = open

        with open_func(filename, 'rt') as f:
            doc = XMLDocument(*self.parse_doc_type(f))

            handler = self.getContentHandler()

            # If the XML document as a DTD template
            if doc.get_dtd() is not None:
                handler.set_dtd_name(doc.get_dtd())

            handler.setDocument(doc)

        # XML parser no longer support text files, we have to open it as binary
        with open_func(filename, 'rb') as f:
            parser = self.createParser()
            # TODO: method ParseFile exists if we use expat module ?
            parser.ParseFile(f)

        if doc.get_dtd() is not None:
            handler.release()

        failed = 0

        if len(handler.elements) != 1:
            failed = 1
        elif handler.elements[0] != doc:
            failed = 1

        if failed:
            raise XMLReaderError('XML document misformatted.')

        return doc


class XMLPickler(XMLReader):
    """full pickler. reads and writes xml files."""

    debug = 0

    def _dumps(self, x, name=None, tags=None):

        if isinstance(x, XMLElement):

            #             try:
            #                 name = x.get_name()

            #             except:
            #                 pass

            result = self._dumps(x.__dict__, name, x.get_tag_order())

        elif type(x) == type(self):
            if hasattr(x, '_xml_state'):
                if self.__class__.debug:
                    print(x._xml_state.im_class.__name__)

                x = x._xml_state(x)

            else:
                x = x.__dict__

            result = self._dumps(x, name)

        elif type(x) == type({}):

            attribs = ''
            lines = []

            if tags is None:
                tags = x.keys()

            for tag in tags:

                try:
                    if tag[:11] == '_XMLElement':
                        continue
                    elif tag[:12] == '_XMLDocument':
                        continue
                except:
                    pass

                block = self._dumps(x[tag], tag)

                if type(block) == type(''):
                    attribs += str(block)

                else:
                    lines += block

            if name is not None:

                if attribs:
                    attribs = ' ' + attribs

                if lines:
                    open_tag = '<' + name + attribs[:-1] + '>'
                    close_tag = '</' + name + '>'
                    lines = ['  ' + s for s in lines]
                    lines.insert(0, open_tag)
                    lines.append(close_tag)

                else:
                    open_tag = '<' + name + attribs[:-1] + '/>'
                    lines = [open_tag]

            result = lines

        elif type(x) in (type([]), type(())):

            lines = []

            for i in range(len(x)):

                value = x[i]

                if type(value) in (type(self), type({})):
                    block = self._dumps(value, name)
                    if type(block) == type(''):
                        block = ['<' + name + ' ' + block[:-1] + '/>']

                else:
                    block = ['<' + name + ' value="' + str(value) + '"/>']

                lines += block

            result = lines

        else:

            # represent None as empty string

            if x is None:
                x = ''
            result = name + '="' + str(x) + '" '

        return result

    def dumps(self, x):
        if type(x) not in (type({}), type(self)):
            s = 'Only instances and dicts can be pickled as XML.'
            raise Exception(s)

        if isinstance(x, XMLDocument):
            lines = self._dumps(x)
            doc_type = x.get_doc_type()
            dtd = x.get_dtd()

            header = '<!DOCTYPE %s SYSTEM "%s">' % (doc_type, dtd)
            lines.insert(0, header)

        else:
            lines = self._dumps(x)

        return '\n'.join(lines)

    def dump(self, x, filename, gzip=0):
        import os

        filename = os.path.expanduser(filename)

        if gzip:
            from .tools import gzip_open as open_func
        else:
            open_func = open

        xml = self.dumps(x)

        f = open_func(filename, 'w')
        f.write(xml)
        f.close()


# TODO: move that one to AriaXML.py, change name to AriaBaseXMLContentHandler

class XMLBasePickler(AriaBaseClass, ContentConverter):
    # TODO [FALLAIN]: define load_from_element here (like ClusteringXMLPickler)?
    # TODO [FALLAIN]: set create and _xml_state as abstract method ?
    # TODO [FALLAIN]: instead of create define settings as abstract property ?
    pass
