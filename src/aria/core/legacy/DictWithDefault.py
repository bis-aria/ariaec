# coding=utf-8
# A dictionary with default values for non-existing entries

# import UserDict, copy
from __future__ import absolute_import, division, print_function
from future.moves.collections import UserDict

import copy


class DictWithDefault(UserDict):
    def __init__(self, default, **kwargs):
        UserDict.__init__(self, **kwargs)
        self.data = {}
        self.default = default

    def __getitem__(self, key):
        try:
            item = self.data[key]
        except KeyError:
            item = copy.copy(self.default)
            self.data[key] = item
        return item

    def __delitem__(self, key):
        try:
            del self.data[key]
        except KeyError:
            pass


class SafeFormatDict(dict):
    """Dictionnary class used to format with string Formatter"""

    def __missing__(self, key):
        """
        Method called when a key is missing

        Parameters
        ----------
        key

        Returns
        -------
        Unformatted string
        """
        return '{' + key + '}'
