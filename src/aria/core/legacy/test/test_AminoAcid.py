# coding=utf-8
"""
Created on 9/16/16

@author: fallain
"""
import pytest
from aria.core.legacy.AminoAcid import AminoAcid


@pytest.fixture(scope="module", params=[
    'A', 'ALA', 'R', 'ARG', 'N', 'ASN', 'D', 'ASP', 'C', 'CYS', 'Q', 'GLN', 'E',
    'GLU', 'G', 'GLY', 'H', 'HIS', 'I', 'ILE', 'L', 'LEU', 'K', 'LYS', 'M',
    'MET', 'F', 'PHE', 'P', 'PRO', 'S', 'SER', 'T', 'THR', 'W', 'TRP', 'Y',
    'TYR', 'V', 'VAL'])
def amino_acid_2l3l(request):
    yield request.param


def test_convert_amino_acid(amino_acid_2l3l):
    aa = AminoAcid(amino_acid_2l3l)
    assert len(aa) == 3
    assert aa[0]
