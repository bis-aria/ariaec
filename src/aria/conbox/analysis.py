# coding=utf-8
"""Analysis tools for protein ensembles"""
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import seaborn as sns
from Bio.PDB.Residue import Residue
from Bio.PDB import PDBParser, PDBIO
from ..core.AriaXML import AriaXMLPickler
from ..core.DataContainer import DATA_SEQUENCE
from ..core.SuperImposer import SuperImposer
from collections import OrderedDict
from glob import glob
from matplotlib.colors import ListedColormap
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA

from ..core.StructureEnsemble import StructureEnsemble, StructureEnsembleSettings
from .common import NotDisordered, Capturing
from .converter import AriaEcXMLConverter

LOG = logging.getLogger(__name__)


def colscatter(X, axe, colors, ndim=2, axtitle="", xlabel="x",
               ylabel="y", zlabel="z", legend_prefix="", others=False):
    """
    Scatter plot with palette colors

    Parameters
    ----------
    X : array-like, shape (n_samples, n_components)
        matrix used for plotting
    axe : matplotlib.Axes
        Axes object containing the plot
    colors : list of int
        color indexes
    ndim : int
         Number of components extracted for the plot (Default value = 2)
    axtitle :
         Plot title (Default value = "")
    xlabel : str
         X axis label (Default value = "x")
    ylabel : str
         Y axis label (Default value = "y")
    zlabel : str
         Z axis label (Default value = "z")
    legend_prefix : str
         Add a prefix to the legend (Default value = "")
    others : bool
         If the last legend label should contain or not `prefix` (Default
         value = False)

    """
    palette = sns.color_palette("hls", len(set(colors)))
    dims = [X[:, i] for i in range(0, ndim)]
    axe.scatter(*dims, c=colors,
                cmap=ListedColormap(palette))
    axe.invert_xaxis()

    if others:
        axe.legend([Line2D([0], [0], linestyle="none", c=palette[colidx],
                           marker="o")
                    for colidx in set(colors)],
                   [legend_prefix + str(colidx) if colidx != max(
                       colors) else "Others" for colidx in set(colors)],
                   numpoints=1)
    else:
        axe.legend([Line2D([0], [0], linestyle="none", c=palette[colidx],
                           marker="o")
                    for colidx in set(colors)],
                   [legend_prefix + str(colidx) for colidx in
                    set(colors)], numpoints=1)

    axe.set_title(axtitle)
    axe.set_xlabel(xlabel)
    axe.set_ylabel(ylabel)
    if ndim == 3:
        axe.set_zlabel(zlabel)


class EnsembleAnalysis(object):
    """ARIA extended ensemble analysis"""

    def __init__(self, settings):
        self.settings = settings

    @staticmethod
    def _get_ensemble_paths(iteration_path):
        """
        Get list of pdb files related to aria ensemble(s)
        
        Parameters
        ----------
        iteration_path : str
            ARIA iteration path where all pdb files are saved            

        Returns
        -------
        list_of_pdb : list of str
            List of pdb file path

        """
        # Get the list of all the generated structures in iteration path if no
        # clustering. Otherwise, get the list of all generated structures
        list_of_pdb = []
        if os.path.exists(os.path.join(iteration_path, "report.clustering")):
            # Get the list of pdb for each cluster
            LOG.info("Clusters found in this iteration, compute analysis for"
                     "each generated cluster ensemble")
            for clustlist in glob(os.path.join(iteration_path, '*clust*.list')):
                with open(clustlist) as cluster:
                    list_of_pdb.append(cluster.read().splitlines())
        else:
            # no clustering, pdb list correspond to all generated structures
            LOG.info("No cluster found in this iteration, compute analysis for"
                     " iteration ensemble")
            list_of_pdb.append(
                [foo for foo in glob(os.path.join(iteration_path, "*.pdb"))
                 if not os.path.basename(foo).startswith('fitted_')])

        LOG.debug("Lists of structures:\n%s", "\n".join(
            [str(_) for _ in list_of_pdb]))
        return list_of_pdb

    @staticmethod
    def violation_analysis(project, iteration_id, restraints, ensemble, out_file,
                           dists_ref=None, headerflag=True, clusteridx=0):
        """
        
        Parameters
        ----------
        clusteridx
        project : :class:`aria.core.Project.Project` object
        iteration_id : 
        restraints :
            
        ensemble :
            
        out_file :
            
        dists_ref : 
            Reference distances (Default value = None)
        headerflag :
            (Default value = True)

        Returns
        -------

        
        """
        protein_id = project.getSettings()['name']
        nbest = project.getProtocol().getIterationSettings(iteration_id)[
            "number_of_best_structures"]
        cutoff = project.getProtocol().getIterationSettings(iteration_id)[
            "violation_analyser_settings"]["violation_tolerance"]
        with open(out_file, 'w') if headerflag else open(out_file, 'a') as out:

            for restraintlist in restraints:

                for rest in restraintlist:

                    output = []
                    dd = []
                    ddref = []

                    for contrib in rest.getContributions():

                        dist = None
                        try:
                            dist = [ensemble.getDistances(*sp.getAtoms())
                                    for sp in contrib]
                            # Liste des distances pour la contribution c pour
                            # chaque structure de l'ensemble (une ou plusieur
                            # distance(s) par structure de l'ensemble si contribution
                            # ambigue ou non)
                            dist = np.power(
                                np.sum(np.power(dist, - 6.), axis=0), -1. / 6)
                            # Liste des distances pour chaque structure de l'ensemble
                            # (liste associant une distance par structure de l'ensemble).
                            # Si contrib non ambig (associee a une seule paire de spins),
                            # cette liste devrait etre identique a la precedente
                            dd.append(dist)

                        except Exception as msg:
                            LOG.warning("%s: %s" % (Exception, msg))
                            pass

                        dref = None
                        if dists_ref:
                            # TODO: prendre en compte un gap dans les matrices
                            try:
                                dref = [dists_ref(*sp.getAtoms()) for sp in contrib]
                                dref = np.power(np.sum(np.power(dref, -6.), axis=0),
                                                -1. / 6)
                                ddref.append(dref)

                            except Exception as msg:
                                LOG.warning("%s: %s" % (Exception, msg))
                                pass

                        tmp = OrderedDict()
                        tmp['protein'] = protein_id
                        tmp['data'] = rest.getReferencePeak().getSpectrum().getName()
                        tmp['iteration'] = iteration_id
                        tmp['clust'] = clusteridx
                        tmp['ens_size'] = nbest
                        tmp['rest_no'] = rest.getId()
                        tmp['contrib_no'] = contrib.getId()
                        # Assuming there is only one spin per contribution
                        tmp['resid_1'] = contrib[0][0].getResidue().getNumber()
                        tmp['resid_2'] = contrib[0][1].getResidue().getNumber()
                        tmp['res_1'] = contrib[0][0].getResidue().getName()[:3]
                        tmp['res_2'] = contrib[0][1].getResidue().getName()[:3]
                        tmp['atm_1'] = contrib[0][0].getName()
                        tmp['atm_2'] = contrib[0][1].getName()
                        tmp['viol_cutoff'] = cutoff
                        tmp['d_target'] = rest.getDistance()
                        tmp['lower_bound'] = rest.getLowerBound() - cutoff
                        tmp['upper_bound'] = rest.getUpperBound() + cutoff
                        tmp['rest_weight'] = rest.getWeight()
                        tmp['dc_min'] = np.min(dist) if dist is not None else ''
                        tmp['dc_avg'] = np.mean(dist) if dist is not None else ''  # Moyenne des distances associes a la contrib c dans l'ensemble
                        tmp['dc_med'] = np.median(dist) if dist is not None else ''
                        tmp['dc_ref'] = np.mean(dref) if dref is not None else ''

                        output.append(tmp)

                    # Liste des distances (effective si plusieurs spin par
                    # contribution) associe aux contraintes
                    dd = np.array(dd)
                    ddref = np.array(ddref)  # Idem pour natif

                    dd_eff = np.power(np.sum(np.power(dd, -6), axis=0), -1. / 6)
                    ddref_eff = np.power(np.sum(np.power(ddref, -6), axis=0), -1. / 6)

                    for x in range(len(output)):
                        # Moyenne des distances effectives dans l'ensemble de
                        # structure. Peut etre biaisee si on a des structures qui
                        # ont une distance anormalement elevee. Dans ce cas, on ne
                        # peut evaluer si la contrainte a ete correctement supprimee
                        #  dans l'ensemble etudie.
                        output[x]['Deff_min'] = np.min(dd_eff)
                        output[x]['Deff_avg'] = np.mean(dd_eff)
                        output[x]['Deff_med'] = np.median(dd_eff)
                        output[x]['Deff_sdev'] = np.std(dd_eff)
                        output[x]['Deff_ref'] = np.mean(ddref_eff)
                        output[x]['pc_viol'] = float(
                            np.sum(np.greater(dd_eff, rest.getUpperBound() + cutoff))) / nbest
                        # Normalement d_ref ne contient qu'un seul elt puisqu'il y a
                        #  qu'une seule structure ( a verifier )
                        # distance effective native
                        # Distance effective minimum dans l'ensemble
                        output[x]['viol'] = True if \
                            output[x]['pc_viol'] >= 0.5 else False
                        # Contrainte consideree comme valide si la distance effective
                        # dans la structure de reference est en dessous du seuil de
                        # violations.
                        output[x]['valid'] = True if \
                            output[x]['upper_bound'] >= output[x]['dc_ref'] >= output[x]['lower_bound'] else False
                        output[x]['contact_5'] = True if \
                            output[x]['dc_ref'] <= 5.0 else False
                        output[x]['contact_8'] = True if \
                            output[x]['dc_ref'] <= 8.0 else False

                        if output[x]['contact_8'] and output[x]['viol']:
                            output[x]['group'] = 'VP viol'
                        elif output[x]['contact_8'] and not output[x]['viol']:
                            output[x]['group'] = 'VP'
                        elif not output[x]['contact_8'] and output[x]['viol']:
                            output[x]['group'] = 'FP viol'
                        else:
                            output[x]['group'] = 'FP'

                        if headerflag:
                            out.write(",".join(output[0].keys()))
                            headerflag = False

                        out.write("\n" + ",".join(["%s" % output[x][k]
                                                   for k in output[x].keys()]))

        LOG.info("Writing violation analysis of clust %s %s file", clusteridx,
                 out_file)

    # TODO: As described in Guillaume paper, clustering on aligned CA
    # coordinates highly depends on the efficiency of the alignment method
    @staticmethod
    def pca_projection(
            ensemble, molecule, infos, atmask="CA",
            title="3D PCA projection on backbone coordinates", outfile=None):
        """
        PCA projection of ensemble coordinates

        Parameters
        ----------
        ensemble :
        molecule :
        infos
        atmask :
            (Default value = "CA")
        title
        outfile

        Returns
        -------
        
        """
        sns.set_style('ticks')
        pca = PCA(n_components=3)
        mask = [a.getId() for c in molecule.get_chains() for r in c.getResidues()
                for a in r.getAtoms() if a.getName() == atmask]

        # Align all structures on ca backbone
        si = SuperImposer(ensemble, molecule)
        si.getSettings()['number_of_best_structures'] = 'all'
        si._fit(mask)

        # Get matrix of coordinates
        fitcoords = si.getFittedCoordinates()
        fitcoords = np.take(fitcoords, mask, axis=1)
        ns, na, xyz = fitcoords.shape
        # Change the shape of coords matrix in order to use pca, kmeans, ...
        fitcoords.shape = ns, na * xyz

        ensemble.getSettings()['number_of_best_structures'] = 15

        fitcoords_reduced = pca.fit_transform(fitcoords)

        #
        fig = plt.figure()

        ax = Axes3D(fig)

        colscatter(
            fitcoords_reduced, ax, infos, ndim=3, axtitle=title,
            xlabel="Principal component 1", ylabel="Principal component 2",
            zlabel="Principal component 3", legend_prefix="Clust ")
        if outfile:
            plt.savefig(outfile)

    def run(self):
        """Execute Ensemble analysis"""
        # Args
        project_path = self.settings.analysis.args["project"]
        # restraints_path = self.settings.analysis.args["restraints"]
        native_path = self.settings.analysis.args.get("ref")
        out_path = self.settings.analysis.args["output_directory"]
        list_name = self.settings.analysis.args["listname"]

        # Create Aria objects
        pickler = AriaXMLPickler()
        project = pickler.load(project_path)
        molecule_path = project.getData(DATA_SEQUENCE)[0].getLocation()[0]
        molecule = pickler.load(molecule_path)

        # If we are at the first iteration, we select the related ensemble and
        # restraints, otherwise we take the ensemble from the previous iteration
        iteration_path = self.settings.analysis.args["iteration"]
        iteration_id = int(re.search(
            '[0-9]+', os.path.basename(iteration_path)).group(0))
        if iteration_id != 0:
            # If we are not at the first iteration, we select analysis
            # parameters related to user input iteration and update iteration
            # path in order to get ensemble of structures from previous
            # iteration
            iteration_path = os.path.join(
                os.path.dirname(iteration_path), "it%d" % (iteration_id - 1))
            LOG.info("Ensemble analysis will be done on restraints and "
                     "ensemble from it%d with violation criteria of it%d",
                     (iteration_id - 1), iteration_id)
            if not os.path.exists(iteration_path):
                LOG.error("Can not found previous iteration (%s)",
                          iteration_path)

        # Load restraints related to the actual iteration
        LOG.info("Reading distance restraints file(s)")
        # restraints = glob(os.path.join(iteration_path, '*restraints.xml')) if\
        #     not self.settings.analysis.args.get('restraint') else \
        #     [self.settings.analysis.args.get('restraint')]
        # if not restraints:
        #     # Load tbl restraints and convert them into aria xml format
        restraints = glob(os.path.join(iteration_path, '*.tbl'))
        restraints = AriaEcXMLConverter.tbl2xml(
            iteration_path, molecule_path, restraints, list_name)
        restraints = [pickler.load(restraint).restraint
                      for restraint in restraints]

        # Get native structure
        # Issue with several pdb files which have the same residue_number in
        #  atm and hetatm sections ... We remove them with bio pdb
        # Load ensemble parameters
        nbest = project.getProtocol().getIterationSettings(iteration_id)[
            "number_of_best_structures"]
        sort_crit = project.getProtocol().getIterationSettings(iteration_id)[
            "sort_criterion"]
        se_settings = StructureEnsembleSettings()
        se_settings['sort_criterion'] = sort_crit
        se_settings['number_of_best_structures'] = nbest

        protein_id = project.getSettings()['name']
        dists_ref = None
        if native_path:
            LOG.info("Reading native structure")
            logging.captureWarnings(True)
            parser = PDBParser()

            # Since segids can be unfilled in the native pdb file, we check here
            # the values according to segid in molecule object
            native = parser.get_structure(protein_id, native_path)
            molsegids = set([m.getSegid() for m in molecule.get_chains()])
            molseqs = [r.getType() for m in molecule.get_chains()
                       for r in m.getResidues()]
            natsegids = set([r.get_segid() for r in native.get_residues()])
            natseqs = [r.get_resname() for r in native.get_residues()
                       if r.get_id()[0] == ' ']
            # cmpseqs = [molseqs[idx] == natseqs[idx]
            #            for idx in range(0, min(len(molseqs), len(natseqs))-1)]

            # TODO: Doesn't work if there is substitutions in the reference file
            # if natsegids != molsegids and False not in cmpseqs:
            if natsegids != molsegids:
                if len(natsegids) == len(molsegids) == 1:
                    parser.structure_builder.init_seg(list(molsegids)[0])
                    native = parser.get_structure(protein_id, native_path)
                    # Recreate a new list of residues with the correct segid
                    newsegid = list(molsegids)[0].strip()
                    for model in native:
                        for chain in model:
                            for residue in chain:
                                if residue.get_id()[0] == ' ':
                                    newres = Residue(
                                        residue.get_id(),
                                        residue.get_resname(),
                                        newsegid
                                    )
                                    if residue.is_disordered():
                                        newres.flag_disordered()
                                    for atom in residue.get_atoms():
                                        newres.add(atom)
                                    chain.detach_child(residue.get_id())
                                    chain.insert(newres.get_id()[1]-1, newres)
                                else:
                                    chain.detach_child(residue.get_id())
                    # Detach wrong residues from native structure entity
                    # Attach the new one
                else:
                    # TODO
                    LOG.warning("Can't handle with PDB files with more than one"
                                " segid")
            native_path = os.path.join(out_path,
                                       protein_id + "_ordered.native.pdb")
            io = PDBIO()
            io.set_structure(native)
            io.save(native_path, select=NotDisordered())

            native = StructureEnsemble(se_settings)
            native.display_warnings = 0
            native.read([native_path], molecule, format='cns')

            dists_ref = native.getDistances
        # Read structure ensembles
        LOG.info("Reading structure ensemble(s)")
        # Get list of pdb related to structure ensemble(s)
        ens_paths = self._get_ensemble_paths(iteration_path)

        with Capturing() as output:
            # We define here as many structure ensemble object as number of
            # list in ens_paths (if clustering used)
            clustensembles = [
                StructureEnsemble(se_settings) for _ in ens_paths]
            [ensemble.read(ens_paths[i], molecule, format='cns')
             for i, ensemble in enumerate(clustensembles)]
            [ensemble.sort()
             for i, ensemble in enumerate(clustensembles)]
            if output:
                LOG.info(output)

        # Get the lowest energy ensemble
        # LOG.info("Sorting structure ensemble(s) with %s criteria", sort_crit)
        # energies = np.array([
        #     np.mean([d['total_energy'] for d in ens.getInfo()[:, 1]][:nbest])
        #     if len(ens) >= nbest else None for ens in clustensembles],
        #     dtype=np.float)

        # We get here the distance of 'number_of_best_structures' in the
        #  ensemble
        # ens_getdists = ensemble.getDistances
        LOG.info("Violation analysis")
        for clustidx, clustensemble in enumerate(clustensembles):
            prefix = self.settings.analysis.args.get("prefixname")
            out_file = os.path.join(out_path, "_".join(filter(None, [prefix, 'violations.csv'])))
            self.violation_analysis(project, iteration_id, restraints,
                                    clustensemble, out_file,
                                    clusteridx=clustidx, dists_ref=dists_ref,
                                    headerflag=True if clustidx == 0 else False)

        # For the plotting part, we need a StructureEnsemble instance with all
        # pdb files from the iteration

        allensemble = StructureEnsemble(se_settings)
        allensemble.read(
            files=[pdbfile for se in ens_paths for pdbfile in se],
            molecule=molecule, format='cns')

        infos = [inf for inf in allensemble.getInfo()]
        [info[1].update({'clust': idx}) for info in infos for idx, clustlist in
         enumerate(ens_paths) if filter(re.compile(info[0]).match, clustlist)]

        # Compute pca projection of cluster labels on all the
        # generated structures. Add later an option to visualize extra
        # information like RMSD, quality, ... with a csv file
        self.pca_projection(
            allensemble, molecule, [info[1].get('clust', 0) for info in infos],
            atmask=self.settings.analysis.config["atmask"],
            title="3D PCA projection on %s backbone coordinates \n%s contacts"
                  " at iteration %s" % (protein_id, list_name,
                                        str(iteration_id)),
            outfile=os.path.join(out_path, "%s_%s_it%s_clusts.3dpca.png" %
                                 (protein_id, list_name, str(iteration_id)))
        )

        # with open(os.path.join(iter_dir,
        #                        "analysis/pyfit/accuracydssp/RMSD.dat")) as rmsdfile:
        #     accdssp = {key: float(value) for key, value in
        #                [line.split() for line in rmsdfile if
        #                 re.search("pdb", line)]}

        # [info[1].update({'ensemble': False})
        #  for info in infos]
        # [info[1].update({'ensemble': True})
        #  for info in infos
        #  for clustlist in [clustlist[0:15] for clustlist in clustlists] if
        #  filter(re.compile(info[0]).match, clustlist)]
        # [info[1].update(
        #     {'accdssp': accdssp.get(os.path.basename(info[0]), None)})
        #  for info in infos]
