# coding=utf-8
"""
                            Input/Output aria_ec scripts
"""
from __future__ import absolute_import, division, print_function

import collections
import datetime
import itertools
import json
import logging
import math
import numpy as np
import os
import pandas as pd
import pickle
import pkg_resources as pkgr
import random
import re
import textwrap
import tempfile
from ..core.AriaXML import AriaXMLPickler
from ..core.ConversionTable import ConversionTable
from ..core.DataContainer import DATA_TYPES, DATA_SEQUENCE, DATA_RDCS, \
    DATA_ANNEALING_RDC
from ..core.Molecule import Molecule
from ..core.PDBReader import PDBReader
from ..core.conversion import Converter, SequenceList, MoleculeSettings
from ..core.tools import string_to_segid
from future.utils import iteritems
from mako.template import Template
from tqdm import tqdm

from ..core.legacy import AminoAcid as AminoAcid
from .common import get_filename, TqdmToLogger
from .protein import Protein
from .protmap import (ResAtmMap, ResMap, SsAaAtmMap)
from .reader import MapFileListReader, TblDistFile, MapFile
from .settings import AriaEcSettings

LOG = logging.getLogger(__name__)


class AriaEcBbConverter(object):
    """Bbcontacts convert main class"""

    def __init__(self, settings):
        # TODO: check_type settings (AriaEcSettings)
        self.settings = settings
        self.protein = Protein(settings)
        self.reader = MapFileListReader()
        self.outprefix = ''

    def run(self):
        """BBConverter main command"""
        # Check input
        """
        main method
        """
        LOG.debug("Settings:\n" + json.dumps(self.settings.setup.config,
                                             indent=4))
        LOG.debug("Args:\n" + json.dumps(self.settings.setup.args,
                                         indent=4))
        # TODO: redirect print output to logging ? (low priority)
        # ----------------------------- Input -------------------------------- #
        self.outprefix = get_filename(self.settings.bbconv.args.get("seq",
                                                                    None))
        # Load Sequence file
        self.protein.set_aa_sequence(self.settings.bbconv.args.get("seq", None))
        # Load secondary structure prediction file
        self.protein.set_sec_struct(self.settings.bbconv.args.get("sspred",
                                                                  None))
        self.reader.read(self.settings.bbconv.args.get("contactfile"),
                         maptypes=[
                             self.settings.bbconv.args.get("contact_type")],
                         protein=self.protein,
                         groupby_method=self.settings.setup.config[
                             'groupby_method'],
                         scsc=self.settings.scsc_min)
        # ---------------------------- Processing ---------------------------- #
        # Convert contacts into couplingmatrix file (CCMPred format)
        self.reader.maps[0].mapdict["scoremap"].to_csv(
            "%s/%s.mat" % (self.settings.outdir, self.outprefix),
            index=False, header=False, sep="\t", float_format="%13.10f")

        self.settings.bbconv.config['couplingmatrix'] = "%s/%s.mat" % (
            self.settings.outdir, self.outprefix)
        # Convert psipred format (fasta-like)
        self.protein.sec_struct.write_ssfasta("%s/%s.psipred" % (
            self.settings.outdir, self.outprefix), desc=self.outprefix)
        self.settings.bbconv.config['psipredfile'] = "%s/%s.psipred" % (
            self.settings.outdir, self.outprefix)
        # Compute diversityvalue
        self.settings.bbconv.config['diversityvalue'] = \
            self.compute_diversityvalue(self.settings.bbconv.args.get("msa"),
                                        len(self.protein.aa_sequence.sequence))
        # Generate .ini file with bbconv options (input args for bbcontacts)
        # TODO: Generate with only bbconv option !
        self.settings.write_config("%s/%s_bbconv.ini" % (self.settings.outdir,
                                                         self.outprefix))

    @staticmethod
    def compute_diversityvalue(msa, seqlen):
        """
        Compute diversity score related to multiple sequence alignment

        Parameters
        ----------
        msa : str
            multiple sequence alignment file path
        seqlen : int
            protein sequence length

        Returns
        -------
        diversity_score : float
            Diversity score

        Notes
        -----
        Diversity score correspond to the square root of the multiple
        sequence alignment length (:math:`m`) over the length of the protein
        sequence (:math:`l`)

        .. math:: Divscore = \\frac{\\sqrt{m}}{l}

        """

        msa_reg = re.compile(r"^>[A-Za-z0-9]+_[A-Za-z0-9]+")
        msalen = 0

        with open(msa) as msafile:
            for line in msafile:
                match = msa_reg.match(line)
                if match and "deselect" not in line:
                    msalen += 1

        return math.sqrt(msalen) / float(seqlen)


class AriaXMLConverter(Converter, object):
    """XML converter for aria files"""

    def __init__(self, settings):
        Converter.__init__(self)
        self._mol_set = MoleculeSettings()
        self._pickler = AriaXMLPickler()
        self._molecule = None
        self._upflag = False
        self._sequence = None
        self.outprefix = ""
        self.settings = settings
        self.structure = None

    @property
    def molecule(self):
        """
        aria.Molecule.Molecule object or None. If a structure has been loaded,
        use it to update the molecule

        Parameters
        ----------

        Returns
        -------

        
        """
        if self.structure and self._molecule and not self._upflag:
            # update self._molecule with self.structure
            # change value of self._upflag
            self.upd_mol(self._molecule, self.structure)
            self._upflag = True
        else:
            return self._molecule

    @property
    def sequence(self):
        return self._sequence

    @staticmethod
    def upd_mol(molecule, structure):
        """
        Update molecule object according to pdb structure

        Parameters
        ----------
        molecule :
            
        structure :
            

        Returns
        -------

        
        """
        # Check if pdb structure is in the same format as molecule
        # Loop over pdb structure and update/remove atoms in molecule chains (cf
        # aria molecule and chain methods
        # TODO
        pass

    def read_seq(self, seqpath):
        """
        Load aria Molecule object from seq file

        Parameters
        ----------
        seqpath :
            

        Returns
        -------

        
        """
        self._mol_set['format'] = 'seq'
        self._mol_set['input'] = os.path.abspath(str(seqpath))
        self._mol_set['output'] = os.path.join(self.settings.infra["xml"],
                                               self.outprefix + ".xml")
        self._mol_set['type'] = 'PROTEIN'
        self._mol_set['segid'] = '   A'
        self._mol_set['first_residue_number'] = 1
        self._mol_set['naming_convention'] = ''
        self._mol_set['name'] = self.outprefix

        segids = self._mol_set['segid']
        segids = segids.split('/')

        segids = [string_to_segid(segid) for segid in segids]
        # Fetch molecule
        chain_types = {}
        for segid in segids:
            chain_types[segid] = self._mol_set['type']

        self._sequence = SequenceList(chain_types,
                                      self._mol_set['first_residue_number'])

        # with Capturing() as output:
        # Initialize SequenceList object with seq file
        self._sequence.parse(self._mol_set['input'], self._mol_set['format'],
                             self._mol_set['naming_convention'])

        # LOG.info("\n" + "".join(output))

        # Initialize aria Atom factory
        factory = self.create_factory()

        factory.reset()
        factory.unfreeze()

        # Create chains in SequenceList object with aria atom factory
        chains = self._sequence.create_chains(factory)

        # Instantiate aria molecule object
        self._molecule = Molecule(self._mol_set['name'])
        for segid in segids:
            self.molecule.add_chain(chains[segid])

    def read_pdb(self, pdbpath):
        """
        

        Parameters
        ----------
        pdbpath :
            

        Returns
        -------

        
        """
        # read pdb file with aria pdb reader and load it in self.structure
        self.structure = PDBReader().read(pdbpath)

    def write_xmlseq(self):
        """Use aria xml pickler to generate xml molecule file"""

        try:
            self._pickler.dump(self.molecule, self._mol_set['output'])

        except Exception as msg:
            LOG.critical("Error for writing xml seq file %s: %s",
                         self._mol_set['output'], msg)
            raise

        return self._mol_set['output']

    @staticmethod
    def deff(atm_dists, dpow=6):
        """
        Compute aria effective distance from input distances

        Parameters
        ----------
        atm_dists : list
            list of atm-atm distances
        dpow : int
            effective distance exponant (Default value = 6)

        Returns
        -------

        
        """
        return math.pow(sum([math.pow(x, -int(dpow)) for x in
                             atm_dists]), -1 / 6)

    # TODO: should define distance restraints in a class, not a dict
    # TODO: Use mako
    @staticmethod
    def write_dist_xml(dist_restraints, outfile):
        """
        Write aria distance restraint xml file

        Parameters
        ----------
        dist_restraints :
            
        outfile :
            

        Returns
        -------

        
        """
        xml_file = open(outfile, "w")
        # Pour chaque contrainte
        # Pour chaque contribution
        xml_content = ""
        for restraint_id in dist_restraints:
            restraint = {
                'rest_id': restraint_id,
                'rest_weight': dist_restraints[restraint_id]["meta"]["weight"],
                'rest_dist': float(
                    dist_restraints[restraint_id]["meta"]["distance"]),
                'lower_bound': float(
                    dist_restraints[restraint_id]["meta"]["lower_bound"]),
                'upper_bound': float(
                    dist_restraints[restraint_id]["meta"]["upper_bound"]),
                'reliable': dist_restraints[restraint_id]["meta"]["reliable"],
                'list_name': dist_restraints[restraint_id]["meta"]["list_name"]
            }
            xml_content += '''\
  <restraint id="{rest_id}" weight="{rest_weight:.1f}" \
distance="{rest_dist:.2f}" lower_bound="{lower_bound:.2f}"\
 upper_bound="{upper_bound:.2f}" active="1" \
reliable="{reliable}" list_name="{list_name}">
'''.format(**restraint)
            for contrib_id in dist_restraints[restraint_id]["contrib"]:
                res_id = dist_restraints[restraint_id]["contrib"][contrib_id][
                    "spin_pair"].keys()
                # TODO: Get segids from molecule file instead
                segids = dist_restraints[restraint_id]["contrib"][contrib_id][
                    "meta"].get("segids")
                segid1 = segids[res_id[0]] if segids else "   A"
                segid2 = segids[res_id[1]] if segids else "   A"
                xml_content += '''\
    <contribution id="{id}" weight="{weight:.8f}">
      <spin_pair>
        <atom segid="{res1_segid}" residue="{res1_pos}" name="{atm1_name}"/>
        <atom segid="{res2_segid}" residue="{res2_pos}" name="{atm2_name}"/>
      </spin_pair>
    </contribution>
'''.format(
                    id=contrib_id, res1_segid=segid1, res2_segid=segid2,
                    weight=dist_restraints[restraint_id]["contrib"][contrib_id][
                        "meta"]["weight"],
                    res1_pos=res_id[0], res2_pos=res_id[1],
                    atm1_name=
                    dist_restraints[restraint_id]["contrib"][contrib_id][
                        "spin_pair"][res_id[0]],
                    atm2_name=
                    dist_restraints[restraint_id]["contrib"][contrib_id][
                        "spin_pair"][res_id[1]])
            xml_content += '''\
  </restraint>
'''
        if xml_content:
            xml_file.write('''\
<!DOCTYPE distance_restraint_list SYSTEM "distance_restraint1.0.dtd">
<distance_restraint_list>
{xml_content}
</distance_restraint_list>
'''.format(xml_content=xml_content))

    @staticmethod
    def _write_helix_hb_tbl(sec_struct, outfile, dminus, dplus):
        """
        

        Parameters
        ----------
        sec_struct :
            
        outfile :
            
        dminus :
            
        dplus :
            

        Returns
        -------

        
        """
        outfile.write("! Short range Hbond list (Alpha helix)")
        n = 0
        for idx, secstr in enumerate(sec_struct):
            if (idx + 4) < len(sec_struct):
                if 'H' in secstr[2] and 'H' in sec_struct[idx + 4][2]:
                    # Si residus dans helice
                    res1 = idx + 1
                    res2 = idx + 5
                    outfile.write('''\
assign (resid {res1} and name o) (resid {res2} and name n)  2.8 {dminus} {dplus} weight 1.0 !spec=hbond
assign (resid {res1} and name o) (resid {res2} and name hn)  1.8 {dminus} {dplus} weight 1.0 !spec=hbond
'''.format(res1=res1, res2=res2, dminus=dminus, dplus=dplus))

    @staticmethod
    def _write_hbmap_tbl(hbmap, outfile, dminus, dplus, n_hb=None,
                         hb_type="main", topo=None):
        """
        Build hbdond distance restraints from a res-res contact map. Tbl
        restraints generated use pseudoatoms since we assume we don't know
        which donnor/acceptor are involved.

        Parameters
        ----------
        hbmap :
            Hbond contact map with first humanidx =  ['donor',
            'acceptor'] => Non symetric matrix !!!
        outfile :
            param dminus:
        dplus :
            param n_hb:
        hb_type :
            Long range hbond type (Default value = "main")
        dminus :
            
        n_hb :
            (Default value = None)
        topo :
            (Default value = None)

        Returns
        -------

        
        """
        # TODO: test with several deviation since these restraints should
        # contain noise !!
        # Parcourt la matrice de contact
        outfile.write('''\
! Long range Hbond list (beta sheet)
''')
        contacts = hbmap["scoremap"].sortedset(human_idx=True)
        # distmap = hbmap.get("distmap")
        if n_hb:
            contacts = contacts[:n_hb]
        LOG.debug(contacts)
        donors = []
        acces = []
        for contact in contacts:
            # The first residu represent the hydrogen bond donor and the
            # second residu represent the hydrogen bond acceptor
            res1, res2 = contact
            if res1 not in donors + acces and res2 not in donors + acces:
                # Add only if new donor and new acces don't belong to
                # previous hbond restraints
                donors.append(res1)
                acces.append(res2)
                # residx1, residx2 = int(res1) - 1, int(res2) - 1
                # TODO: find a way to use dist
                # dist = distmap.iat[(residx1, residx2)]
                if hb_type == "all":
                    # This method is deprecated seems it should add noise ...
                    # if topo:
                    #     print(topo)
                    #     print(res1, res2)
                    #     print(topo[residx1][1]['dono'])
                    #     print(topo[residx2][1]['acce'])
                    #     print([x[0] for x in topo])
                    #     print(hbmap['maplot'].sequence[residx1])
                    #     print(hbmap['maplot'].sequence[residx2])
                    #     print(distmap.index[residx2])
                    #     print(dist)
                    #     print(hbmap["scoremap"])
                    # Get all donor from cns topology file and make
                    # group atom/pseudoatm
                    # Get all acceptor from cns topology file
                    # !!!! Ignore case flag !!!!!
                    raise NotImplementedError
                else:
                    if hb_type != "main":
                        LOG.error("Wrong longrange hbond type given. Default "
                                  "option used (main)")
                    # First residue = donor for a backbone hydrogen bond
                    # (N-H...O=C)
                    outfile.write('''\
    assign (resid {res1} and name o) (resid {res2} and name n)  2.8 {dminus} {dplus}
    assign (resid {res1} and name o) (resid {res2} and name hn)  1.8 {dminus} {dplus}
    '''.format(res1=res1, res2=res2, dminus=dminus, dplus=dplus))
        LOG.info("Writing %d hbonds from hbmap in %s", len(donors),
                 outfile.name)

    def write_hb_tbl(self, protein, outfile, hbmap=None, dminus=0.0,
                     dplus=0.5, n_hb=None, lr_type='main'):
        """
        

        Parameters
        ----------
        protein :
            param outfile:
        hbmap :
            param dminus: (Default value = None)
        dplus :
            param n_hb: (Default value = 0.5)
        lr_type :
            return: (Default value = 'main')
        outfile :
            
        dminus :
            (Default value = 0.0)
        n_hb :
            (Default value = None)

        Returns
        -------

        
        """
        with open(outfile, 'w') as outfile:
            # Write short range hbond tbl restraints
            if protein.sec_struct:
                self._write_helix_hb_tbl(protein.sec_struct.ss_matrix, outfile,
                                         dminus,
                                         dplus)
            if hbmap:
                # TODO: add another flag in order to generate hbmap xml
                # ambig. Tbl pseudoatoms is probably not the best way to use
                # hbmap
                self._write_hbmap_tbl(hbmap, outfile, dminus, dplus,
                                      n_hb=n_hb, hb_type=lr_type,
                                      topo=protein.topology)

    @staticmethod
    def write_ssdist_tbl(sec_struct, ss_dist, outfile, weight):
        """
        

        Parameters
        ----------
        sec_struct :
            
        ss_dist :
            
        outfile :
            

        Returns
        -------

        
        """
        # Build global secondary structure distance restraints (H-H+4, E-E+1,
        #  ...)
        with open(outfile, 'w') as outfile:
            for resid1 in range(len(sec_struct) - 1):
                for resid2 in range(resid1 + 1, len(sec_struct)):
                    ss1 = sec_struct[resid1][2]
                    ss2 = sec_struct[resid2][2]
                    if (ss1 != ss2) or 'C' in ss1 or 'C' in ss2:
                        continue
                    else:
                        gap = resid2 - resid1  # ecart en nombre de residus
                        ss_dict_key = '%s,%s+%d' % (ss1[0], ss2[0], gap)
                        ss_dist_subdict = ss_dist.get(ss_dict_key)
                        if ss_dist_subdict:
                            for atoms, (dist, sdev) in ss_dist_subdict.items():
                                if dist is not None and sdev is not None:
                                    atm1, atm2 = tuple(atoms.split('-'))
                                    res1 = resid1 + 1
                                    res2 = resid2 + 1
                                    outfile.write('''\
assign (resid {res1} and name {atm1}) (resid {res2} and name {atm2})  {dist} {sdev} {sdev} weight {weight} !spec=ssdist
'''.format(res1=res1, atm1=atm1, res2=res2, atm2=atm2, dist=dist, sdev=sdev,
           weight=weight))

    @staticmethod
    def write_dihedral_tbl(secstructs, outfile, knrj=0.5, exp=2):
        """
        Build dihedral angle restraints (phi, psi) in tbl format

        Parameters
        ----------
        secstructs :
            
        outfile :
            
        knrj :
            (Default value = 0.5)
        exp :
            (Default value = 2)

        Returns
        -------

        
        """
        with open(outfile, "w") as outfile:
            for i in range(len(secstructs)):
                if (i + 1) != len(secstructs):
                    if secstructs[i][2] == secstructs[i + 1][2]:
                        # Si ss identique
                        secstruct = secstructs[i][2][0]
                        res1 = i + 1
                        res2 = i + 2

                        if secstruct == 'H':
                            phi = -57.0
                            psi = -47.0
                            anglerange = 7.0
                        elif secstruct == 'E':
                            phi = -127.0
                            psi = 122.0
                            anglerange = 20.0
                        else:
                            phi, psi, anglerange = (None, None, None)

                        if phi and psi:
                            outfile.write('''\
assign (resid {res1} and name c) (resid {res2} and name n) (resid {res2} and name ca) \
(resid {res2} and name c)  {k} {phi} {anglerange} {e}
assign (resid {res1} and name n) (resid {res1} and name ca) (resid {res1} and name c) \
(resid {res2} and name n)  {k} {psi} {anglerange} {e}
'''.format(res1=res1, res2=res2, k=knrj, phi=phi, psi=psi,
           anglerange=anglerange, e=exp))


class AriaEcXMLConverter(AriaXMLConverter):
    """XML converter ariaec to aria"""

    # TODO: use aria api instead to generate xml files
    def __init__(self, *args, **kwargs):
        self.restraint_list = []
        super(AriaEcXMLConverter, self).__init__(*args, **kwargs)

    @staticmethod
    def _write_hbmap_tbl(hbmap, outfile, dminus, dplus, n_hb=None,
                         hb_type="main", topo=None):
        """
        

        Parameters
        ----------
        hbmap :
            
        outfile :
            
        dminus :
            
        dplus :
            
        n_hb :
            (Default value = None)
        hb_type :
            (Default value = "main")
        topo :
            (Default value = None)

        Returns
        -------

        
        """
        AriaXMLConverter._write_hbmap_tbl(hbmap, outfile, dminus, dplus)

    # def atm_product(self, idx1, res1, idx2, res2, list_type="min", product=True):
    #
    #     """
    #
    #
    #     Parameters
    #     ----------
    #     idx1 :
    #     res1:
    #     idx2 :
    #     res2:
    #     list_type :
    #         type of atoms in the product (Default value = "min")
    #     product :
    #     res1 :
    #
    #     res2 :
    #
    #
    #     Returns
    #     -------
    #
    #
    #     """
    #
    #     def resname(res):
    #         """
    #
    #
    #         Parameters
    #         ----------
    #         res :
    #             return:
    #
    #         Returns
    #         -------
    #
    #
    #         """
    #         return AminoAcid.AminoAcid(res)[0]
    #
    #     def min_filter(aa1, aa2, atms):
    #         """
    #
    #
    #         Parameters
    #         ----------
    #         aa1 : residue name 1
    #
    #         aa2 : residue name 2
    #
    #         atms : atom pair list of residue 1
    #
    #
    #         Returns
    #         -------
    #
    #
    #         """
    #         return [atm for atm in atms if atm in (
    #             'CA', 'CB',
    #             self.settings.scsc_min[resname(aa1)][resname(aa2)][0])]
    #
    #     atms1 = self.molecule.get_chains()[0].getResidues()[idx1].atoms.keys()
    #     atms2 = self.molecule.get_chains()[0].getResidues()[idx2].atoms.keys()
    #
    #     if list_type == "min":
    #         atms1 = min_filter(res1, res2, atms1)
    #         atms2 = min_filter(res2, res1, atms2)
    #     elif list_type == "heavy":
    #         atms1 = filter(ResAtmMap.heavy_reg.match, atms1)
    #         atms2 = filter(ResAtmMap.heavy_reg.match, atms2)
    #     elif list_type != 'all':
    #         LOG.warning("Wrong pair_list option. Pair_list set to min")
    #         atms1 = min_filter(res1, res2, atms1)
    #         atms2 = min_filter(res2, res1, atms2)
    #
    #     if product:
    #         return list(itertools.product(atms1, atms2))
    #     else:
    #         # Return atm1-atm2 list if both atoms are of the same type (except
    #         # for min atom list with the side chain atom
    #         return [
    #             (atm1, atm2) for (atm1, atm2) in list(itertools.product(atms1, atms2))
    #             if atm1 == atm2 or (
    #                 atm1 not in ('CA', 'CB')
    #                 and atm2 not in ('CA', 'CB'))] if list_type == 'min' else [
    #             (atm1, atm2) for (atm1, atm2) in list(itertools.product(atms1, atms2))
    #             if atm1 == atm2]

    def atm_list(self, idx1, res1, idx2, res2, list_type="min"):
        """
        
        Parameters
        ----------
        idx1
        res1
        idx2
        res2
        list_type

        Returns
        -------

        """

        def resname(res):
            """


            Parameters
            ----------
            res :
                return:

            Returns
            -------


            """
            return AminoAcid.AminoAcid(res)[0]

        def min_filter(aa1, aa2, atms):
            """


            Parameters
            ----------
            aa1 : residue name 1

            aa2 : residue name 2

            atms : atom pair list of residue 1


            Returns
            -------


            """
            return [atm for atm in atms if atm in (
                'CA', 'CB',
                self.settings.scsc_min[resname(aa1)][resname(aa2)][0])]

        atms1 = self.molecule.get_chains()[0].getResidues()[idx1].atoms.keys()
        atms2 = self.molecule.get_chains()[0].getResidues()[idx2].atoms.keys()

        if list_type == "min":
            atms1 = min_filter(res1, res2, atms1)
            atms2 = min_filter(res2, res1, atms2)
        elif list_type == "heavy":
            atms1 = filter(ResAtmMap.heavy_reg.match, atms1)
            atms2 = filter(ResAtmMap.heavy_reg.match, atms2)
        elif list_type != 'all':
            LOG.warning("Wrong pair_list option. Pair_list set to min")
            atms1 = min_filter(res1, res2, atms1)
            atms2 = min_filter(res2, res1, atms2)

        return tuple(atms1), tuple(atms2)

    def atm_product(self, idx1, res1, idx2, res2, list_type="min",
                    adr_flag=False, product_type="allvsall"):
        """


        Parameters
        ----------
        product_type
        adr_flag
        idx1 :
        res1:
        idx2 :
        res2:
        list_type :
            type of atoms in the product (Default value = "min")
        res1 :

        res2 :


        Returns
        -------


        """

        def resname(res):
            """


            Parameters
            ----------
            res :
                return:

            Returns
            -------


            """
            return AminoAcid.AminoAcid(res)[0]

        atms1, atms2 = self.atm_list(idx1, res1, idx2, res2,
                                     list_type=list_type)

        if adr_flag and product_type == "same":
            # We return here only minimized list with equal atoms
            # Return atm1-atm2 list if both atoms are of the same type (except
            # for min atom list with the side chain atom
            return [[
                ((idx1, atm1), (idx2, atm2)) for (atm1, atm2) in
                list(itertools.product(atms1, atms2))
                if atm1 == atm2 or (
                        atm1 not in ('CA', 'CB')
                        and atm2 not in ('CA', 'CB')
                ) or (atm1, atm2) == self.settings.scsc_min[
                       resname(res1)][resname(res2)]], ] if list_type == 'min' \
                else [[(atm1, atm2) for (atm1, atm2) in
                       list(itertools.product(atms1, atms2))
                       if atm1 == atm2], ]
        elif adr_flag and product_type == "onevsall":
            # Nested list with combination between one atom against all
            # other atoms in the other list
            result = []
            for atm1 in atms1:
                result.append([
                    ((idx1, atm1), (idx2, atm2)) for (atm1, atm2) in
                    list(itertools.product([atm1], atms2))])
            return result
        elif adr_flag and product_type == "allvsall":
            # ADR with allvsall
            return [[
                ((idx1, atm1), (idx2, atm2)) for (atm1, atm2) in
                list(itertools.product(atms1, atms2))], ]
        elif not adr_flag and product_type == "same":
            # We return here only minimized list with equal atoms
            # Return atm1-atm2 list if both atoms are of the same type (except
            # for min atom list with the side chain atom
            return [
                ((idx1, atm1), (idx2, atm2)) for (atm1, atm2) in
                list(itertools.product(atms1, atms2))
                if atm1 == atm2 or (
                        atm1 not in ('CA', 'CB')
                        and atm2 not in (
                            'CA', 'CB')) or (atm1, atm2) ==
                   self.settings.scsc_min[
                       resname(res1)][
                       resname(res2)]] if list_type == 'min' else [
                (atm1, atm2) for (atm1, atm2) in
                list(itertools.product(atms1, atms2))
                if atm1 == atm2]
        else:
            # Unambig restraints, we have only one list with all possible
            # combination between atoms. No matter what kind of product type, it
            # will be the same
            return [
                ((idx1, atm1), (idx2, atm2)) for (atm1, atm2) in
                list(itertools.product(atms1, atms2))]

    def _pdbdistmaps(self):
        # TODO: use precision level, actually we only use SsAaAtmMaps
        precision_level = self.settings.setup.config.get(
            "pdbdistance_level", "ss")
        atom_groups = self.settings.setup.config.get("atom_groups", "min")

        # TODO: Do the same for heavy matrices, it is actually hard coded to min
        # matrices sinces we only have this kind of information in data/pdbdists
        pdbstasts_groups = (
            ('INTERLOWERBOUNDS', 'interlowerbounds_pdbstat'),
            ('INTERTARGET', 'intertarget_pdbstat'),
            ('INTERUPPERBOUNDS', 'interupperbounds_pdbstat'),
            ('INTRALOWERBOUNDS', 'intralowerbounds_pdbstat'),
            ('INTRATARGET', 'intratarget_pdbstat'),
            ('INTRAUPPERBOUNDS', 'intraupperbounds_pdbstat')
        )
        pdbdistmaps = {}

        for pdbstasts_group in pdbstasts_groups:
            path = self.settings.main.config.get(pdbstasts_group[1])
            pkgpath = getattr(self.settings, pdbstasts_group[0])
            # TODO: ONLY MIN PAIR ATOMS FOR TARGET PDB DIST
            targetmap = SsAaAtmMap(atom_groups=atom_groups)

            if path and not os.path.exists(path) or not path:
                with pkgr.resource_stream(__name__, pkgpath) as pkgfile:
                    pdbdistmap = pickle.load(pkgfile)
            else:
                try:
                    with open(path) as pdbdistmapfile:
                        pdbdistmap = pickle.load(pdbdistmapfile)
                except (IOError, KeyError, TypeError):
                    with pkgr.resource_stream(__name__, pkgpath) as pkgfile:
                        pdbdistmap = pickle.load(pkgfile)

            pdbdistmap = pd.DataFrame.from_dict(pdbdistmap)
            # We do not want 0. values here ...
            pdbdistmap.replace(0., np.NaN, inplace=True)
            pdbdistmap.columns.set_names(
                ['SecStruct', 'AminoAcid', 'Atom'], inplace=True)
            pdbdistmap.index.set_names(
                ['SecStruct', 'AminoAcid', 'Atom'], inplace=True)
            pdbdistmap.sort_index(inplace=True)
            pdbdistmap.sort_index(axis=1, inplace=True)
            targetmap.sort_index(inplace=True)
            targetmap.sort_index(axis=1, inplace=True)
            targetmap[:] = pdbdistmap[:]
            # # TODO: Find a way to rearrange index or
            pdbdistmaps[pdbstasts_group[0]] = targetmap

        return pdbdistmaps

    @staticmethod
    def neighcontact(contact, maxidx, offset=0):
        """
        
        Parameters
        ----------
        contact: tuple object
        maxidx: int
        offset: int

        Returns
        -------

        """
        neighcontacts = []
        # Add N - 1 contact
        if 0 + offset not in contact:
            neighcontacts.append((contact[0] - 1, contact[1] - 1))
        neighcontacts.append((contact[0], contact[1]))
        # Add N + 1 contact
        if maxidx not in contact:
            neighcontacts.append((contact[0] + 1, contact[1] + 1))
        return neighcontacts

    @staticmethod
    def neighcontacts(contacts, maxidx, offset=0):
        """
        
        Parameters
        ----------
        contacts: list of tuple
        maxidx: int
            Maximum index value in pairs
        offset: int
            If indexes in pairs start by 0 or 0 + offset

        Returns
        -------

        """
        neighcontacts = []

        for contact in contacts:
            neighcontacts += AriaEcXMLConverter.neighcontact(
                contact, maxidx, offset)

        return neighcontacts

    def targetdistmaps(self, sequence, distfile=None):
        """
        

        Parameters
        ----------
        distfile :
            param groupby: (Default value = None)
        sequence :
            

        Returns
        -------

        
        """
        # TODO: valeur par defaut de distfile au  fichier contenant les infos
        #  fixes
        # ! target est une ResAtmMap pour les 20 aa, sinon il s'agit d'une
        # carte de distance (native ou predite)
        # Fixed
        # -----
        #   => target contient des infos pour les 20 aa qui modifieront le champ
        #      distance de la contrainte
        #   => AaAtmMap (20 aa) OU AtmMap
        # pdbstat
        # -------
        #   => target contient des infos pour les 20 aa. Ces valeurs
        #      correspondant au mode des distribution des structures de haute
        #      resolution.
        #   => Construire Maps en fonction des modes des distribution
        #   => SsAaAtmMap (Ajout info structure secondaire) OU AaAtmMap (20
        # aa) OU AaMap (20 aa) OU AtmMap pour les 20 aa
        # distfile
        # ---------
        #   => target contient les infos de distances predites en fonction
        # des positions
        #   => ResMap (contacts pdb ou predits)

        # 2 solutions when we use pdbstat:
        #
        targetmaps = {
            'INTERLOWERBOUNDS': None,
            'INTERTARGET': None,
            'INTERUPPERBOUNDS': None,
            'INTRALOWERBOUNDS': None,
            'INTRATARGET': None,
            'INTRAUPPERBOUNDS': None
        }
        distype = self.settings.setup.config.get("distance_type", "fixed")

        if distype == "distfile" and distfile:
            # Retourne ResAtmMap associe au fichier de distance pdb
            targetmaps['INTERTARGET'] = distfile.mapdict.get('alldistmap')
        elif distype == "fixed":
            # Fixed distances with bound in aria_ec.ini
            # retourne ResAtmMap (20 aa)
            # targetmap = ResAtmMap(sequence)
            # targetmap[:] = self.settings.setup.config["restraint_distance"]
            # targetmaps['INTERTARGET'] = targetmap
            targetmaps['INTERTARGET'] = self.settings.setup.config[
                "restraint_distance"]
        elif distype == "pdbstat":
            # TODO: pdbstats a partir d'une matrice pickle (faire une
            # fonction qui genere la matrice en dehors et la sauvegarde
            # dans le dossier data)
            # Met a jour les valeurs de targetmaps en fonction de celles dispo
            #  dans pdbstat

            # settings = self.settings.setup.config
            # pdbdict = {atmbound.replace('_upper_bound', ''): settings.get(
            #     atmbound, None) for atmbound in (bound for bound in settings
            #                                      if '_upper_bound' in bound.lower())}
            # targetmaps.subfill(pdbdict, level=1)
            # targetmaps = SsAaAtmMap()
            targetmaps = self._pdbdistmaps()
        else:
            # retourne fixed AtmMap
            raise ValueError("Unknown distance_type %s or wrong distance file" %
                             distype)
        return targetmaps

    def get_dist(self, distmap, contribs, defaultdist, protein,
                 groupby=None):
        """
        Get target distance from distance map
        
        Parameters
        ----------
        distmap
        contribs
        defaultdist
        protein
        groupby

        Returns
        -------

        """

        def resname(res):
            """


            Parameters
            ----------
            res :
                return:

            Returns
            -------


            """
            return AminoAcid.AminoAcid(res)[1]

        distype = self.settings.setup.config.get("distance_type", "fixed")
        groupby = groupby if groupby else self.settings.setup.config[
            "groupby_method"]
        # TODO: Avoid reduce call if possible (slow)
        if distype != "fixed":
            distmap = distmap if protein.sec_struct else distmap.reduce(
                groupby=groupby)
        # Definir le type de la carte Puis
        # Calculer l'index relatif a la paire ou la liste de paires d'atomes
        dist_list = []

        if type(distmap) == ResAtmMap and \
                distmap.sequence == protein.aa_sequence.sequence:
            for contrib in contribs:
                mapidx_x = distmap.index.levels[0][contrib[0][0]]
                mapidx_y = distmap.index.levels[0][contrib[1][0]]
                dist_list.append(distmap.loc[
                                     mapidx_x, contrib[0][1]][
                                     mapidx_y, contrib[1][1]])
        elif type(distmap) == SsAaAtmMap and not protein.sec_struct:
            # TODO: Actually only inter distances used
            # TODO: Works only with minimal target map (with CA, CB, SC indexes)
            for contrib in contribs:
                residx1 = contrib[0][0]
                res1 = resname(protein.aa_sequence.sequence[residx1])
                atm1 = contrib[0][1]
                atm1 = atm1 if atm1 in ('CA', 'CB') else 'SC'
                residx2 = contrib[1][0]
                res2 = resname(protein.aa_sequence.sequence[residx2])
                atm2 = contrib[1][1]
                atm2 = atm2 if atm2 in ('CA', 'CB') else 'SC'
                idx1 = res1, atm1
                idx2 = res2, atm2
                dist = distmap.sort_index().loc[idx1][idx2]
                dist = dist if pd.notnull(dist) and dist != 0. else \
                    distmap.sort_index().loc[idx2][idx1]
                if pd.notnull(dist) and dist != 0.:
                    dist_list.append(dist)
                else:
                    dist_list.append(defaultdist)
        elif type(distmap) == SsAaAtmMap:
            # TODO: Actually only inter distances used
            # TODO: Works only with minimal target map (with CA, CB, SC indexes)
            for contrib in contribs:
                residx1 = contrib[0][0]
                res1 = resname(protein.aa_sequence.sequence[residx1])
                atm1 = contrib[0][1]
                atm1 = atm1 if atm1 in ('CA', 'CB') else 'SC'
                ss1 = protein.sec_struct.sequence[residx1][0]
                ss1 = ss1 if ss1 in ('E', 'H') else 'X'
                residx2 = contrib[1][0]
                res2 = resname(protein.aa_sequence.sequence[residx2])
                atm2 = contrib[1][1]
                atm2 = atm2 if atm2 in ('CA', 'CB') else 'SC'
                ss2 = protein.sec_struct.sequence[residx2][0]
                ss2 = ss2 if ss2 in ('E', 'H') else 'X'
                idx1 = ss1, res1, atm1
                idx2 = ss2, res2, atm2
                dist = distmap.sort_index().loc[idx1][idx2]
                dist = dist if pd.notnull(dist) and dist != 0. else \
                    distmap.sort_index().loc[idx2][idx1]
                if pd.notnull(dist) and dist != 0.:
                    dist_list.append(dist)
                else:
                    dist_list.append(defaultdist)
        elif type(distmap) == ResMap and \
                distmap.sequence == protein.aa_sequence.sequence:
            for contrib in contribs:
                dist_list.append("%.2f" % distmap.iat[
                    contrib[0][0], contrib[1][0]])
        elif type(distmap) == float:
            return distmap
        else:
            return defaultdist

        # Retourner la distance si elle est unique ou une valeur utilisant la
        # methode groupby
        if len(dist_list) == 1:
            return dist_list[0]
        elif groupby == "deff":
            # Compute effective distance from distance list between
            # all pairs in the adr
            return "%.2f" % self.deff(dist_list,
                                      self.settings.setup.config["deffpow"])
        elif groupby == "mean":
            return np.mean(dist_list)
        elif groupby == "min":
            return min(dist_list)
        else:
            return defaultdist

    def write_map_restraint(self, protein, contactmap, nb_c, targetdists,
                            scoremap=None, listname=""):
        """
        Translate maplot in ARIA XML restraint

        Parameters
        ----------
        protein
        listname :
            param contactmap: ResAtmMap for a protein (Default value = "")
        nb_c :
            Number of restraints selected
        targetdists : dict
            ResAtmMap for each atom for each amino acid
        scoremap :
            sorted list of contacts (Default value = None)
        contactmap :
            

        Returns
        -------

        """

        if scoremap is not None:
            # TODO: implement viterbiscore as scoremap for bbcontacts ?
            LOG.info("Using contact scores as selection criteria")
            contacts = scoremap.sortedset()[:nb_c]
        else:
            # TODO: Probleme selection des contacts utilises en l'absence de
            # poids:
            #       - aleatoire (implementation actuelle) => Tester la
            # robustesse
            #       - trouver une maniere d'attribuer des poids dans le cas
            # d'une matrice de distance d'un pdb ?
            #       - Def le max independent set dans le graphe (matrice de
            # contacts) => utiliser tout les contact de ce set ou def des
            # poids  pour selectionner l * f contacts de ce set
            if nb_c < len(contactmap.contactset()):
                LOG.warning(
                    "Initialize sampling for contact selection with seed %s",
                    self.settings.setup.config['seed'])
                random.seed(self.settings.setup.config['seed'])
                contacts = random.sample(contactmap.contactset(), nb_c)
            else:
                LOG.warning("Using all contacts")
                contacts = contactmap.contactset()[:]

        hum_list = [(con1 + 1, con2 + 1) for con1, con2 in contacts]
        LOG.info("Selecting %d contacts", nb_c)
        LOG.debug("\n%s", '\n'.join(textwrap.wrap(
            ' '.join(["(%2d, %2d)" % pair for pair in hum_list]), width=80)))

        weight_list = list(float(10.0 / (x + 1)) for x, v in enumerate(
            contacts)) if \
            self.settings.setup.config['evfold_weight'] and \
            scoremap is not None else list(1.0 for _ in contacts)

        neigh_flag = self.settings.setup.config['neighborhood_contact']
        adr_flag = self.settings.setup.config["ambiguous_distance_restraint"]
        contribs_type = self.settings.setup.config["contributions_type"]
        atm_groups = self.settings.setup.config["atom_groups"]
        default_target_dist = self.settings.setup.config["restraint_distance"]
        # TODO: Lower bound and upper bound matrix not used actually...
        targetdistmap = targetdists.get('INTERTARGET')
        nat_reliable = self.settings.setup.config["native_reliable"]

        def resname_3l(residx):
            """
            

            Parameters
            ----------
            residx :
                return:

            Returns
            -------

            
            """
            return AminoAcid.AminoAcid(contactmap.index.values[residx][-3:])[1]

        max_seqidx = len(contactmap.sequence)
        restraint_dict = collections.OrderedDict()
        rest_id = 0
        contrib_id = 0
        tqdm_out = TqdmToLogger(LOG, level=logging.INFO)
        for contactidx, subcontacts in enumerate(
                tqdm(contacts, file=tqdm_out, mininterval=1.)):

            LOG.debug("Writing restraint related to contact %s" %
                      str((subcontacts[0] + 1, subcontacts[1] + 1)))
            # Get weight related to the contact
            contactweight = weight_list[contactidx]

            # Define Residue lists for the contact. Those lists contains
            # normally only one element by default except if we introduce
            # neighbors with neigh_flag triggered
            subcontacts = self.neighcontact(subcontacts,
                                            max_seqidx - 1) if neigh_flag \
                else [subcontacts]

            # Define list of atom pairs
            contribs_lists = []
            # Compute all lists of atompair related to the contact
            for subcontact in subcontacts:
                contribs_lists += self.atm_product(
                    subcontact[0], resname_3l(subcontact[0]), subcontact[1],
                    resname_3l(subcontact[1]), list_type=atm_groups,
                    adr_flag=adr_flag, product_type=contribs_type)

            # We have a unique atmpair list, trick to have a unique loop for
            # all kind of restraints in the following
            contribs_lists = [
                contribs_lists, ] if not adr_flag else contribs_lists

            # If ADR, build a restraint with contributions containing
            # all atom pair in atmpairs => Get 1 target distance related to
            # atmpairs list
            # Else unambig, build a restraint with one contribution get a
            # target distance for each atom pair
            for contribs_list in contribs_lists:
                rest_id += 1
                # For each atom list (only one list normally if unambig restraints)
                # If adr flag, each list is a contribution list for one restraint

                for contrib_idx, contrib in enumerate(contribs_list):

                    res1_idx, atm1 = contrib[0]
                    res2_idx, atm2 = contrib[1]

                    # Increment restraint id if not adr else keep the same
                    # restraint id for the actual contribution list
                    rest_id = rest_id + 1 if contrib_idx != 0 and not adr_flag else rest_id
                    # Increment contribution id if adr else reset to 1
                    contrib_id = contrib_id + 1 if contrib_idx != 0 and adr_flag else 1

                    lower_bound = float(
                        self.settings.setup.config["lower_bound"])
                    targetdist = float(
                        self.get_dist(targetdistmap, contribs_list,
                                      default_target_dist, protein)) if \
                        adr_flag else float(
                        self.get_dist(targetdistmap, [contrib],
                                      default_target_dist, protein))
                    upper_bound = float(
                        self.settings.setup.config["ca_upper_bound"]) if \
                        (atm1, atm2) == ("CA", "CA") and not adr_flag else \
                        float(self.settings.setup.config["cb_upper_bound"]) if \
                            (atm1, atm2) == ("CB", "CB") and not adr_flag else \
                            float(self.settings.setup.config["def_upper_bound"])

                    # TODO: Better way to define upper_bound if target > upper ???
                    upper_bound = upper_bound if targetdist <= upper_bound else \
                        targetdist + lower_bound

                    if restraint_dict.get(rest_id, None) is None:
                        restraint_dict[rest_id] = {
                            "meta": {
                                "weight": contactweight,
                                "distance": targetdist,
                                "lower_bound": lower_bound,
                                "upper_bound": upper_bound,
                                "active": 1,
                                "reliable": 1 if nat_reliable and listname in (
                                    "native", "native_full", "pdb") else 0,
                                "list_name": listname
                            },
                            "contrib": collections.OrderedDict()
                        }

                    restraint_dict[rest_id]["contrib"][contrib_id] = {
                        "meta": {
                            "weight": 1.0
                        },
                        "spin_pair": {
                            res1_idx + 1: atm1,
                            res2_idx + 1: atm2
                        }
                    }

        # Compute xml restraint file path
        xml_file = self.settings.infra["xml"] + "/" + "_".join((
            self.outprefix, listname)) + ".xml"
        LOG.info("Write %d xml distance restraints in %s", nb_c, xml_file)
        self.write_dist_xml(restraint_dict, xml_file)
        return xml_file, contacts

    def write_maplist_restraints(self, maplist, targetmaps, protein):
        """
        Generate ARIA XML restraints from maplist

        Parameters
        ----------
        maplist : list of ResAtmMap or ResMap object
            
        targetmaps : dict

        protein

        Returns
        -------

        
        """
        out = ([], [])

        for maptype in maplist:
            LOG.info("Writing %s ARIA XML distance restraints", maptype)
            outfile, pairlist = self.write_map_restraint(
                protein, maplist[maptype]['maplot'], maplist[maptype]["nb_c"],
                targetmaps, listname=maptype,
                scoremap=maplist[maptype].get("scoremap"))
            out[0].append(outfile)
            maplist[maptype]["filteredlist"] = pairlist
            out[1].append(pairlist)
        return out

    def write_tbl_restraints(self, protein, hbmap=None):
        """
        Write tbl restraints
        
        Parameters
        ----------
        protein
        hbmap
            Extra hbond map (eg: metapsicov hbonds) (Default value = None)
        n_hb

        Returns
        -------
        dict
            {'hbond': hb_file, 'dihed': dihed_file, 'ssdist': ssdist_file}
        """
        # Setting contact number limit for hbmap
        n_hb = int(len(self.sequence.bases) *
                   self.settings.setup.config.get("nf_longrange_hb"))
        dihed_file = os.path.join(self.settings.infra["tbl"], self.outprefix +
                                  "_dihed.tbl")
        hb_file = os.path.join(self.settings.infra["tbl"], self.outprefix +
                               "_hbond.tbl")
        ssdist_file = os.path.join(self.settings.infra["tbl"], self.outprefix +
                                   "_ssdist.tbl")
        if protein.sec_struct:
            LOG.info("   Dihedral restraints for secondary structures (%s)",
                     dihed_file)
            self.write_dihedral_tbl(protein.sec_struct.ss_matrix, dihed_file)
            LOG.info("   Secondary structure restraints (%s)",
                     ssdist_file)
            self.write_ssdist_tbl(protein.sec_struct.ss_matrix,
                                  protein.sec_struct.ssdist,
                                  ssdist_file,
                                  weight=self.settings.setup.config[
                                      'ss_weight'])
            LOG.info("   Helix bond restraints (%s)", hb_file)
            self.write_hb_tbl(protein, hb_file,
                              hbmap=hbmap, n_hb=n_hb,
                              lr_type=self.settings.setup.config[
                                  'longrange_hbtype'],
                              dminus=self.settings.setup.config['hb_dminus'],
                              dplus=self.settings.setup.config['hb_dplus'])
        else:
            dihed_file = None
            ssdist_file = None
            if not hbmap:
                hb_file = None
        return {'hbond': hb_file, 'dihed': dihed_file, 'ssdist': ssdist_file}

    #
    def tbl_as_xml(self, xmlseq_file, tbl_files, list_name):

        outdir_path = self.settings.infra["xml"]
        xml_files = self.tbl2xml(outdir_path, xmlseq_file, tbl_files, list_name)

        return xml_files

    def write_ariaproject(self, aria_template, seqfile, dist_files, tbl_files,
                          desclist=None):
        """
        Generate ariaproject.xml file
        
        Parameters
        ----------
        aria_template: str
            ariaproject.xml template path
        seqfile: str
            path of molecule sequence in SEQ format
        dist_files: list of str
            List of distance restraint files in ARIA XML format
        tbl_files: dict
            Distance restraint files in CNS TBL format
        desclist: list of str
            List of data descriptor for the actual run (Default value = None).

        Returns
        -------
        None
        """
        # TODO: Use aria API to build project in the future
        LOG.info("Loading aria template file %s", aria_template)

        molecule = self.molecule.getName()
        datas = desclist if desclist else ''

        templatepath = os.path.abspath(aria_template)
        ariaproj_template = Template(filename=templatepath,
                                     module_directory=tempfile.mkdtemp())

        try:
            with open(templatepath):
                pass
        except Exception as msg:
            LOG.critical("""Can't open template file "%s". %s""" %
                         (templatepath, msg))
            raise

        aria_project_dict = {}

        aria_project_dict.update(self.settings.setup.config)

        work_dir = os.path.abspath(self.settings.outdir)
        temp_dir = os.path.join(os.path.abspath(aria_project_dict['temp_root']),
                                molecule, "_".join(datas))

        for dir_path in (work_dir, temp_dir):
            if not os.path.exists(dir_path):
                LOG.info("Directory %s doesn't exist.", dir_path)
                LOG.info("Create new directory %s", dir_path)
                os.makedirs(dir_path)

        aria_project_dict['working_directory'] = work_dir
        aria_project_dict['temp_root'] = temp_dir

        if "-o" not in aria_project_dict['host_command']:
            LOG.warning("No output file defined in host_command. You should add"
                        " the output option (-o) to your command.")

        project = {'project_name': self.outprefix,
                   'date': datetime.date.today().isoformat(),
                   'file_root': self.outprefix}
        aria_project_dict.update(project)

        data_molecule = {'molecule_file': seqfile}
        aria_project_dict.update(data_molecule)

        un_dict = dict()
        am_dict = dict()

        # SS dist tbl restraint
        if tbl_files["ssdist"]:
            un_dict['ss'] = {
                'file': tbl_files["ssdist"],
                'format': self.settings.setup.config['ss_dist_format'],
                'ccpn_id': '',
                'enabled': self.settings.setup.config['ss_dist_enabled'],
                'add_to_network': self.settings.setup.config[
                    'ss_dist_add_to_network'],
                'calibrate': self.settings.setup.config['ss_dist_calibrate'],
                'run_network_anchoring': self.settings.setup.config[
                    'ss_dist_run_network_anchoring'],
                'filter_contributions': self.settings.setup.config[
                    'ss_dist_filter_contributions']}

        if self.settings.setup.config["ambiguous_distance_restraint"]:
            # Ajout dist_files a am_dict
            for distfile in dist_files:
                am_dict[distfile] = {
                    'file': distfile,
                    'format': self.settings.setup.config['dist_format'],
                    'ccpn_id': '',
                    'enabled': self.settings.setup.config['dist_enabled'],
                    'add_to_network': self.settings.setup.config[
                        'dist_add_to_network'],
                    'calibrate': self.settings.setup.config['dist_calibrate'],
                    'run_network_anchoring': self.settings.setup.config[
                        'dist_run_network_anchoring'],
                    'filter_contributions': self.settings.setup.config[
                        'dist_filter_contributions'],
                    'avg_exponent': self.settings.setup.config[
                        'dist_avg_exponent']
                }
        else:
            # Ajout dist_files a un_dict
            for distfile in dist_files:
                un_dict[distfile] = {
                    'file': distfile,
                    'format': self.settings.setup.config['dist_format'],
                    'ccpn_id': '',
                    'enabled': self.settings.setup.config['dist_enabled'],
                    'add_to_network': self.settings.setup.config[
                        'dist_add_to_network'],
                    'calibrate': self.settings.setup.config['dist_calibrate'],
                    'run_network_anchoring': self.settings.setup.config[
                        'dist_run_network_anchoring'],
                    'filter_contributions': self.settings.setup.config[
                        'dist_filter_contributions']}

        aria_project_dict.update({'unambiguous_distance_restraints': un_dict})
        aria_project_dict.update({'ambiguous_distance_restraints': am_dict})

        aria_project_dict.update({
            'hbond_dist_file': tbl_files["hbond"]
            if tbl_files['hbond'] else None})

        aria_project_dict.update({
            'dihed_angle_file': tbl_files["dihed"]
            if tbl_files['dihed'] else None})

        aria_project_dict['n_cpus'] = self.settings.setup.config.get('cpus',
                                                                     '20')

        proj_file = "%s/ariaproject.xml" % work_dir
        with open(proj_file, 'w') as proj_xml:
            LOG.info("Writing ARIA project file (%s)", proj_file)
            proj_xml.write(ariaproj_template.render(**aria_project_dict))

        # TODO: Load aria project file if exists and extract data and rdc
        # parameters (use flag in args)
        if self.settings.setup.args['ariaproject']:
            LOG.info("Reading ARIA project file")
            ecproject = self._pickler.load(proj_file)
            project = self._pickler.load(
                self.settings.setup.args['ariaproject'])
            if not self.settings.setup.args['extractall']:
                # Add only data to the generated project
                for datatype in DATA_TYPES:
                    datas = [dat for dat in project.getData(datatype)]
                    if datas:
                        ecdatas = [dat for dat in ecproject.getData(datatype)]
                        if [dat.as_dict() for dat in datas] != \
                                [dat.as_dict() for dat in ecdatas]:
                            LOG.info("Update %s data in project", datatype)
                            LOG.debug(datas)
                            LOG.debug("Old %s section:\n%s", datatype,
                                      ecproject.getData(datatype))
                            # TODO: check if sequence file is similar with the
                            #  one generated
                            if datatype == DATA_SEQUENCE:
                                [ecproject.delData(dat)
                                 for dat in ecproject.getData(datatype)]
                            [ecproject.addData(dat) for dat in datas]
                            LOG.debug("Updated %s section:\n%s", datatype,
                                      ecproject.getData(datatype))
                            if datatype == DATA_RDCS:
                                LOG.info("Update %s annealing parameters",
                                         datatype)
                                rdc_params = project.getStructureEngine().getAnnealingParameters().get(
                                    DATA_ANNEALING_RDC)
                                engine = ecproject.getStructureEngine()
                                annealing = engine.getAnnealingParameters()
                                annealing[DATA_ANNEALING_RDC] = rdc_params
                                engine.setAnnealingParameters(annealing)
                                ecproject.setStructureEngine(engine)
            else:
                raise NotImplementedError

            LOG.info("Writing new ARIA xml file (%s)", proj_file)
            self._pickler.dump(ecproject, proj_file)

    @staticmethod
    def tbl2xml(outdir_path, molecule_path, tbl_paths, list_name):
        """
        Read cns distance restraints in TBL format and convert them into ARIA
        XML format
        
        Parameters
        ----------
        outdir_path : str
            Path where ARIA XML files are saved            
        molecule_path : str
            Path of molecule file in ARIA XML format            
        tbl_paths : list of str
            List of distance restraints paths
        list_name : str
            Name of spec in tbl file which will be converted into XML format

        Returns
        -------
        xml_paths: str
            List of distance restraints in ARIA XML format
        
        """
        pickler = AriaXMLPickler()
        molecule = pickler.load(molecule_path)
        convtable = ConversionTable()
        xml_paths = []

        list_name = MapFile.conkit_alias[list_name] \
            if list_name in MapFile.conkit_alias else list_name

        # Read tbl file(s) and build dist_restraints dict
        for tbl_path in tbl_paths:
            LOG.info("Read TBL file %s", tbl_path)
            tbl = TblDistFile(tbl_path)
            tbl.load()
            xml_path = os.path.join(
                outdir_path,
                os.path.splitext(
                    os.path.basename(tbl_path))[0] + "_restraints.xml")

            xmldists = collections.OrderedDict()
            restno = None
            listflag = False

            for idline, line in iteritems(tbl.lines):

                assign_flag = True if "assign" in line.get(
                    "restflag") else False
                res1_id = int(line.get("resid1"))
                res2_id = int(line.get("resid2"))
                segid1 = line.get("segid1")
                if segid1 is None:
                    segid1 = molecule.get_chains()[0].getSegid()
                segid2 = line.get("segid2")
                if segid2 is None:
                    segid2 = segid1
                res1 = molecule.getChain(segid1)[res1_id]
                res2 = molecule.getChain(segid2)[res2_id]

                # Convert atom name
                # TBL file follow cns nomenclature for atom names
                # ARIA XML distance restraints follow iupac names
                try:
                    atm1_name = convtable.convert_atom(
                        res1.getType(), line.get("atm1").upper(), 'cns',
                        'iupac',
                        'AMINO_ACID', raise_error=0)

                    atm2_name = convtable.convert_atom(
                        res2.getType(), line.get("atm2").upper(), 'cns',
                        'iupac',
                        'AMINO_ACID', raise_error=0)
                except:
                    atm1_name = atm2_name = None

                if not res1.hasAtom(atm1_name) or not res2.hasAtom(atm2_name):
                    continue

                if not atm1_name or not atm2_name:
                    continue

                # If assign flag, initialize a new restraint
                if assign_flag:
                    listflag = False
                    restno = line.get("restno")
                    if restno is None:
                        restno = len(xmldists) + 1
                    restlistname = line.get("listname")
                    if list_name.lower() == restlistname.lower():
                        listflag = True
                        dtarg = float(line.get("dtarget"))
                        lwbound = dtarg - float(line.get("dminus"))
                        upbound = dtarg + float(line.get("dplus"))
                        xmldists[restno] = {
                            "meta": {
                                "weight": float(line.get("weight")),
                                "distance": dtarg,
                                "lower_bound": lwbound,
                                "upper_bound": upbound,
                                "active": 1,
                                "reliable": 0,
                                "list_name": restlistname
                            },
                            "contrib": collections.OrderedDict()
                        }
                        contrib_id = len(xmldists[restno]["contrib"]) + 1
                        xmldists[restno]["contrib"][contrib_id] = {
                            "meta": {
                                "weight": 1.0
                            },
                            "spin_pair": collections.OrderedDict([
                                (res1_id, atm1_name),
                                (res2_id, atm2_name)
                            ])
                        }
                # Else add contrib to restraint
                elif listflag:
                    contrib_id = len(xmldists[restno]["contrib"]) + 1
                    xmldists[restno]["contrib"][contrib_id] = {
                        "meta": {
                            "weight": 1.0
                        },
                        "spin_pair": collections.OrderedDict([
                            (res1_id, atm1_name),
                            (res2_id, atm2_name)
                        ])
                    }

            # Generate distance restraint file using self.write_dist_xml
            if len(xmldists) != 0:
                LOG.info("Wrote %d xml distance restraints in %s",
                         len(xmldists),
                         xml_path)
                xml_paths.append(xml_path)
            else:
                LOG.info("No distance restraints found in %s. %s will be empty",
                         tbl_path, xml_path)

            AriaXMLConverter.write_dist_xml(xmldists, xml_path)

        return xml_paths

    def run_tbl2xml(self):
        """Main command to convert tbl file(s) in ARIA XML Format"""
        # Check if we have the correct args in self.settings
        outdir_path = self.settings.tbl2xml.args["output_directory"]
        molecule_path = self.settings.tbl2xml.args["molecule"]
        tbl_paths = self.settings.tbl2xml.args["infiles"]
        list_name = self.settings.tbl2xml.args["listname"]

        self.tbl2xml(outdir_path, molecule_path, tbl_paths, list_name)


class AriaEcConfigConverter(object):
    """
    Configuration file converter
    """

    OUTFILE = "configs.csv"

    def __init__(self, settings):
        self.settings = settings

    @staticmethod
    def formatcell(value):
        """
        Format value in order to save it into a panda dataframe

        Parameters
        ----------
        value: object
            Instance which has to be saved into a unique pandas cell

        Returns
        -------
        value: object
            Instance compatible to value of ONE cell

        """
        if isinstance(value, list):
            return ';'.join(value)
        elif value is None:
            return np.NaN
        else:
            return value

    def conftodf(self, path=None, data=None):
        """
        Convert config file into panda dataframe

        Parameters
        ----------
        path: str
            Path to the config file
        data :

        Returns
        -------
        config: pd.Dataframe object
            pandas dataframe with all options saved as a 1xn matrix
            (n corresponding to the number of columns)
        """
        conf = AriaEcSettings('config')
        conf.load_config(path)
        columns = ['config']
        row = [
            os.path.splitext(os.path.basename(path))[0] if path else "default"]
        tmp = {
            'config': os.path.splitext(
                os.path.basename(path))[0] if path else "default"}
        for section in conf.SECTIONS:
            tmpcols = getattr(getattr(conf, section), 'config').keys()
            tmp.update({col: [getattr(getattr(conf, section), 'config')[col]]
                        for col in tmpcols})
            columns += tmpcols
            row += [self.formatcell(
                getattr(getattr(conf, section), 'config')[key])
                for key in tmpcols]
        if path is not None:
            tmpdf = data.tail(1).copy()
            tmpdf.update(pd.DataFrame.from_dict(tmp))
            data = pd.concat([data, tmpdf])
        else:
            data = pd.DataFrame.from_dict(tmp)
        return data

    def confstodf(self, paths):
        """
        Read several config files and convert them into a unique pandas
        dataframe

        Parameters
        ----------
        paths: list
            list of config files

        Returns
        -------
        maindf: pd.Dataframe object
        """
        maindf = None
        for i, path in enumerate(paths):
            maindf = self.conftodf(path, data=maindf)

            # maindf = pd.concat(maindf, df), ignore_index=True) if i != 0 else df
        return maindf

    def run(self):
        """
        Fetch config files and convert them into a unique csv file
        """
        confiles = [None] + [confile for confile in
                             self.settings.iniconv.args.get("confiles")]
        LOG.info("Reading configuration file(s)")
        configs = self.confstodf(confiles)
        LOG.info("Generate output csv file (%s)", self.OUTFILE)
        configs.to_csv(
            os.path.join(self.settings.iniconv.args.get("output_directory"),
                         self.OUTFILE)
            if os.path.exists(
                self.settings.iniconv.args.get("output_directory"))
            else self.OUTFILE, index=False)
