# coding=utf-8
"""
                    ARIA Evolutionary Constraints Tools
"""
from __future__ import absolute_import, division, print_function

from ..core import ConversionTable as ConversionTable
import collections
import csv
import datetime
import itertools
import logging
import matplotlib; matplotlib.use("Agg")
import numpy as np
import operator
import os
import pandas as pd
import re
import seaborn as sns
import six
import sklearn.metrics as skm
import string
import textwrap
from collections import defaultdict
from copy import deepcopy
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D

from ..core.legacy import AminoAcid as AminoAcid
from .common import (tickmin, tickrot, titleprint, addtup)
from .ndconv import net_deconv


LOG = logging.getLogger(__name__)
# TODO: check dataframe symmetry or always use unstack
# TODO: objet MapContainer contenant les differentes maps en attributs ! (et
# non en clef de dict)


class Map(pd.DataFrame):
    """
    Distance/contact matrix abstract class accepting only one type of value according to mtype arg

    Examples
    --------
    Score/distance matrix

    >>> d = Map(index=[0,1,2], columns=[0,1,2])
    >>> d
        0   1   2
    0 NaN NaN NaN
    1 NaN NaN NaN
    2 NaN NaN NaN
    >>> d = Map(index=[0,1,2], columns=[0,1,2], mtype='contact')
    >>> d
           0      1      2
    0  False  False  False
    1  False  False  False
    2  False  False  False

    """

    mtype_choices = {'contact': bool, 'distance': float, "score": float}
    _metadata = pd.DataFrame._metadata + ['_sort_list']

    def update(self, *args, **kwargs):
        """
        

        Parameters
        ----------
        args :
            param kwargs:
        *args :
            
        **kwargs :
            

        Returns
        -------

        
        """
        super(Map, self).update(*args, **kwargs)

    # def _constructor_expanddim(self):
    #     super(Map, self)._constructor_expanddim()

    def __init__(self, index=None, columns=None, mtype='distance',
                 duplicate_levels=False, data=None, dtype=None, sym=True,
                 desc="", path="", **kwargs):
        """
        
        Parameters
        ----------
        index
        columns
        mtype
        duplicate_levels
            Allow duplicate levels in dataframe
        data
        dtype
        sym
        desc
        path
        """
        if not dtype:
            dtype = self.check_type(mtype)
        # TODO: should probably init to np.NaN
        if data is None:
            data = False if dtype == bool else np.NaN
        super(Map, self).__init__(data=data, dtype=dtype, index=index,
                                  columns=columns)
        self.duplicate_levels = duplicate_levels
        self.mtype = mtype
        self.dtype = dtype
        if mtype == "score":
            self._sort_list = []
        self.sym = sym
        self.desc = desc
        self.path = path

    def __str__(self):
        return super(Map, self).__str__()

    @property
    def sort_list(self):
        return self._sort_list

    def sortedset(self, human_idx=False):
        """
        

        Parameters
        ----------
        human_idx :
            (Default value = False)

        Returns
        -------

        
        """
        # Remove duplicate in sort_list
        n = 1 if human_idx else 0
        if hasattr(self, "sort_list"):
            if self.sym:
                # Use OrderedDict to keep the order
                return [(x + n, y + n)
                        for x, y in
                        collections.OrderedDict.fromkeys(frozenset(x)
                                                         for x in
                                                         self.sort_list)]
            else:
                # Asym matrix, no need to remove duplicate
                return [(x + n, y + n) for x, y in self.sort_list]
        else:
            return None

    def check_type(self, mtype):
        """
        

        Parameters
        ----------
        mtype :
            return:

        Returns
        -------

        
        """
        value = self.mtype_choices.get(mtype)
        if value:
            return value
        else:
            LOG.error("Map type should be in list %s",
                      self.mtype_choices.keys())
            return None

    def reduce(self):
        """Low complexcity dataframe"""
        raise NotImplementedError

    def copy(self, **kwargs):
        """
        Copy the current map

        Parameters
        ----------
        **kwargs :
            

        Returns
        -------

        
        """
        raise NotImplementedError

    def remove(self, rm_list):
        """
        

        Parameters
        ----------
        rm_list :
            

        Returns
        -------

        
        """
        # Reset values at positions in rm_list
        value = False if self.dtype == bool else np.NaN
        for contact in rm_list:
            idx1, idx2 = self.index[contact[0]], self.index[contact[1]]
            self.set_value(idx1, idx2, value)
            # self.iat[(contact[0], contact[1])] = value
            # self.iat[(contact[1], contact[0])] = value
            if hasattr(self, 'sort_list'):
                # ! sort_list start at 1
                if (contact[0], contact[1]) in self.sort_list:
                    self.sort_list.remove((contact[0], contact[1]))
                if (contact[1], contact[0]) in self.sort_list and self.sym:
                    self.sort_list.remove((contact[1], contact[0]))

    def to_series(self):
        """Return panda series related to lower triangle values"""
        df = self.copy()
        df = df.astype(float)
        # remove values from upper triangle
        df.values[np.triu_indices_from(df, k=1)] = np.nan
        # pd.series with only lower triangle values
        return df.unstack().dropna()

    def topmap(self, scoremap, nb_topcontact):
        """
        

        Parameters
        ----------
        scoremap :
            param nb_topcontact:
        nb_topcontact :
            

        Returns
        -------

        
        """
        if self.dtype != bool:
            LOG.info("Error when retrieving top contact map. The type of "
                     "the given map is not a contact type!")
            return self
        self[:] = False
        if self.shape == scoremap.shape:
            pair_list = scoremap.sortedset()[:nb_topcontact]
            for contact in pair_list:
                self.iat[(contact[1], contact[0])] = True
                self.iat[(contact[0], contact[1])] = True
            return self
        else:
            LOG.error("Given scoremap has not the same dimension !")
            return None

    def subfill(self, pairdict, level=0):
        """
        Fill map with dict giving

        Parameters
        ----------
        pairdict :
            param level:
        level :
            (Default value = 0)

        Returns
        -------

        
        """
        pairdict = {k.upper(): v for k, v in pairdict.items()}
        if "def" in pairdict:
            self[:] = pairdict["def"]
        for idxval in pairdict:
            idx = self.index.get_level_values(level) == idxval
            self.loc[idx, idx] = pairdict[idxval]

    # TODO: remove sym attribute --> information is duplicated !!!!
    def set_value(self, index, col, value, **kwargs):
        """
        Assign value at [index, col] in the related dataframe

        >>> d = Map(index=[0,1,2], columns=[0,1,2])
        >>> d.set_value(1, 2, 2)
        >>> d.at[1, 2]
        2.0

        Parameters
        ----------
        index :
            param col:
        value :
            param kwargs:
        col :
            
        **kwargs :
            

        Returns
        -------

        
        """
        LOG.debug("Update {index}, {col} with {value} value {default}".format(index=index, col=col, value=value,
                                                                              default=self.at[index, col]))
        # super(Map, self).set_value(index, col, value, **kwargs)
        super(Map, self).at[index, col] = value
        if self.sym:
            super(Map, self).at[col, index] = value
            # super(Map, self).set_value(col, index, value, **kwargs)


# TODO: Matrices PosAaAtmMap, AaAtmMap, AtmMap
class ProteinMap(Map):
    """
    Abstract class for protein contact map objects
    """
    # TODO: matrix should be not only for heavy atoms
    # Matrix only for heavy atoms.
    heavy_reg = re.compile(r"[CNOS][ABGDEZH][0-9]?")
    all_reg = re.compile(r"^((?!cns|dyana).*)$")
    # TODO: Autre methodes de dist
    distance_method = 'euclidean'
    _metadata = Map._metadata + ['_evflags', '_maplot', '_sequence',
                                 '_atom_groups']

    def __init__(self, sequence, flaglist=None, atom_groups="min", **kwargs):
        if kwargs.get("index") is None or kwargs.get("columns") is None:
            kwargs["index"], kwargs["columns"] = self.create_index(
                sequence, **kwargs)
        super(ProteinMap, self).__init__(**kwargs)
        self._evflags = flaglist
        self._maplot = None
        self._sequence = sequence
        self._atom_groups = atom_groups

    # def _constructor_expanddim(self):
    #    return self._constructor_expanddim()
    @property
    def sequence(self):
        """
        Specific sequence related to the map in a string
        Returns
        -------
        str

        """
        raise NotImplementedError

    @property
    def contact_flags(self):
        return self._evflags

    def create_heatmap(self):
        """
        Generate heatmap

        Parameters
        ----------

        Returns
        -------


        """
        raise NotImplementedError

    def contact_map(self, contactdef, scsc_min=None):
        """
        Generate contact map

        Parameters
        ----------
        contactdef :
            param scsc_min:
        scsc_min :
            (Default value = None)

        Returns
        -------


        """
        raise NotImplementedError

    def create_index(self, sequence, **kwargs):
        """

        Parameters
        ----------
        sequence
        kwargs

        Returns
        -------

        """
        # Indexation matrice (tous les atomes ou tous les residus)
        raise NotImplementedError

    def reduce(self):
        """Lower index level if multi index"""
        # TODO: check if multiindex
        columns = ['-'.join(idx) for idx in self.columns]
        index = ['-'.join(idx) for idx in self.index]
        return getattr(self, '__init__')(self.sequence,
                                         path=self.path,
                                         data=self.as_matrix(),
                                         index=index, desc=self.desc,
                                         sym=self.sym, mtype=self.mtype,
                                         columns=columns, dtype=self.dtype)

    def copy(self, **kwargs):
        """
        Copy dataframe and related attributes of the map

        Parameters
        ----------
        **kwargs :
            

        Returns
        -------

        
        """
        df = super(Map, self).copy()
        return ProteinMap(
            sequence=self.sequence, path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)

    def plotflush(self):
        """Flush contact map plot"""
        plt.clf()
        plt.cla()
        plt.close()
        self._maplot = None

    @property
    def maplot(self, linewidths=0.0):
        """
        Contact map plot
        :return:

        Parameters
        ----------
        linewidths :
            (Default value = 0.0)

        Returns
        -------

        
        """
        # Contact map Plot
        if not self._maplot:
            # Flush matplot
            LOG.debug("Build maplot")
            minticks = tickmin(self, shift=1)  # Nb graduations

            self._maplot = sns.heatmap(self, square=True, cbar=False,
                                       linewidths=linewidths, vmax=1, vmin=-1,
                                       cmap=sns.diverging_palette(20, 220, n=7,
                                                                  as_cmap=True),
                                       xticklabels=minticks[0],
                                       yticklabels=minticks[1],
                                       linecolor="grey")
        return self._maplot

    def saveplot(self, outdir='', outprefix="protein", size_fig=10,
                 plot_ext="pdf", plot_dpi=200):
        """
        Save plot

        Parameters
        ----------
        outdir :
            param outprefix: (Default value = 'protein')
        size_fig :
            param plot_ext: (Default value = 10)
        plot_dpi :
            (Default value = 200)
        outprefix :
            (Default value = "protein")
        plot_ext :
            (Default value = "pdf")

        Returns
        -------


        """
        plotpath = os.path.join(outdir, "%s.maplot.%s" % (
            outprefix, plot_ext))
        LOG.info("Generate contact map plot (%s)", plotpath)
        f, ax = plt.subplots()
        tickrot(self.maplot.axes, self.maplot.figure)
        self.maplot.figure.set_size_inches(size_fig, size_fig)
        map_title = "%s contacts map" % outprefix
        self.maplot.set_title(map_title)
        self.maplot.figure.tight_layout()

        f.tight_layout()
        self.maplot.figure.savefig(plotpath, dpi=plot_dpi)
        plt.close('all')

    def contactset(self, human_idx=False):
        """
        Remove duplicate in contact_list

        Parameters
        ----------
        human_idx :
            return: (Default value = False)

        Returns
        -------
        list
        
        """
        contacts = self.contact_list(human_idx)
        if contacts:
            contacts = sorted(set((tuple(sorted((x, y)))
                                   for x, y in contacts)))
        return contacts

    def contact_list(self, human_idx=False):
        """
        Return contact list

        Parameters
        ----------
        human_idx :
            return: (Default value = False)

        Returns
        -------
        list
        
        """
        contact_list = []
        n = 1 if human_idx else 0
        if self.dtype is bool:
            # for irow, row in enumerate(self):
            for irow, row in enumerate(self.index):
                for icol, value in enumerate(self[row]):
                    if value:
                        contact_list.append((irow + n, icol + n))
        return contact_list

    def compareplot(self, protmap, save_fig=True, alpha=None, **kwargs):
        """
        Compare 2 contact map and plot the differences

        Parameters
        ----------
        protmap :
        save_fig :
        alpha :
        kwargs :

        Returns
        -------

        """
        self.plotflush()
        # Contact map plot
        if getattr(protmap, "shape") and self.shape != protmap.shape:
            logging.error("Cant't compare %s map with %s" % (
                protmap.__class__.__name__, self.__class__.__name__))
            return None
        else:
            cmplist = protmap.contact_list(human_idx=True)

            ymax = len(self.sequence) - 1
            if protmap.contact_flags:
                flags = set(protmap.contact_flags.values())
                # Color palette
                pal = sns.color_palette("hls", len(flags))
                for i, flag in enumerate(flags):
                    conlist = [contact for contact in protmap.contact_flags if
                               protmap.contact_flags[contact] == flag]
                    xind = [x - .5 for x in
                            zip(*conlist)[0] + zip(*conlist)[1]]
                    yind = [ymax - y + 1.5 for y in
                            zip(*conlist)[1] + zip(*conlist)[0]]
                    color = pal[i]
                    mark = Line2D.filled_markers[i]
                    for x, y in zip(xind, yind):
                        self.maplot.axes.scatter(x, y, s=8, c=color,
                                                 linewidths=0.1, alpha=alpha,
                                                 marker=mark)
            else:
                LOG.info("Contact list: %s", cmplist)
                xind = [x - .5 for x in
                        zip(*cmplist)[0] + zip(*cmplist)[1]]
                yind = [ymax - y + 1.5 for y in
                        zip(*cmplist)[1] + zip(*cmplist)[0]]
                LOG.debug("Xind: %s", xind)
                LOG.debug("Yind: %s", yind)
                color = "red"
                # width = [0.3 for _ in xind]
                # for x, y, h in zip(xind, yind, width):
                for x, y in zip(xind, yind):
                    self.maplot.axes.scatter(x, y, s=30, c=color,
                                             linewidths=0,
                                             alpha=alpha)
            if save_fig:
                self.saveplot(**kwargs)

    @staticmethod
    def _write_report(reportpath, **scores):
        """
        Write report file from score dict

        Parameters
        ----------
        reportpath :
            
        scores :
            
        **scores :
            

        Returns
        -------

        
        """
        with open(reportpath, 'w') as reportf:
            msg = string.Formatter().vformat("""\
## Report {map1name} vs. {map2name}
##
## Date: {date}
##
## Plots: {outdir}
## Reference map: {map1path}
## Contact map: {map2path}
##
## -----------------------------------------------------------------------------
##
## Sequence:
## {seq}
## Protein length: {protlen}
##
## -----------------------------------------------------------------------------
##
## Matthews correlation coefficient (MCC): {mcc}
## F1 score: {f1s}
## F2 score: {f2s}
## F0.5 score: {f05s}
##
## Precision: {precision}
## Recall (Sensibility): {recall}
## Accuracy: {accuracy}
##
## Hamming loss: {hamm}
## Hinge loss: {hin}
##
## -----------------------------------------------------------------------------
##
##                                  Plot scores
##
## ROC Area Under Curve: {roc_auc}
## Average precision score: {aver_prec}
##
## -----------------------------------------------------------------------------
##
##                          Precision recall curve
##
## Precision values:
## {allprec}
## Recall values:
## {allrec}
## Score tresholds ({map2name}):
## {prthres}
##
## -----------------------------------------------------------------------------
##
##                               ROC curve
##
## True Positive Rate (Sensibility) values:
## {alltpr}
## False Positive Rate (1 - Specificity) values:
## {allfpr}
## Score tresholds ({map2name}):
## {rocthres}""", (), defaultdict(str, **scores))
            LOG.debug("\n" + msg)
            reportf.write(msg)

    @staticmethod
    def classification_metrics(y_true, y_pred, y_scores=None):
        """
        Compute classification metrics

        Parameters
        ----------
        y_true : numpy array of true values
            
        y_pred : numpy array of predicted values
            
        y_scores : numpy array of prediction scores
            (Default value = None)

        Returns
        -------

        
        """
        metrics = {}

        if 1 in y_pred and 1 in y_true:
            metrics.update({
                'accuracy': skm.accuracy_score(y_true, y_pred),
                'precision': skm.precision_score(y_true, y_pred),
                'recall': skm.recall_score(y_true, y_pred),
                'mcc': skm.matthews_corrcoef(y_true, y_pred),
                'f1s': skm.f1_score(y_true, y_pred),
                'f2s': skm.fbeta_score(y_true, y_pred, 2),
                'f05s': skm.fbeta_score(y_true, y_pred, 0.5),
                'hamm': skm.hamming_loss(y_true, y_pred),
                'hin': skm.hinge_loss(y_true, y_pred)
            })

        if y_scores:
            # ROC plot
            # Replace nan values in y_scores
            y_scores[np.isnan(y_scores)] = np.floor(np.nanmin(y_scores))
            metrics.update({
                'roc_auc': skm.roc_auc_score(y_true, y_scores),
                'aver_prec': skm.average_precision_score(y_true, y_scores)
            })
            metrics['allfpr'], metrics['alltpr'], metrics['rocthres'] = \
                skm.roc_curve(y_true, y_scores, pos_label=1)
            metrics['allprec'], metrics['allrec'], metrics['prthres'] = \
                skm.precision_recall_curve(y_true, y_scores)

        return metrics

    @staticmethod
    def _metrics_plot(metrics, mapnames=('', ''), plotdir="", outprefix="",
                      plot_ext="pdf"):
        """
        

        Parameters
        ----------
        metrics :
            
        mapnames :
            (Default value = ('')
        '' :
            

        Returns
        -------

        
        """
        outprefix = outprefix if outprefix else "maplot"
        csv_roc = os.path.join(plotdir, "%s.roc.csv" % outprefix)
        LOG.info("Generate roc file (%s)", csv_roc)
        with open(csv_roc, "w") as f:
            f.write("TPR,FPR,Treshold\n")
            writer = csv.writer(f)
            writer.writerows(zip(metrics['alltpr'], metrics['allfpr'],
                                 metrics['rocthres']))

        plotpath = os.path.join(plotdir, "%s.roc.%s" % (outprefix,
                                                        plot_ext))
        LOG.info("Generate roc plot (%s)", plotpath)
        plt.figure()
        plt.plot(metrics['allfpr'], metrics['alltpr'],
                 label='ROC curve (area = %0.2f)' % metrics['roc_auc'])
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic %s vs. %s' % (
            mapnames[0], mapnames[1]))
        plt.legend(loc="lower right")
        plt.savefig(plotpath)

        csv_precall = os.path.join(plotdir, "%s.roc.csv" % outprefix)
        LOG.info("Generate precall file (%s)", csv_precall)
        with open(csv_precall, "w") as f:
            f.write("Precision,Recall,Treshold\n")
            writer = csv.writer(f)
            writer.writerows(zip(metrics['allprec'], metrics['allrec'],
                                 metrics['prthres']))

        plotpath = os.path.join(plotdir, "%s.precall.%s" % (outprefix,
                                                            plot_ext))
        LOG.info("Generate precall plot (%s)", plotpath)
        # Precision recall curve
        plt.clf()
        plt.plot(metrics['allrec'], metrics['allprec'],
                 label='Precision-Recall curve')
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.ylim([0.0, 1.0])
        plt.xlim([0.0, 1.0])
        plt.title('Precision-Recall {1} vs. {2}: AUC={0:0.2f}'.format(
            metrics['aver_prec'], mapnames[0], mapnames[1]))
        plt.legend(loc="lower left")
        plt.savefig(plotpath)

    def report(self, cmpmap, scoremap=None, outprefix="", outdir="", plotdir="",
               # plot_ext="pdf", plotag=True, n_factors=(0.5, 1.0, 1.5)):
               plot_ext="pdf", plotag=True):
        """
        Generate contact map report file

        Parameters
        ----------
        cmpmap :
            
        scoremap :
            (Default value = None)
        outprefix :
            (Default value = "")
        outdir :
            (Default value = "")
        plotdir :
            (Default value = "")
        plot_ext :
            (Default value = "pdf")
        plotag :
            (Default value = True)

        Returns
        -------

        """
        filename = ".".join((outprefix, "mapreport")) if outprefix else \
            "mapreport"
        reportpath = "%s/%s" % (outdir, filename)
        LOG.info("Generate map report file (%s)", reportpath)

        # TODO: evaluate map statistics for several treshold (use submap var)
        # nb_c = int(len(self.sequence) * float(n_factors[0]))
        # submap = cmpmap.copy()
        # print(np.array_equal(cmpmap.values.astype(int).flat,
        #                      submap.topmap(scoremap, nb_c).values.astype(int).flat))

        y_true = list(self.values.astype(int).flat)
        y_pred = list(cmpmap.values.astype(int).flat)
        y_scores = scoremap.values.astype(float).flat if scoremap is not None else None

        # Compute classification metrics for the entire map
        metrics = self.classification_metrics(y_true, y_pred, y_scores)

        self._write_report(
            reportpath, map1name=self.desc, map2name=cmpmap.desc,
            map1path=self.path, map2path=cmpmap.path,
            seq="\n## ".join(textwrap.wrap(self.sequence, width=77)),
            protlen=len(self.sequence),
            date=datetime.date.today().strftime("%A %d. %B %Y"),
            outdir=outdir, **metrics)

        if plotag and scoremap is not None:
            self._metrics_plot(metrics, mapnames=(self.desc, cmpmap.desc),
                               outprefix=outprefix, plot_ext=plot_ext,
                               plotdir=plotdir)

    def compare_contactmap(self, cmpmap, contactlist, outprefix,
                           outdir="", distmap=None, human_idx=True):
        """
        Compare 2 contact map and plot differences

        Parameters
        ----------
        cmpmap :
            param contactlist:
        outprefix :
            param outdir:
        distmap :
            param human_idx: (Default value = None)
        contactlist :
            
        outdir :
            (Default value = "")
        human_idx :
            (Default value = True)

        Returns
        -------

        
        """
        # CSV file giving TP/FP contacts
        outpath = "%s/%s.contactcmp.csv" % (outdir,
                                            outprefix if outprefix else "maplot")
        LOG.info("Generate stat file (%s)", outpath)
        with open(outpath, 'w') as outfile:
            offset = 1 if human_idx else 0
            extra_header = "" if distmap is None else ",dmin"
            outfile.write("#resid1,resid2,res1,res2,TP/FP%s\n" % extra_header)
            for x in contactlist:
                if extra_header:
                    dmin = "," + str(distmap.iat[(int(x[0]) - offset, int(x[1]) -
                                                  offset)])
                else:
                    dmin = ""
                contact = self.iat[(int(x[0]) - offset, int(x[1]) - offset)]
                cmpcontact = cmpmap.iat[(int(x[0]) - offset, int(x[1]) - offset)]
                if contact and cmpcontact:
                    eq = "TP"
                elif contact and not cmpcontact:
                    eq = "FP"
                elif not contact and cmpcontact:
                    eq = "FN"
                else:
                    eq = "TN"
                msg = "%s,%s,%s,%s,%s%s\n" % (x[0], x[1],
                                            self.sequence[int(x[0]) - offset],
                                            self.sequence[int(x[1]) - offset],
                                            eq,
                                            dmin)
                outfile.write(msg)

    def write_contacts(self, filename, outdir="", prefix="", human_idx=True,
                       scoremap=None):
        """
        

        Parameters
        ----------
        prefix
        filename :
            param outdir:
        human_idx :
            param scoremap: (Default value = True)
        outdir :
            (Default value = "")
        scoremap :
            (Default value = None)

        Returns
        -------

        
        """
        prefix = prefix + "_" if prefix else prefix
        filepath = "%s/%s%s.contact.txt" % (outdir, prefix, filename.replace(".", "_"))
        LOG.info("Generate contact file (%s)", filepath)
        with open(filepath, 'w') as outfile:
            offset = 1 if human_idx else 0
            # contacts = [sorted(contact) for contact in self.contactset()]
            # for contact in sorted(contacts):
            for contact in self.contactset():
                contact = sorted([contact[0] + offset, contact[1] + offset])
                if scoremap is not None:
                    score = scoremap.iat[(int(contact[0]) - offset,
                                          int(contact[1]) - offset)]
                    outfile.write("%d %d %.4f\n" %
                                  (contact[0], contact[1], score))
                else:
                    outfile.write("%d %d\n" % (contact[0], contact[1]))


class ResAtmMap(ProteinMap):
    """
    Protein distance/contact matrix for all atom pairs. If no sequence given,
    protein distance/contact matrix for all amino acids

    Attributes
    ----------

    Examples
    --------

    """

    # def _constructor_expanddim(self):
    #     super(ResAtmMap, self)._constructor_expanddim()

    def __init__(self, sequence, flaglist=None, atom_groups="min", **kwargs):
        # Sequence: 1L string or MultiIndex object
        # Dataframe is in 3L code
        if not sequence:
            sequence = ConversionTable.ConversionTable().table['AMINO_ACID'][
                'iupac'].keys()
        # Super call will initialize index and columns with self.create_index()
        super(ResAtmMap, self).__init__(sequence=sequence, flaglist=flaglist,
                                        atom_groups=atom_groups, **kwargs)

    @property
    def sequence(self):
        """
        Amino Acid sequence string in humanidx

        Parameters
        ----------

        Returns
        -------

        """
        if self._sequence:
            # If non empty string
            return self._sequence
        elif self.index:
            self._sequence = "".join(AminoAcid.AminoAcid(_.split("-")[1])[0]
                                     for _ in self.index.levels[0])
        else:
            # No information given
            self._sequence = "".join(sorted(ConversionTable.ConversionTable().table[
                'AMINO_ACID']['iupac'].keys()))
        return self._sequence

    def create_index(self, sequence, seq_pos=True, seqidx=None, idxnames=None,
                     colnames=None, **kwargs):
        """
        
        Parameters
        ----------
        sequence
        seq_pos
        seqidx: pandas.MultiIndex, optional
        idxnames
        colnames

        Returns
        -------
        tuple of pandas.MultiIndex objects
        """
        LOG.debug("Indexing ResAtm - ResAtm dataframe")
        # Atom table for residues (keys are in 3L code)
        seqidx = seqidx if seqidx and len(seqidx) == len(sequence) else None
        iupac_aa = ConversionTable.ConversionTable().table['AMINO_ACID'][
            'iupac']
        # Amino acid conversion table 1L -> 3L
        # Making humanidx list for pandas dataframe
        seq = [AminoAcid.AminoAcid(aa)[1] for aa in sequence]
        # Repeat each res for each heavy atm (humanidx value + 1 in order to
        # start at 1 as in pdb file)
        if set(sequence) == sequence:
            # General ResAtmMap for 20 aminoacid
            res_list = ["%s" % aa for aa in seq for _ in
                        (filter(self.heavy_reg.match, iupac_aa[aa].keys()))]
        elif seqidx:
            # If we already have an index related to the sequence
            res_list = [
                "%03d-%s" % (seqidx[i], aa) for i, aa in enumerate(seq)
                for _ in filter(self.heavy_reg.match, iupac_aa[aa].keys())]
        else:
            # If no index related to the sequence
            res_list = [
                "%03d-%s" % (i + 1, aa) for i, aa in enumerate(seq)
                for _ in filter(self.heavy_reg.match, iupac_aa[aa].keys())]
        # TODO: Inutile de repeter a chaque fois le calcul des listes
        # d'atomes lourd pour chaque residu -> Deplacer au niveau de l'init
        atm_list = [atm for aa in seq for atm in filter(self.heavy_reg.match,
                                                        iupac_aa[aa].keys())]
        if len(atm_list) != len(res_list):
            LOG.error("Index lists aren't the same size\n%s\n%s",
                      res_list, atm_list)

        idxnames = idxnames if idxnames and len(idxnames) == 2 else [
            "residuex", "atomx"]
        colnames = colnames if colnames and len(colnames) == 2 else [
            "residuey", "atomy"]
        index = pd.MultiIndex.from_tuples(list(zip(*[res_list, atm_list])),
                                          names=idxnames)
        columns = pd.MultiIndex.from_tuples(list(zip(*[res_list,
                                                       atm_list])),
                                            names=colnames)
        LOG.debug("Index:\n%s", index)
        return index, columns

    def create_heatmap(self):
        """:return:"""
        unidf = self.reduce()
        if unidf.isnull().values.sum() == 0:
            # If there's no nan values
            return sns.heatmap(unidf)
        return None

    def reduce(self, groupby="min"):
        """
        

        Parameters
        ----------
        groupby :
            return: (Default value = "min")

        Returns
        -------

        
        """
        if self.index.nlevels == 1 or not groupby:
            return self
        newmap = ResMap(self.sequence, dtype=self.dtype, desc=self.desc,
                        sym=self.sym, path=self.path)
        if self.dtype == bool:
            newmap[:] = self.copy().stack(dropna=False).groupby(level=0).any()
        elif groupby == "mean":
            newmap[:] = self.copy().stack(dropna=False).groupby(level=0).mean()
        elif groupby == "min":
            newmap[:] = self.copy().stack(dropna=False).groupby(level=0).min()
        return newmap

    def contact_map(self, contactdef, scsc_min=None, def_cut=5.0):
        """
        Contact map generator from all atoms distance map. There's a contact
        with 2 residues iff distance between 2 atoms are below the given treshold

        Parameters
        ----------
        def_cut : float
            Default cutoff (Default value = 5.0)
        contactdef :
            Contact definition for all atom pair
        scsc_min :
            (Default value = None)

        Returns
        -------

        
        """
        if self.dtype == bool:
            # If self is already a contact map
            return None
        # TODO: issue with sc_sc treshold !!!!!
        LOG.info("Generate contact map using contact definition %s",
                 contactdef)
        # Initialize contact map to a boolean matrix filled with False as values
        contact_map = ResAtmMap(sequence=self.sequence, mtype="contact",
                                desc=self.desc, sym=self.sym, path=self.path)
        def_cutoff = float(contactdef.get("default_cutoff")) if float(
            contactdef.get("default_cutoff")) else def_cut
        treshold = None

        if type(contactdef) == float:
            # ???
            contact_map[:] = self.applymap(lambda x: x < contactdef)

        elif sum(x is not None for x in contactdef.values()) == 1 \
                and def_cutoff:
            LOG.info("Using default cutoff")
            contact_map[:] = self.applymap(
                lambda x: x < contactdef.get("default_cutoff"))

        elif sum(x is not None for x in contactdef.values()) > 1:
            # treshconv = lambda x: x < contactdef.get("default_cutoff")
            # contact_map[:] = self.applymap(treshconv)
            atm_list = set(self.index.get_level_values(1))
            atms_list = set([(a, b) for a in atm_list for b in atm_list])
            for pair in contactdef.keys():
                treshold = contactdef[pair]
                if pair == "default_cutoff":
                    # Since there is more than one treshold, we do not take
                    # default cutoff into account
                    continue
                if pair == "all":
                    tmp = self.applymap(lambda x: x < treshold)
                    contact_map.update(tmp)
                    continue
                else:
                    pair = tuple(pair.upper().split("_"))
                    LOG.info(
                        "Filtering values in matrix related to %s (%s)",
                        str(pair), str(treshold))
                    if pair in (("SC", "SC"), ("sc", "sc")):
                        # Use scsc_min to apply treshold updateonly for selected atom
                        # sidechain
                        idx_list = []
                        col_list = []
                        for res1 in scsc_min:
                            for res2 in scsc_min[res1]:
                                pair = (AminoAcid.AminoAcid(res1)[1],
                                        AminoAcid.AminoAcid(res2)[1])
                                atm1, atm2 = scsc_min[res1][res2]
                                idx_list.append(
                                    self.index.map(lambda x: x[0].endswith(
                                        pair[0]) and x[1] == atm1))
                                col_list.append(
                                    self.index.map(lambda x: x[0].endswith(
                                        pair[1]) and x[1] == atm2))
                        mask = ([any(tup) for tup in zip(*idx_list)],
                                [any(tup) for tup in zip(*col_list)])
                    elif pair not in atms_list:
                        LOG.error("Pair %s doesn't exist ...", str(pair))
                        # Already applied a treshold for this pair
                        continue
                    else:
                        LOG.debug("Apply treshold for %s", str(pair))
                        atms_list.discard(pair)
                        # Selecting rows for each atom
                        mask = (self.index.get_level_values(1) == pair[0],
                                self.index.get_level_values(1) == pair[1])
                    tmp = self.loc[mask].apply(lambda x: x < float(treshold))
                    contact_map.update(tmp)
        else:
            LOG.error("Missing values in contact definition section. Add "
                      "at least a default_cutoff value.")
        LOG.debug("Contact map\n%s", contact_map.head())
        return contact_map

    def copy(self, **kwargs):
        """
        Copy dataframe and related attributes of the map

        Parameters
        ----------
        **kwargs :
            

        Returns
        -------

        
        """
        df = super(Map, self).copy()
        return ResAtmMap(
            sequence=self.sequence, path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)


class ResMap(ResAtmMap):
    """Res - res distance/contact matrix"""

    def __init__(self, sequence, flaglist=None, **kwargs):
        if not sequence:
            sequence = ConversionTable.ConversionTable().table['AMINO_ACID'][
                'iupac'].keys()
        super(ResMap, self).__init__(sequence, flaglist=flaglist, **kwargs)

    # def _constructor_expanddim(self):
    #     super(ResMap, self)._constructor_expanddim()

    @property
    def sequence(self):
        """
        Amino Acid sequence string in humanidx

        Parameters
        ----------

        Returns
        -------

        
        """
        # return "".join(AminoAcid.AminoAcid(aa.split("-")[1])[0] for aa in
        #                self.index)
        return super(ResMap, self).sequence

    def create_index(self, sequence, seqidx=None, idxnames=None,
                     colnames=None, **kwargs):
        """
        
        Parameters
        ----------
        sequence
        seqidx: pandas.Index
        idxnames
        colnames
        kwargs

        Returns
        -------

        """
        LOG.debug("Indexing Res - Res dataframe")
        seq = [AminoAcid.AminoAcid(aa)[1] for aa in sequence]
        seqidx = seqidx if seqidx and len(seqidx) == len(sequence) else None
        if seqidx:
            res_list = ["%03d-%s" % (seqidx[i], aa) for i, aa in enumerate(seq)]
        else:
            res_list = ["%03d-%s" % (i + 1, aa) for i, aa in enumerate(seq)]
        idxnames = idxnames if idxnames and len(idxnames) == 1 else "residuex"
        colnames = colnames if colnames and len(colnames) == 1 else "residuey"
        index = pd.Index(res_list, name=idxnames)
        col = pd.Index(res_list, name=colnames)
        return index, col

    def create_heatmap(self):
        """:return:"""
        if self.as_matrix().isnull().values.sum() == 0:
            # If there's no nan values
            return sns.heatmap(self.as_matrix())
        return None

    def contact_map(self, contactdef, **kwargs):
        """
        

        Parameters
        ----------
        contactdef :
            param kwargs:
        **kwargs :
            

        Returns
        -------

        
        """
        contact_map = ResMap(self.sequence, mtype="contact", desc=self.desc,
                             sym=self.sym)

        def treshconv(x):
            """
            

            Parameters
            ----------
            x :
                return:

            Returns
            -------

            
            """
            return x <= treshold

        # Applique treshold sur la matrice ssi c'est une matrice de distance
        if self.dtype == bool:
            # If self is already a contact map
            return None
        treshold = contactdef.get("default_cutoff", 5)
        contact_map[:] = self.applymap(treshconv)
        return contact_map

    def __deepcopy__(self):
        deepcopy_method = self.__deepcopy__
        self.__deepcopy__ = None
        cp = deepcopy(self)
        self.__deepcopy__ = deepcopy_method

        cp.dtype = self.dtype
        # custom treatments
        # for instance: cp.id = None

        return cp

    def copy(self, **kwargs):
        """
        Copy dataframe and related attributes of the map

        Parameters
        ----------
        **kwargs :
            

        Returns
        -------

        
        """
        df = super(Map, self).copy()
        return ResMap(
            sequence=self.sequence, path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)


class AaMap(ProteinMap):
    """Amino Acid Distance Matrix"""

    def contact_map(self, contactdef, scsc_min=None):
        """
        
        Parameters
        ----------
        contactdef
        scsc_min

        Returns
        -------

        """
        raise NotImplementedError

    def create_heatmap(self):
        """
        
        Returns
        -------

        """
        raise NotImplementedError

    # def _constructor_expanddim(self):
    #     super(AaMap, self)._constructor_expanddim()

    def __init__(self, atom_groups="min", **kwargs):
        # if ("index", "columns") not in kwargs:
        #     idx, col = self.create_index()
        #     kwargs["index"] = idx
        #     kwargs["columns"] = col
        super(AaMap, self).__init__(sequence=self.sequence,
                                    atom_groups=atom_groups, **kwargs)

    # TODO: Actually useless since it raised an notimplemented error
    def reduce(self):
        """Reduce level of the contactmap"""
        return super(AaMap, self).reduce()

    @property
    def sequence(self):
        """
        Returns
        -------
        str
            String containing all amino acid 1L code
        """
        return ''.join(sorted([
            AminoAcid.AminoAcid(aa)[0]
            for aa in ConversionTable.ConversionTable().table[
                'AMINO_ACID']['iupac'].keys()]))

    def copy(self, **kwargs):
        """
        

        Parameters
        ----------
        kwargs :
            
        **kwargs :
            

        Returns
        -------

        
        """
        df = super(Map, self).copy()
        return AaMap(
            path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)

    def create_index(self, sequence, **kwargs):
        """
        
        Returns
        -------

        """
        res_list = [AminoAcid.AminoAcid(aa)[1] for aa in sequence]
        index = pd.Index(res_list, name="aax")
        col = pd.Index(res_list, name="aay")
        return index, col


class AaAtmMap(AaMap):
    """
    Amino acid map with atom levels
    """

    def create_heatmap(self):
        """
        
        Returns
        -------

        """
        raise NotImplementedError

    def contact_map(self, contactdef, scsc_min=None):
        """
        
        Parameters
        ----------
        contactdef
        scsc_min

        Returns
        -------

        """
        raise NotImplementedError

    def __init__(self, atom_groups="min", **kwargs):
        super(AaAtmMap, self).__init__(atom_groups=atom_groups, **kwargs)

    def create_index(self, sequence, atom_groups="min", **kwargs):
        """
        Update Aa index with atoms
        Returns
        -------

        """
        # TODO: better way to define indexs
        # idx, col = super(AaAtmMap, self).create_index()
        atomtable = ConversionTable.ConversionTable().table['AMINO_ACID'][
            'iupac']
        seq = [AminoAcid.AminoAcid(aa)[1] for aa in sequence]
        res_list = [
            "%s" % aa for i, aa in enumerate(seq)
            for _ in filter(self.heavy_reg.match, atomtable[aa].keys())]
        atm_list = [
            atm for aa in seq for atm in ('CA', 'CB', 'SC')] \
            if atom_groups == "min" else [
            atm for aa in seq for atm in filter(self.heavy_reg.match,
                                                atomtable[aa].keys())] \
            if atom_groups == "heavy" else [
            atm for aa in seq for atm in atomtable[aa].keys()]
        idx = pd.MultiIndex.from_tuples(list(zip(*[res_list, atm_list])),
                                        names=('AminoAcid', 'Atom'))
        col = pd.MultiIndex.from_tuples(list(zip(*[res_list, atm_list])),
                                        names=('AminoAcid', 'Atom'))
        return idx, col

    def copy(self, **kwargs):
        """


        Parameters
        ----------
        kwargs :

        **kwargs :


        Returns
        -------


        """
        df = super(Map, self).copy()
        return AaAtmMap(
            path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype,
            atom_groups=self.atom_groups)


class SsAaAtmMap(AaAtmMap):
    """
    Amino acid map with atom and secondary structure levels
    """
    ss_types = ("H", "E", "X")

    def contact_map(self, contactdef, scsc_min=None):
        """
        
        Parameters
        ----------
        contactdef
        scsc_min

        Returns
        -------

        """
        pass

    def create_heatmap(self):
        """
        
        Returns
        -------

        """
        pass

    def __init__(self, atom_groups="min", **kwargs):
        super(SsAaAtmMap, self).__init__(atom_groups=atom_groups, **kwargs)

    def create_index(self, sequence, atom_groups="min", **kwargs):
        """
        
        Parameters
        ----------
        atom_groups
        sequence
        kwargs

        Returns
        -------

        """
        # TODO: better way to define indexs, duplicated code here !!!
        # idx, col = super(AaAtmMap, self).create_index()
        atomtable = ConversionTable.ConversionTable().table['AMINO_ACID'][
            'iupac']
        seq = [AminoAcid.AminoAcid(aa)[1] for aa in sequence]
        ss_list, res_list, atm_list = [], [], []

        for ss in self.ss_types:
            for aa in seq:
                atms = ('CA', 'CB', 'SC') if atom_groups == "min" else filter(
                    self.heavy_reg.match, atomtable[aa].keys()) if atom_groups == "heavy" else atomtable[aa].keys()
                for atm in atms:
                    ss_list.append(ss)
                    res_list.append(aa)
                    atm_list.append(atm)
        idx = pd.MultiIndex.from_tuples(
            list(zip(*[ss_list, res_list, atm_list])),
            names=('SecStruct', 'AminoAcid', 'Atom'))
        col = pd.MultiIndex.from_tuples(
            list(zip(*[ss_list, res_list, atm_list])),
            names=('SecStruct', 'AminoAcid', 'Atom'))
        return idx, col

    def reduce(self, groupby="min"):
        """


        Parameters
        ----------
        groupby :
            return: (Default value = "min")

        Returns
        -------


        """
        if self.index.nlevels == 1 or not groupby:
            return self
        newmap = AaAtmMap(path=self.path, desc=self.desc,
                          sym=self.sym, mtype=self.mtype, dtype=self.dtype,
                          atom_groups=self.atom_groups)
        if self.dtype == bool:
            newmap[:] = self.copy().stack('SecStruct', dropna=False).groupby(
                level=[1, 2]).any()
        elif groupby == "mean":
            newmap[:] = self.copy().stack('SecStruct', dropna=False).groupby(
                level=[1, 2]).mean()
        elif groupby == "min":
            newmap[:] = self.copy().stack('SecStruct', dropna=False).groupby(
                level=[1, 2]).min()
        return newmap

    def copy(self, **kwargs):
        """


        Parameters
        ----------
        kwargs :

        **kwargs :


        Returns
        -------


        """
        df = super(Map, self).copy()
        return SsAaAtmMap(
            path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)


class AtmMap(Map):
    """Atom Distance Matrix"""

    # def _constructor_expanddim(self):
    #     super(AtmMap, self)._constructor_expanddim()

    def __init__(self, *args, **kwargs):
        super(AtmMap, self).__init__(*args, **kwargs)

    def reduce(self):
        """Reduce level of the contactmap"""
        return super(AtmMap, self).reduce()

    def copy(self, **kwargs):
        """
        

        Parameters
        ----------
        kwargs :
            
        **kwargs :
            

        Returns
        -------

        
        """
        df = super(Map, self).copy()
        return AtmMap(
            path=self.path, data=df, desc=self.desc,
            sym=self.sym, mtype=self.mtype, dtype=self.dtype)

    def create_index(self):
        """:return:"""
        raise NotImplementedError


class ProtMapCollection(object):
    """Group all protein maps"""

    def __init__(self, settings):
        self._alldistmap = None
        self._allscoremap = None
        self._allcontactmap = None
        self._distmap = None
        self._scoremap = None
        self._contactmap = None
        self.settings = settings

    @property
    def alldistmap(self):
        """
        Atom level distance map.
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        return self._alldistmap

    @alldistmap.setter
    def alldistmap(self, resatmap):
        """
        Atom level distance map setter. The only way is to give resatmmap object

        Parameters
        ----------
        resatmap :
            type resatmap: ResAtmMap

        Returns
        -------

        
        """
        self._alldistmap = resatmap

    @property
    def allcontactmap(self):
        """
        Atom level contact map. If alldistmap exists, return the maplot
        form
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        if self._alldistmap:
            # settings.contactdef.config
            # settings.main.scsc_min_file
            return self._alldistmap.contact_map(contactdef=self.settings[
                "contactdef"], scsc_min=self.settings["scsc_min_file"])
        else:
            return self._allcontactmap

    @allcontactmap.setter
    def allcontactmap(self, resatmap):
        """
        Atom level contact map setter

        Parameters
        ----------
        resatmap :
            binary res atm map

        Returns
        -------

        
        """
        self._allcontactmap = resatmap

    @property
    def allscoremap(self):
        """
        Atom level score map
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        return self._allscoremap

    @allscoremap.setter
    def allscoremap(self, resatmap):
        """
        Atom level score map setter
        :return:

        Parameters
        ----------
        resatmap :
            

        Returns
        -------

        
        """
        self._allscoremap = resatmap

    @property
    def distmap(self):
        """:return:"""
        if self._alldistmap:
            return self._alldistmap.reduce(groupby=self.settings[
                "groupby_method"])
        else:
            return self._distmap

    @distmap.setter
    def distmap(self, resmap):
        """
        

        Parameters
        ----------
        resmap :
            

        Returns
        -------

        
        """
        self._distmap = resmap


class MapFilter(object):
    """
    Filter maplot/distancemap
        nd      : Network deconvolution
        pos     : remove close contacts
        cons    : remove contacts with highly conservated residues
        cys-cys : unicity of ss contacts
        ssclash : secondary structure conflict

    Parameters
    ----------

    Returns
    -------

    
    """
    filter_types = ("pos", "cons", "ssclash", "cys")
    clash_dict = {
        "pos": {
            "clash": "physical proximity",
            "desc": "sequence position"},
        "cons": {
            "clash": "888",
            "desc": "high conservation"},
        "ssclash": {
            "clash": "999",
            "desc": "secondary structure prediction conflict"},
        "cys": {
            "clash": "222",
            "desc": "disulfide bond unicity"}}

    def __init__(self, settings, nofilter=False):
        self.settings = settings
        self.nofilter = nofilter

    def nd_filter(self, mapdict):
        """
        

        Parameters
        ----------
        mapdict :
            

        Returns
        -------

        
        """
        # TODO: build ROC curve with number of top contacts as the parameter
        """

        :param mapdict:
        :return:
        """
        LOG.info("...Network deconvolution filter (alpha=%.2f, beta=%.2f)",
                 self.settings["nd_beta"], self.settings["nd_alpha"])
        scoremap = mapdict["scoremap"]
        LOG.info(net_deconv(mapdict["scoremap"].as_matrix(),
                            beta=self.settings["nd_beta"],
                            alpha=self.settings["nd_alpha"]))
        # TODO: MAJ score map avec matrice obtenue !!!
        # !!!! Verifier que scoremap est bien maj et UTILISEE !!
        return scoremap

    def pos_filter(self, mapdict, **kwargs):
        """
        Position filter on maplot

        Parameters
        ----------
        kwargs :
            param mapdict: dict with maplot key
        mapdict :
            
        **kwargs :
            

        Returns
        -------

        
        """
        # Liste les contacts proches
        clash_list = kwargs.get("clash_list")
        LOG.info("...Position filter")
        close_list = []
        contact_list = mapdict["maplot"].contact_list()

        for contact in contact_list:
            gap = abs(int(contact[0]) - int(contact[1]))
            if gap <= self.settings['position_treshold']:
                if clash_list and contact in clash_list:
                    continue
                close_list.append((contact[0], contact[1]))
        return {'clash': close_list, 'desc': None}

    def cons_filter(self, mapdict, **kwargs):
        """
        

        Parameters
        ----------
        mapdict :
            
        **kwargs :
            

        Returns
        -------

        
        """
        # Liste les contacts aves des residus fortement conserves
        """

        :param mapdict:
        :param kwargs:
        :return:
        """
        LOG.info("...Conservation filter")
        sec_struct = kwargs.get("sec_struct")
        clash_list = kwargs.get("clash_list")
        cons_pair = []
        cons_res = []
        contact_list = mapdict["maplot"].contact_list()

        if not sec_struct:
            LOG.warning(
                "No conservation information. Can't use related filter")
            return {'clash': None, 'desc': None}

        if sec_struct.filetype != "indextableplus":
            LOG.info("Conservation filter only works with indextableplus "
                     "files !")
            return {'clash': None, 'desc': None}

        # parcours la liste de paires dans la matrice struct secondaire
        for index, reslist in enumerate(sec_struct.ss_matrix):
            # TODO: Initialize conservation score if other value than int
            cons_score = int(reslist[5]) if reslist[5] != "-" else 0
            if cons_score > self.settings["conservation_treshold"] \
                    and reslist[1] != 'C':
                cons_res.append(index)

        for contact in contact_list:
            if contact[0] in cons_res or contact[1] in cons_res:
                if clash_list and contact in clash_list:
                    # If this clash already exist
                    continue
                cons_pair.append(contact)
        LOG.debug("Highly conserved residue list: %s", cons_res)
        return {'clash': cons_pair, 'desc': None}

    @staticmethod
    def cys_filter(mapdict, **kwargs):
        """
        

        Parameters
        ----------
        mapdict :
            
        **kwargs :
            

        Returns
        -------

        
        """
        # Si scoremap existe, selectionner les contacts cys-cys qui ont les
        # meilleurs scores, fournit une liste des contacts disulfures qui
        # possedent des scores plus faibles
        """

        :param mapdict:
        :param kwargs:
        :return:
        """
        LOG.info("...Disulfure bridge unicity filter")
        clash_list = kwargs.get("clash_list")
        unidisbridge_list = []  # Liste les ponts disulfures uniques
        clashdisbridge_list = []  # Liste les ponts disulfures incompatibles
        desc = []

        if mapdict.get("scoremap") is not None:
            scoremap = mapdict.get("scoremap")
            # List of all cysteine
            cys_list = [idx for idx, val in enumerate(scoremap.index.values)
                        if val.endswith('CYS')]
            dis_bridge_list = {ss: scoremap.iat[ss] for ss
                               in list(itertools.combinations(cys_list, 2))}
            if dis_bridge_list:
                # Get list of all disulfure bridges sorted
                sorted_ss = zip(*sorted(dis_bridge_list.items(),
                                        key=operator.itemgetter(1),
                                        reverse=True))[0]
            else:
                return {'clash': None, 'desc': None}
            for dis_bridge in sorted_ss:
                # foreach disulfure bridge
                LOG.debug('Checking %s %s', addtup(dis_bridge),
                          scoremap.iat[dis_bridge])
                if dis_bridge in clash_list:
                    # given contact already removed with previous filters
                    continue
                else:
                    # Check for each cys in dis_bridge if they aready exists
                    # in unidisbridge_list
                    exdis = None
                    if unidisbridge_list:
                        for cys in dis_bridge:
                            for unidis in unidisbridge_list:
                                if cys in unidis:
                                    exdis = unidis
                                    break
                        if exdis:
                            LOG.debug("Ss bridge %s have at least one cys in "
                                      "common with previous ss filtered (%s). ",
                                      addtup(dis_bridge),
                                      map(addtup, unidisbridge_list))
                            if scoremap.iat[dis_bridge] > scoremap.iat[exdis]:
                                # Better cys--cys contact
                                # List cys-cys contacts that will be removed in
                                #  unidisbridge_list
                                LOG.debug("%s has better score than %s",
                                          addtup(dis_bridge),
                                          addtup(exdis))
                                remcys = (unidis for cys in dis_bridge for unidis
                                          in unidisbridge_list if cys in unidis)
                                for dis in remcys:
                                    # PB si un des dis supprime est
                                    clashdisbridge_list.append(dis)
                                    clashdisbridge_list.append(dis[::-1])
                                    unidisbridge_list.remove(dis)
                                    LOG.debug("Added %s to clashlist %s",
                                              addtup(dis),
                                              map(addtup, clashdisbridge_list))
                                unidisbridge_list.append(dis_bridge)
                                continue
                            else:
                                clashdisbridge_list.append(dis_bridge)
                                clashdisbridge_list.append(dis_bridge[::-1])
                                LOG.debug("Added %s to ss clashlist %s",
                                          addtup(dis_bridge),
                                          map(addtup, clashdisbridge_list))
                                continue
                    LOG.debug('Adding bridge %s to ss filtered list', dis_bridge)
                    unidisbridge_list.append(dis_bridge)
            return {'clash': clashdisbridge_list, 'desc': desc}
        else:
            # If no score given, return empty list
            return {'clash': None, 'desc': None}

    @staticmethod
    def ssclash_filter(mapdict, **kwargs):

        """
        

        Parameters
        ----------
        mapdict :
            param kwargs:
        **kwargs :
            

        Returns
        -------

        
        """

        def hum_contact(xy):
            """
            

            Parameters
            ----------
            xy :
                return:

            Returns
            -------

            
            """
            return xy[0] + 1, xy[1] + 1

        # TODO: better add clash list and sec_struct as object attribute
        LOG.info("...Secondary structure clash filter")
        sec_struct = kwargs.get("sec_struct")
        clash_list = kwargs.get("clash_list")
        ssclash_pair = []
        desc_dict = {}

        if not sec_struct:
            LOG.warning("No secondary structure information. Can't use secondary"
                        " structure filter")
            return {'clash': None, 'desc': None}

        ss_matrix = sec_struct.ss_matrix
        ss_list = zip(*ss_matrix)[2]
        # contact_list from contact map start at 0 !!
        contact_list = mapdict["maplot"].contact_list()

        ss_start_end = collections.defaultdict(lambda: [None, None])

        # TODO: deplacer construction du dic ss_start_end dans SsList
        for res_ind in range(len(ss_matrix)):
            # Construction du dict ss_start_end
            if res_ind == 0:
                # If first residue
                # Save ss start humanidx related to res_ind
                ss_start_end[ss_matrix[res_ind][2]][0] = res_ind
                if ss_matrix[res_ind][2][0] != ss_matrix[res_ind + 1][2][0]:
                    # If next res is not in the same ss
                    ss_start_end[ss_matrix[res_ind][2]][1] = res_ind
                    ss_start_end[ss_matrix[res_ind + 1][2]][0] = res_ind + 1
            elif res_ind == len(ss_matrix) - 1:
                # Si dernier res
                ss_start_end[ss_matrix[res_ind][2]][1] = res_ind
            elif ss_matrix[res_ind][2][0] != ss_matrix[res_ind + 1][2][0]:
                # If next res not in the same ss
                ss_start_end[ss_matrix[res_ind][2]][1] = res_ind
                ss_start_end[ss_matrix[res_ind + 1][2]][0] = res_ind + 1

        start_list = [ss_start_end[elm][0] for elm in ss_start_end]
        end_list = [ss_start_end[elm][1] for elm in ss_start_end]
        LOG.debug("Checking secondary structures clashes for contact list {0} "
                  "[{1}] with secondary structure list {2} [{3}]".format(
            str(contact_list), len(contact_list), str(ss_list), len(ss_list)))
        for icontact, contact in enumerate(contact_list):
            # For each res-res contact
            outcontact = str(hum_contact(contact))
            if contact in clash_list:
                continue
            resi = contact[0]  # Pos number (0...n-1)
            resj = contact[1]
            ssi = ss_list[resi]
            ssj = ss_list[resj]

            if ssi == ssj and (ssi[0], ssj[0]) in (("H", "H"), ("E", "E")):
                # If both residues are in same helix or strand
                desc = "%s,%s" % (ssi[0], ssj[0])
                desc_dict[contact] = desc
                LOG.debug("Ss conflict for contact %d %s (%s)",
                          icontact, outcontact, desc)
                ssclash_pair.append(contact)
            # ELIF encadre H ou E
            elif ssi != ssj:
                # If both residues are not in the same ss
                ssclash = None
                for n in (1, 2, 3, 4):

                    # Search type of the bond (H-1 H, E-2 E, ...)
                    for i in range(2):
                        # Test both sides

                        resi = contact[i]
                        resj = contact[i - 1]
                        ssi = ss_list[resi]
                        ssj = ss_list[resj]

                        try:
                            ssi_pn = ss_list[resi + n]
                            ssi_mn = ss_list[resi - n]
                            ssj_pn = ss_list[resj + n]
                            ssj_mn = ss_list[resj - n]
                        except IndexError:
                            continue

                        # (E-n, E) OR (H-n, H)
                        if ssj[0] in ("H", "E") \
                                and resi + n in start_list \
                                and ssi_pn == ssj:
                            # --i**[-----j-----]--- (n: **)
                            #           E/H
                            ssclash = "%s-%d,%s" % (ssi_pn[0], n, ssj[0])
                            break
                        if ssj[0] in ("H", "E") \
                                and resi - n in end_list \
                                and ssi_mn == ssj:
                            # -----[-----j-----]**i--- (n: **)
                            #           E/H
                            ssclash = "%s+%d,%s" % (ssi_mn[0], n, ssj[0])
                            break
                        if ssi[0] in ("H", "E") \
                                and resj + n in start_list \
                                and ssj_pn == ssi:
                            # --j**[-----i-----]--- (n: **)
                            #           E/H
                            ssclash = "%s-%d,%s" % (ssj_pn[0], n, ssi[0])
                            break
                        if ssi[0] in ("H", "E") \
                                and resj - n in end_list \
                                and ssj_mn == ssi:
                            # -----[-----i-----]**j--- (n: **)
                            #           E/H
                            ssclash = "%s+%d,%s" % (ssj_mn[0], n, ssi[0])
                            break

                        # (E+n, E-n), (H+n, H-n)
                        if ssi_mn[0] in ("H", "E") \
                                and resi - n in end_list \
                                and resj + n in start_list \
                                and ssi_mn == ssj_pn:
                            # --j**[-----H/E-----]**i-- (n: **)
                            ssclash = "%s+%d,%s-%d" % (ssi_mn[0], n,
                                                       ssj_pn[0], n)
                            break
                        if ssi_pn[0] in ("H", "E") \
                                and resi + n in start_list \
                                and resj - n in end_list \
                                and ssi_pn == ssj_mn:
                            # --i**[-----H/E-----]**j-- (n: **)
                            ssclash = "%s-%d,%s+%d" % (ssi_pn[0], n,
                                                       ssj_mn[0], n)
                            break

                    if ssclash:
                        LOG.debug("Ss clash for contact %d %s (%s)",
                                  icontact, outcontact, ssclash)
                        if ssclash in ("H-2,H", "H+2,H") \
                                and abs(resi - resj) == 6:
                            # Allow contact to the fifth residue in the
                            # helix
                            LOG.debug("Found (H-2, H) for contact %s clash "
                                      "but contact with fifth residue is "
                                      "actually allowed",
                                      outcontact)
                            ssclash = None
                        elif ssclash in ("H-3,H", "H+3,H") \
                                and abs(resi - resj) < 12:
                            LOG.debug("Found (H-3, H) for contact %s clash "
                                      "but contact between 3rd and 10th "
                                      "residues are actually allowed",
                                      outcontact)
                            # Allow contact between 3rd residue and 10th
                            ssclash = None
                        elif ssclash in ("H-4,H", "H+4,H"):
                            LOG.debug("H-4,H are actually allowed")
                            ssclash = None
                        elif ssclash in ("H-2,H+2", "H+2,H-2"):
                            LOG.debug("H-2,H+2 are actually allowed")
                            ssclash = None
                        elif ssclash in ("H-3,H+3", "H+3,H-3"):
                            LOG.debug("H-3,H+3 are actually allowed")
                            ssclash = None
                        elif ssclash in ("H-4,H+4", "H+4,H-4"):
                            LOG.debug("H-4,H+4 are actually allowed")
                            ssclash = None
                        elif ssclash == "E-2,E+2":
                            strand = ssj if ssj[0] == "E" else ssi
                            start = ss_start_end[strand][1]
                            end = ss_start_end[strand][0]
                            if abs(start - end + 1) <= 5:
                                LOG.debug("Found (E-2, E+2) for contact "
                                          "%s clash but strand "
                                          "is < 5 residues", outcontact)
                                # Allow contact if strand < 5 residues (gap <8)
                                ssclash = None
                        elif ssclash in ("E-3,E+3", "E+3,E-3"):
                            LOG.debug("E-3,E+3 are actually allowed")
                            ssclash = None
                        elif ssclash in ("E-4,E+4", "E+4,E-4"):
                            LOG.debug("E-4,E+4 are actually allowed")
                            ssclash = None
                        elif ssclash in ("E-4,E", "E+4,E") \
                                and abs(resi - resj) < 8:
                            LOG.debug("Found (E-4, E) for contact %s clash "
                                      "but contacts below 4th residue are "
                                      "actually allowed", outcontact)
                            ssclash = None
                        if ssclash:
                            LOG.debug(
                                "Ss conflict for contact %d %s (%s)",
                                icontact, outcontact, ssclash)
                            desc_dict[contact] = ssclash
                            ssclash_pair.append(contact)
                            break
        return {'clash': ssclash_pair, 'desc': desc_dict}

    def __call__(self, mapdict, mtype, contactlist, inprot,
                 mapfilters=None, outprefix="", outdir="", clashlist=None):
        """
        Filter contact map
        :param mapdict: dict containing ResMap and ResAtmMap for contacts,
        distances and scores
        :return:
        """
        clash_list = []
        desc_dict = {}
        clash_dict = {}
        mapfilters = self.settings.get("contactfilter") if mapfilters is None \
            else mapfilters

        if self.settings["net_deconv"]:
            # TODO: maj scoremap ou maj mapcollection
            mapdict["scoremap"] = self.nd_filter(mapdict)

        if mapfilters == "all" and not self.nofilter:
            mapfilters = self.filter_types
        elif self.nofilter or mapfilters == "none":
            LOG.info("Filtering steps removed")
            mapfilters = None
        elif not mapfilters and not self.nofilter:
            LOG.info("No filtering steps. Will only use default filter (closed "
                     "contact filter)")
            mapfilters = ["pos"]
        else:
            if type(mapfilters) == list:
                mapfilters = [elm for elm in mapfilters if elm in self.filter_types]
            else:
                mapfilters = [mapfilters]
            if "pos" not in mapfilters and mtype != "bbcontacts":
                mapfilters.insert(0, "pos")
        if mapfilters:
            LOG.info("Filtering %s contact map", mtype)
            for flt in mapfilters:
                # /!\ cys unicity filter must  be the last filter !
                # TODO: contacts_flt.out checking if given clash in coupling
                # matrix are the same
                # Filter on maplot for nd or contact_list
                kw = {"clash_list": clash_list,
                      "sec_struct": inprot.sec_struct}
                flt_res = getattr(self, "%s_filter" % flt)(mapdict, **kw)
                if flt_res.get("clash"):
                    clash_list.extend(flt_res.get("clash"))
                    hum_list = [(x + 1, y + 1) for x, y in flt_res.get("clash")]
                    LOG.info("Removed %d contacts.", len(flt_res.get("clash")) / 2)
                    LOG.debug(":\n%s", '\n'.join(
                        textwrap.wrap(' '.join(
                            ["(%2d, %2d)" % pair for pair in hum_list]),
                            width=80)))
                clash_dict[flt] = flt_res.get("clash")
                if flt_res.get("desc"):
                    desc_dict.update(flt_res.get("desc"))

            # write filter.out
            self.write_filtout(clash_dict, desc_dict, contactlist, outdir=outdir,
                               outprefix=outprefix + '.' + mtype, clashlist=clashlist)

        # Contactmap always filtered
        # TODO: could set a treshold instead of n_factor
        LOG.info("Setting contact number with treshold %s",
                 self.settings.get("n_factor"))
        if self.settings.get("n_factor") == "all":
            LOG.warning("Factor set to all. All the contacts will be used !")
            nb_c = len(mapdict["maplot"].contactset())
        elif isinstance(self.settings.get("n_factor"), six.string_types) \
                and "%" in self.settings.get("n_factor"):
            reg = re.compile(r"([0-9]+\.?[0-9]*)%")
            perc = float(reg.match(self.settings.get("n_factor")).group(1))

            nb_c = int(perc/100 * len(mapdict["maplot"].contactset()))
            LOG.warning("Factor set as %.2f percentage of all the contacts "
                        "(random selection).", perc)
        else:
            nb_c = int(len(mapdict["maplot"].sequence) * float(
                self.settings.get("n_factor")))
            # Number of selected contact
            # If there isn't enough contacts in input contact map after filtering
            # step, change nb_c
            nb_c = nb_c if nb_c < len(mapdict["maplot"].contactset()) else len(
                mapdict["maplot"].contactset())
        mapdict["nb_c"] = nb_c

        LOG.info("Update %s maplot", mtype)
        mapdict["maplot"].remove(clash_list)

        if mapdict["scoremap"] is not None:
            LOG.info("Update %s scoremap", mtype)
            mapdict["scoremap"].remove(clash_list)
            # Get nb_c top maps
            LOG.info("Select top %d contacts according to scoremap", nb_c)
            # TODO: deplacer cette partie dans l'appel de maplot une fois
            #  la classe mapcollections utilisee
            mapdict["maplot"] = mapdict["maplot"].topmap(
                mapdict["scoremap"], nb_c)
        if mapdict["distmap"] is not None:
            LOG.info("Update %s distmap", mtype)
            mapdict["distmap"].remove(clash_list)

        return mapdict

    @staticmethod
    def write_filtout(clash_dict, desc_dict, contactlist, outdir="",
                      outprefix="protein", clashlist=None, human_idx=True):
        """
        

        Parameters
        ----------
        clash_dict :
            
        desc_dict :
            
        contactlist :
            
        outdir :
            (Default value = "")
        outprefix :
            (Default value = "protein")
        clashlist :
            (Default value = None)
        human_idx :
            (Default value = True)

        Returns
        -------

        
        """
        # TODO: utiliser self.clash_dict au lieu de meta_clash
        meta_clash = {
            "cons": {
                "flag": 888, "msg": "", "warn": "",
                "desc": "high conservation"},
            "cys": {
                "flag": 222, "msg": "", "warn": "",
                "desc": "disulfide bonds unicity"},
            "ssclash": {"flag": 999, "msg": "", "warn": "",
                        "desc": "secondary structure prediction conflict"},
            "pos": {
                "flag": "physical proximity", "msg": "", "warn": "",
                "desc": "sequence position"},
            "nd": {
                "flag": "deconvol", "msg": "", "warn": "",
                "desc": "network deconvolution"}
        }
        offset = 1 if human_idx else 0

        for icontact, contact in enumerate(contactlist):
            clash = "0"
            clash_t = ""
            raw_contact = (contact[0] - offset, contact[1] - offset)
            for clash_t in sorted(clash_dict.keys()):
                if clash_dict[clash_t] and raw_contact in clash_dict[clash_t]:
                    clash = meta_clash[clash_t]["flag"]
                    meta_clash[clash_t]["msg"] += """
{clash_type} flag at pair {pair_nb} : res {res1} and res {res2} {clash_desc}\
""".format(clash_type=clash, pair_nb=icontact + 1,
                        clash_desc=desc_dict.get(raw_contact, ''),
                        res1=contact[0], res2=contact[1])
                    break
            if clashlist and str(clash) != str(clashlist[icontact]):
                if clash == "0":
                    op = "removed"
                    ctype = str(clashlist[icontact])
                else:
                    op = "added"
                    ctype = clash
                meta_clash[clash_t]["warn"] += "\n/!\ Clash: {clash_desc} {clash} flag for contact " \
                                               "{res_pos} ({res1}, {res2})".format(
                    clash_desc=op, clash=ctype, res_pos=icontact + 1,
                    res1=contact[0], res2=contact[1])

        with open("%s/%s.filter.out" % (outdir, outprefix), 'w') as out:
            titleprint(out, progname=__doc__, desc='Contacts filter')
            for flt in ("nd", "pos", "cons", "ssclash", "cys"):
                out.write('''

{filter_desc}
{hd}
'''.format(filter_desc=meta_clash[flt]["desc"].capitalize(),
                    hd="=" * len(meta_clash[flt]["desc"])))

                if meta_clash[flt].get("warn"):
                    out.write(meta_clash[flt]["warn"])
                out.write(meta_clash[flt]["msg"])
