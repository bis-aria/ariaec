# coding=utf-8
"""
                        PDB distance distribution generation
"""

import logging
import os
import pandas as pd
import pbxplore as pbx
import sys
from Bio.PDB import PDBList, PDBParser, Selection, is_aa, NeighborSearch, \
    MMCIFParser
from Bio.PDB.DSSP import dssp_dict_from_pdb_file
from collections import defaultdict, OrderedDict
from future.utils import iteritems
from glob import glob

from ..core.legacy.AminoAcid import AminoAcid
# from .base import ppdict
from .common import Capturing
from .protmap import ResAtmMap
from .reader import CulledPdbFile

LOG = logging.getLogger(__name__)


class PDBDist(object):
    """
    Extract pdb distance distribution from culled list of pdb files provided by
    PICSES server [G. Wang and R. L. Dunbrack, Jr. PISCES: a protein sequence
    culling server. Bioinformatics, 19:1589-1591, 2003.] with Bio.PDB tools
    [Hamelryck, T., Manderick, B. (2003) PDB pdbparser and structure class
    implemented in Python. Bioinformatics 19: 2308–2310]

    Parameters
    ----------

    Returns
    -------

    
    """

    def __init__(self, settings):
        self.settings = settings

        self._obsoletedir = self.settings.pdbdist.config.get(
            'obsolete_directory', '/tmp/obsolete')
        # Setting trash directory for old downloaded pdbs
        if not os.access(self._obsoletedir, os.F_OK):
            LOG.warning("%s directory does not exist. Making a new directory",
                        self._obsoletedir)
            os.makedirs(self._obsoletedir)

        self.pdblist = PDBList(obsolete_pdb=self._obsoletedir)
        self.pdbparser = PDBParser()
        self.mmcifparser = MMCIFParser()

    @staticmethod
    def get_proteinblocks(pdb, chain):
        """
        Get Protein Block assignment index for chain with pbxplore

        Parameters
        ----------
        pdb : pdb file path
            
        chain : str
            

        Returns
        -------

        
        """
        with Capturing() as output:
            chain = [ch[1] for ch in pbx.chains_from_files([pdb])
                     if ch[1].name == chain][0]
            if output:
                LOG.info(output)

        # Assign protein blocks
        tmpblocks = pbx.assign(chain.get_phi_psi_angles())
        # Get related list of resid
        sequence = set([atm.resid for atm in chain.atoms])
        # In case of missing residues, define result as default dict
        pblocks = defaultdict(lambda: '')

        for idx, res_id in enumerate(sequence):
            pblocks[res_id] = "%s" % tmpblocks[idx]

        return pblocks

    # TODO: similar function in another part of the package, have to merge them
    @staticmethod
    def get_secstructs(backbone, dssp_dict, chain):
        """
        Return related secondary structure dictionary for each backbone residue

        Parameters
        ----------
        backbone : :obj: `list` of :obj: `Bio.PDB.Residue.Residue`
            List of bacbkone residues
        dssp_dict :
            
        chain :
            

        Returns
        -------

        
        """
        LOG.debug("Computing secondary structure index")
        ss_idx = defaultdict(lambda: '')
        ss_count = {'H': 1, 'E': 1, 'X': 1}

        # Sort backbone by resid
        backbone = sorted(backbone, key=lambda resx: resx.id[1])

        for idx, res_x in enumerate(backbone):

            resid_x = res_x.id[1]
            # Get secondary structure in dssp dict or set ss as X element
            ss_x = dssp_dict.get((chain, (" ", resid_x, " ")), ("", "X"))[1]
            ss_x = "X" if ss_x not in ["H", "E"] else ss_x
            # Udate secondary structure index for residue x
            ss_idx[resid_x] = "%s%d" % (ss_x, ss_count[ss_x])

            # Check if next residue still belongs to the same secondary
            # structure
            if idx != len(backbone) - 1:
                resid_j = backbone[idx + 1].id[1]
                ss_j = dssp_dict.get((chain, (" ", resid_j, " ")), ("", "X"))[1]
                ss_j = "X" if ss_j not in ["H", "E"] else ss_j

                if ss_x != ss_j:
                    # If end of secondary structure element, increase related ss
                    # count
                    ss_count[ss_x] += 1

        LOG.debug(ss_idx)
        return ss_idx

    def pdbdists(self, pdb, pdbid, chain, pair_list="all"):
        """
        Get distance matrix from pdb file and write distances into output file

        Parameters
        ----------
        pdb : str
            Path of pdb file
        pdbid : str
            PDB id
        chain : str
            Chain id
        pair_list : {'all', 'min', 'heavy'}
            Defining if we generate a distance distribution for a minimal set
            of atom pairs, heavy pairs or all atom pairs (Default value = "all")

        Returns
        -------
        dists: :obj: `list` of :obj: `OrderedDict`

        
        """
        # TODO: Do we have to take disordered atoms into account ?
        LOG.info("Compute atom distances for chain %s of %s", chain, pdbid)
        # out = ""
        dists = []
        cutoff = self.settings.pdbdist.config.get("contact_cutoff")
        # Check if dssp executable exists otherwise we can't find secondary
        # structures
        dssp_exec = self.settings.pdbdist.config.get("dssp_exec")
        if not os.access(dssp_exec, os.F_OK):
            LOG.error("DSSP executable not found. Check if dssp_exec parameter"
                      " in config file is correct !")
            sys.exit(1)

        # Get chain structure object with Bio.PDB pdbparser or mmcifparser
        #  (related to model 0)
        if os.path.splitext(pdb)[1] in ('.pdb', '.ent'):
            structure = self.pdbparser.get_structure(pdbid, pdb)[0][chain]
        else:
            structure = self.mmcifparser.get_structure(pdbid, pdb)[0][chain]

        # Get list of residues by ignoring heteroatoms, water molecules
        # by looking at the hetero-flag in residue id (H_ for hetero atoms or
        # W_ for water molecules) with only one resx object by amino acid (ca
        # filter). Also ignore insertion mutants
        residues = [res for res in structure.get_residues()
                    if res.get_id()[0][0] not in ['H', 'W']
                    and res.get_id()[2] == ' '
                    and is_aa(res) and res.has_id('CA')]

        # Unfold atom list related to selected residues
        atoms = Selection.unfold_entities(residues, "A")

        # Get dssp dictionary and related dssp keys from pdb file
        logging.captureWarnings(True)

        try:
            dssp_dict, dssp_keys = dssp_dict_from_pdb_file(
                pdb, DSSP=self.settings.pdbdist.config.get("dssp_exec"))
        except Exception as msg:
            LOG.error("%s: %s", Exception, msg)
            return None

        # Compute secondary structure index
        ss_idx = self.get_secstructs(residues, dssp_dict, chain)
        pb_idx = self.get_proteinblocks(pdb, chain)

        LOG.debug("Protein Blocks assignment (%s):\n%s", len(pb_idx), pb_idx)
        LOG.debug("Secondary structure index (%s):\n%s", len(ss_idx), ss_idx)

        # TODO: could be a better idea to use protmap.AtmMap class
        # Initialize square checking matrix (if a resx has already been seen)
        atoms_idx = pd.Index([atmx.get_serial_number() for atmx in atoms])
        seen = pd.DataFrame(data=0, dtype=int, index=atoms_idx,
                            columns=atoms_idx)

        # Initialize Neighbor searching object
        neighsearch = NeighborSearch(atoms)

        for resx in residues:

            resx_name = AminoAcid(resx.get_resname())[0]
            resx_id = resx.id[1]
            ssx = ss_idx[resx_id]
            pblocx = pb_idx[resx_id]

            LOG.debug("Start at residue %s (%s) with %s atoms", resx_id,
                      resx_name, resx.get_list())

            for atmx in resx.get_list():

                atmx_id = atmx.get_serial_number()
                atmx_name = atmx.get_name()

                # Find neighboring res around atom in resi within defined cutoff
                LOG.debug("Looking for residues in contact with atom %s of res "
                          "%s (%s)", atmx_name, resx_name, resx_id)
                neighbors = neighsearch.search(
                    atmx.get_coord(), cutoff, level="R")
                # Select long range contacts
                longr_contacts = [res for res in neighbors if
                                  abs(res.id[1] - resx_id) > 5]

                # For each residue in contact with the selected atom
                for resy in longr_contacts:

                    resy_name = AminoAcid(resy.get_resname())[0]
                    resy_id = resy.id[1]
                    ssy = ss_idx[resy_id]
                    pblocy = pb_idx[resy_id]

                    # Ignore disulfide bridges and unkonwn residues
                    if (resx_name == "CYS" and resy_name == "CYS") \
                            or not resx_name or not resy_name:
                        continue

                    scx_name, scy_name = self.settings.scsc_min[resx_name][
                        resy_name]

                    if (pair_list == "min" and
                            atmx_name not in ("CA", "CB", scx_name)) or \
                            (pair_list == "heavy"
                             and not ResAtmMap.heavy_reg.match(atmx_name)):
                        continue

                    LOG.debug("Contact detected for atom %s of res %s (%s) with"
                              " res %s (%s)",
                              atmx_name, resx_id, resx_name, resy_id, resy_name)

                    # Define secondary structure type
                    ss_type = ssx[0] + ssy[0]
                    if ssx != ssy:
                        if ss_type in ("HH", "EE", "HE", "EH"):
                            # Contact between helice or beta strand
                            ss_type = "HE" if ss_type not in ("HH", "EE") \
                                else ss_type
                        else:
                            # Contact with loops or other secondary
                            # structure
                            ss_type = "XH" if "H" in ss_type else "XE" \
                                if "E" in ss_type else "XX"
                    else:
                        # Same secondary structure
                        ss_type = ssx[0]

                    # PB type
                    pb_type = ''.join(sorted(pblocx + pblocy))

                    for atmy in resy.get_list():

                        atmy_id = atmy.get_serial_number()
                        atmy_name = atmy.get_name()
                        # Filter atom pairs if pair_list is not equal to "all"

                        LOG.debug("Contact filter: %s-%s (%s,%s) %s-%s (%s,%s)",
                                  resx_name, resy_name, resx_id, resy_id,
                                  atmx_name, atmy_name, atmx_id, atmy_id)

                        if (pair_list == "min" and
                                atmy_name not in ("CA", "CB", scy_name)) or \
                                (pair_list == "heavy"
                                 and not ResAtmMap.heavy_reg.match(atmy_name)):
                            seen[atmx_id][atmy_id] = 1
                            seen[atmy_id][atmx_id] = 1
                            continue

                        LOG.debug("...valid")

                        if not seen[atmx_id][atmy_id]:
                            # If contact between two atoms not already seen,
                            # write distance in the output file
                            dist = atmx - atmy

                            dists.append(OrderedDict([
                                ('pdbid', pdbid), ('ss_type', ss_type),
                                ('ssx', ssx), ('ssy', ssy),
                                ('pb_type', pb_type), ('pblocx', pblocx),
                                ('pblocy', pblocy), ('resx_id', str(resx_id)),
                                ('resy_id', str(resy_id)),
                                ('resx_name', resx_name),
                                ('resy_name', resy_name),
                                ('atmx_id', str(atmx_id)),
                                ('atmy_id', str(atmy_id)),
                                ('atmx_name', atmx_name),
                                ('atmy_name', atmy_name),
                                ('dist', str(dist))
                            ]))

                            seen[atmx_id][atmy_id] = 1
                            seen[atmy_id][atmx_id] = 1
        return dists

    def run(self):
        """main method"""

        # Set pdb directory
        outdir = self.settings.pdbdist.args.get("output_directory", os.getcwd())
        pdbdir = self.settings.pdbdist.args.get('pdbdir', None)
        if not self.settings.pdbdist.args.get('pdbdir') or not os.access(pdbdir,
                                                                         os.F_OK):

            if not self.settings.pdbdist.config.get("download_pdbs", False):
                LOG.warning(
                    "PDB directory not given and download inactivated."
                    " PDBDist command can't download pdb files.")
                return None
            else:
                # We have to download all files so we set outdir as pdbdir
                pdbdir = os.path.join(os.path.abspath(outdir), 'pdbs')
                if not os.access(pdbdir, os.F_OK):
                    os.makedirs(pdbdir)
        else:
            pdbdir = os.path.abspath(pdbdir)

        pair_flag = self.settings.pdbdist.config.get("pair_list", "all")
        distpath = os.path.join(outdir, "pdbdists.dat")
        distpath = distpath if pair_flag in distpath else \
            os.path.splitext(distpath)[0] + "_" + pair_flag + \
            os.path.splitext(distpath)[1]

        # Load cullpdb file
        LOG.info("Loading culled list of pdb file(s)")
        cullfile = CulledPdbFile(self.settings.pdbdist.args.get("cullpdbs"))
        cullfile.load()

        # Load all pdb files and generate distance file
        LOG.info("Looking for pdb entries into %s folder", pdbdir)
        header = ""

        with open(distpath, 'w') as distfile:

            for idline, pdbent in iteritems(cullfile.lines):

                pdbid = pdbent.get("pdb_id")

                # Download file if necessary
                if not glob('%s/*%s*' % (pdbdir, pdbid.lower())):
                    LOG.debug("Download pdb entry for %s pdb", pdbid)
                    try:
                        self.pdblist.retrieve_pdb_file(pdbid, pdir=pdbdir,
                                                       file_format="pdb")
                    except Exception as msg:
                        LOG.error("%s: %s", Exception, msg)
                        continue
                    if not glob('%s/*%s*' % (pdbdir, pdbid.lower())):
                        LOG.info("Error during download, try to download in "
                                 "mmCIF format")
                        try:
                            self.pdblist.retrieve_pdb_file(pdbid, pdir=pdbdir,
                                                           file_format="mmCif")
                        except Exception as msg:
                            LOG.error("%s: %s", Exception, msg)
                            continue

                pdbpaths = glob('%s/*%s*' % (pdbdir, pdbid.lower()))
                LOG.debug("Found %s file(s) related to %s pdb entry into %s folder "
                          "(%s)", len(pdbpaths), pdbid, pdbdir, pdbpaths)

                # Get distance matrix from pdb file and write distances into
                # output file
                dists = self.pdbdists(
                    pdbpaths[0], pdbid, pdbent.get("chain_id"),
                    pair_list=pair_flag)

                if idline == 1:
                    LOG.info("Writing header line")
                    header = [key for key in dists[0].keys()]
                    LOG.debug(",".join(header))
                    distfile.write("%s\n" % ",".join(header))

                for dist in dists:
                    distfile.write("%s\n" % ",".join([dist[key] for key in header]))

                if self.settings.pdbdist.config.get("remove_pdbs", False):
                    LOG.info("Remove %s pdb file", pdbid)
                    os.remove(pdbpaths[0])

        LOG.info("PDBdist finished")
