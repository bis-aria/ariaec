# coding=utf-8
"""
                            Input/Output aria_ec scripts
"""
from __future__ import absolute_import, division, print_function

import os
import json
import logging

from .protein import Protein
from .reader import MapFileListReader
from .common import get_filename
from .protmap import MapFilter
from .converter import AriaEcXMLConverter

# TODO: S'inspirer de pandas/__init__.py pour les dependances

LOG = logging.getLogger(__name__)


class AriaEcSetup(object):
    """Aria Ec Setup protocol"""

    def __init__(self, settings):
        """
        :param settings:
        :return:
        """
        # TODO: check_type settings (AriaEcSettings)
        self.settings = settings
        self.protein = Protein(settings)
        self.outprefix = ''
        self.reader = MapFileListReader(cont_def=settings.contactdef.config)
        self.allresmap = {}
        self.targetmaps = None
        self.refmaps = None
        self.hbmaps = None
        self.filter = MapFilter(
            settings.setup.config,
            nofilter=settings.setup.args.get("no_filter"))
        self.converter = AriaEcXMLConverter(settings)

    def run(self):
        """
        main method
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        # Check input
        LOG.debug("Settings:\n" + json.dumps(self.settings.setup.config,
                                             indent=4))
        LOG.debug("Args:\n" + json.dumps(self.settings.setup.args,
                                         indent=4))
        self.settings.make_infra()
        # -------------------------------------------------------------------- #
        # ----------------------------- Input -------------------------------- #
        # -------------------------------------------------------------------- #
        self.outprefix = get_filename(self.settings.setup.args.get("seq", ""))
        self.converter.outprefix = self.outprefix
        # ------------------------- Load sequence ---------------------------- #
        self.protein.set_aa_sequence(
            self.settings.setup.args.get("seq", None))
        # -------------- Load secondary structure prediction ----------------- #
        if self.settings.setup.args.get("sspred", None):
            self.protein.set_sec_struct(self.settings.setup.args.get("sspred",
                                                                     None),
                                        ssdist_filename=self.settings.ssdist,
                                        ssidx=self.settings.setup.args.get(
                                            "ssidx", False))
        # -------------------------------------------------------------------- #
        # ---------------------------- Processing ---------------------------- #
        # -------------------------------------------------------------------- #
        # TODO: write submatrix in a file
        # TODO: change read method in reader to __call__
        # -------------------------- contact maps ---------------------------- #
        self.reader.read(self.settings.setup.args.get("infiles"),
                         maptypes=self.settings.setup.args.get(
                             "contact_types"),
                         protein=self.protein,
                         groupby_method=self.settings.setup.config[
                             'groupby_method'],
                         scsc=self.settings.scsc_min)
        for mapfile in self.reader.maps:
            # fo need a maplot in order to wite XML dist restraints
            # TODO: filter pour toutes les map de mapdict !! (fonction remove
            #  s'applique sur l'humanidx contenant les residus)
            self.filter(mapfile.mapdict, mapfile.filetype, mapfile.contactlist,
                        self.protein, clashlist=mapfile.clashlist,
                        outprefix=self.outprefix,
                        outdir=self.settings.infra.get("etc", ''))
            self.allresmap[mapfile.filetype] = mapfile.mapdict

            if mapfile.filetype != "pdb" and "pdb" in self.allresmap:
                mapfile.maplot.compareplot(self.allresmap["pdb"])

        # ---------------------------- target map ---------------------------- #
        if self.settings.setup.args.get("distfile") and \
                self.settings.setup.config.get("distance_type") == "distfile":
            LOG.info("Loading target distance file")
            # Read distance file
            self.reader.read(
                self.settings.setup.args.get("distfile"),
                maptypes="distfile",
                protein=self.protein,
                groupby_method=self.settings.setup.config['groupby_method'],
                scsc=self.settings.scsc_min)
            self.targetmaps = self.converter.targetdistmaps(
                self.protein.aa_sequence.sequence, distfile=self.reader.maps[0])
        else:
            # Default targetmaps using config parameters
            self.targetmaps = self.converter.targetdistmaps(
                self.protein.aa_sequence.sequence)

        # ---------------------------- ref map ----------------------------- #
        if self.settings.setup.args.get("ref"):
            self.reader.read(
                self.settings.setup.args.get("ref"),
                maptypes="pdb",
                protein=self.protein,
                groupby_method=self.settings.setup.config['groupby_method'],
                scsc=self.settings.scsc_min)
            self.refmaps = self.reader.maps[0].mapdict

        # ---------------------------- hbond map ----------------------------- #
        if self.settings.setup.args.get("hb") and \
                self.settings.setup.config.get("longrange_hb", False):
            self.reader.read(
                self.settings.setup.args.get("hb"),
                maptypes="metapsicovhb",
                protein=self.protein,
                groupby_method=self.settings.setup.config['groupby_method'],
                scsc=self.settings.scsc_min)
            self.hbmaps = self.reader.maps[0].mapdict
        elif self.settings.setup.config.get("longrange_hb", False):
            # Create HBMAP with naive metapsicov method
            # Consider as hbond ec predicted between beta strand
            raise NotImplementedError

        # -------------------------------------------------------------------- #
        # ------------------------------ Output ------------------------------ #

        # ----------------------------- SEQ file ----------------------------- #
        seqrange = self.settings.setup.args.get("seqrange")
        # TODO: make seqrange functionnal
        # self.protein.write_seq(
        #     os.path.join(self.settings.infra.get("etc", ''),
        #                  self.outprefix + ".seq"), seqrange=seqrange)
        self.protein.write_seq(
            os.path.join(self.settings.infra.get("etc", ''),
                         self.outprefix + ".seq"))
        # Load aria molecule object from seq file and convert it into xml format
        LOG.info("Load molecule file and convert it into xml format")
        self.converter.read_seq(self.protein.seqfile_path)
        # --------------------------- TBL restraints ------------------------- #

        LOG.info("Writing tbl files ...")
        tbl_files = self.converter.write_tbl_restraints(
            self.protein, hbmap=self.hbmaps)

        # --------------------------- XML restraints ------------------------- #
        # Setting contact number limit for map restraints (native, ec, ...)

        dist_files = self.converter.write_maplist_restraints(
            self.allresmap, self.targetmaps, self.protein)[0]

        # --------------------------- XML SEQ file --------------------------- #
        xmlseq_file = self.converter.write_xmlseq()

        # --------------------------- TBL restraints as XML ------------------ #
        if self.settings.setup.args.get('ssxml', False):
            LOG.info("Writing SS as XML files ...")

            dist_files += self.converter.tbl_as_xml(xmlseq_file, [tbl_files['hbond']],
                                                    'hbond')
            dist_files += self.converter.tbl_as_xml(xmlseq_file, [tbl_files['ssdist']],
                                                    'ssdist')
            tbl_files = {'hbond': None, 'dihed': None, 'ssdist': None}
            # or just dihedrals tbl_files =  tbl_files{dihed}

        # ---------------------- ARIA XML project file ----------------------- #
        self.converter.write_ariaproject(
            self.settings.template, xmlseq_file, dist_files, tbl_files,
            desclist=self.allresmap.keys())
        # -------------------------------- etc ------------------------------- #
        self.write_optional_files()

    def write_optional_files(self):
        """
        Write filtered contacts & distance maps (.csv)

        Parameters
        ----------

        Returns
        -------

        
        """
        # Indextableplus file (submatrix)
        # Contacts_refined.out
        for maptype in self.allresmap:
            self.allresmap[maptype].get("maplot").write_contacts(
                "_".join((self.outprefix, maptype, "filtered")),
                self.settings.infra.get("etc"), scoremap=self.allresmap[
                    maptype].get("scoremap"))
            if self.allresmap[maptype]["alldistmap"] is not None:
                self.allresmap[maptype]["alldistmap"].to_csv(
                    "%s/%s.distmap.csv" % (self.settings.infra.get("etc"),
                                           maptype))
            if self.refmaps:
                self._write_contacts(
                    self.allresmap[maptype].get("filteredlist", None),
                    self.protein.aa_sequence.sequence,
                    self.settings.infra.get("etc", ''),
                    "_".join((self.outprefix, maptype, "filtered")),
                    ref=self.refmaps["maplot"],
                    distmap=self.refmaps["distmap"])
        if self.refmaps:
            self.refmaps["alldistmap"].to_csv(
                "%s/%s_%s.distmap.csv" % (self.settings.infra.get("etc"),
                                          self.outprefix,
                                          self.refmaps.filetype))

    def _write_contacts(self, contacts_list, seq, out, prefix, nc=None,
                        append=False, ref=None, distmap=None):
        """
        Write contacts from contact_list (sorted !)

        Parameters
        ----------
        contacts_list :
            param seq:
        out :
            param prefix:
        nc :
            param append: (Default value = None)
        ref :
            return: (Default value = None)
        seq :
            
        prefix :
            
        append :
            (Default value = False)
        distmap :
            (Default value = None)

        Returns
        -------

        
        """
        mapy = []
        tp_count = 0
        filemode = 'a' if append else 'w'
        dist_desc = '\tTP/FP\tdist%s(ref)' % self.settings.setup.config[
            "groupby_method"] if \
            distmap is not None \
            else ''
        with open("%s/%s.contacts.txt" % (out, prefix), filemode) as outfile:
            if not append:
                outfile.write(
                    '''# resid1\tresid2\tres1\tres2%s\n''' % dist_desc)

            if hasattr(contacts_list, 'keys'):
                contacts = contacts_list.keys() if not nc else \
                    contacts_list.keys()[:nc]
                d_type = True
            else:
                # Check if contacts is 2-tuple
                if False in [len(item) == 2 for item in contacts_list]:
                    raise TypeError('Contact list must be 2-tuple !')
                contacts = contacts_list if not nc else contacts_list[:nc]
                d_type = False

            LOG.debug("Contact list %s", contacts)
            for contact in contacts:

                if d_type:
                    # If dictionary
                    resid1 = int(contacts_list[contact].setdefault('res1_nb'))
                    resid2 = int(contacts_list[contact].setdefault('res2_nb'))
                else:
                    resid1 = int(contact[0])
                    resid2 = int(contact[1])

                LOG.debug("Contact %s", str(contact))
                if distmap is not None:
                    dist = distmap.ix[(resid1, resid2)]
                else:
                    dist = {'dist': '', 'atoms': ''}

                if (resid1, resid2) not in mapy:
                    mapy.append((resid1, resid2))
                    if ref is not None:
                        if ref.ix[(resid1, resid2)]:
                            asses = 'TP'
                            tp_count += 1
                        else:
                            asses = 'FP'
                        outfile.write(
                            "%s\t%s\t%s\t%s\t%s\t%s\n" % (resid1 + 1,
                                                          resid2 + 1,
                                                          seq[resid1],
                                                          seq[resid2], asses,
                                                          dist))
                    else:
                        outfile.write("%s\t%s\t%s\t%s\n" % (resid1 + 1,
                                                            resid2 + 1,
                                                            seq[resid1],
                                                            seq[resid2]))

            ptp = (tp_count / float(len(contacts))) * 100.0 if ref is not None else \
                None
            outfile.write('''
# TP number : {tp} ({ptp:.2f} %)
# Number of contacts : {nc}
'''.format(tp=tp_count, ptp=ptp, nc=len(contacts)))


if __name__ == "__main__":
    # Test AriaEcCommand object
    from .settings import AriaEcSettings

    logging.basicConfig(level=logging.DEBUG)
    LOG = logging.getLogger("Setup")
    AriaEcSettings('setup').load_config('config.ini')
