# coding=utf-8
"""
                            PDB distance distribution analysis
"""

import itertools
import logging
import numpy as np
import os
import pandas as pd
import pickle
import re
import sklearn.mixture as mixture
from ..core.ConversionTable import ConversionTable
from pathos.multiprocessing import ProcessingPool
from tqdm import tqdm

from ..core.legacy.AminoAcid import AminoAcid
from .common import TqdmToLogger
from .protmap import SsAaAtmMap

LOG = logging.getLogger(__name__)


class PDBStat(object):
    """
    Analyze distance distribution (should be extracted with pdbdist cmd)
    """

    def __init__(self, settings):
        self.settings = settings

    @staticmethod
    def ss_type(ss1, ss2, intra=True):
        """
        
        Parameters
        ----------
        ss1, ss2: str
            2L code for secondary structure (e.g. H1, E2, ...)
        intra: bool, optional
            Define if we return or not intra code (1L)

        Returns
        -------
        ss_type: str
            2L or 1L code defining contact with secondary structures (e.g. XE, 
            HH, ...)

        """
        ss_type = ss1[0] + ss2[0]
        if ss1 != ss2 or not intra:
            if ss_type in ("HH", "EE", "HE", "EH"):
                # Contact between helice or beta strand
                return "HE" if ss_type not in ("HH", "EE") else ss_type
            else:
                # Contact with loops or other secondary
                # structure
                return "XH" if "H" in ss_type else "XE" if "E" in ss_type \
                    else "XX"
        else:
            # Same secondary structure
            return ss1[0]

    def subdist(self, distmatrix, group, intra=False):
        """
        
        Parameters
        ----------
        intra
        distmatrix
        group

        Returns
        -------

        """
        ss1, res1, atm1 = group[0]
        ss2, res2, atm2 = group[1]

        ss_type = self.ss_type(ss1, ss2, intra=intra)
        heavy_reg = re.compile(r"[CNOS][ABGDEZH][0-9]?")
        atomtable = ConversionTable().table['AMINO_ACID']['iupac']
        list_t = []
        if atm1 == "SC" and atm2 in ("CA", "CB"):
            for atm1 in filter(heavy_reg.match, atomtable[AminoAcid(res1)[1]].keys()):
                if atm1 not in ("CA", "CB"):
                    list_t.append((ss_type, AminoAcid(res1)[0], AminoAcid(res2)[0], atm1, atm2))
                    list_t.append((ss_type, AminoAcid(res2)[0], AminoAcid(res1)[0], atm2, atm1))
        elif atm2 == "SC" and atm1 in ("CA", "CB"):
            for atm2 in filter(heavy_reg.match, atomtable[AminoAcid(res2)[1]].keys()):
                if atm2 not in ("CA", "CB"):
                    list_t.append((ss_type, AminoAcid(res1)[0], AminoAcid(res2)[0], atm1, atm2))
                    list_t.append((ss_type, AminoAcid(res2)[0], AminoAcid(res1)[0], atm2, atm1))
        else:
            list_t = [(ss_type, AminoAcid(res1)[0], AminoAcid(res2)[0], atm1, atm2),
                      (ss_type, AminoAcid(res2)[0], AminoAcid(res1)[0], atm2, atm1)]

        return pd.DataFrame(
            list_t, columns=["ss_type", "resx_name", "resy_name", "atmx_name",
                             "atmy_name"]).merge(distmatrix).as_matrix(columns=["dist"])

    @staticmethod
    def gmm(dists, n_components_range):
        """
        
        Parameters
        ----------
        dists
        n_components_range: list of int

        Returns
        -------

        """
        # gmms = {}
        best_gmm = None
        best_logmm = None
        df = pd.DataFrame()

        cv_types = ['spherical', 'tied', 'diag', 'full']
        x = np.linspace(1, max(dists) + 1, 10000).reshape(-1, 1)
        logx = np.log(x.reshape(-1, )).reshape(-1, 1)
        lowest_bic = np.infty
        lowest_logbic = np.infty

        for i, cv_type in enumerate(cv_types):
            for n_components in n_components_range:
                # Fit a Gaussian mixture with EM
                gmm = mixture.GaussianMixture(n_components=n_components,
                                              covariance_type=cv_type)
                gmm.fit(dists)
                bic, aic = gmm.bic(dists), gmm.aic(dists)
                logprob = gmm.score_samples(x)
                responsibilities = gmm.predict_proba(x)
                pdf = np.exp(logprob)
                pdf_individual = responsibilities * pdf[:, np.newaxis]

                logmm = mixture.GaussianMixture(n_components=n_components,
                                                covariance_type=cv_type)
                logmm.fit(np.log(dists))
                dlog_bic, dlog_aic = logmm.bic(np.log(dists)), logmm.aic(np.log(dists))
                dlog_logprob = logmm.score_samples(logx)
                dlog_responsibilities = logmm.predict_proba(logx)
                dlog_pdf = np.exp(dlog_logprob)
                dlog_pdf_individual = dlog_responsibilities * dlog_pdf[:, np.newaxis]

                for n in range(pdf_individual.shape[1]):
                    # For each component of a GMM

                    tmp = pd.DataFrame({
                        "d": x.reshape(-1, ),
                        "log(d)": logx.reshape(-1, ),
                        "pdf": pdf,
                        "dlog_pdf": dlog_pdf,
                        "pdf_idv": pdf_individual[:, n],
                        "dlog_pdf_idv": dlog_pdf_individual[:, n],
                    })
                    tmp["n_comps"] = n_components
                    tmp["comp"] = int(n + 1)
                    tmp["cv_type"] = cv_type
                    tmp["aic"] = aic
                    tmp["bic"] = bic
                    tmp["dlog_aic"] = dlog_aic
                    tmp["dlog_bic"] = dlog_bic
                    tmp["mixt_mean"] = float(gmm.means_[n])
                    tmp["logmixt_mean"] = float(logmm.means_[n])
                    tmp["mixt_weight"] = float(gmm.weights_[n])
                    tmp["logmixt_weight"] = float(logmm.weights_[n])
                    tmp["mixt_convflag"] = bool(gmm.converged_)
                    tmp["logmixt_convflag"] = bool(logmm.converged_)

                    df = df.append(tmp)

                # TODO: CHANGE DEFAULT SELECTION CRITERIA TO AIC
                if bic < lowest_bic:
                    lowest_bic = bic
                    best_gmm = gmm

                if dlog_bic < lowest_logbic:
                    lowest_logbic = dlog_bic
                    best_logmm = logmm

        return df, best_gmm, best_logmm

    def gmm_worker(self, dists, contactype,
                   n_components_range=range(1, 8)):
        """
        Mixture analysis for a specific atom-atom distance
        
        Parameters
        ----------
        contactype
        dists
        n_components_range

        Returns
        -------

        """
        minsize = int(self.settings.pdbstat.config.get("sample_minsize", 20))

        results = {
            'mindists': None,
            'maxdists': None,
            'logmm_means': None,
            'logmm_min': None,
        }

        if dists.size > minsize:
            # Get mixture models related to log distribution
            results['maxdists'] = float(np.max(dists))
            results['mindists'] = float(np.min(dists))
            logmm = self.gmm(
                dists, n_components_range=n_components_range)[2]
            results['logmm_means'] = np.exp(logmm.means_)
            results['logmm_min'] = np.min(results.get('logmm_means'))

        return contactype, results

    def run(self):
        """
        Command line method
        
        Returns
        -------

        """
        ncpus = int(self.settings.pdbstat.args.get("njobs")) if \
            self.settings.pdbstat.args.get("njobs") else None
        minflag = self.settings.pdbstat.args.get("minflag")

        interfiles = [
            os.path.join(self.settings.pdbstat.args.get("output_directory"),
                         foo + ".inter.p")
            for foo in ("lowerbounds", "targetdists", "upperbounds")]
        intrafiles = [
            os.path.join(self.settings.pdbstat.args.get("output_directory"),
                         foo + ".intra.p")
            for foo in ("lowerbounds", "targetdists", "upperbounds")]

        LOG.info("Reading csv matrix")
        # Read pdb distance file
        dists = pd.read_csv(self.settings.pdbstat.args["pdbdists"],
                            low_memory=False,
                            usecols=["ss_type", "resx_name", "resy_name",
                                     "atmx_name", "atmy_name", "dist"])
        # LOG.debug(self.dists.head())

        # Initialize target distance maps
        inter_lowerbounds, inter_targetdists, inter_upperbounds = [
            SsAaAtmMap(scsc_min=minflag) if minflag else SsAaAtmMap()
            for _ in range(3)]
        intra_lowerbounds, intra_targetdists, intra_upperbounds = [
            SsAaAtmMap(scsc_min=minflag) for _ in range(3)]

        # Iterative product through all groups
        # Since the target map is a square matrix, we compute only half
        # of the dataframe => Need a set of unique index corresponding to
        # half of the dataframe
        LOG.info("Extracting pdb distances (inter secondary structure) for each"
                 " contact type")
        groups = inter_targetdists.index.get_values().tolist(), \
            inter_targetdists.columns.get_values().tolist()
        groups = set(itertools.product(*groups))
        groups = list({tuple(group) for group in map(sorted, groups)})

        # TODO: Find a way to accelerate interdists
        # Faire un ensemble de threads qui vont calculer separement les sous
        # ensembles de distance avant de faire les gmm
        # Pour chaque sous ensemble de groups
        # groups = groups[:5]
        # pool = ProcessingPool(nodes=ncpus)
        # iteresults = pool.imap(self.subdist, [dists]*len(groups), groups)
        # interdists = []

        # TODO: use sql to select subdataframes in order to work with heavy
        # data http://pythondata.com/working-large-csv-files-python/
        tqdm_out = TqdmToLogger(LOG, level=logging.INFO)
        interdists = [
            self.subdist(dists, group) for group in tqdm(groups, file=tqdm_out,
                                                         mininterval=60)]
        # for igroup, group in enumerate(tqdm(groups)):
            # tqdm.write("%s %s" % (igroup, group[:2]))
            # interdists.append(iteresults.next())
            # tqdm.write("%s %s" % (group, str(gmm_results)))
        # return None
        # Regrouper l'ensemble des sous distances pour chaque restype2

        pool = ProcessingPool(nodes=ncpus)
        LOG.info("Computing gaussian mixtures")
        iteresults = pool.imap(self.gmm_worker, interdists, groups,
                               n_components_range=range(1, 8))

        for igroup, group in enumerate(tqdm(groups, file=tqdm_out,
                                            mininterval=30)):
            # tqdm.write("%s %s" % (igroup, group[:2]))
            workergroup, results = iteresults.next()
            if workergroup == group:
                inter_lowerbounds[group[0]][group[1]] = results.get('mindists',
                                                                    np.NaN)
                inter_upperbounds[group[0]][group[1]] = results.get('maxdists',
                                                                    np.NaN)
                # TODO: actually using only minimal mode extracted from the
                # distribution
                inter_targetdists[group[0]][group[1]] = results.get(
                    'logmm_min', np.NaN)
        del interdists
        LOG.info("Serializing target distance maps")
        pickle.dump(inter_lowerbounds.to_dict(),
                    open(interfiles[0], "wb"))
        pickle.dump(inter_targetdists.to_dict(),
                    open(interfiles[1], "wb"))
        pickle.dump(inter_upperbounds.to_dict(),
                    open(interfiles[2], "wb"))

        LOG.info("Extracting pdb distances (intra secondary structure) for each"
                 " contact type")
        intradists = [
            self.subdist(dists, group, intra=True)
            for group in tqdm(groups, file=tqdm_out, mininterval=60)]

        pool = ProcessingPool(nodes=ncpus)
        LOG.info("Computing gaussian mixtures")
        iteresults = pool.imap(self.gmm_worker, intradists, groups,
                               n_components_range=range(1, 8))

        for igroup, group in enumerate(tqdm(groups, file=tqdm_out,
                                            mininterval=30)):
            # tqdm.write("%s %s" % (igroup, group[:2]))
            workergroup, results = iteresults.next()
            if workergroup == group:
                intra_lowerbounds[group[0]][group[1]] = results.get('mindists',
                                                                    np.NaN)
                intra_upperbounds[group[0]][group[1]] = results.get('maxdists',
                                                                    np.NaN)
                # TODO: actually using only minimal mode extracted from the
                # distribution
                intra_targetdists[group[0]][group[1]] = results.get('logmm_min',
                                                                    np.NaN)
        del intradists

        LOG.info("Serializing target distance maps")
        pickle.dump(intra_lowerbounds.to_dict(),
                    open(intrafiles[0], "wb"))
        pickle.dump(intra_targetdists.to_dict(),
                    open(intrafiles[1], "wb"))
        pickle.dump(intra_upperbounds.to_dict(),
                    open(intrafiles[2], "wb"))
