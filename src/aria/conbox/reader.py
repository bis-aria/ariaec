# coding=utf-8
"""
                            Reader objects
"""
from __future__ import absolute_import, division, print_function

import os
import re
import logging
import os.path
import collections
import pkg_resources as pkgr
import scipy.spatial.distance as distance
from Bio import pairwise2
from conkit import io as conio
from conkit.core.sequence import Sequence
from conkit.core.contactfile import ContactFile
from conkit.core.sequencefile import SequenceFile
from .common import sort_2dict
from .protmap import (ResMap, ResAtmMap)

LOG = logging.getLogger(__name__)
# TODO: check if Atom is still used ...
Atom = collections.namedtuple("Atom", ["name", "coords"])


class RegexFile(object):
    """File which can be parsed with a regex"""

    def __init__(self, filepath, filetype='', regex=r'', sort='', default=''):
        """

        Parameters
        ----------
        filepath :
        filetype :
        regex : :py:class:`re._pattern_type`
        sort :
        default :
        """
        self.regex = regex
        self.sort = sort
        self.lines = {}
        self.filetype = filetype
        self._filename = None
        self._filepath = filepath
        self._defaultpath = default if pkgr.resource_exists(__name__, default) \
            else None
        self._pkgfile = False
        self._checkpath()

    def _checkpath(self):
        """Check if filepath exists or is in package ressources"""
        LOG.debug("Checking if %s file exists", self.filetype)
        if self._filepath and os.path.exists(self._filepath):
            # File actually exists outside of the package
            LOG.debug("File exists.")
        elif self._filepath and pkgr.resource_exists(__name__,
                                                     self._filepath):
            # File is in pkgr
            LOG.debug("Use %s file from package ressources",
                      self.filetype)
            self._pkgfile = True
        elif self._defaultpath:
            LOG.debug("Use default %s file", self.filetype)
            self._filepath = self._defaultpath
            self._pkgfile = True
        else:
            LOG.warning("File %s does not exists", self.filetype)

    @property
    def filename(self):
        """Get basename of filepath"""
        if self._filepath and not self._filename:
            self._filename = os.path.basename(
                os.path.splitext(self._filepath)[0])
        return self._filename

    @property
    def filepath(self):
        """File path"""
        return self._filepath

    def load(self, *args):
        """
        Fill lines with dictionary. Each key is a line number in the given file
        :return: None

        Parameters
        ----------

        Returns
        -------

        
        """
        lines_dict = {}

        if not self.regex:
            LOG.error("Can't parse file %s without defined regex",
                      self.filepath)
            return None

        with pkgr.resource_stream(__name__, self.filepath) if self._pkgfile \
                else open(self.filepath) as infile:

            for index, line in enumerate(infile):
                match = self.regex.match(line)
                if match:
                    lines_dict[index] = match.groupdict()

        if self.sort:
            lines_dict = sort_2dict(lines_dict, self.sort)

        self.lines = lines_dict


class CulledPdbFile(RegexFile):
    """Reader class for culled pdb list"""
    regex = re.compile(
        r"^(?P<pdb_id>\w{4})"
        r"(?P<chain_id>\w)\s+"
        r"(?P<length>\d+)\s+"
        r"(?P<exp>\w+)\s+"
        r"(?P<resolution>-?\d+\.\d+)\s+"
        r"(?P<r_factor>-?\d+\.\d+)\s+"
        r"(?P<free_rvalue>-?\d+\.\d+)\s*$")
    filetype = 'cullpdb'
    default = 'data/cullpdb/160427/cullpdb_pc25_res1.6_R0.25_d160427_chains3743'

    def __init__(self, *args, **kwargs):
        kwargs['regex'] = self.regex
        kwargs['filetype'] = self.filetype
        kwargs['default'] = self.default
        super(CulledPdbFile, self).__init__(*args, **kwargs)


class TblDistFile(RegexFile):
    """Reader class for TBL Distance restraint file"""
    regex = re.compile(
        r"^\s*(?P<restflag>or|assign)\s+\((segid\s+\"(?P<segid1>[\s\w]+)"
        r"\"\s+and\s+)?resid\s+(?P<resid1>\d+)\s+and\s+name\s+(?P<atm1>\w+)"
        r"\)\s+\((segid\s+\"(?P<segid2>[\s\w]+)\"\s+and\s+)?resid\s+"
        r"(?P<resid2>\d+)\s+and\s+name\s+(?P<atm2>\w+)\)\s*"
        r"(?P<dtarget>\d+\.\d+)?\s*(?P<dminus>\d+\.\d+)?\s*(?P<dplus>\d+\.\d+)"
        r"?\s*(weight)?\s*(?P<weight>\d+\.\d+)?\s*!?\s*(spec=)?"
        r"(?P<listname>\w+)?(,\s*no=)?(?P<restno>\d+)?(,\s*id=)?")
        #r"(?P<restid>\d+)?(,\s*vol=)?(?P<vol>[\w.\-+]+)?$")
    filetype = 'tbldist'

    def __init__(self, *args, **kwargs):
        kwargs['regex'] = self.regex
        kwargs['filetype'] = self.filetype
        super(TblDistFile, self).__init__(*args, **kwargs)


class MapFile(RegexFile):
    """Abstract class related to all kind of protein maps"""
    # List of 3tuples ("regex_file", "filetype", "sort_field")
    # sort_field allow sorting lines with values into this field
    # TODO: wrong regex for native_full ?
    # TODO: smarter dict ...
    conkit_alias = {
        "raptorx": "casp",
    }
    types = {
        "plmdca": {
            "regex": re.compile(r"^(?P<res1_nb>\d+)\s+(?P<res1_name>\w)\s+"
                                r"(?P<res2_nb>\d+)\s+(?P<res2_name>\w)\s+"
                                r"(?P<mi_score>\d)\s+"
                                r"(?P<plm_score>-?\d+\.\d+)\s*$"),
            "score_field": "plm_score"
        },
        "plm": {
            "regex": re.compile(r"^(?P<res1_nb>\d+)\s+(?P<res1_name>\w)\s+"
                                r"(?P<res2_nb>\d+)\s+(?P<res2_name>\w)\s+"
                                r"(?P<mi_score>\d)\s+"
                                r"(?P<plm_score>-?\d+\.\d+)\s*$"),
            "score_field": "plm_score"
        },
        "plmev": {
            "regex": re.compile(r"^(?P<res1_nb>\d+)\s+(?P<res1_name>\w)\s+"
                                r"(?P<res2_nb>\d+)\s+(?P<res2_name>\w)\s+"
                                r"(?P<mi_score>\d)\s+"
                                r"(?P<plm_score>-?\d+\.\d+)\s*$"),
            "score_field": "plm_score"
        },
        "plmc": {
            "regex": re.compile(r"^(?P<res1_nb>\d+)\s+(?P<res1_name>\w)\s+"
                                r"(?P<res2_nb>\d+)\s+(?P<res2_name>\w)\s+"
                                r"(?P<mi_score>\d)\s+"
                                r"(?P<plm_score>-?\d+\.\d+)\s*$"),
            "score_field": "plm_score"
        },
        "evcoupling": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+),(?P<res2_nb>\d+),'
                r'(?P<ec_score>-?\d+\.\d+e?-?\d*),'
                r'(?P<placeholder>\d),(?P<res1_cons>\d+),'
                r'(?P<res2_cons>\d+),(?P<ss_filter>\d|\d{3}),'
                r'(?P<high_cons_filter>\d|\d{3}),'
                r'(?P<cc_filter>\d|\d{3}),(?P<res1_name>\w),'
                r'(?P<res2_name>\w)$'),
            "score_field": "ec_score"
        },
        "pconsc": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) '
                r'(?P<ec_score>-?\d+\.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "pconsc1": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) '
                r'(?P<ec_score>-?\d+\.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "pconsc2": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) '
                r'(?P<ec_score>-?\d+\.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "metapsicov_stg1": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) \d (?P<res_dist>-?\d+.?\d*) '
                r'(?P<ec_score>-?\d+.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "metapsicov_stg2": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) \d (?P<res_dist>-?\d+.?\d*) '
                r'(?P<ec_score>-?\d+.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "psicov": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+) (?P<res2_nb>\d+) \d (?P<res_dist>-?\d+.?\d*) '
                r'(?P<ec_score>-?\d+.\d+e?-?\d*)$'),
            "score_field": "ec_score"
        },
        "gremlin": {
            "regex": re.compile(
                r'^(?P<res1_nb>\d+)\t(?P<res2_nb>\d+)\t'
                r'(?P<res1_id>\d+_[AC-IK-NP-TVWYZ])\t'
                r'(?P<res2_id>\d+_[AC-IK-NP-TVWYZ])\t'
                r'(?P<raw_score>-?\d+\.\d+e?-?\d*)\t'
                r'(?P<scale_score>-?\d+\.\d+e?-?\d*)\t'
                r'(?P<prob>-?\d+\.\d+e?-?\d*)'),
            "score_field": "scale_score"
        },
        "native": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+('
                r'?P<ca_ca>\d+\.\d+)\s+(?P<cb_cb>\d+\.\d+)\s+'
                r'(?P<sc_sc>\d+\.\d+)\s+(?P<valid>\w+)'),
            "score_field": None
        },
        "native_full": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+('
                r'?P<ca_ca>\d+\.\d+)\s+(?P<cb_cb>\d+\.\d+)\s+'
                r'(?P<sc_sc>\d+\.\d+)\s+(?P<valid>\w+)'),
            "score_field": None
        },
        "bbcontacts": {
            "regex": re.compile(
                r'\s*(?P<identifier>\w+)\s+(?P<diversity>-?\d+\.?\d*)'
                r'\s+(?P<direction>Parallel|Antiparallel)'
                r'\s+(?P<viterbiscore>-?\d+\.?\d*)'
                r'\s+(?P<indexpred>\d+)'
                r'\s+(?P<state>first|internal|last)'
                r'\s+(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)'),
            "score_field": None
        },
        "contactlist": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)[\s,;]+(?P<res2_nb>\d+)[\s,'
                r';]*(?P<con_flag>\w*)'),
            "score_field": None
        },
        "metapsicovhb": {
            "regex": re.compile(
                r'^\s*(?P<res_donor>\d+)[\s,;]+'
                r'(?P<res_acceptor>\d+)[\s,;]+(?P<dist_bound>\d)[\s,;]+'
                r'(?P<res_dist>-?\d+.?\d*)[\s,;]+'
                r'(?P<hbscore>-?\d+\.?\d*)'),
            "score_field": "hbscore"
        },
        "pfrmatrr": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+('
                r'?P<dmin>\d+)\s+(?P<dmax>\d+)\s+'
                r'(?P<score>\d+\.\d+)'),
            "score_field": "score"
        },
    }
    unknown = {
        "default_1": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+'
                r'(?P<resn1>\w+)\s+'
                r'(?P<resn2>\w+)\s+'
                r'(?P<score>[\w\d.+\-]+)'),
            "score_field": "score"
        },
        "default_2": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+'
                r'(?P<score>[\w\d.+\-]+)'),
            "score_field": "score"
        },
        "default_3": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+'
                r'(?P<resn1>\w+)\s+'
                r'(?P<resn2>\w+)\s+'),
            "score_field": None
        },
        "default_4": {
            "regex": re.compile(
                r'^\s*(?P<res1_nb>\d+)\s+(?P<res2_nb>\d+)\s+'),
            "score_field": None
        },
        "empty": {
            "regex": re.compile(r'^\s*$'),
            "score_field": None
        }
    }

    def __init__(self, *args, **kwargs):
        """

        Parameters
        ----------
        args
        kwargs
        """
        self.conioflag = kwargs.pop("conkit_reader") \
            if "conkit_reader" in kwargs else False
        self.checkflag = kwargs.pop("check_type") \
            if "check_type" in kwargs else True
        super(MapFile, self).__init__(*args, **kwargs)
        self.filetype = self.filetype if self.filetype not in self.conkit_alias else \
            self.conkit_alias[self.filetype]
        if not self.conioflag and self.checkflag:
            LOG.info("The file format {ftype} is not supported by the conkit "
                     "plugin. Switching to homemade parsers.".format(
                ftype=self.filetype))
            LOG.debug("Using {module}".format(module=__name__))
            self.regex, self.filetype, self.sort = self.check_maptype()
        self.mapdict = {"alldistmap": None,
                        "allcontactmap": None,
                        "distmap": None,
                        "maplot": None,
                        "scoremap": None}
        self.clashlist = []
        self.distlist = []
        self.contactlist = []
        self.flaglist = {}
        self.scorelist = []
        # self.maplot = None
        # self.distmap = None

    def create_map(self, protein, contactdef, flaglist=None, offset=0, sym=True,
                   path="", **kwargs):
        """

        Parameters
        ----------
        protein
        contactdef
        flaglist: dict
        offset: str
        sym
        path
        kwargs
        """
        raise NotImplementedError("Class %s doesn't implement create_map" %
                                  self.__class__.__name__)

    def update_map(self, resmap):
        """
        

        Parameters
        ----------
        resmap :
            return:

        Returns
        -------

        
        """
        raise NotImplementedError("Class %s doesn't implement update_map" %
                                  self.__class__.__name__)

    def check_maptype(self):
        """

        Returns
        -------

        """
        LOG.info(
            "Checking if file %s correspond to our definition of %s format",
            self.filepath,
            self.filetype)
        # Check if given type is supported
        # TODO: report this check into commands section
        if os.stat(self.filepath).st_size == 0:
            LOG.warning("File %s is empty !", self.filepath)
            return [
                self.types["empty"].get("regex"),
                self.filetype,
                self.types["empty"].get("score_field")
            ]
        with open(self.filepath) as infile:
            # Check first and second line of file
            for index, line in enumerate(infile):
                if self.filetype in self.types:
                    match = self.types[self.filetype].get("regex").match(line)
                else:
                    LOG.error("Format %s not supported. Please refer"
                              " to the documentation for supported files",
                              self.filetype)
                    match = None
                if match:
                    return [
                        self.types[self.filetype].get("regex"),
                        self.filetype,
                        self.types[self.filetype].get("score_field")
                    ]
                if index > 2:
                    LOG.warning("Given type do not correspond, checking if "
                                "the file corresponds to default format for "
                                "contact lists...")
                    for subformat in self.unknown:
                        if self.unknown.get(subformat)["regex"].match(line):
                            LOG.info("Default format type found") \
                                if subformat != "empty" else \
                                LOG.warning("The file seems to be empty ...")
                            return [
                                self.unknown[subformat].get("regex"),
                                self.filetype,
                                self.unknown[subformat].get("score_field")
                            ]
                    # Stop checking after second line
                    LOG.error("Can't read %s file.", self.filetype)
                    break
        LOG.error("Wrong format type given. Please refer to the "
                  "documentation to check if the given format is correct.")
        return [None] * 3

    def load(self, *args):
        super(MapFile, self).load(*args)

    def conioload(self, protein):
        """Conkit loader

        Use conkit io parser to load the entire file data

        Returns
        -------
        None
        """
        with open(self.filepath) as f_in:
            hierarchy = conio.read(f_in, self.filetype)
            if isinstance(hierarchy, ContactFile):
                conmap = hierarchy.top_map
                # TODO: uniformize both packages later
                # TODO: alignment aa sequence with the one assigned to the
                # conmap
                # TODO: check for unassigned contacts
                conmap.sequence = Sequence("sequence_id",
                                           protein.aa_sequence.sequence)
                conmap.assign_sequence_register()
                if self.sort:
                    conmap.sort(self.sort, reverse=True, inplace=True)

                for contact in conmap:
                    conkeys = tuple(sorted([contact.res1_seq,
                                            contact.res2_seq]))
                    self.contactlist.append(conkeys)
                    self.scorelist.append(contact.raw_score)
                    self.distlist.append(contact.distance_bound[1])
                # TODO: use ContactMap object instead of contactlist
                self.contactlist = [contact
                                    for idx, contact in
                                    enumerate(self.contactlist)]
                self.clashlist = []
                LOG.debug("New contact list (%d contacts)\n%s",
                          len(self.contactlist), self.contactlist)

            elif isinstance(hierarchy, SequenceFile):
                raise NotImplementedError("SequenceFile instance not yet "
                                          "supported")
            else:
                raise IOError("Instance file not recognized")

    def read(self, protein, contactdef=5.0, groupby_method="min",
             scsc=None):
        """


        Parameters
        ----------
        protein :
            param contactdef: (Default value = None)
        groupby_method :
            param scsc: (Default value = "min")
        contactdef :
            (Default value = 5.0)
        scsc :
            (Default value = None)

        Returns
        -------

        
        """
        # filetype flag should only be defined when we don't use conkit for
        # the reading process
        LOG.info("Reading %s file", self.filepath)
        # Both should fill self.lines attribute ?
        if self.conioflag:
            self.conioload(protein)
        else:
            # Read file with regex related to filetype
            self.load(protein)
            LOG.debug(self.lines)

        self.create_map(protein, contactdef,
                        groupby_method=groupby_method, scsc=scsc,
                        flaglist=self.flaglist,
                        sym=False if self.filetype == "metapsicovhb" else True,
                        path=self.filepath)


class ContactMapFile(MapFile):
    """
    Contact map file
    """

    def __init__(self, filepath, filetype, **kwargs):
        """

        :param filepath:
        :param filetype:
        :return:
        """
        super(ContactMapFile, self).__init__(filepath, filetype, **kwargs)

    def update_map(self, resmap):
        """
        

        Parameters
        ----------
        resmap :
            return:

        Returns
        -------

        
        """
        # TODO: swap dataframe factory here
        raise NotImplementedError

    def create_map(self, protein, *args, **kwargs):
        """
        Initialize and fill Res - Res maps based on contactlist

        Parameters
        ----------
        protein :
            
        args :
            
        kwargs :
            
        *args :
            
        **kwargs :
            

        Returns
        -------

        
        """
        offset = min(protein.index)  # Should be 1 or upper (human_idx)
        idxnames = "residuex" if self.filetype != "metapsicovhb" else "donor"
        colnames = "residuey" if self.filetype != "metapsicovhb" else "acceptor"
        contactmap = ResMap(protein.aa_sequence.sequence, mtype='contact',
                            flaglist=kwargs['flaglist'],
                            seqidx=protein.index, idxnames=idxnames,
                            colnames=colnames, sym=kwargs['sym'],
                            desc=self.filetype, path=kwargs.get("path"))
        # DataFrame containing ec scores
        scoremap = ResMap(protein.aa_sequence.sequence, mtype='score',
                          seqidx=protein.index, idxnames=idxnames,
                          colnames=colnames, sym=kwargs['sym'],
                          path=kwargs.get("path"),
                          desc=self.filetype) if self.sort else None
        distmap = ResMap(protein.aa_sequence.sequence, mtype='distance',
                         seqidx=protein.index, idxnames=idxnames,
                         path=kwargs.get("path"),
                         colnames=colnames, sym=kwargs['sym'],
                         desc=self.filetype) if self.filetype == "metapsicovhb" else None

        for idx, contact in enumerate(self.contactlist):
            contact_id = idx + 1
            resid1, resid2 = contact[0], contact[1]
            dist = self.distlist[idx] if self.distlist else None

            # Res id start from 0 in res-res map
            residx1 = contactmap.index[resid1 - offset]
            residx2 = contactmap.index[resid2 - offset]

            if (int(residx1.split("-")[0]) != resid1) or \
                    (resid2 != int(residx2.split("-")[0])):
                LOG.error("Wrong resid humanidx (%d, %d) in contact (%d) is "
                          "not the same in resmap (%s, %s)",
                          resid1, resid2, contact_id,
                          residx1, residx2)

            contactmap.set_value(residx1, residx2, True)
            if self.sort:
                scoremap.sort_list.append((resid1 - offset, resid2 - offset))
                scoremap.set_value(residx1, residx2, self.scorelist[idx])
            if distmap is not None:
                distmap.set_value(residx1, residx2, dist)
        LOG.debug("%s contact map:\n%s", self.filetype, contactmap)
        self.mapdict["maplot"] = contactmap
        LOG.debug("%s score map:\n%s", self.filetype, scoremap)
        self.mapdict["scoremap"] = scoremap
        if distmap is not None:
            self.mapdict["distmap"] = distmap

    def load(self, protein):
        """Load contact file"""
        super(ContactMapFile, self).load(protein)
        LOG.info("Loading contact file")
        aaseq = {}
        confields = ("res_donor", "res_acceptor") if \
            self.filetype == "metapsicovhb" else ('res1_nb', 'res2_nb')
        for contact in self.lines:

            # If contact defined
            if self.lines[contact].get(confields[0]) and \
                    self.lines[contact].get(confields[1]):
                conkeys = tuple(sorted([
                    int(self.lines[contact].get(confields[0])),
                    int(self.lines[contact].get(confields[1]))]))

                self.contactlist.append(conkeys)

                if self.sort:
                    self.scorelist.append(
                        float(self.lines[contact].get(self.sort)))

                if self.filetype == "contactlist":
                    self.flaglist[conkeys] = self.lines[contact].get("con_flag")

                if self.filetype == "metapsicovhb":
                    self.distlist.append(self.lines[contact].get("res_dist"))

                if self.filetype in ("evcoupling", "plmdca", "plm", "plmev"):
                    self.clashlist.append(next(
                        (el for el in (
                            self.lines[contact].get("ss_filter"),
                            self.lines[contact].get("high_cons_filter"),
                            self.lines[contact].get("cc_filter")) if el != "0"),
                        "0"))
                    if self.lines[contact].get("res1_name") and \
                            self.lines[contact].get("res2_name"):
                        aaseq[int(self.lines[contact].get("res1_nb"))] = \
                            self.lines[
                                contact].get("res1_name")
                        aaseq[int(self.lines[contact].get("res2_nb"))] = \
                            self.lines[
                                contact].get("res2_name")

        if aaseq:
            # Align amino acid sequence with sequence obtained from seq
            # file
            seq = ''.join([
                aaseq[key] if key in aaseq else '*'
                for key in range(1, max(aaseq) + 1)])
            # With gap penalty set to -1, we should only have an alignment
            # without gap since mismatch is the preferred way (with score set
            #  to 0)
            alignment = pairwise2.align.localxs(
                seq, protein.aa_sequence.sequence, -1, -1,
                one_alignment_only=True)[0]
            LOG.info(
                'Alignment of sequence in contact file ({0})'
                ' with reference ({1})\n{2}'.format(
                    self.filetype,
                    os.path.basename(protein.aa_sequence.fileName),
                    pairwise2.format_alignment(*alignment)))
            shift = re.match(r'^-*', alignment[1])
            shift = len(shift.group(0)) if shift else 0
            if shift:
                LOG.warning("Found a shift of %d residues in positions given"
                            " within contact list", shift)

                LOG.info("Update index in contact list and remove unassigned "
                         "contacts")
                LOG.debug("Old contact list\n%s", self.contactlist)
                self.contactlist = [
                    (contact[0] - shift, contact[1] - shift)
                    for contact in self.contactlist
                ]
                LOG.debug("New contact list\n%s", self.contactlist)
                LOG.info("Remove contacts outside sequence bonds")

        # Checking for unassigned contacts
        validx = range(1, len(protein.aa_sequence.sequence) + 1)
        unascon = [contactidx
                   for contactidx, contact in enumerate(self.contactlist)
                   if contact[0] not in validx or contact[1] not in validx]
        LOG.debug("Old contact list (%d contacts)\n%s",
                  len(self.contactlist), self.contactlist)
        self.contactlist = [contact
                            for idx, contact in enumerate(self.contactlist)
                            if idx not in unascon]
        LOG.debug("New contact list (%d contacts)\n%s",
                  len(self.contactlist), self.contactlist)
        self.clashlist = [clash for idx, clash in enumerate(self.clashlist)
                          if idx not in unascon] if self.clashlist else []
        self.distlist = [dist for idx, dist in enumerate(self.distlist)
                         if idx not in unascon] if self.distlist else []
        self.scorelist = [score for idx, score in enumerate(self.scorelist)
                          if idx not in unascon] if self.scorelist else []


class PDBFile(MapFile):
    """PDB file"""
    pdbreg = re.compile(r'^(?P<record>ATOM {2}|HETATM)(?P<serial>[\s\w]{5})'
                        r'\s(?P<name>[\s\w]{4})'
                        r'(?P<altLoc>[\s\w])'
                        r'(?P<resName>\w{3})\s(?P<chainID>\w)'
                        r'(?P<resSeq>[\s\w]{4})(?P<iCode>[\s\w])'
                        r'\s{3}(?P<x>[\s\d-]{4}\.\d{3})(?P<y>[\s\d-]{4}\.\d{3})'
                        r'(?P<z>[\s\d-]{4}\.\d{3})'
                        r'(?P<occupancy>[\s\d-]{3}\.\d{2})'
                        r'(?P<tempFactor>[\s\d-]{3}\.\d{2})'
                        r'\s{10}(?P<element>[\s\w]{2})'
                        r'(?P<charge>[\s\w]{2})')

    def __init__(self, *args, **kwargs):
        # TODO: use PDB object in aria
        # TODO: write dataframe in a separated file
        super(PDBFile, self).__init__(*args, regex=self.pdbreg, filetype="pdb",
                                      check_type=False, **kwargs)

    def create_map(self, protein, contactdef, groupby_method="min", scsc=None,
                   flaglist=None, sym=True, path=""):
        """
        

        Parameters
        ----------
        protein :
        contactdef:
        groupby_method :
            (Default value = "min")
        scsc: 
            (Default value = None)
        flaglist :
            (Default value = None)
        sym: 
            (Default value = True)
        path:
            (Default value = "")

        Returns
        -------

        
        """
        resmap = ResAtmMap(protein.aa_sequence.sequence, mtype='distance',
                           flaglist=flaglist, path=path,
                           seqidx=protein.index, desc=self.filetype)
        # noinspection PyTypeChecker
        resmap = self.update_map(resmap, sym=sym)
        # df.combine_first(d2)
        LOG.debug("pdb distance map:\n%s", resmap)
        self.mapdict["alldistmap"] = resmap
        self.mapdict["distmap"] = resmap.reduce(groupby=groupby_method)
        self.mapdict["allcontactmap"] = resmap.contact_map(
            contactdef=contactdef, scsc_min=scsc)
        self.mapdict["maplot"] = self.mapdict["allcontactmap"].reduce()

    def update_map(self, resmap, sym=True):
        """
        

        Parameters
        ----------
        resmap :
            param sym:
        sym :
            (Default value = True)

        Returns
        -------

        
        """
        # Map only on heavy atoms
        # TODO: check if same sequence in pdb file
        LOG.info("Updating distance map with pdb file")
        newmap = resmap.copy()
        heavylist = []
        error_list = set()
        for atom in self.lines:
            if resmap.heavy_reg.match(self.lines[atom]['name'].strip()):
                heavylist.append(atom)

        # For each heavy atom
        for x, atomx in enumerate(heavylist):
            for atomy in heavylist[x:]:
                # TODO: Check first residue number in pdb file
                indx = "%03d-%s" % (int(self.lines[atomx]['resSeq']),
                                    self.lines[atomx]['resName'].strip()), \
                       self.lines[atomx]['name'].strip()
                indy = "%03d-%s" % (int(self.lines[atomy]['resSeq']),
                                    self.lines[atomy]['resName'].strip()), \
                       self.lines[atomy]['name'].strip()
                coordx = (float(self.lines[atomx]['x']),
                          float(self.lines[atomx]['y']),
                          float(self.lines[atomx]['z']))
                coordy = (float(self.lines[atomy]['x']),
                          float(self.lines[atomy]['y']),
                          float(self.lines[atomy]['z']))

                dist = distance.euclidean(coordx, coordy)
                if indx[0] in list(resmap.index.get_level_values("residuex")) \
                        and indy[0] in list(
                    resmap.index.get_level_values("residuex")):
                    LOG.debug("Update distance value (%s, %s)", indx, indy)
                    newmap.at[indx, indy] = dist
                    if sym:
                        # If symmetric matrix
                        newmap.at[indy, indx] = dist
                elif indx[0] not in list(
                        resmap.index.get_level_values("residuex")):
                    error_list.add(indx[0])
                elif indy[0] not in list(
                        resmap.index.get_level_values("residuex")):
                    error_list.add(indy[0])
        if error_list:
            # Listing related humanidx in the initial df
            idxlist = list(resmap.index.get_level_values("residuex"))
            erridx = [idx.split("-")[0] for idx in list(error_list)]
            missidx = list(set([idx for idx in idxlist
                                if idx.split("-")[0] in erridx]))
            for idx in missidx:
                newmap.loc[idx] = None
                if sym:
                    newmap.loc[:][idx] = None
            LOG.error("Can't update pdb distance map for pos in pdb file "
                      "%s with %s", list(error_list), missidx)
        return newmap


class DistanceMapFile(MapFile):
    """Distance matrix file"""

    def __init__(self, filepath, filetype):
        super(MapFile).__init__(filepath, filetype)
        raise NotImplementedError

    def create_map(self, aa_seq, contactdef, **kwargs):
        """
        

        Parameters
        ----------
        aa_seq :
            param contactdef:
        kwargs :
            return:
        contactdef :
            
        **kwargs :
            

        Returns
        -------

        
        """
        pass

    # Native dist
    def update_map(self, resmap):
        """
        

        Parameters
        ----------
        resmap :
            return:

        Returns
        -------

        
        """
        pass
        # Construit map avec la liste de residus +  infos de distance du fichier
        # return DistanceMap


class MapFileListReader(object):
    """Reader class for Map files"""

    def __init__(self, cont_def=5.0):
        """
        Parameters
        ----------
        cont_def: float, optional
            Contact threshold used to filter native contacts (if reference
            structure given)
        """
        self.maps = []
        self.contactdef = cont_def

    def clear(self):
        """Initialize object from scratch"""
        # TODO: Init supprime bien les fichiers du cache ?
        self.__init__(self.contactdef)

    def _add_maps(self, maps, maptypes=None):
        """
        Add map file instance to the MapFile reader

        Parameters
        ----------
        maps : :obj: `list` of :obj: `str`
            List of Map file paths
        maptypes : :obj: `list` of :obj: `str`, optional
            (Default value = None)

        Returns
        -------

        
        """
        maps = [maps] if type(maps) != list else maps
        maptypes = [maptypes] if type(maptypes) != list else maptypes
        if not maptypes or len(maps) != len(maptypes):
            maptypes = [os.path.splitext(_)[1][1:] for _ in maps]
        # LOG.info("Analyzing input file(s) %s %s", maps,
        #          maptypes)
        for i, filepath in enumerate(maps):
            if os.path.exists(filepath):
                # TODO: check_type functionstr
                LOG.debug("Adding %s file to watchlist", maptypes[i])
                if maptypes[i].lower() == "pdb" or (
                        maptypes[i].lower() == "distfile" and
                        os.path.splitext(filepath)[-1][1:] == "pdb"):
                    self.maps.append(PDBFile(filepath))
                # TODO: find another solution in the future since
                # conio.CONTACT_FILE_PARSERS might be deprecated
                elif maptypes[i].lower() in conio.CONTACT_FILE_PARSERS.keys() \
                        or maptypes[i].lower() in MapFile.conkit_alias:
                    self.maps.append(ContactMapFile(filepath,
                                                    maptypes[i],
                                                    conkit_reader=True,
                                                    sort="raw_score"))
                else:
                    self.maps.append(ContactMapFile(filepath,
                                                    maptypes[i],
                                                    conkit_reader=False))
                    if not self.maps[-1].regex:
                        LOG.warning("Can't read %s", filepath)
                        self.maps.pop()
                # else:
                #     self.maps.append(DistanceMapFile(filepath,
                #                                          maptypes[i]))
                # TODO: DistanceMapFile condition

    def read(self, maps, maptypes=None, protein=None, scsc=None, **kwargs):
        """
        Read contact map files. The generated maps will be accessible in
        `self.maps`

        Parameters
        ----------
        maps : :obj: `list` of :obj: `str`
            List of Map file paths
        maptypes : :obj: `list` of :obj: `str`, optional
            List of Map file types (Default value = None)
        protein : ariaec.protein.Protein object or None, optional
            (Default value = None)
        scsc : dict, optional
            Optional dictionary used for Atom Map reduction by selecting only
            one atom in the side chain. (Default value = None)
        kwargs :
            Optional arguments used for MapFile readers
        **kwargs :
            

        Returns
        -------

        
        """
        self.clear()
        self._add_maps(maps, maptypes=maptypes)
        for fo in self.maps:
            fo.read(protein=protein, contactdef=self.contactdef,
                    scsc=scsc, **kwargs)
