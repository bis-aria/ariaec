# coding=utf-8
"""
                                Settings section
"""
from __future__ import absolute_import, division, print_function

import os
import logging
import pickle
import collections
# noinspection PyCompatibility
from ConfigParser import ConfigParser
import pkg_resources as pkgr

from .common import format_dict

LOG = logging.getLogger(__name__)


class Setting(object):
    """Main setting object with args and config section"""

    # TODO: merge config and args
    def __init__(self):
        self.config = collections.defaultdict()
        self.args = collections.defaultdict()

    def __getitem__(self, item):
        if item in self.args:
            return self.args[item]
        elif item in self.config:
            return self.config[item]
        else:
            raise KeyError(item)

    def get(self, key, default=None):
        return self.config[key] if key in self.config else self.args[key] \
            if key in self.args else default

    def __repr__(self):
        return "Setting object\n    config: %s\n    args  : %s" % (self.config,
                                                                   self.args)


class Settings(object):
    """Group settings with each section corresponding to a Setting object"""

    def __init__(self, sections):
        self._sections = set(sections)
        self.configfile = ''
        for section in self._sections:
            setattr(self, section, Setting())
        self.load_config(pkg=True)

    def load_config(self, configpath="conf/config.ini", pkg=False):
        """
        Use ConfigParser module to load config sections

        Parameters
        ----------
        pkg :
            file is inside the package (Default value = False)
        configpath :
            return:

        Returns
        -------

        
        """
        if os.path.exists(configpath):
            self.configfile = configpath
        elif not pkg:
            LOG.error("Configuration file not found (%s)", configpath)
            return None
        # config = SafeConfigParser(allow_no_value=True)
        config = ConfigParser(allow_no_value=True)
        if pkg:
            with pkgr.resource_stream(__name__, configpath) as conf:
                config.readfp(conf)
        else:
            config.read(configpath)
        LOG.debug(config)
        for section in config.sections():
            if hasattr(self, section):
                LOG.debug("Formatting items of %s section", section)
                tmp = format_dict(dict(config.items(section)))
                getattr(self, section).config.update(tmp)
                LOG.debug("%s config updated", section)
                LOG.debug("%s.%s : %s", self.__class__.__name__, section,
                          getattr(self, section))
            else:
                LOG.warning("Unknow config section %s", section)

    def write_config(self, filename):
        """
        Write config of all sections into another file

        Parameters
        ----------
        filename :
            

        Returns
        -------

        
        """
        LOG.info("Writing .ini file (%s)", filename)
        config = ConfigParser(allow_no_value=True)
        iniout = open(filename, mode="w")
        for section in self._sections:
            config.add_section(section)
            if hasattr(self, section):
                for opt in getattr(self, section).config:
                    config.set(section, str(opt),
                               str(getattr(self, section).config.get(opt)))
        config.write(iniout)

    def __repr__(self):
        return "<Settings object>\n    sections: %s" % self._sections


# TODO: AriaEcSettings should be an astract class. Sections are actually
# duplicated with AriaEcCommands
class AriaEcSettings(Settings):
    """
    Settings object for ariaec
    @DynamicAttrs
    """
    # ss_dist = os.path.join(os.path.dirname(os.path.realpath(__file__)),
    #                        'conf/ss_dist.txt')
    # TODO: move these constant variable in objects which can read these file !!
    # TODO: Baseclass inspired from this class and ariabase class. All
    # objects in this package should extend the base object
    DEFAULT_CONFIG = "conf/config.ini"
    ARIAPROJ_TEMPLATE = 'templates/aria_project_v2.3.0.xml'
    SS_DIST = 'data/ss_dist.txt'
    SCSC_MIN = 'data/scsc_min.p'
    INTERLOWERBOUNDS = 'data/pdbdists/lowerbounds.inter.p'
    INTERTARGET = 'data/pdbdists/targetdists.inter.p'
    INTERUPPERBOUNDS = 'data/pdbdists/upperbounds.inter.p'
    INTRALOWERBOUNDS = 'data/pdbdists/lowerbounds.intra.p'
    INTRATARGET = 'data/pdbdists/targetdists.intra.p'
    INTRAUPPERBOUNDS = 'data/pdbdists/upperbounds.intra.p'
    # TODO: If aria installed in site-packages, use topallhdg file from aria !
    TOPO = 'data/topallhdg5.3.pro'
    # TODO: Should define sections in one object, actually we have to define
    # sections in settings and also command objects ...
    SECTIONS = ("main", "setup", "maplot", "bbconv", "contactdef",
                "pdbqual", "pdbdist", "analysis", "tbl2xml", "pdbstat",
                "iniconv")

    # TODO: options in main section should be accessible from all sections ! DO
    #  not use this a other section as it is done actually

    def __init__(self, name):
        """
        Initiate settings with name related to a command
        :param name:
        """
        super(AriaEcSettings, self).__init__(self.SECTIONS)
        self._infra = {}
        self._scsc_min = None
        self._ssdist = None
        self._template = None
        self.outdir = os.getcwd()
        self.name = name

    def _up_infra(self):
        """ """
        for dirpath in self.infra:
            LOG.debug("set %s dir: %s", dirpath, os.path.join(self.outdir,
                                                              dirpath))
            self._infra[dirpath] = os.path.join(self.outdir, dirpath)

    @property
    def infra(self):
        """
        Infrastructure for a specific command
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        if self.name == "setup" and not self._infra:
            self._infra = {"xml": "", "tbl": "", "etc": ""}
            self._up_infra()
        elif self.name == "maplot" and not self._infra:
            self._infra = {"graphics": ""}
            self._up_infra()
        return self._infra

    @infra.setter
    def infra(self, root):
        """
        Change infra root

        Parameters
        ----------
        root :
            return:

        Returns
        -------

        
        """
        self.outdir = os.path.abspath(root)
        if self.infra:
            self._up_infra()

    @property
    def ssdist(self):
        """
        Get distance file for secondary structures in the package or in config
        file
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        if not self._ssdist:
            if self["ss_dist_file"] and \
                    os.path.exists(self["ss_dist_file"]):
                self._ssdist = self["ss_dist_file"]
            else:
                self._ssdist = pkgr.resource_filename(__name__, self.SS_DIST)
        return self._ssdist

    @property
    def template(self):
        """
        Get template files in config file or in the package
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        if not self._template:
            templatepath = "templates/aria_project_v%s.xml" % \
                           str(self["ariaproject_template"])
            if os.path.exists(pkgr.resource_filename(__name__, templatepath)):
                self._template = pkgr.resource_filename(__name__, templatepath)
            else:
                LOG.error("Template version for aria project (%s) is not "
                          "supported", self["ariaproject_template"])
                self._template = pkgr.resource_filename(__name__,
                                                        self.ARIAPROJ_TEMPLATE)
        return self._template

    @property
    def scsc_min(self):
        """
        Get contact index for side chains in package or config file
        :return:

        Parameters
        ----------

        Returns
        -------

        
        """
        if not self._scsc_min:
            try:
                # Read scsc_min_file given in config.ini
                with open(self["scsc_min_file"]) as scsc:
                    scsc_min = pickle.load(scsc)
            except (IOError, KeyError, TypeError):
                # If file can't be open or given key is invalid, load default
                # package file
                with pkgr.resource_stream(__name__, self.SCSC_MIN) as scsc:
                    scsc_min = pickle.load(scsc)
            scsc_min = dict(
                (aa1, dict((aa2, tuple(atm_pair.split(', ')))
                           for aa2, atm_pair in atm_pairs.items()))
                for aa1, atm_pairs in scsc_min.items())
            self._scsc_min = scsc_min
            return scsc_min
        else:
            return self._scsc_min

    def make_infra(self):
        """Generate infrastructure"""
        LOG.info("Making output directories")
        for direct in self.infra:
            LOG.debug("Create %s directory", self.infra[direct])
            if not os.path.exists(self.infra[direct]):
                os.makedirs(os.path.abspath(self.infra[direct]))

    def load_config(self, configpath=None, **kwargs):
        """
        

        Parameters
        ----------
        configpath :
            param kwargs:
        **kwargs :
            

        Returns
        -------

        
        """
        if configpath:
            super(AriaEcSettings, self).load_config(configpath, **kwargs)
        else:
            super(AriaEcSettings, self).load_config(
                self.DEFAULT_CONFIG, pkg=True)

    def __getitem__(self, item):
        for section in self._sections:
            if item in getattr(self, section).args:
                return getattr(self, section).args[item]
            elif item in getattr(self, section).config:
                return getattr(self, section).config[item]
