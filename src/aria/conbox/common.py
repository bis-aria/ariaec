# coding=utf-8
"""
                            Basic tools ariaec
"""
from __future__ import absolute_import, division, print_function
from abc import ABCMeta, abstractmethod

import io
import logging
import logging.config
import os
import json
import re
import ast
import sys
import shutil
import six
import numpy as np
import multiprocessing
import pkg_resources as pkgr
import matplotlib.artist as art
import tempfile
from Bio.PDB import Select

LOG = logging.getLogger(__name__)


class TqdmToLogger(io.StringIO):
    """
        Output stream for TQDM which will output to logger module instead of
        the StdOut.
        Adapted from ddofborg response (https://stackoverflow.com/a/41224909)
    """
    logger = None
    level = None
    buf = ''

    def __init__(self, logger, level=None):
        super(TqdmToLogger, self).__init__()
        self.logger = logger
        self.level = level or logging.INFO

    def write(self, buf):
        """
        
        Parameters
        ----------
        buf

        Returns
        -------

        """
        self.buf = buf.strip('\r\n\t ')

    def flush(self):
        """
        
        Returns
        -------

        """
        self.logger.log(self.level, self.buf)


# Code below adapated from an answer of klaus se on stackoverflow
# (http://stackoverflow.com/a/16071616)
def worker(f, task_queue, done_queue):
    """
    Worker process. Query the task_queue if there is an item available. Each 
    item is a 2-tuple (jobid, data) or (None, None) used to submit a job in 
    done_queue

    Parameters
    ----------
    f: function object
    task_queue: multiprocessing.Queue object
        Queue of task to submit
    done_queue: multiprocessing.Queue object
        Queue where all the task are running
    Returns
    -------
    None

    """
    while True:
        # Remove and return an item from the queue. Block if necessary until an
        # item is available.
        i, x = task_queue.get()
        if i is None:
            break
        done_queue.put((i, f(x)))


def parmap(f, X, nprocs=multiprocessing.cpu_count()):
    """
    Map in parallel a function on elements of X using multiprocessing module

    Parameters
    ----------
    f: function object
        Function to launch with multiprocessing. Accept only one arguments which
        is an element of X
    X: list
        Input data where each element of X is submitted with f function
    nprocs: int
        Number of parallel processs

    Returns
    -------

    """
    # Create queues
    task_queue = multiprocessing.Queue(1)
    done_queue = multiprocessing.Queue()

    # Initialize list of processes (1 for each cpu according to nprocs)
    proc = [
        multiprocessing.Process(target=worker, args=(f, task_queue, done_queue))
        for _ in range(nprocs)]

    # Start worker processes
    for p in proc:
        p.daemon = True
        p.start()

    # Sent as many task as the number of elements in X by putting a list with
    # (jobid, data)
    sent = [task_queue.put((i, x)) for i, x in enumerate(X)]
    # (None, None) as the last item indicates to workers that it has reached the
    #  end of the sequence of items for each process.
    [task_queue.put((None, None)) for _ in range(nprocs)]
    # Get all the results
    res = [done_queue.get() for _ in range(len(sent))]

    # Wait all the process to finish
    [p.join() for p in proc]

    return [x for i, x in sorted(res)]


def addtup(tup, inc=1):
    """
    Increment all values by 1 in a tuple

    Parameters
    ----------
    tup :
        param inc:
    inc :
        (Default value = 1)

    Returns
    -------

    """
    return tuple((val + inc for val in tup))


def titleprint(outfile, progname='', desc=''):
    """
    Init log file

    Parameters
    ----------
    outfile :
        param progname: docstring
    desc :
        return: (Default value = '')
    progname :
        (Default value = '')

    Returns
    -------

    """
    out = '''
================================================================================
{progname}
                                {desc}
================================================================================
'''.format(progname=progname, desc=desc.capitalize())
    outfile.write(out[1:])


def get_filename(path):
    """
    Search filename in the given path

    Parameters
    ----------
    path :
        return:

    Returns
    -------

    """
    return "_".join(path.split("/")[-1].split(".")[:-1])


def reg_load(regex, filepath, sort=None):
    """

    Parameters
    ----------
    regex :
        param filepath:
    sort :
        return: (Default value = None)
    filepath :

    Returns
    -------

    """

    lines_dict = {}

    with open(filepath) as infile:
        for index, line in enumerate(infile):
            match = regex.match(line)
            if match:
                lines_dict[index] = match.groupdict()

    if sort:
        lines_dict = sort_2dict(lines_dict, sort)

    return lines_dict


def sort_2dict(unsort_dict, key, reverse=True):
    """
    Sort 2d dict by key

    Parameters
    ----------
    reverse :
        param key: (Default value = True)
    unsort_dict :
        return: sorted dict
    key :

    Returns
    -------

    """
    sorted_index = sorted(unsort_dict, key=lambda x: float(unsort_dict[x][key]),
                          reverse=reverse)
    sorted_dict = {}

    for rank, ind in enumerate(sorted_index):
        sorted_dict[rank + 1] = unsort_dict[ind]

    return sorted_dict


def cart_dist(vectx, vecty):
    """
    Evaluate cartesian distance beetween 2 points x, vecty

    Parameters
    ----------
    vectx :
        numpy array (len = n dimensions)
    vecty :
        numpy array (len = n dimensions)

    Returns
    -------

    """
    return np.sqrt(sum(np.power(vectx - vecty, 2)))


def format_str(string):
    """
    Convert str in bool, float, int or str

    Parameters
    ----------
    string :
        return:

    Returns
    -------

    """
    if re.search(r"^\s*(true)\s*$", string, re.I):
        return True
    if re.search(r"^\s*(false)\s*$", string, re.I):
        return False
    if re.search(r"^\s*\d+\s*$", string):
        return int(string)
    if re.search(r"^[\s\d-]+\.\d+\s*$", string):
        return float(string)
    if re.search(r'^".+"$', string):
        # remove " characters
        return string[1:-1]
    if "," in string:
        return string.split(',')
    if "+" in string:
        return string.split('+')
    if "/" in string and os.path.exists(string):
        return os.path.abspath(string)
    if re.search(r"[/\w]+", string):
        return string
    if string:
        try:
            ev_str = ast.literal_eval(string)
        except ValueError:
            LOG.error("Don't understand given string %s. Please check "
                      "format.", string)
            return None
        except SyntaxError:
            LOG.error("Given string %s is not a valid expression", string)
            return None
        return ev_str
    return None


def format_dict(indict):
    """

    Parameters
    ----------
    indict :
        return:

    Returns
    -------

    """
    for key in indict:
        if isinstance(indict[key], str):
            indict[key] = format_str(indict[key])
    return indict


def ppdict(indict, indent=2):
    """

    Parameters
    ----------
    indict :
        param indent:
    indent :
        (Default value = 2)

    Returns
    -------

    """
    return json.dumps(indict, indent=indent)


def tickmin(pandata, ntick=None, shift=0, offset=5):
    """
    Minimise number of ticks labels for matplotlib or seaborn plot

    Parameters
    ----------
    offset :
        param shift: (Default value = 5)
    pandata :
        pandas dataframe
    ntick :
        number of ticks wanted per axes (Default value = None)
    shift :
        (Default value = 0)

    Returns
    -------

    """
    yticks = [_ + shift for _ in range(0, len(pandata.index))]
    xticks = [_ + shift for _ in range(0, len(pandata.columns))]
    if ntick:
        keptticks = yticks[::(len(pandata.index) // int(ntick))]
        yticks = [_ if _ in keptticks else '' for _ in yticks]
        # If shift != 0, need to initialize first value of ticks

        keptticks = xticks[::(len(xticks) // int(ntick))]
        xticks = [_ if _ in keptticks else '' for _ in xticks]
    else:
        keptticks = [ytick for i, ytick in enumerate(yticks)
                     if (i + shift) % offset == 0]
        # keptticks = yticks[::offset - shift]
        yticks = [_ if _ in keptticks else '' for _ in yticks]
        # If shift != 0, need to initialize first value of ticks

        keptticks = [xtick for i, xtick in enumerate(xticks)
                     if (i + shift) % offset == 0]
        # keptticks = xticks[::offset - shift]
        xticks = [_ if _ in keptticks else '' for _ in xticks]
    return xticks, yticks


def tickrot(axes, figure, rotype='horizontal', xaxis=True, yaxis=True):
    """
    Matplot rotation of ticks labels

    Parameters
    ----------
    axes :
        param figure:
    rotype :
        param xaxis: (Default value = 'horizontal')
    yaxis :
        return: (Default value = True)
    figure :

    xaxis :
        (Default value = True)

    Returns
    -------

    """
    if yaxis:
        art.setp(axes.get_yticklabels(), rotation=rotype)
    if xaxis:
        art.setp(axes.get_xticklabels(), rotation=rotype)
    figure.canvas.draw()


class AbcLogFilter(logging.Filter):

    abc_words = ("abc_funcName", "abc_lineno")

    def filter(self, record):
        """Set default values for abc attributes according to the
        original attribute in LogRecord object"""
        reg = re.compile(r"(?P<prefix>[A-Za-z0-9]*(?=_))?_?(?P<name>\w+)")

        for abc_word in self.abc_words:
            if not hasattr(record, abc_word):
                prefix, name = [reg.match(abc_word).groupdict()[_]
                                for _ in ('prefix', 'name')]
                setattr(record, abc_word, getattr(record, name))

        return True


# TODO: Add another level when we use verbose options instead of displaying
#  debug messages
class CustomLogging(object):
    """
    Customized python logging config
    """
    # default_file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
    #                             "conf/logging.json")
    default_file = "conf/logging.json"

    def __init__(self, level=logging.INFO, desc=None, welcome=True,
                 decorator=True):
        """

        Parameters
        ----------
        level : int
            logging level
        desc : str
            logging description used as welcome message
        """
        # TODO: detect path log filenames and makedirs if not exists
        logging.basicConfig(level=level)
        self.msg = desc.strip() if desc else ""
        self.config = self.load_config()
        self.update_log()
        if welcome:
            self.welcome(decorator=decorator)

    def update_msg(self, desc):
        """

        Parameters
        ----------
        desc :
            return:

        Returns
        -------

        """
        if isinstance(self.msg, list):
            self.msg += desc
            self.msg = " - ".join(self.msg)
        elif isinstance(self.msg, str):
            self.msg = " - ".join((self.msg, desc.capitalize()))

    def update_log(self):
        """
        Update logging config with config attribute
        """
        logging.config.dictConfig(self.config)

    def load_config(self, abc=False):
        """
        Build default configuration from default json file

        Returns
        -------
        config: dict
            default configuration for logging
        """
        # with open(self.default_file, 'rt') as f:
        conf = pkgr.resource_stream(__name__, self.default_file).read().decode()
        config = json.loads(conf)
        for handler in config["handlers"]:
            if config["handlers"][handler].get("filename"):
                path_elts = os.path.split(config["handlers"][handler]["filename"])
                config["handlers"][handler]["filename"] = "".join(
                    path_elts[:-1]) + "/" + os.path.split(tempfile.mktemp())[1] + "_" + \
                    path_elts[-1]

        return config

    def up_outfiles(self, outfile):
        base = os.path.splitext(outfile)[0]
        for hand in self.config["handlers"]:
            if "filename" in self.config["handlers"][hand]:
                oldpath = self.config["handlers"][hand]["filename"]
                old_ext = os.path.splitext(oldpath)[1]
                newpath = os.path.abspath(base + old_ext)
                self.config["handlers"][hand]["filename"] = newpath
                shutil.copy2(oldpath, newpath)
        self.update_log()

    def set_outdir(self, outdir, logdir=True):
        """
        Create log directory and change log files location

        Parameters
        ----------
        outdir :
            path output directory

        Returns
        -------

        """
        outdir = os.path.join(outdir,
                              "log") if "log" not in outdir and logdir else outdir
        if os.path.exists(os.path.abspath(outdir)):
            # Avoid overwriting files with w mode after copy2 call
            shutil.rmtree(os.path.abspath(outdir))
        os.makedirs(outdir)
        if outdir and "handlers" in self.config:
            for hand in self.config["handlers"]:
                if "filename" in self.config["handlers"][hand]:
                    oldpath = self.config["handlers"][hand]["filename"]
                    newpath = os.path.abspath(os.path.join(
                        outdir, os.path.basename(
                            self.config["handlers"][hand]["filename"])))
                    self.config["handlers"][hand]["filename"] = newpath
                    shutil.copy2(oldpath, newpath)
            self.update_log()

    def welcome(self, decorator=True):
        """

        Returns
        -------

        """
        desc = '''\
================================================================================

{:^80}

================================================================================
'''.format(self.msg) if decorator else self.msg
        for hand in self.config.get("handlers"):
            if "filename" in self.config["handlers"][hand]:
                with open(self.config["handlers"][hand]["filename"],
                          'w') as outfile:
                    outfile.write(desc)
            elif hand == "console":
                print(desc)


class Capturing(list):
    """Capture output"""

    def __enter__(self):
        """

        Returns
        -------

        """
        # Stock default stdout and redirect current stdout to this class
        self._stdout = sys.stdout
        self._stderr = sys.stderr
        # All print calls are saved into self._stringio
        self._stringio = six.moves.StringIO()
        sys.stdout = self._stringio
        sys.stderr = self._stringio
        return self

    def __exit__(self, *args):
        """

        Parameters
        ----------
        args

        Returns
        -------

        """
        self.extend("\n".join(self._stringio.getvalue().splitlines()))
        self._stringio.truncate(0)
        del self._stringio  # free up some memory
        sys.stdout = self._stdout
        sys.stderr = self._stderr


class RedirectStdStreams(object):
    """Redirect standard ouput and errors"""

    def __init__(self, stdout=None, stderr=None):
        self._stdout = stdout or sys.stdout
        self._stderr = stderr or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush()
        self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush()
        self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr


class CommandProtocol(object):
    """Abstract class for subcommand protocol"""
    __metaclass__ = ABCMeta

    def __init__(self, settings):
        self.settings = settings

    @abstractmethod
    def run(self):
        """
        Main method to launch protocol

        Parameters
        ----------

        Returns
        -------

        """
        raise NotImplementedError


class NotDisordered(Select):
    """Define an atom as disordered or not in pdb selection"""

    def accept_atom(self, atom):
        """
        Accept or not the atom if it does not correspond to an alternative
        location

        Parameters
        ----------
        atom:

        Returns
        -------

        """
        return not atom.is_disordered() or atom.get_altloc() == 'A'


if __name__ == "__main__":
    # Test Logger
    CustomLogging().set_outdir("../examples/out")
    LOG = logging.getLogger("TEST")
    LOG.info(dir(LOG))
    LOG.info("Log test")
