# coding=utf-8
"""
Created on 9/5/16
@author: fallain

Derived from qual.py script by Dr. Benjamin Bardiaux
"""
import logging
import os
import shutil

from ..core.legacy.QualityChecks.QualityChecks import runChecks, FILENAME_REPORT
from .common import CommandProtocol

LOG = logging.getLogger(__name__)


class AriaEcPdbqual(CommandProtocol):
    """Quality pdb tool"""

    def __init__(self, *args, **kwargs):
        super(AriaEcPdbqual, self).__init__(*args, **kwargs)

    def run(self):
        """:return:"""
        LOG.info("Starting quality runs with %s file(s)", self.settings.pdbqual["infiles"])
        LOG.info("Copying file(s) to output directory %s", self.settings.pdbqual["output_directory"])
        for infile in self.settings.pdbqual["infiles"]:
            if os.path.abspath(infile) != os.path.abspath(self.settings.pdbqual["output_directory"]):
                shutil.copy(infile, self.settings.pdbqual["output_directory"])
        runChecks(
            workingDirectory=self.settings.pdbqual["output_directory"],
            trashDirectory=self.settings.pdbqual["trash_directory"],
            procheckExe=self.settings.setup["procheck_executable"]
            if self.settings.setup["procheck_executable"] else '',
            procheckOnOff=1 if self.settings.setup["procheck_enabled"] in ("yes", True, "True") else 0,
            whatIfExe=self.settings.setup["whatif_executable"]
            if self.settings.setup["whatif_executable"] else '',
            whatifOnOff=1 if self.settings.setup["whatif_enabled"] in ("yes", True, "True") else 0,
            clashlistExe=self.settings.setup["clashlist_executable"]
            if self.settings.setup["clashlist_executable"] else '',
            clashlistOnOff=1 if self.settings.setup["clashlist_enabled"] in ("yes", True, "True") else 0,
            prosaExe=self.settings.setup["prosa_executable"]
            if self.settings.setup["prosa_executable"] else "",
            prosaOnOff=1 if self.settings.setup["prosa_enabled"] in ("yes", True, "True") else 0,
            cshExe=self.settings.pdbqual["csh_executable"],
            howManyPdb=len(self.settings.pdbqual["infiles"]),
            verbose=1,
            fileList=self.settings.pdbqual["infiles"],
            skipPrefix=self.settings.pdbqual["skip_prefix"]
        )
        LOG.info("%s generated",
                 os.path.join(self.settings.pdbqual["output_directory"],
                              FILENAME_REPORT))
        LOG.info("Removing infile(s) in output directory %s", self.settings.pdbqual["output_directory"])
        # [os.remove(
        #     os.path.join(self.settings.pdbqual["output_directory"],
        #                  os.path.basename(infile)))
        #  for infile in self.settings.pdbqual["infiles"]
        #  if os.path.exists(os.path.join(
        #     self.settings.pdbqual["output_directory"],
        #     os.path.basename(infile)))
        # ]
