# coding=utf-8
"""
                            Input/Output aria_ec scripts
"""
from __future__ import absolute_import, division, print_function

import sys
import json
import logging
from .common import get_filename
from .reader import MapFileListReader
from .protmap import MapFilter
from .protein import Protein

LOG = logging.getLogger(__name__)


class AriaEcContactMap(object):
    """Contact maplot class"""

    def __init__(self, settings):
        # TODO: check_type settings (AriaEcSettings)
        self.settings = settings
        self.protein = Protein(settings)
        self.file_reader = MapFileListReader(
            cont_def=settings.contactdef.config)
        self.filter = MapFilter(settings.setup.config,
                                nofilter=settings.maplot.args.get("no_filter"))
        self.protname = ''
        self.allresmap = {}
        self.refmap = None
        self.refname = None
        self.reftype = ''

    def run(self):
        """Contact map analysis command"""
        # Check input
        LOG.debug("Settings:\n" + json.dumps(self.settings.maplot.config,
                                             indent=4))
        LOG.debug("Args:\n" + json.dumps(self.settings.maplot.args,
                                         indent=4))
        if not self.settings.maplot.args.get("onlyreport", False):
            self.settings.make_infra()
        # ----------------------------- Input -------------------------------- #
        self.protname = get_filename(self.settings.maplot.args.get("seq", None))
        # Load Sequence file
        self.protein.set_aa_sequence(self.settings.maplot.args.get("seq", None))
        # Load secondary structure prediction file
        self.protein.set_sec_struct(self.settings.maplot.args.get("sspred",
                                                                  None),
                                    ssdist_filename=self.settings.ssdist,
                                    ssidx=self.settings.maplot.args.get(
                                        "ssidx", False))
        # ---------------------------- Processing ---------------------------- #

        # Reading step
        # ------------
        self.file_reader.read(self.settings.maplot.args.get("infiles"),
                              maptypes=self.settings.maplot.args.get("contact_types"),
                              protein=self.protein,
                              groupby_method=self.settings.setup.config[
                                  'groupby_method'],
                              scsc=self.settings.scsc_min)

        for idx, fo in enumerate(self.file_reader.maps):
            # fo need a maplot in order to wite XML dist restraints
            # TODO: filter pour toutes les map de mapdict !! (fonction remove
            #  s'applique sur l'humanidx contenant les residus)
            if idx == 0:
                LOG.info("%s map set as reference", fo.filetype.capitalize())
                self.refmap = fo.mapdict
                self.reftype = fo.filetype
                self.refname = fo.filename if type(self.protname) != list \
                    else self.protname[idx]

            # Filtering step
            # --------------
            if (self.settings.maplot.args.get("filter") and idx != 0) or \
                    (self.settings.maplot.args.get("filter")
                     and len(self.file_reader.maps) == 1):
                # Filtering all maplot except the reference map
                LOG.info("Filtering %s map", fo.filetype)
                self.filter(fo.mapdict, fo.filetype, fo.contactlist,
                            self.protein, clashlist=fo.clashlist,
                            outprefix=self.protname[idx] if type(
                                self.protname) == list else self.protname,
                            outdir=self.settings.outdir)
            # else:
            #     Use only position filter
            #     self.filter(fo.mapdict, fo.filetype, fo.contactlist,
            #                 self.protein, clashlist=fo.clashlist,
            #                 protname=self.protname,
            #                 outdir=self.settings.outdir, mapfilters="pos")

            # Save the loaded map into allresmap dict
            self.allresmap[(fo.filename if type(self.protname) != list else
                            self.protname[idx], fo.filetype,
                            fo.filepath)] = fo.mapdict

        try:
            refmap = self.refmap["maplot"]
        except TypeError:
            LOG.error("First contact map should be a valid file")
            sys.exit(1)

        # nb_c = int(len(self.protein.aa_sequence.sequence) *
        #            self.settings.setup.config.get("n_factor"))
        plotparams = {k: self.settings.maplot.config.get(k, None)
                      for k in ('size_fig', 'plot_ext', 'plot_dpi')}
        outdir = self.settings.outdir

        # Reference contact map plot
        if self.settings.maplot.config.get("save_fig") and not \
                self.settings.maplot.args.get("onlyreport", False):
            refmap.saveplot(outdir=outdir,
                            outprefix=self.protname[0] if type(
                                self.protname) == list else self.protname,
                            **plotparams)

        # Merge maps if asked by the user
        if self.settings.maplot.args.get("merge", None):
            # Combine map with other maps
            mergelist = self.settings.maplot.args.get("merge")
            # LOG.info("Merging contact maps (%s)" % ', '.join(mergelist))
            for mergetype in mergelist:
                if mergetype in zip(self.allresmap.keys())[0]:
                    mergemaps = self.allresmap.pop(mergetype)
                    mergecontactmap = mergemaps.get("maplot")
                    for mapname, mapt in self.allresmap.keys():
                        if mapt != self.reftype:
                            # TODO: DOESN'T WORK !!!!
                            LOG.info("Merging %s with %s map",
                                     mergetype, mapt)
                            up_map = self.allresmap[mapt]["maplot"]
                            up_map[:] = up_map[:] + mergecontactmap[:]
                            mergekey = "%s_%s" % (mapt, mergetype)
                            self.allresmap[mergekey] = {}
                            self.allresmap[mergekey]["maplot"] = up_map
            raise NotImplementedError

        # ------------------------------ Output ------------------------------ #

        for mapname, mapt, mapath in self.allresmap.keys():
            prefix = self.settings.maplot.args.get("prefixname") if self.settings.maplot.args.get("prefixname") else ""

            if mapname == self.refname:
                if not self.settings.maplot.args.get("onlyreport", False):
                    refmap.write_contacts(mapname, prefix=prefix,
                                          outdir=outdir,
                                          scoremap=self.refmap.get("scoremap",
                                                                   None))
                continue

            prefix = "%s_%svs%s" % (self.protname, mapt, self.reftype) if \
                self.settings.maplot.args.get("prefix") and not \
                self.settings.maplot.args.get("prefixname") else \
                self.settings.maplot.args.get("prefixname") if \
                self.settings.maplot.args.get("prefixname") else ""
            scoremap = self.allresmap[(mapname, mapt, mapath)].get(
                'scoremap', None)
            # if self.allresmap[mapt].get("maplot") is not None and \
            #         self.allresmap[mapt].get("scoremap") is not None:
            #     Get top contact map/list
            #     cmpmap = self.allresmap[mapt]["maplot"].topmap(
            #         self.allresmap[mapt]["scoremap"], nb_c)
            #     cmplist = self.allresmap[mapt]['scoremap'].sortedset(
            #         human_idx=True)[:nb_c]
            # elif self.allresmap[mapt].get("maplot") is not None:
            #     If no score given, use all contact list
            cmpmap = self.allresmap[(mapname, mapt, mapath)]["maplot"]
            cmplist = self.allresmap[(mapname, mapt, mapath)][
                'maplot'].contact_list(human_idx=True)
            # else:
            #     LOG.warning("%s map doesn't have any score related. Can't "
            #                    "define top list related to this map", mapt)
            #     continue

            # TODO: only one function for output files
            # Write contact list in txt file
            refmap.report(cmpmap, scoremap=scoremap,
                          outdir=outdir, outprefix=prefix,
                          plotag=not self.settings.maplot.args.get(
                              "onlyreport"),
                          plotdir=self.settings.infra.get("graphics", outdir)
                          if not self.settings.maplot.args.get(
                              "onlyreport") else self.settings.outdir)
            # Contact map comparison plot
            # TODO: elementwise error with compare method
            # Write cmp stats
            if not self.settings.maplot.args.get("onlyreport", False):
                cmpmap.write_contacts(mapname,
                                      scoremap=scoremap,
                                      outdir=outdir)
                cmpmap.compare_contactmap(refmap, cmplist,
                                          prefix if prefix else "cmp",
                                          distmap=self.refmap["distmap"],
                                          human_idx=True,
                                          outdir=outdir)
                LOG.info(prefix)
                refmap.compareplot(cmpmap,
                                   outprefix=prefix if prefix else "ref",
                                   outdir=outdir,
                                   save_fig=self.settings.maplot.config.get(
                                       "save_fig"),
                                   alpha=self.settings.maplot.config.get(
                                       "alpha"),
                                   **plotparams)
                # Contingency table
                # print(cmpmap.to_series())
                # LOG.info(pd.crosstab(cmpmap.values, refmap.values,
                #                         rownames=[mapt], colnames=[self.reftype]))
