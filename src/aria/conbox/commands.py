# coding=utf-8
"""
                            Input/Output aria_ec
"""
from __future__ import absolute_import, division, print_function

import os
import logging
import argparse as argp

from . import __doc__ as conboxdoc
from abc import ABCMeta, abstractproperty
from .common import format_dict, CustomLogging
from .settings import AriaEcSettings
from .maplot import AriaEcContactMap
from .converter import AriaEcBbConverter, AriaEcXMLConverter, \
    AriaEcConfigConverter
from .reader import MapFile
##  TODO: Solve dependencie issue with mdanalysis and pbxplore
#from .pdbdist import PDBDist
from .pdbstat import PDBStat
from .setup import AriaEcSetup
from .pdbqual import AriaEcPdbqual
from .analysis import EnsembleAnalysis
from conkit.io import CONTACT_FILE_PARSERS


LOG = logging.getLogger(__name__)


def check_file(prospective_file):
    """
    

    Parameters
    ----------
    prospective_file :
        

    Returns
    -------

    
    """
    LOG.debug("Checking if %s is a readable file", prospective_file)
    if not os.path.exists(prospective_file):
        raise argp.ArgumentTypeError("readable_file:'{0}' is not a valid "
                                     "path".format(prospective_file))
    if not os.access(prospective_file, os.R_OK):
        raise argp.ArgumentTypeError("readable_file:'{0}' is not a readable "
                                     "file".format(prospective_file))


def seqrange_type(s):
    """
    Convert a string as a 2-tuple of int values

    Parameters
    ----------
    s : str
        String containing int values separated by dash character

    Returns
    -------
    tuple
        2-tuple of int values
    """
    try:
        x, y = map(int, s.split('-'))
        return x, y
    except:
        raise argp.ArgumentTypeError("Indexes must be start-end")


class ReadableFile(argp.Action):
    """Class used with argparse action to check if a file is readable"""

    def __init__(self, *args, **kwargs):
        super(ReadableFile, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if isinstance(values, list):
            for prospective_file in values:
                check_file(prospective_file)
            setattr(namespace, self.dest,
                    [os.path.abspath(os.path.expanduser(val)) for val in values])
        elif isinstance(values, str):
            check_file(values)
            setattr(namespace, self.dest, os.path.abspath(os.path.expanduser(values)))


class ReadableDir(argp.Action):
    """Class used with argparse action to check if a directory exists"""

    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir = values
        if not os.path.isdir(prospective_dir):
            raise argp.ArgumentTypeError(
                "readable_dir:{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argp.ArgumentTypeError(
                "readable_dir:{0} is not a readable dir".format(
                    prospective_dir))


class CLI(object):
    """
    Abstract class for command line interface
    """
    # TODO: maybe to complex. Actually support only cli with subcommands...
    __metaclass__ = ABCMeta

    def __init__(self, logger=None):
        self._parser = None
        self._args = None
        self._logger = logger if logger else CustomLogging()

    @property
    def parser(self):
        """command line parser"""
        if not self._parser:
            self._parser = self._create_argparser()
        return self._parser

    @property
    def args(self):
        """argparse arguments"""
        if not self._args:
            self._args = self.parser.parse_args()
            self._update_logger(self._logger)
        return self._args

    @abstractproperty
    def command_list(self):
        """
        List of commands available in the CLI
        """
        raise NotImplementedError

    @abstractproperty
    def desc_list(self):
        """
        List of command description in the CLI (should follow the same order as
        the command_list attribute)
        """
        raise NotImplementedError

    def _create_argparser(self):
        """
        Create main argument parser with basic options

        Returns
        -------
        argparse.ArgumentParser
        """
        parser = argp.ArgumentParser(
            formatter_class=argp.ArgumentDefaultsHelpFormatter)

        return parser

    # TODO: should be called in child classes .....
    def add_subparsers(self, parser):
        """
        Generate subcommands

        Parameters
        ----------
        parser : :py:class:`argparse.ArgumentParser`
            argument parser instance

        Returns
        -------
        None
        """
        # TODO: find a way to raise NotImplementedError for each non defined
        # command_argparser
        for index, command in enumerate(self.command_list):
            # Create subparser defined in command list
            # TODO: Is it the best way ? => More like AriaEcSettings ?
            subcommand = getattr(self, "_" + command + "_argparser")(
                desc=self.desc_list[index])
            # TODO: Not the right way ?
            parser.add_parser(command, parents=[subcommand])

    def _update_logger(self, log):
        """
        If output directory is provided, change location of log files

        Parameters
        ----------
        log : :py:class:`aria.conbox.Customlogging`
        """
        if log and hasattr(self.args, "output_directory"):
            if not self.args.nolog:
                # Don't generate log files
                # TODO: get handler list from json file or customlogging object
                LOG.removeHandler("info_file_handler")
                LOG.removeHandler("error_file_handler")
                LOG.removeHandler("debug_file_handler")
                log.set_outdir(self.args.output_directory)
            if hasattr(self.args, "command"):
                log.update_msg(self.args.command)

    def run(self):
        """Call method relative to args.command"""
        LOG.debug("Run %s command", self.args.command)
        # TODO: find a way to raise NotImplementedError for non defined command(s)
        getattr(self, self.args.command)()


class AriaEcCommands(CLI):
    """
    Command line interface for aria_ec

    Attributes
    ----------
    AriaEcCommands.command_list: list
        available command line tools
    AriaEcCommands.desc_list: list

    """
    command_list = ("setup", "bbconv", "maplot", "pdbqual", "analysis",
                    "tbl2xml", "pdbdist", "pdbstat", "iniconv")
    desc_list = (u"Setup ARIA infrastructure with contact maps translated "
                 u"into ARIA restraints",
                 u"Convert a contact map in bbcontact format",
                 u"Contactmap visualization tool",
                 u"Quality tool for pdb file(s)",
                 u"Extended ARIA ensemble analysis on a specific iteration ",
                 u"XML converter for tbl distance restraint",
                 u"Extract distance distribution from culled list of pdb files",
                 u"Analyze distance distribution with GMM",
                 u"Convert configuration files into a unique csv file")

    contact_types = set(MapFile.types).union(
        set(CONTACT_FILE_PARSERS)).union(set(MapFile.conkit_alias))
    default_confile = "conf/config.ini"

    def __init__(self):
        super(AriaEcCommands, self).__init__(logger=CustomLogging(desc=conboxdoc))

    def _create_argparser(self):
        """
        Update default CLI in order to add the confi
        """
        parser = super(AriaEcCommands, self)._create_argparser()
        # parser.add_argument("-c", "--conf", action=ReadableFile,
        #                     dest="conf_file",
        #                     default=None, help="configuration file")
        # parser.add_argument("-o", "--output", dest="output_directory", type=str,
        #                     help="Output directory", required=True,
        #                     action=ReadableDir)
        # parser.add_argument("--nolog", action="store_true",
        #                     default=False, help="Don't generate log files")
        # parser.add_argument("-d", "--debug", dest="verbose", default=False,
        #                     action='store_true',
        #                     help="Increase output verbosity")
        # TODO: not really practical, we HAVE to call line below since every
        # subcommand argparser have to be defined in the same scope
        self.add_subparsers(parser.add_subparsers(dest="command"))
        return parser

    def _setup_argparser(self, desc=None):
        """
        setup opt & args

        Parameters
        ----------
        desc :
            command descriptor (Default value = None)

        Returns
        -------


        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        # Options
        # Args
        group = parser.add_argument_group('required arguments')
        group.add_argument("seq", action=ReadableFile,
                           help="Sequence file [FASTA]")
        # group.add_argument("sspred", action=ReadableFile,
        #                    help="secondary structure prediction file")
        group.add_argument("infiles", nargs="*", metavar="infile",
                           action=ReadableFile,
                           help="Contact or PDB file(s) used to build ARIA "
                                "distance restraints")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="Configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        group.add_argument("-d", "--distfile", dest="distfile",
                           help="Pdb or distance matrix iif distance_type "
                                "set  to   distfile in conf  file,  "
                                "use distances in the  given file as "
                                "target  distance to build  distance "
                                "restraints")
        group.add_argument("-s", "--ssfile", dest="sspred", action=ReadableFile,
                           help="secondary structure prediction file")
        group.add_argument("-p", "--ariaproject", dest="ariaproject",
                           action=ReadableFile,
                           help="ARIA project file used to initialize a new "
                                "project with contact map data.")
        group.add_argument("-t", "--type",  required=True,
                           nargs="*", dest="contact_types",
                           choices=self.contact_types, help="Infile(s) contact "
                                                            "type(s)")
        group.add_argument("-r", "--range", dest="seqrange", default=(1, -1),
                           help="Index range if we don't want tu use the whole "
                                "sequence and map",
                           type=seqrange_type)
        group.add_argument("-n", "--native", dest="ref",
                           help="Native pdb. Allow TP/FP detection.")
        group.add_argument("--hb", dest="hb",
                           help="H-bonds contact file (eg: metapsicov.hb)")
        group.add_argument("--ssidx", dest="ssidx", action="store_true",
                           default=False, help="Use secondary structure index")
        group.add_argument("--no-filter", dest="no_filter", action="store_true",
                           default=False, help="Do not filter contact map.")
        group.add_argument("--extract-all", dest="extractall", action="store_true",
                           default=False, help="Extract data or all data and "
                                               "parameters if an ARIA project"
                                               "is defined with -p option")

        parser.add_argument("--ssxml", action="store_true", dest="ssxml",
                            default=False, help="Secondary structure restraints "
                                                 "as XML files")
        return parser

    def _bbconv_argparser(self, desc=None):
        """
        bbconv opt & args

        Parameters
        ----------
        desc :
            command descriptor (Default value = None)

        Returns
        -------

        
        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        # args
        parser.add_argument("contactfile", help="contacts file (pconsc, plm)",
                            action=ReadableFile)
        parser.add_argument("sspred", help="psipred file",
                            action=ReadableFile)
        parser.add_argument("seq", help="sequence file [FASTA]",
                            action=ReadableFile)
        parser.add_argument("msa", nargs='?',
                            help="MSA [FASTA] used to compute diversityvalue ")
        parser.add_argument("-t", "--type", required=True, dest="contact_type",
                            choices=self.contact_types, help="Infile contact "
                                                             "type")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _iniconv_argparser(self, desc=None):
        """
        iniconv opt & args

        Parameters
        ----------
        desc :
            command descriptor (Default value = None)

        Returns
        -------


        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        # args
        parser.add_argument("confiles", nargs='+',
                            type=str, help="config files")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _maplot_argparser(self, desc=None):
        """
        maplot opt & args

        Parameters
        ----------
        desc :
            command descriptor (Default value = None)

        Returns
        -------

        
        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        parser.add_argument("seq", action=ReadableFile,
                            help="sequence file [FASTA]")
        parser.add_argument("sspred", action=ReadableFile,
                            help="secondary structure prediction file")
        parser.add_argument("infiles", nargs="+", metavar="infile",
                            action=ReadableFile,
                            help="contact or pdb file(s) used to build aria "
                                 "distance restraints")
        parser.add_argument("-t", "--type", required=True,
                            nargs="+", dest="contact_types",
                            choices=self.contact_types, help="Infile(s) "
                                                             "contact "
                                                             "type(s)")
        parser.add_argument("--merge", nargs="+", dest="merge",
                            choices=self.contact_types,
                            help="Merge given contact types with other maps")
        parser.add_argument("--filter", dest="filter", action="store_true",
                            default=False, help="Use contact list filter "
                                                "and top n contacts selection")
        parser.add_argument("--onlyreport", dest="onlyreport",
                            action="store_true",
                            default=False, help="Generate only report file")
        parser.add_argument("--no-filter", dest="no_filter", action="store_true",
                            default=False, help="Do not filter contact map.")
        parser.add_argument("--ssidx", dest="ssidx", action="store_true",
                            default=False,
                            help="Use secondary structure index")
        parser.add_argument("--prefix", dest="prefix", action="store_true",
                            default="",
                            help="Generate prefix for file names")
        parser.add_argument("--prefixname", dest="prefixname",
                            default="",
                            help="Prefix name for file names")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _pdbqual_argparser(self, desc=None):
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        parser.add_argument("infiles", nargs="+", metavar="infile",
                            action=ReadableFile,
                            help="PDB file(s) used to run quality tools with "
                                 "aria API")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _analysis_argparser(self, desc=None):
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        parser.add_argument("project", action=ReadableFile,
                            help="ARIA project file [XML]")
        parser.add_argument("iteration", metavar="iteration_path",
                            action=ReadableFile,
                            help="Iteration path used to run violation analysis")
        parser.add_argument(
            "listname", metavar="list_name",
            help="Name of restraint list analyzed in the tbl file")
        parser.add_argument("-r", "--ref", dest="ref",
                            help="Native pdb. Allow TP/FP detection.")
        parser.add_argument("-d", dest="restraint", default=None,
                            help="Distance restraint file (XML) used for "
                                 "violation analysis. Otherwise, use restraints"
                                 " of the current iteration")
        parser.add_argument("-p", "--prefixname", dest="prefixname",
                            default=None,
                            help="Prefix name for file names")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _tbl2xml_argparser(self, desc=None):
        """
        

        Parameters
        ----------
        desc :
            (Default value = None)

        Returns
        -------

        
        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        parser.add_argument(
            "molecule", metavar="molecule.xml", action=ReadableFile,
            help="ARIA XML molecule file")
        parser.add_argument("listname", metavar="list_name",
                            help="Restraint list name in the tbl file")
        parser.add_argument("infiles", nargs="+", metavar="infile.tbl",
                            action=ReadableFile,
                            help="TBL distance restraint file(s)")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _pdbdist_argparser(self, desc=None):
        """
        

        Parameters
        ----------
        desc :
            (Default value = None)

        Returns
        -------

        
        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        # TODO: find a way to list all cullpdb file in package ressources
        # Maybe move this args to the config file

        # Culled pdb list are actually saved in data/cullpdb folder with yymmdd
        # pattern as naming convention
        parser.add_argument(
            "--cullist", dest="cullpdbs", default='',
            metavar="CULLED_PDB_LIST", action=ReadableFile,
            help="Culled list of pdb files from PISCES server ["
                 "G. Wang and R. L. Dunbrack, Jr. PISCES: a protein sequence "
                 "culling server. Bioinformatics, 19:1589-1591, 2003.]")
        parser.add_argument(
            "--pdbdir", dest="pdbdir", default='',
            metavar="PDB_FOLDER",
            help="Folder containing pdb file entries")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def _pdbstat_argparser(self, desc=None):
        """


        Parameters
        ----------
        desc :
            (Default value = None)

        Returns
        -------


        """
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        # TODO: find a way to list all cullpdb file in package ressources
        # Maybe move this args to the config file

        # Culled pdb list are actually saved in data/cullpdb folder with yymmdd
        # pattern as naming convention
        parser.add_argument(
            "pdbdists", action=ReadableFile,
            help="PDB distance file in csv format")
        parser.add_argument(
            "-j", dest="njobs", default=None, metavar="N_JOBS",
            help="Number of cpus used to run mixture in parallel. By default,"
                 " use max of available cpus."
        )
        parser.add_argument(
            "--min", dest="minflag", action="store_true", default=False,
            help="Compute stats only on a minimized list of atom (CA, CB and 1 "
                 "SC)")
        parser.add_argument("-c", "--conf", action=ReadableFile,
                            dest="conf_file",
                            default=None, help="configuration file")
        parser.add_argument("-o", "--output", dest="output_directory", type=str,
                            help="Output directory", required=True,
                            action=ReadableDir)
        parser.add_argument("--nolog", action="store_true",
                            default=False, help="Don't generate log files")
        parser.add_argument("--debug", dest="verbose", default=False,
                            action='store_true',
                            help="Increase output verbosity")
        return parser

    def create_settings(self):
        """Create settings relative to args.command"""

        if self.args.verbose:
            LOG.info("Toggle on debug mode")
            logging.getLogger().setLevel(logging.DEBUG)
        LOG.info("Initialize settings")
        settings = AriaEcSettings(self.args.command)
        LOG.debug("Loading default config file")
        if self.args.conf_file:
            LOG.info("Updating settings according to config file")
            settings.load_config(self.args.conf_file)
        # Update settings associated to command section
        LOG.debug("Updating %s args settings", self.args.command)
        LOG.debug(self.args.__dict__)
        getattr(settings, self.args.command).args.update(format_dict(self.args.__dict__))
        LOG.debug(getattr(settings, self.args.command).args)
        if self.args.output_directory:
            LOG.debug("Output directory: %s", self.args.output_directory)
            settings.infra = self.args.output_directory
        return settings

    def setup(self):
        """Setup call"""
        setup_inst = AriaEcSetup(self.create_settings())
        setup_inst.run()

    def bbconv(self):
        """bbcontacts converter call"""
        bbconverter = AriaEcBbConverter(self.create_settings())
        bbconverter.run()

    def maplot(self):
        """instantiate AriaEcContactmap with AriaSettings"""
        econtactmap = AriaEcContactMap(self.create_settings())
        econtactmap.run()

    def pdbqual(self):
        """Quality run subcommand"""
        qualprot = AriaEcPdbqual(self.create_settings())
        qualprot.run()

    def tbl2xml(self):
        """tbl2xml command"""
        tblconverter = AriaEcXMLConverter(self.create_settings())
        tblconverter.run_tbl2xml()

    def pdbdist(self):
        """Extract pdb distance distributions"""
        # TODO: See todo at the beginning of this file
        #inst = PDBDist(self.create_settings())
        #inst.run()

    def pdbstat(self):
        """Analyse pdb distance distribution"""
        inst = PDBStat(self.create_settings())
        inst.run()

    def analysis(self):
        """Extended ensemble analysis of distance restraints"""
        inst = EnsembleAnalysis(self.create_settings())
        inst.run()

    def iniconv(self):
        """Configuration file CSV converter command"""
        inst = AriaEcConfigConverter(self.create_settings())
        inst.run()


def main():
    """Launch ariaec command interface"""

    command = AriaEcCommands()

    command.run()


if __name__ == "__main__":
    # Test AriaEcCommand object
    logging.basicConfig(level=logging.DEBUG)
    LOG = logging.getLogger("IO")
    AriaEcCommands()
