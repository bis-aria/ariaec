# coding=utf-8
"""
                                ARIA CNS tool
"""
from __future__ import absolute_import, division, print_function


import re
import os
import time
import shutil
import logging
import subprocess
import tempfile as tmp
import argparse as argp
import pkg_resources as pkgr
from .conbox.common import CustomLogging
from .conbox.commands import CLI, ReadableDir


CNS_PATH = 'CNS_SOLVE'
LOG = logging.getLogger(__name__)


class CNSPatchCommand(CLI):
    """
    CLI for cns compilation with ARIA files
    """
    command_list = ('patch', )
    desc_list = (
        u"Compile a new CNS executable according with ARIA CNS patches", )

    def __init__(self):
        super(CNSPatchCommand, self).__init__(logger=CustomLogging(desc=__doc__))

    def _create_argparser(self):
        """Update default CLI"""
        parser = super(CNSPatchCommand, self)._create_argparser()
        self.add_subparsers(parser.add_subparsers(dest="command"))
        return parser

    @staticmethod
    def _patch_argparser(desc):
        """CNS patch arguments & options"""
        parser = argp.ArgumentParser(description=desc,
                                     add_help=False)
        parser.add_argument("cns_path", metavar="CNS_SOLVE",
                            action=ReadableDir,
                            help="CNS directory path. If CNS has been installed "
                                 "correctly, this will correspond to the "
                                 "CNS_SOLVE environment variable")
        return parser

    def patch(self):
        """
        CNS patch command which compile a new CNS executable
        """

        # Create temporary directory where CNS source will be copied
        tmp.tempdir = ''
        local_tempdir = tmp.mktemp()
        local_tempdir += str(int(time.time()))
        local_tempdir = os.path.join(self.args.output_directory, "tmp",
                                     local_tempdir)

        # Move CNS files in the temporary directory
        LOG.info("Moving CNS files in the temporary directory (%s)", local_tempdir)
        shutil.copytree(self.args.cns_path, local_tempdir)

        # Overwrite CNS files with ARIA cns patches
        LOG.info("Patching CNS files")
        for cnsrc in pkgr.resource_listdir(__name__, "cns/src"):
            if not pkgr.resource_isdir(__name__, "cns/src/" + cnsrc):
                shutil.copy(pkgr.resource_filename(__name__, "cns/src/" + cnsrc),
                            os.path.join(local_tempdir, "source"))

        # Edit cns_solve_env file
        cns_solve_env = os.path.join(local_tempdir, "cns_solve_env")
        if os.path.exists(cns_solve_env):
            with open(cns_solve_env, 'r+') as cnsolve:
                content = cnsolve.read()
                cnsolve.seek(0)
                cnsolve.write(
                    re.sub(r"(CNS_SOLVE\s+')(_CNSsolve_location_)(')",
                           r"\1%s\3" % local_tempdir, content, flags=re.M))

        # Call Make install command
        cmd = "cd {cns_solve}; make install".format(cns_solve=local_tempdir)

        # Get executable file using CNS_INST env var (can get it from cns_solve_env)

        # Move executable to the output directory

        # Delete tmp directory


def main():
    """Launch ariaec command interface"""

    command = CNSPatchCommand()

    command.run()
